#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Minimizes a protein and writes the results to stdout.

The protein should be prepared before minimizing it. Use the proteinprep.py script
to do this. For example:

  pyflare proteinprep.py protein.pdb | pyflare minimization.py

By default, the AMBER force field is used to perform the minimization. To change this, use the
--small-molecule-force-field option. Minimization for the AMBER and Open force fields are performed
on GPU by default. To run on CPU instead, --openmm-use-cpu can be specified.

If the AMBER or Open force fields are used for the small molecules then AMBER's FF14SB force field
will be used for the protein. If the XED force field is used for the small molecules then
the XED force field will also be used for the protein.

The purpose of this script is to minimize a protein or the active site of a protein.
If you wish to minimize a set of ligands within a fixed protein active site see the
minimizeligands.py script.

examples:
  pyflare minimization.py --openmm-use-cpu protein.pdb
    Uses the AMBER force field on the CPU to minimize the protein and write the results
    to stdout.

  pyflare minimization.py --small-molecule-force-field xed --ligand ligand.sdf --radius 6 protein.pdb
    Uses the XED force field to minimize the ligand within the protein and write the
    results to stdout. Only the ligand atoms and protein atoms within 6A of the
    ligand are allowed to move.

  pyflare minimization.py --residue LIG --radius 6 protein.pdb
    Uses the AMBER force field on the GPU to minimize the residue 'LIG' within the
    protein and write the results to stdout. Only the residue 'LIG' atoms and atoms
    within 6A of the residue are allowed to move.

  pyflare minimization.py --help
    Prints the help text including all available options and exits.
"""  # noqa: E501
import sys
import argparse
import traceback

from cresset import flare

from _internal import cmdutil


def main(argv):
    """Run the minimization process using the command line args."""
    try:
        minimization = flare.Minimization()

        # Configure the minimization process with the command line arguments
        args = _parse_args(minimization, argv)
        project = _configure_process(minimization, args)

        logging_context = cmdutil.LoggingContext(project.log, verbose=args.verbose)

        # Start the process
        cmdutil.start_process(minimization, args.broker, args.verbose)

        # Display the progress of the minimization process
        cmdutil.wait_for_finish(minimization, logging_context=logging_context)

        # Raises an exception if the process had any errors
        cmdutil.check_errors(minimization)

        # Writes the results based on the command line arguments
        if not minimization.is_cancelled():
            _write_results(minimization, project, args)

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(minimization, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )
    parser.add_argument(
        "protein",
        nargs="?",
        help="The protein to run minimization on. If not set the protein is read from stdin.",
    )

    group = parser.add_argument_group("input/output")
    group.add_argument(
        "-C",
        "--protein-chain-types",
        metavar="protein,water,other,ligand",
        default="protein,water,other,ligand",
        type=str.lower,
        help="Select the types of chains to include in the calculation as a comma separated list. 'p', 'w', 'o' and 'l' can be used as a shorthand for 'protein', water', 'other', 'ligand'. The default is to include all the chains.",  # noqa: E501
    )
    group.add_argument(
        "-m",
        "--ligand",
        help="The ligand molecule. If set then only this ligand atoms and atoms in residues near to the ligand are allowed to move. This is mutual exclusivity with --residue.",  # noqa: E501
    )
    group.add_argument(
        "-r",
        "--residue",
        help='The name of the ligand residue in the protein, for example "LIG" or "A LIG 15A". If set then only this ligand atoms and atoms in residues near to the ligand are allowed to move. This is mutual exclusivity with --ligand.',  # noqa: E501
    )
    group.add_argument(
        "-i",
        "--input",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for reading the protein. The default is to autodetect.",
    )
    group.add_argument(
        "-o",
        "--output",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for writing the protein. The default is 'pdb'.",
    )
    group.add_argument(
        "-u",
        "--radius",
        type=float,
        default=0.0,
        help="Atoms in residues that are within the radius or the ligand set by --ligand or --residue are allowed to move.",  # noqa: E501
    )
    group.add_argument(
        "-P",
        "--project",
        help="Write the Flare project file with the results to the given file path.",
    )

    # OpenMM
    group = parser.add_argument_group("force field")
    cmdutil.add_dynamics_force_field_arguments(group, allow_xed=True)

    group = parser.add_argument_group("solvent")
    group.add_argument(
        "-w",
        "--solvent-model",
        choices=[
            "implicit",
            "vacuum",
            "gbn",
            "gbn2",
            "hct",
            "obc1",
            "obc2",
        ],
        help="The solvent model to use. Implicit (obc2) solvent is used by default.",
    )

    group = parser.add_argument_group("GPU/CPU")
    cmdutil.add_gpu_arguments(group, minimization)
    cmdutil.add_cpu_arguments(group, minimization)

    # OpenMM & XED
    group = parser.add_argument_group("openmm and xed")
    group.add_argument(
        "-R",
        "--gradient-cutoff",
        type=float,
        # No default as the value is different for OpenMM and XED
        help=cmdutil.numpy_doc_to_help(flare.Minimization.gradient_cutoff.__doc__),
    )
    group.add_argument(
        "-I",
        "--max-iterations",
        type=int,
        # No default as the value is different for OpenMM and XED
        help=cmdutil.numpy_doc_to_help(flare.Minimization.max_iterations.__doc__),
    )
    cmdutil.add_constraint_arguments(group, minimization)

    group = parser.add_argument_group("general options")
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        default=minimization.local_engine_count,
        help="Set the number of local engines to start. Use 0 for no local processing: you must specify some remote engines with -g if you do so.",  # noqa: E501
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker in the format hostname:port. Field Engines which have been registered with the Broker will be used during the calculation. The CRESSET_BROKER environment variable can be used instead of this option.",  # noqa: E501
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )
    args = parser.parse_args(argv)

    return args


def _configure_process(minimization, args):
    """Configure `minimization` using the command line arguments."""
    if args.list_open_versions:
        cmdutil.print_open_force_field_versions_and_exit()

    project = flare.Project()

    if args.ligand and args.residue:
        raise ValueError("either --ligand or --residue must be set, not both")

    if args.protein:
        project.proteins.extend(flare.read_file(args.protein, args.input))
    else:
        # If the protein is not set read it from stdin
        file_format = args.input if args.input else "pdb"
        project.proteins.extend(flare.read_file(sys.stdin, file_format))

    if not project.proteins:
        raise ValueError("no protein molecules were read from the input")
    if len(project.proteins) > 1:
        raise ValueError("only one protein can be specified")

    minimization.protein = project.proteins[0]
    minimization.protein.is_prepared = True  # Suppress warnings
    minimization.sequences = cmdutil.chain_types_to_sequences(
        minimization.protein, args.protein_chain_types
    )

    if args.ligand:
        project.ligands.extend(flare.read_file(args.ligand, args.input))
        if not project.ligands:
            raise ValueError("no ligand molecules were read from the input")
        if len(project.ligands) > 1:
            raise ValueError("only one ligand can be specified")

        minimization.ligands = [project.ligands[0]]

    elif args.residue:
        residues = minimization.protein.residues.find(args.residue)
        if len(residues) == 0:
            raise ValueError("protein does not contain the given residue")
        if len(residues) > 1:
            raise ValueError(
                f"protein contains {len(residues)} residues which match the given residue"
            )

        minimization.ligands = [project.ligands.append(residues[0])]
        minimization.protein.residues.remove(residues[0])

    if minimization.ligands:
        moving_atoms = flare.atom_pick.grow(
            minimization.ligands, minimization.ligands + [minimization.protein], args.radius
        )
        moving_atoms = flare.atom_pick.expand_to_residues(moving_atoms)
        minimization.atoms = moving_atoms
    else:
        minimization.atoms = minimization.protein.atoms

    # Amber / OpenFF Settings
    cmdutil.configure_dynamics_force_field_process(minimization, args)

    # OpenMM Settings
    cmdutil.configure_gpu_process(minimization, args)
    if args.openmm_use_cpu:
        minimization.processor_type = flare.Minimization.ProcessorType.CPU
    else:
        minimization.processor_type = flare.Minimization.ProcessorType.GPU

    if args.solvent_model == "implicit":
        minimization.solvate.model = flare.SolventModel.ImplicitGBn2
    elif args.solvent_model == "vacuum":
        minimization.solvate.model = flare.SolventModel.Vacuum
    elif args.solvent_model == "gbn":
        minimization.solvate.model = flare.SolventModel.ImplicitGBn
    elif args.solvent_model == "gbn2":
        minimization.solvate.model = flare.SolventModel.ImplicitGBn2
    elif args.solvent_model == "hct":
        minimization.solvate.model = flare.SolventModel.ImplicitHCT
    elif args.solvent_model == "obc1":
        minimization.solvate.model = flare.SolventModel.ImplicitOBC1
    elif args.solvent_model == "obc2":
        minimization.solvate.model = flare.SolventModel.ImplicitOBC2

    # OpenMM & XED Settings
    if args.gradient_cutoff is not None:
        with cmdutil.CheckValue("gradient-cutoff"):
            minimization.gradient_cutoff = args.gradient_cutoff

    if args.max_iterations is not None:
        with cmdutil.CheckValue("max-iterations"):
            minimization.max_iterations = args.max_iterations

    cmdutil.configure_constraint_process(minimization, args)

    # Other Settings
    with cmdutil.CheckValue("localengines"):
        minimization.local_engine_count = args.localengines

    return project


def _write_results(minimization, project, args):
    """Write the results."""
    # If requested write the project file
    if args.project:
        try:
            project.save(args.project)
        except Exception as err:
            print(err, file=sys.stderr)
            # print the error and continue

    # Merge the ligand back into the protein
    if minimization.ligands:
        minimization.protein.merge(minimization.ligands[0])

    # Print the result protein to stdout
    file_format = args.output if args.output else "pdb"
    minimization.protein.write_file(sys.stdout, file_format)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
