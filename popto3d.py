#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Pops molecules to 3D, add fields and performs a quick minimization.

examples:
  pyflare popto3d.py molecule.sdf > results.sdf
    Pops the molecule in `molecule.sdf` to 3D and writes the results to
    `results.sdf`.

  pyflare popto3d.py --help
    Prints the help text including all available options and exits.
"""
import sys
import argparse
import traceback

from cresset import flare

from _internal import cmdutil

DEFAULT_BATCH_SIZE = 10000


def main(argv):
    """Run the AddFields process using the command line args.

    The AddFields process will pop molecules to 3D,
    add fields and perform a quick minimization.
    """
    try:
        add_fields = flare.AddFields()

        args = _parse_args(add_fields, argv)
        project = _configure_process(add_fields, args)

        imported_tags = set()

        logging_context = cmdutil.LoggingContext(project.log, verbose=args.verbose)

        # Read each batch in turn and process them
        any_ligands_read = False
        for i, batch in enumerate(_read_batch(project, args), 1):
            any_ligands_read = True

            imported_tags = imported_tags.union(set(project.ligands.columns.user_keys()))

            add_fields.ligands = project.ligands

            # Run the calculation
            cmdutil.start_process(add_fields, args.broker, args.verbose)
            cmdutil.wait_for_finish(
                add_fields, prefix=f"Batch {i}, ", logging_context=logging_context
            )

            # If there are errors don't stop, write the results and continue on to the next batch.
            # As if a few pop to 3D failed we still want to print the results for all other
            # successful pops to 3D.
            if add_fields.errors():
                print("\n".join(add_fields.errors()), file=sys.stderr)

            if not add_fields.is_cancelled():
                _write_results(add_fields, project, imported_tags, args)

        if not any_ligands_read:
            raise ValueError("no ligands were read from the input")

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(add_fields, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )
    # Input settings
    parser.add_argument(
        "ligands",
        nargs="*",
        help="The ligand files to pop to 3D. If not given the ligands are read from stdin.",
    )

    group = parser.add_argument_group("input/output")
    group.add_argument(
        "-i",
        "--input",
        choices=["auto", "pdb", "sdf", "mol2", "smi", "xed"],
        default="auto",
        type=str.lower,
        help="Set the file format for reading the ligands. The default is to autodetect when reading from files, or to assume SDF files when reading from standard input.",  # noqa: E501
    )
    group.add_argument(
        "-Z",
        "--input-ignore-errors",
        action="store_true",
        help="By default, the calculation stops when errors are encountered in the input molecules. However, by setting this option the calculation will continue processing the remaining molecules and log the errors to standard error (stderr).",  # noqa: E501
    )
    # Output settings
    group.add_argument(
        "-o",
        "--output",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for writing the molecules. The default is 'sdf'.",
    )

    group = parser.add_argument_group("general options")
    group.add_argument(
        "--batch-size",
        type=int,
        default=DEFAULT_BATCH_SIZE,
        help="Limit the number of molecules which are read into memory. Smaller values will reduce memory usage but will reduce performance by a small amount. If --project/-P is set then this option is ignored.",  # noqa: E501
    )
    group.add_argument(
        "-L",
        "--log",
        action="store_true",
        help="Request that the calculation log be written as one of the SDF tags for each alignment. Only has an effect if '-o sdf' (the default) is active. The calculation log is written as tag 'FlareAlign_Log'.",  # noqa: E501
    )
    group.add_argument(
        "-P",
        "--project",
        help="Write the Flare project file with the results to the given file path.",
    )
    # Other settings
    group.add_argument(
        "--config",
        help=cmdutil.CONFIG_ARG_HELP_TEXT,
    )
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        default=add_fields.local_engine_count,
        help="Set the number of local engines to start. Use 0 for no local processing: you must specify some remote engines with -g if you do so.",  # noqa: E501
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker in the format hostname:port. Field Engines which have been registered with the Broker will be used during the calculation. The CRESSET_BROKER environment variable can be used instead of this option.",  # noqa: E501
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )
    args = parser.parse_args(argv)

    return args


def _configure_process(add_fields, args):
    """Configure `add_fields` using the command line arguments."""
    project = flare.Project()

    # Load the config file first. Any other settings given will override the config file.
    if args.config:
        add_fields.read_config(args.config)

    with cmdutil.CheckValue("localengines"):
        add_fields.local_engine_count = args.localengines

    return project


def _read_batch(project, args):
    """Yield batches of molecules read from the command line options."""
    # Read from stdin if no ligand files are given
    mol_files = args.ligands if args.ligands else ["-"]
    # If a project is set then all ligands need to be kept in memory so the project can be
    # saved
    if args.project and args.batch_size != DEFAULT_BATCH_SIZE:
        print("--batch-size will be ignored as -P/--project was given.\n", file=sys.stderr)
    batch_size = args.batch_size if not args.project else sys.maxsize

    raise_exception_on_error = not args.input_ignore_errors

    project.ligands.clear()
    for mol_file in mol_files:
        # If reading from stdin and the file format is not given assume "sdf" format
        input = args.input if args.input != "auto" else None
        if mol_file == "-" and input is None:
            input = "sdf"

        for batch, errors in cmdutil.batch_read_files(
            [mol_file], input, batch_size, raise_exception_on_error
        ):
            project.ligands.extend(batch)

            for error in errors:
                print(error, file=sys.stderr)

            if len(project.ligands) >= batch_size:
                yield
                project.ligands.clear()

    if len(project.ligands) > 0:
        yield


def _write_results(add_fields, project, imported_tags, args):
    """Write the results."""
    if args.project:
        try:
            project.save(args.project)
        except Exception as err:
            print(err, file=sys.stderr)
            # print the error and continue

    for ligand in add_fields.ligands:
        # If the ligand was read from a smi file it may not have a title.
        # Replace the default generated title with the ligand smiles
        if ligand.title.startswith("pyflare_stream"):
            ligand.title = ligand.smiles()

        log_text = cmdutil.molecule_log_as_sdf_string(ligand) if args.log else None
        ligand.properties["FlarePopTo3D_Log"].value = log_text

    file_format = args.output if args.output else "sdf"

    allowed_cresset_tags = [
        "_cresset_fieldpoints",
        "Filename",
        "FlarePopTo3D_Log",
    ]
    list_tags_to_keep = list(imported_tags)
    list_tags_to_keep.extend(allowed_cresset_tags)

    def modify_tags(ligand, tags):
        """Add/Removes SDF tags for each ligand."""
        tags["Filename"] = tags["Filename_cresset"]
        tags_to_delete = [tag for tag in tags if tag not in list_tags_to_keep]
        for tag in tags_to_delete:
            del tags[tag]

        return tags

    flare.write_file(sys.stdout, add_fields.ligands, file_format, modify_tags)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
