#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Ligand preparation: adds hydrogens, recharge to pH 7, pop to 3D, strip salts, file format
conversion

examples:
  pyflare ligprep.py -3 -c molecule.sdf > results.sdf
    Recharge the molecule in `molecule.sdf`, pop to 3D and write the result to
    `results.sdf`.

  pyflare ligprep.py --help
    Prints the help text including all available options and exits.
"""
import sys
import argparse
import traceback

from cresset import flare

from _internal import cmdutil

DEFAULT_BATCH_SIZE = 10000


def main(argv):
    """Run the LigandPrep process using the command line arguments."""
    try:
        prep = flare.LigandPrep()

        args = _parse_args(prep, argv)
        project = _configure_process(prep, args)

        imported_tags = set()

        logging_context = cmdutil.LoggingContext(project.log, verbose=args.verbose)

        # Read each batch in turn and process them
        any_ligands_read = False
        for i, batch in enumerate(_read_batch(project, args), 1):
            any_ligands_read = True

            imported_tags = imported_tags.union(set(project.ligands.columns.user_keys()))

            prep.ligands = project.ligands

            cmdutil.start_process(prep, args.broker, args.verbose)
            cmdutil.wait_for_finish(prep, prefix=f"Batch {i}, ", logging_context=logging_context)

            if args.verbose:
                for ligand in project.ligands:
                    print("Processed molecule {}".format(ligand.title), file=sys.stderr)

            if prep.errors():
                print("\n".join(prep.errors()), file=sys.stderr)

            if not prep.is_cancelled():
                _write_results(project, imported_tags, args)

        if not any_ligands_read:
            raise ValueError("no ligands were read from the input")

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(prep, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )
    # Input settings
    parser.add_argument(
        "ligands",
        nargs="*",
        help="The ligand files to prepare. If not given the ligands are read from stdin.",
    )

    group = parser.add_argument_group("input/output")
    group.add_argument(
        "-i",
        "--input",
        choices=["auto", "pdb", "sdf", "mol2", "smi", "xed"],
        default="auto",
        type=str.lower,
        help="Set the file format for reading the ligands. The default is to autodetect when reading from files, or to assume SDF files when reading from standard input.",  # noqa: E501
    )
    group.add_argument(
        "-Z",
        "--input-ignore-errors",
        action="store_true",
        help="By default, the calculation stops when errors are encountered in the input molecules. However, by setting this option the calculation will continue processing the remaining molecules and log the errors to standard error (stderr).",  # noqa: E501
    )
    # Output settings
    group.add_argument(
        "-o",
        "--output",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for writing the molecules. The default is 'sdf'.",
    )
    group.add_argument(
        "--batch-size",
        type=int,
        default=DEFAULT_BATCH_SIZE,
        help="Limit the number of molecules which are read into memory. Smaller values will reduce memory usage but will reduce performance by a small amount.",  # noqa: E501
    )

    group = parser.add_argument_group("general options")
    group.add_argument(
        "-c",
        "--charge",
        action="store_true",
        help=cmdutil.numpy_doc_to_help(prep.__class__.recharge.__doc__, True),
    )
    group.add_argument(
        "-3",
        "--pop-to-3d",
        action="store_true",
        help=cmdutil.numpy_doc_to_help(prep.__class__.pop_to_3d.__doc__, True),
    )
    group.add_argument(
        "-m",
        "--minimize",
        action="store_true",
        help=cmdutil.numpy_doc_to_help(prep.__class__.minimize.__doc__, True),
    )
    group.add_argument(
        "-s",
        "--strip-salts",
        action="store_true",
        help=cmdutil.numpy_doc_to_help(prep.__class__.strip_salts.__doc__, True),
    )
    group.add_argument(
        "-e",
        "--enumerate-chiral-centers",
        choices=["all", "undefined", "off"],
        default="off",
        help="",
    )
    group.add_argument(
        "-t",
        "--enumerate-tautomers",
        choices=["all", "canonical", "highlylikely", "likely", "unlikely", "off"],
        default="off",
        help="",
    )
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        default=prep.local_engine_count,
        help="Set the number of local engines to start. Use 0 for no local processing: you must specify some remote engines with -g if you do so.",  # noqa: E501
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker in the format hostname:port. Field Engines which have been registered with the Broker will be used during the calculation. The CRESSET_BROKER environment variable can be used instead of this option.",  # noqa: E501
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print progress information to stderr.",
    )
    args = parser.parse_args(argv)

    return args


def _read_batch(project, args):
    """Yield batches of molecules read from the command line options."""
    # Read from stdin if no ligand files are given
    mol_files = args.ligands if args.ligands else ["-"]
    # If a project is set then all ligands need to be kept in memory so the project can be
    # saved

    raise_exception_on_error = not args.input_ignore_errors

    project.ligands.clear()
    for mol_file in mol_files:
        # If reading from stdin and the file format is not given assume "sdf" format
        input = args.input if args.input != "auto" else None
        if mol_file == "-" and input is None:
            input = "sdf"

        for batch, errors in cmdutil.batch_read_files(
            [mol_file], input, args.batch_size, raise_exception_on_error
        ):
            project.ligands.extend(batch)

            for error in errors:
                print(error, file=sys.stderr)

            if len(project.ligands) >= args.batch_size:
                yield
                project.ligands.clear()

    if len(project.ligands) > 0:
        yield


def _configure_process(prep, args):
    """Configure `LigandPrep` using the command line arguments."""
    project = flare.Project()

    if args.strip_salts is not None:
        with cmdutil.CheckValue("strip-salts"):
            prep.strip_salts = args.strip_salts

    if args.charge is not None:
        with cmdutil.CheckValue("charge"):
            prep.recharge = args.charge

    if args.pop_to_3d is not None:
        with cmdutil.CheckValue("pop_to_3d"):
            prep.pop_to_3d = args.pop_to_3d

    if args.minimize is not None:
        with cmdutil.CheckValue("minimize"):
            prep.minimize = args.minimize

    if args.enumerate_chiral_centers is not None:
        if args.enumerate_chiral_centers == "all":
            prep.enumerate_chiral_centers = flare.LigandPrep.EnumerateChiralCenters.All
        elif args.enumerate_chiral_centers == "undefined":
            prep.enumerate_chiral_centers = flare.LigandPrep.EnumerateChiralCenters.Undefined
        elif args.enumerate_chiral_centers == "off":
            prep.enumerate_chiral_centers = flare.LigandPrep.EnumerateChiralCenters.Off

    if args.enumerate_tautomers is not None:
        if args.enumerate_tautomers == "all":
            prep.enumerate_tautomers = flare.LigandPrep.EnumerateTautomers.All
        elif args.enumerate_tautomers == "canonical":
            prep.enumerate_tautomers = flare.LigandPrep.EnumerateTautomers.Canonical
        elif args.enumerate_tautomers == "highlylikely":
            prep.enumerate_tautomers = flare.LigandPrep.EnumerateTautomers.HighlyLikely
        elif args.enumerate_tautomers == "likely":
            prep.enumerate_tautomers = flare.LigandPrep.EnumerateTautomers.Likely
        elif args.enumerate_tautomers == "unlikely":
            prep.enumerate_tautomers = flare.LigandPrep.EnumerateTautomers.Unlikely
        elif args.enumerate_tautomers == "off":
            prep.enumerate_tautomers = flare.LigandPrep.EnumerateTautomers.Off

    with cmdutil.CheckValue("localengines"):
        prep.local_engine_count = args.localengines

    return project


def _write_results(project, imported_tags, args):
    """Write the results."""
    for ligand in project.ligands:
        # If the ligand was read from a smi file it may not have a title.
        # Replace the default generated title with the ligand smiles
        if ligand.title.startswith("pyflare_stream"):
            ligand.title = ligand.smiles()

    file_format = args.output if args.output else "sdf"

    allowed_cresset_tags = [
        "_cresset_fieldpoints",
        "Filename",
    ]
    list_tags_to_keep = list(imported_tags)
    list_tags_to_keep.extend(allowed_cresset_tags)

    def modify_tags(ligand, tags):
        """Add/Removes SDF tags for each ligand."""
        tags["Filename"] = tags["Filename_cresset"]
        tags_to_delete = [tag for tag in tags if tag not in list_tags_to_keep]
        for tag in tags_to_delete:
            del tags[tag]

        return tags

    flare.write_file(sys.stdout, project.ligands, file_format, modify_tags)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
