#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Prints the results of a Relative or Absolute FEP calculation.

The results of the FEP calculation will be printed to standard output. The default is to print the
results for each node in a tab-separated table including the predicted delta g.
The data for the links can be viewed by using the --links, --cycles or --overlap options. The log
can be printed via the --log option.

If the results have only been partially completed, then the results so far will be printed. To see
if the calculation is complete use the --status option.

examples:
  pyflare fepview.py --project project.flr
    Prints the results of a Relative or Absolute FEP calculation.

  pyflare fepview.py --project project.flr --name fep --links
    Prints the results of the links in a Relative or Absolute FEP project called "fep".

  pyflare fepview.py --help
    Prints the help text including all available options and exits.
"""
import sys
import argparse
import traceback

from cresset import flare


def main(argv):
    """Print the results of a calculation to standard output."""
    try:
        args = _parse_args(argv)
        _print_results(args)

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )

    group = parser.add_argument_group("input/output")
    group.add_argument(
        "-P",
        "--project",
        required=True,
        help="The Flare project containing the FEP project to run the calculation on.",
    )
    group.add_argument(
        "-n",
        "--name",
        help="The name of the FEP project to run the calculation on. If not specified and the Flare project contains one FEP project then that project is used.",  # noqa: E501
    )
    group.add_argument("-L", "--log", action="store_true", help="Prints the calculation log.")

    group = parser.add_argument_group("print options")
    group.add_argument(
        "-o",
        "--overlap",
        action="store_true",
        help="Prints the bound and free overlap matrix for each link in a tab-separated table.",
    )
    group.add_argument(
        "-l",
        "--links",
        action="store_true",
        help="Prints the details of each link in a tab-separated table.",
    )
    group.add_argument(
        "-c",
        "--cycles",
        action="store_true",
        help="Prints the cycles and cycle errors in the graph.",
    )
    group.add_argument(
        "-s",
        "--status",
        action="store_true",
        help='Prints either "Completed" if no more calculation is needed. "Error" if part of the calculation has failed, run with --log to show the error. "Partially Complete" if some of the calculation has been run but the final results have not been calculated yet. "Not Started" if the calculation has not been started.',  # noqa: E501
    )

    group = parser.add_argument_group("general options")
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )

    args = parser.parse_args(argv)

    return args


def _print_results(args):
    """Configure `fep` calculation using the command line arguments."""
    project = flare.Project.load(args.project)

    fep_project = None
    if args.name:
        fep_projects = [proj for proj in project.fep_projects if proj.name == args.name]
        if not fep_projects:
            raise ValueError(f"no fep projects found called {args.name}")
        elif len(fep_projects) == 1:
            fep_project = fep_projects[0]
        else:
            raise ValueError("multiple fep projects with the same name detected")
    else:
        if not project.fep_projects:
            raise ValueError("no fep projects found")
        elif len(project.fep_projects) == 1:
            fep_project = project.fep_projects[0]
        else:
            names = (
                '"' + '", "'.join([fep_project.name for fep_project in project.fep_projects]) + '"'
            )
            raise ValueError(
                "multiple fep projects detected, "
                + "specify a fep project name with --name. "
                + f"The names of the fep projects are {names}."
            )

    if (int(args.log) + int(args.overlap) + int(args.links) + int(args.status)) > 1:
        raise ValueError(
            "the options --log, --links, --overlap and --status are mutually exclusive"
        )

    if args.log:
        _print_log(fep_project)
    elif args.overlap:
        _print_overlap(fep_project)
    elif args.links:
        _print_links(fep_project)
    elif args.cycles:
        _print_cycles(fep_project)
    elif args.status:
        _print_status(fep_project)
    else:
        _print_nodes(fep_project)


def _print_log(fep_project):
    for log in fep_project.log:
        print(log.header)
        print("~" * len(log.header))
        if log.summary:
            print(log.summary)
        print(log.comment)
        print("\n")


def _lambda_type_to_string(lambda_type):
    if lambda_type == flare.FepLambdaType.Discharge:
        return "Discharge"
    elif lambda_type == flare.FepLambdaType.Vanish:
        return "Vanish"
    elif lambda_type == flare.FepLambdaType.Restrain:
        return "Restrain"
    return ""  # Including flare.FepLambdaType.Morph


def _structure_type_to_string(structure_type):
    if structure_type == flare.FepStructureType.Free:
        return "Free"
    elif structure_type == flare.FepStructureType.Bound:
        return "Bound"
    return ""


def _link_title(link, lambda_type, structure_type):
    title = link.from_node().title
    if not link.to_node().is_absolute_dummy:
        title += "~"
        title += link.to_node().title

    lambda_str = _lambda_type_to_string(lambda_type)
    if lambda_str:
        title += " "
        title += lambda_str

    structure_str = _structure_type_to_string(structure_type)
    if structure_str:
        title += " "
        title += structure_str
    return title


def _print_overlap(fep_project):
    lambda_types = [
        flare.FepLambdaType.Morph,
        flare.FepLambdaType.Discharge,
        flare.FepLambdaType.Vanish,
        flare.FepLambdaType.Restrain,
    ]

    for link_1, link_2 in fep_project.links:
        for link in (link_1, link_2):
            if link:
                for lambda_type in lambda_types:
                    if link.bound_overlap_matrix(lambda_type):
                        print(_link_title(link, lambda_type, flare.FepStructureType.Bound))
                        _print_overlap_matrix(link.bound_overlap_matrix(lambda_type))
                        print()
                    if link.free_overlap_matrix(lambda_type):
                        print(_link_title(link, lambda_type, flare.FepStructureType.Free))
                        _print_overlap_matrix(link.free_overlap_matrix(lambda_type))
                        print()


def _print_overlap_matrix(matrix):
    for row in matrix:
        line = "\t".join([f"{cell:.4f}" for cell in row])
        print(line)


def _contains_relative(fep_project):
    for node in fep_project.nodes:
        if node.is_relative_enabled:
            return True
    return False


def _contains_absolute(fep_project):
    for node in fep_project.nodes:
        if node.is_absolute_enabled:
            return True
    return False


def _print_links(fep_project):
    contains_relative = _contains_relative(fep_project)

    title_1 = []
    title_2 = []
    title_1.append("")
    title_2.append("Link")
    title_1.append("Delta Delta G")
    title_2.append("Left To Right")
    if contains_relative:
        title_1.append("Delta Delta G")
        title_2.append("Right To Left")
        title_1.append("")
        title_2.append("Diff")
        title_1.append("")
        title_2.append("Score")
        title_1.append("Include")
        title_2.append("Heuristics")
        title_1.append("Include")
        title_2.append("User")
        title_1.append("")
        title_2.append("Atom Mapping")

    table = [title_1, title_2]
    for link_1, link_2 in fep_project.links:
        # link_2 is None for absolute links
        row = []
        # Link
        row.append(_link_title(link_1, None, None))
        # Delta Delta G
        if link_1.delta_delta_g() is not None:
            row.append(f"{link_1.delta_delta_g():.2f}")
        else:
            row.append("n/a")

        if contains_relative:
            if link_2 is not None and link_2.delta_delta_g() is not None:
                row.append(f"{link_2.delta_delta_g():.2f}")
            else:
                row.append("n/a")
            # Diff
            if (
                link_2 is not None
                and link_1.delta_delta_g() is not None
                and link_2.delta_delta_g() is not None
            ):
                diff = abs(link_1.delta_delta_g() + link_2.delta_delta_g())
                row.append(f"{diff:.2f}")
            else:
                row.append("n/a")
            # Score
            if link_1.score() is not None:
                row.append(f"{link_1.score():.3f}")
            else:
                row.append("n/a")
            # Heuristics
            row.append("Yes" if link_1.heuristics_include() else "No")
            # User
            if link_1.include == flare.FepLink.Include.Exclude:
                row.append("Exclude")
            elif link_1.include == flare.FepLink.Include.Include:
                row.append("Include")
            else:
                row.append("Heuristics")
            # Atom Mapping
            map = " ".join([f"{k+1}:{v+1}" for k, v in link_1.final_atom_mapping().items()])
            row.append(map)

        table.append(row)

    _print_table(table)


def _print_cycles(fep_project):
    cycle_rows = []
    for cycle in fep_project.cycles():
        error = 0.0
        molecules = []
        cycle_calculated = True
        for link in cycle:
            if link.delta_delta_g() is not None:
                error += link.delta_delta_g()
            else:
                cycle_calculated = False
            molecules.append(link.from_node().title)
        molecules.append(molecules[0])

        if cycle_calculated:
            cycle_rows.append([len(cycle), abs(error), molecules])

    cycle_rows.sort(key=lambda c: c[1])

    title = ["Size", "Error", "Molecules"]

    table = [title]

    for row in cycle_rows:
        error_text = f"{row[1]:.1f}" if row[1] is not None else "n/a"
        table.append([f"{row[0]}", error_text, "~".join(row[2])])

    _print_table(table)


def _print_status(fep_project):
    failed = False
    started = False
    finished = True

    for node in fep_project.nodes:
        failed = failed or node.failed()
        started = started or node.is_calculation_started()
        finished = finished and node.is_calculation_complete()

    for node in fep_project.nodes:
        # Note a node may be in both a relative and absolute calculation
        if node.is_relative_enabled:
            relative_delta_g, error = node.relative_delta_g()
            finished = finished and relative_delta_g is not None and error is not None

        if node.is_absolute_enabled:
            # Activity error may be None in absolute FEP
            predicted_delta_g, _ = node.predicted_delta_g(flare.FepCalculationType.Absolute)
            finished = finished and predicted_delta_g is not None

    for link_1, link_2 in fep_project.links:
        for link in (link_1, link_2):
            if link is not None:
                failed = failed or link.failed()
                started = started or link.is_calculation_started()
                finished = finished and link.is_calculation_complete()

    if failed:
        print("Errors")
    elif finished:
        print("Completed")
    elif not started:
        print("Not Started")
    else:
        print("Partially Complete")


def _print_nodes(fep_project):
    contains_relative = _contains_relative(fep_project)
    contains_absolute = _contains_absolute(fep_project)

    title = []
    title.append("Molecule")
    title.append("Experimental Activity")
    if contains_relative:
        title.append("Relative Activity")
        title.append("Relative Activity Error")
        title.append("Predicted Activity")
        title.append("Predicted Activity Error")

    if contains_absolute:
        title.append("Absolute Predicted Activity")
        title.append("Absolute Predicted Activity Error")

    table = [title]
    for node in fep_project.nodes:
        if node.is_absolute_dummy:
            # Skip the absolute dummy node as it has no data.
            continue

        row = []
        # Molecule
        row.append(f"{node.title}")
        # Experimental Activity
        if node.experimental_delta_g is not None:
            row.append(f"{node.experimental_delta_g:.2f}")
        else:
            row.append("n/a")

        if contains_relative:
            # Relative Activity
            relative_delta_g, relative_dg_error = node.relative_delta_g()
            if relative_delta_g is not None:
                row.append(f"{relative_delta_g:.2f}")
            else:
                row.append("n/a")
            # Relative Activity Error
            if relative_dg_error is not None:
                row.append(f"{relative_dg_error:.2f}")
            else:
                row.append("n/a")

            # Predicted Activity
            predicted_delta_g, predicted_dg_error = node.predicted_delta_g(
                flare.FepCalculationType.Relative
            )
            if predicted_delta_g is not None:
                row.append(f"{predicted_delta_g:.2f}")
            else:
                row.append("n/a")
            # Predicted Activity Error
            if predicted_dg_error is not None:
                row.append(f"{predicted_dg_error:.2f}")
            else:
                row.append("n/a")

        if contains_absolute:
            # Absolute Predicted Activity
            predicted_delta_g, predicted_dg_error = node.predicted_delta_g(
                flare.FepCalculationType.Absolute
            )
            if predicted_delta_g is not None:
                row.append(f"{predicted_delta_g:.2f}")
            else:
                row.append("n/a")
            # Predicted Activity Error
            if predicted_dg_error is not None:
                row.append(f"{predicted_dg_error:.2f}")
            else:
                row.append("n/a")

        table.append(row)

    _print_table(table)


def _print_table(table):
    column_count = max([len(row) for row in table])
    column_sizes = [0] * column_count
    for row in table:
        for i in range(len(row)):
            if i != len(row) - 1:
                column_sizes[i] = max(column_sizes[i], len(row[i]))

    for row in table:
        line = []
        for i, cell in enumerate(row):
            line.append(cell.ljust(column_sizes[i]))
        print("\t".join(line))


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
