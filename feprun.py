#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Runs a Relative FEP calculation.

The FEP calculation will be run on the FEP project within the Flare project specified by --project.
The FEP project can be created by the fepcreate.py script or in Flare. The Flare project file will
be periodically updated as the calculation runs.

The FEP calculation requires GPU resources to run. It is recommended to use a Cresset Engine Broker
by setting the --broker option. However, on Linux local GPU resources may be used.
To use multiple GPU devices, the --cuda-device-index or --opencl-device-index options should be
specified multiple times, e.g. specifying '--cuda-device-index 0 --cuda-device-index 1'
will allow the calculation to use both GPUs 0 and 1.

The calculation can be interrupted at any time by pressing Ctrl+C. The current progress will be
saved in the specified Flare project file. To resume the calculation, run the script with the
--continue option. If you want to restart the calculation from the beginning and discard any
previous results, use the --delete option.

Once the FEP calculation is complete the results can be viewed by the fepview.py script.

examples:
  pyflare feprun.py --project project.flr --broker example:9000
    Runs the FEP calculation on the FEP project within project.flr.

  pyflare feprun.py --help
    Prints the help text including all available options and exits.
"""
import sys
import argparse
import traceback
import os

from cresset import flare

from _internal import cmdutil
from _internal import feputil


def main(argv):
    """Run the FEP calculation on a project file."""
    try:
        fep = flare.Fep()

        args = _parse_args(fep, argv)
        project, fep_project = _configure_process(fep, args)
        cmdutil.start_process(fep, args.broker, args.verbose)
        feputil.wait_for_finish(fep, project, fep_project, args)
        cmdutil.check_errors(fep)
        _write_results(project, fep_project, args)

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(fep, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )

    group = parser.add_argument_group("input")
    feputil.add_input_arguments(group, fep)

    group = parser.add_argument_group("force field")
    cmdutil.add_dynamics_force_field_arguments(group)

    group = parser.add_argument_group("FEP options")
    feputil.add_fep_arguments(group, fep)
    group.add_argument(
        "-e",
        "--simulation-length-delta-q",
        type=feputil.float_or_two_floats,
        help="The simulation length in nanoseconds for links where there is a charge perturbation."
        + " Enter two comma-separated values for free and bound states respectively, or a single "
        + "value to use in both states.",
    )
    group.add_argument(
        "-q",
        "--quick-mode",
        action="store_true",
        help="Enable quick mode, which performs link calculations in only one direction.",
    )

    group = parser.add_argument_group("GPU")
    cmdutil.add_gpu_arguments(group, fep)

    group = parser.add_argument_group("openmm")
    cmdutil.add_openmm_common_arguments(group, fep)
    cmdutil.add_openmm_time_step_arguments(group, fep)
    cmdutil.add_openmm_fep_ws_arguments(group, fep)
    cmdutil.add_openmm_advanced_arguments(group, fep, process_is_fep=True)

    group = parser.add_argument_group("gcncmc arguments")
    cmdutil.add_gcncmc_arguments(group, fep)

    group = parser.add_argument_group("general options")
    feputil.add_general_arguments(group, fep)

    args = parser.parse_args(argv)

    return args


def _configure_process(fep, args):
    """Configure `fep` calculation using the command line arguments."""

    project = feputil.configure_process(fep, args)

    if args.simulation_length_delta_q:
        if isinstance(args.simulation_length_delta_q, float):
            fep.simulation_length_delta_q_free = float(args.simulation_length_delta_q)
            fep.simulation_length_delta_q_bound = float(args.simulation_length_delta_q)
        else:
            fep.simulation_length_delta_q_free = args.simulation_length_delta_q[0]
            fep.simulation_length_delta_q_bound = args.simulation_length_delta_q[1]

    if args.quick_mode:
        fep.quick_mode = True

    cmdutil.configure_gpu_process(fep, args)

    return (project, fep.fep_project)


def _write_results(project, fep_project, args):
    """Write the results."""
    if not args.project:
        return
    try:
        feputil.save_project(project, args)

        script_dir = os.path.join(os.path.dirname(sys.argv[0]), "fepview.py")
        print("FEP calculation is complete.")
        print("View the results in Flare or by running:")
        print(
            f'"{sys.executable}" "{script_dir}" '
            + f'--project "{args.project}" --name "{fep_project.name}"'
        )
        print(
            f'"{sys.executable}" "{script_dir}" '
            + f'--project "{args.project}" --name "{fep_project.name}" --links'
        )
        print(
            f'"{sys.executable}" "{script_dir}" '
            + f'--project "{args.project}" --name "{fep_project.name}" --cycles'
        )
        print(
            f'"{sys.executable}" "{script_dir}" '
            + f'--project "{args.project}" --name "{fep_project.name}" --overlap'
        )
        print(
            f'"{sys.executable}" "{script_dir}" '
            + f'--project "{args.project}" --name "{fep_project.name}" --log '
        )
    except Exception as err:
        print(err, file=sys.stderr)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
