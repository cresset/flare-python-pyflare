#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""A script to manage and update databases for Spark

examples:
  pyflare sparkdbupdate.py VeryCommon
    Install/update the "VeryCommon" database.

  pyflare sparkdbupdate.py -u
    Upgrade all of the installed databases to the latest version.

environment:
  FLARE_SPARK_DB
    If a relative path is specified with the -d option, the path will be interpreted relative to the
    directory pointed to by this variable.

notes:
  Note that all Spark databases files can be freely moved and copied between installations: you can
  safely download databases to one location and then move or copy them to a different location or
  machine.
"""

import os
import sys
import argparse
import time
import pathlib
from cresset import flare


def bytes_to_readable(nbytes: int) -> str:
    if nbytes > 2000000:
        return f"{nbytes/1000000} MB"
    elif nbytes > 0:
        return f"{nbytes/1000} kB"
    else:
        return "0 B"


def main(argv: [str]) -> int:
    args, parser = _parse_args(argv)

    os.environ["FLARE_DISABLE_SPARK_REMOTE_DBS"] = "1"
    os.environ["FLARE_SPARK_ONLY_CATALOG_DBS"] = "1"
    force_dl_dir = None
    if args.dir is not None and os.path.isdir(args.dir):
        force_dl_dir = str(pathlib.Path(args.dir).resolve())
        os.environ["FLARE_SPARK_DB"] = force_dl_dir

    # Just print out a list or csv of databases
    # Don't have to check for AND as argparse would have already prevented both from being defined
    unfiltered_databases_list = flare.SparkDatabase.databases()
    db_catalog_list = []
    databases_list = []
    for db in unfiltered_databases_list:
        try:
            db_catalog_list.append(flare.SparkCatalog(db))
            databases_list.append(db)
        except ValueError:
            pass
    if args.csv or args.list:
        if args.list:
            padding = 0
            for db in databases_list:
                padding = len(db.name) if len(db.name) > padding else padding
            padding = padding + (padding % 8) - 1
            for i in range(0, len(databases_list)):
                db = databases_list[i]
                db_cat = db_catalog_list[i]
                key_letter = "X"
                if db_cat.is_installed:
                    key_letter = "U" if db_cat.is_update_available else "I"
                elif db_cat.can_be_installed:
                    key_letter = "A"
                print(db.name.ljust(padding), key_letter, "", db.description)
        else:
            print('"Name","Description","Release","Size (MB)","Installed","Path","Requires update"')
            for i in range(0, len(databases_list)):
                db = databases_list[i]
                db_cat = db_catalog_list[i]
                print(
                    f'"{db.name}","{db.description}",{db.release},{db.file_size},'
                    + f'{db_cat.is_installed},"{db.url}",{db_cat.is_update_available}'
                )
    elif len(args.names) == 0 and not (args.update or args.standard or args.reagents):
        parser.print_help()
        print("ERROR: No names given and update/standard/reagents argument not defined")
        return 1
    else:
        STANDARD_DBS = {"VeryCommon", "Common", "ChEMBL_common"}
        for i in range(0, len(databases_list)):
            db = databases_list[i]
            db_cat = db_catalog_list[i]
            if (db.name in args.names) or (
                (args.update and db_cat.is_update_available)
                or (args.standard and db.name in STANDARD_DBS)
                or (args.reagents and db.type == flare.SparkDatabase.Type.Reagents)
            ):
                if not args.dryrun:
                    db_cat.start(force_dl_dir)
                print(f"Downloading/updating '{db.name}'", end="\r")
                while not args.dryrun and db_cat.is_downloading:
                    speed_str = bytes_to_readable(db_cat.current_download_speed) + "/sec"
                    print(
                        f"Downloading/updating '{db.name}': "
                        + f"{bytes_to_readable(db_cat.bytes_downloaded)}/"
                        + f"{bytes_to_readable(db.file_size)}"
                        + f" | {db_cat.current_download_time} secs left | {speed_str}"
                        + f" | {db.url}",
                        end="\r",
                    )
                    time.sleep(1)
                print("")
    return 0


def _parse_args(argv: [str]) -> (argparse.Namespace, argparse.ArgumentParser):
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawTextHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )

    parser.add_argument(
        "names",
        nargs="*",
        help="The database name to install/update.",
    )

    group = parser.add_argument_group("output options")
    exclusive_group = group.add_mutually_exclusive_group()
    exclusive_group.add_argument(
        "-c",
        "--csv",
        action="store_true",
        help="Print the list of available databases to standard output in CSV format.",
    )
    exclusive_group.add_argument(
        "-l",
        "--list",
        action="store_true",
        help="Print the list of available databases to standard output. "
        + "The name of the databases and the descriptions are printed, "
        + "along with a status code:\n\n"
        + "I - installed and up-to-date\nU - installed, needs updating\n"
        + "A - not installed, available to download\nX - Invalid database"
        + " file or unwritable directory",
    )

    group = parser.add_argument_group("install/update options")
    group.add_argument(
        "-d",
        "--dir",
        help="Set the directory to write the downloaded Spark databases to.",
    )
    group.add_argument(
        "-u",
        "--update",
        action="store_true",
        help="Update all installed databases that can be updated.",
    )
    group.add_argument(
        "-s",
        "--standard",
        action="store_true",
        help="Update or install all of the standard Cresset-recommended databases "
        + "(not including the reagent databases).",
    )
    group.add_argument(
        "-r",
        "--reagents",
        action="store_true",
        help="Update or install all of the Cresset reagent databases.",
    )
    group.add_argument(
        "--dryrun",
        action="store_true",
        help="Dry run: don't actually download the databases.",
    )

    group = parser.add_argument_group("general options")
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print log and progress information to standard error.",
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )

    return (parser.parse_args(argv), parser)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
