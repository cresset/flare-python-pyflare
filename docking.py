#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Docks ligands to a protein and writes the results to stdout.

The ligands are docked to the protein in the region defined by the --grid option.
This option takes the 2 opposite corners of the region in the format
min_x,min_y,min_z,max_x,max_y,max_z. For example --grid=-3,-6.2,2,12,8.6,5.

A quick method to find the appropriate grid values to use is to open the protein structure within
Flare and pick the atoms which define the grid. Then in the Python Console run these commands which
will print the grid values:

>>> grid = flare.atom_pick.corners(flare.main_window().picked_atoms)
>>> print(f'{grid[0][0]},{grid[0][1]},{grid[0][2]},{grid[1][0]},{grid[1][1]},{grid[1][2]}')

The region may also be defined by the size and location of the first molecule in a file by using
the --reference file.sdf option.

To save time spent in recalculating the grid when running the script multiple times using the same
protein docking region, the generated grid can be written to a file using the option
--write-grid grid_file and then used in subsequent runs by specifying the
--grid-from-file grid_file option.

The protein should be prepared before docking ligands. Use the proteinprep.py script
to do this. For example:

  pyflare proteinprep.py protein.pdb > prep_protein.pdb
  pyflare docking.py --protein prep_protein.pdb --grid=-3,-6.2,2,12,8.6,5 ligands.sdf

examples:
  pyflare docking.py --protein protein.pdb --grid=-3,-6.2,2,12,8.6,5 ligands.sdf
    Docks the ligands to the protein in the box defined by the grid.
    The results are written to stdout.

  pyflare docking.py --protein protein.pdb --reference ref.sdf --project project.flr ligands.sdf
    Docks the ligands to the protein in the box defined by the first molecule in ref.sdf.
    The results are written to stdout and project.flr.

  pyflare docking.py --protein protein1.pdb --protein protein2.pdb --covalent-residue "A LYS 745" --covalent-residue "A LYS 553" --reference ref.sdf ligands.sdf
    Docks the ligands to an ensemble of proteins in the box defined by the first molecule in ref.sdf.
    Also set the covalent residues assigned to the order of the given proteins.
    The results are written to stdout.

  pyflare docking.py --help
    Prints the help text including all available options and exits.
"""  # noqa: E501
import sys
import argparse
import csv
import traceback

from cresset import flare

from _internal import cmdutil

DEFAULT_BATCH_SIZE = 10000


def main(argv):
    """Run the docking process using the command line args."""
    try:
        docking = flare.Docking()

        args = _parse_args(docking, argv)
        project, ligands_to_dock_role = _configure_process(docking, args)

        imported_tags = set()

        logging_context = cmdutil.LoggingContext(project.log, verbose=args.verbose)

        # Read each batch in turn and process them
        any_ligands_read = False
        for i, batch in enumerate(_read_batch(ligands_to_dock_role, args), 1):
            any_ligands_read = True

            imported_tags = imported_tags.union(set(project.ligands.columns.user_keys()))

            docking.ligands = ligands_to_dock_role.ligands

            # Run the calculation
            cmdutil.start_process(docking, args.broker, args.verbose)
            cmdutil.wait_for_finish(docking, prefix=f"Batch {i}, ", logging_context=logging_context)

            # If there are errors don't stop, write the results and continue on to the next batch.
            # As if a few ligands failed to dock we still want to print the results for all other
            # successfully docked ligands.
            if docking.errors():
                print("\n".join(docking.errors()), file=sys.stderr)

            if not docking.is_cancelled():
                _write_results(docking, project, imported_tags, args)

            docking.system.grid_box = None

        if args.write_grid:
            byte_array = docking.protein.docking_info.grid_bytes()
            with open(args.write_grid, "wb") as grid_file:
                grid_file.write(byte_array)

        if not any_ligands_read:
            raise ValueError("no ligand molecules were read from the input")

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(docking, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )
    parser.add_argument(
        "ligands",
        nargs="*",
        help="The ligand file to dock. If not given the ligands are read from stdin.",
    )

    group = parser.add_argument_group("input/output")
    group.add_argument(
        "-p",
        "--protein",
        action="append",
        help="The protein to dock to. Specify multiple times to perform ensemble docking.",
    )
    group.add_argument(
        "--protein-chain-types",
        metavar="protein,water,other,ligand",
        default="protein,water,other",
        type=str.lower,
        help="Select the types of chains to include in the calculation as a comma separated list. 'p', 'w', 'o' and 'l' can be used as a shorthand for 'protein', water', 'other', 'ligand'. The default is the 'protein', 'water', and 'other' chain.",  # noqa: E501
    )
    group.add_argument(
        "-d",
        "--grid",
        help="The region of the protein to dock to. This is defined as 2 opposite vertices in the format min_x,min_y,min_z,max_x,max_y,max_z. For example --grid=-3,-6.2,2,12,8.6,5.",  # noqa: E501
    )
    group.add_argument(
        "-w",
        "--write-grid",
        help="Write the generated docking grid to the specified file. This can be used in subsequent runs with the option --grid-from-file",  # noqa: E501
    )
    group.add_argument(
        "-f",
        "--grid-from-file",
        help="Read the docking grid from the specified file instead of regenerating it.",
    )
    group.add_argument(
        "-e",
        "--reference",
        help="The region of the protein to dock to defined by the location and size of the first molecule in the file.",  # noqa: E501
    )
    group.add_argument(
        "-i",
        "--input",
        choices=["auto", "pdb", "sdf", "mol2", "smi", "xed"],
        default="auto",
        type=str.lower,
        help="Set the file format for reading the ligands. The default is auto.",
    )
    group.add_argument(
        "--input-ignore-errors",
        action="store_true",
        help="By default, the calculation stops when errors are encountered in the input molecules. However, by setting this option the calculation will continue processing the remaining molecules and log the errors to standard error (stderr).",  # noqa: E501
    )
    group.add_argument(
        "-j",
        "--input-protein",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for reading the protein. The default is to autodetect.",
    )
    group.add_argument(
        "-o",
        "--output",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for writing the molecules. The default is 'sdf'.",
    )
    group.add_argument(
        "-L",
        "--log",
        action="store_true",
        help="Request that the calculation log be written as one of the SDF tags for each pose. Only has an effect if '-o sdf' (the default) is active. The calculation log is written as tag 'FlareDocking_Log'.",  # noqa: E501
    )
    group.add_argument(
        "-P",
        "--project",
        help="Write the Flare project file with the results to the given file path.",
    )
    group.add_argument(
        "--csv",
        help="Write a CSV file containing the docking scores to the given file path.",
    )

    group = parser.add_argument_group("docking options")
    group.add_argument(
        "--minimize-ligands",
        action="store_true",
        help=cmdutil.numpy_doc_to_help(flare.Docking.minimize_ligands.__doc__, True),
    )
    group.add_argument(
        "--max-poses",
        type=int,
        help=cmdutil.numpy_doc_to_help(flare.Docking.max_poses.__doc__, True),
    )
    group.add_argument(
        "-c",
        "--covalent-residue",
        action="append",
        metavar="residue_name",
        help='The name of the residue in the protein that the ligands should be covalently docked to. This should be surrounded with quotation marks, e.g. --covalent-residue "A LYS 745". Specify multiple times to perform ensemble docking is also being performed. The order of the --covalent-residue options should match the order of the --protein options.',  # noqa E501
    )
    group.add_argument(
        "--flickering-water",
        metavar="residue_name",
        action="append",
        help='The name of the water residue in the protein that should be classed as a flickering water. This should be surrounded with quotation marks, e.g. --flickering-water "A HOH 1024". When set the `--protein-chain-types` will be adjusted to include water chains. Each ligand will be docked with and without the water residue and the best poses will be kept. As the ligand is docked multiple times this increases the length of the calculation by 2^N where N is the number of flickering waters. This argument can be used multiple times to specify multiple flickering water residues.',  # noqa E501
    )
    group.add_argument(
        "--docking-constraint",
        nargs=4,
        metavar=("residue_name", "atom_name", "constraint_type", "constraint_strength"),
        action="append",
        help="A constraint for docking. This should be a residue name, an atom name, a type, and an associated strength. The type can be 'h' for hydrogen bonding, 'm' for metal, 's' for salt bridge, 'p' for pi-pi, or 'c' for cation-pi. The constraint strength can be weak (w), normal (n) or strong (s). As the residue name contains spaces it should be surrounded with quotation marks. This argument can be used multiple times to specify multiple constraints, e.g. `--docking-constraint \"A LEU 10\" H h w --docking-constraint \"A LYS 12\" NZ c s` specifies two constraints.",  # noqa E5
    )
    group.add_argument(
        "--max-constraint-penalty",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.LeadFinderSystem.maximum_constraint_penalty.__doc__),
    )
    group.add_argument(
        "--frozen-torsion",
        nargs=2,
        metavar=("SMARTS", "angle_degrees"),
        action="append",
        help='A torsion in the ligand which will be fixed at an angle. The torsion is defined by a SMARTS pattern with the atom labels 1, 2, 3, and 4 which define the atoms which make up the torsion. The angle should be between -180 and 180 degrees. This argument can be used multiple times to specify multiple torsions, e.g. --frozen-torsion "O[S:3](=[O:4])(=O)[c:2]1[c:1]cc(cc1)C" 90 --frozen-torsion "[N:3]([c:2]1[c:1]cccc1)[c:4]2ncccn2" -45 specifies two frozen torsions.',  # noqa E5
    )
    group.add_argument(
        "--number-of-runs",
        type=int,
        help=cmdutil.numpy_doc_to_help(flare.LeadFinderSystem.number_of_runs.__doc__),
    )
    group.add_argument(
        "--pool-size",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.LeadFinderSystem.pool_size.__doc__),
    )
    group.add_argument(
        "--population-size",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.LeadFinderSystem.population_size.__doc__),
    )
    group.add_argument(
        "--quality",
        choices=["exhaustive", "normal", "virtual-screening", "score", "score-fixed"],
        type=str.lower,
        help=cmdutil.numpy_doc_to_help(flare.LeadFinderSystem.quality.__doc__, True),
    )
    group.add_argument(
        "--rotate-amide-bonds",
        action="store_true",
        help=cmdutil.numpy_doc_to_help(flare.LeadFinderSystem.rotate_amide_bonds.__doc__, True),
    )
    group.add_argument(
        "--rotate-ester-bonds",
        action="store_true",
        help=cmdutil.numpy_doc_to_help(flare.LeadFinderSystem.rotate_ester_bonds.__doc__, True),
    )
    group.add_argument(
        "--timeout",
        type=int,
        help=cmdutil.numpy_doc_to_help(flare.LeadFinderSystem.timeout.__doc__),
    )
    group.add_argument(
        "--additional-ring-conformation-sampling",
        action="store_true",
        help=cmdutil.numpy_doc_to_help(
            flare.LeadFinderSystem.additional_ring_conformation_sampling.__doc__, True
        ),
    )
    group.add_argument(
        "--template-ligand", help="An optional ligand to use to guide the docking process."
    )

    group = parser.add_argument_group("general options")
    group.add_argument(
        "--batch-size",
        type=int,
        default=DEFAULT_BATCH_SIZE,
        help="Limit the number of molecules which are read into memory. Smaller values will reduce memory usage but will reduce performance by a small amount. If --project/-P is set then this option is ignored.",  # noqa: E501
    )
    group.add_argument(
        "--config",
        help=cmdutil.CONFIG_ARG_HELP_TEXT,
    )
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        default=docking.local_engine_count,
        help="Set the number of local engines to start. Use 0 for no local processing: you must specify some remote engines with -g if you do so.",  # noqa: E501
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker in the format hostname:port. Field Engines which have been registered with the Broker will be used during the calculation. The CRESSET_BROKER environment variable can be used instead of this option.",  # noqa: E501
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )
    args = parser.parse_args(argv)

    return args


def _find_format(mol_file, args):
    # If reading from stdin and the file format is not given assume "sdf" format
    input = args.input if args.input != "auto" else None
    if mol_file == "-" and input is None:
        input = "sdf"
    return input


def _configure_process(docking, args):
    """Configure `docking` using the command line arguments."""
    project = flare.Project()

    # Load the config file first. Any other settings given will override the config file.
    if args.config:
        docking.read_config(args.config)

    if sum(map(bool, [args.reference, args.grid, args.grid_from_file])) > 1:
        raise ValueError("only one of --reference, --grid and --grid-from-file should be set")

    ligands_role = project.roles.append("Ligands")

    if not args.protein and not args.proteins:
        raise ValueError("at least one protein should be set (--protein or --proteins)")

    if args.protein:
        for protein in args.protein:
            project.proteins.extend(flare.read_file(protein, args.input_protein))

    if not project.proteins:
        raise ValueError("no protein molecules were read from the input")

    docking.proteins = project.proteins
    for protein in docking.proteins:
        protein.is_prepared = True  # Suppress warnings
    chain_types = args.protein_chain_types
    # If flickering waters are being used force the inclusion of the water chains
    if args.flickering_water:
        chain_types = chain_types + ",water"
    for protein in docking.proteins:
        docking.set_sequences_for_protein(
            protein, cmdutil.chain_types_to_sequences(protein, chain_types)
        )

    if args.grid:
        grid_list = args.grid.split(",")
        if len(grid_list) != 6:
            raise ValueError("expected grid to be 6 float values separated by commas")
        top_left = (grid_list[0], grid_list[1], grid_list[2])
        bottom_right = (grid_list[3], grid_list[4], grid_list[5])
        docking.system.grid_box = (top_left, bottom_right)

    elif args.reference:
        role = project.roles.append("Reference", "The molecule which define the region to dock to")
        role.ligands.extend(flare.read_file(args.reference, _find_format(args.reference, args)))
        if not role.ligands:
            raise ValueError(f"file '{args.reference}' does not contain any molecules")
        if len(role.ligands) > 1:
            print(
                "Warning: multiple references specified, only the first reference molecule will "
                + "be used to define the region of the protein to dock to",
                file=sys.stderr,
            )

        del role.ligands[1:]
        docking.system.grid_box = role.ligands[0]

    elif args.grid_from_file:
        with open(args.grid_from_file, "rb") as grid_file:
            grid_bytes = grid_file.read()
            docking.system.grid_box = grid_bytes

    else:
        if not args.reference and not args.grid and not args.grid_from_file:
            raise ValueError("one of --reference, --grid or --grid-from-file must be set")

    if args.minimize_ligands:
        docking.minimize_ligands = args.minimize_ligands

    if args.max_poses is not None:
        with cmdutil.CheckValue("max-poses"):
            docking.max_poses = args.max_poses
    elif args.config is None:
        with cmdutil.CheckValue("max-poses"):
            docking.max_poses = 1  # This script has a different default to Flare GUI

    if args.covalent_residue:
        # Note that docking.proteins can reorder the proteins, so get the protein list
        # from the project instead
        total_proteins = len(project.proteins)
        if total_proteins != len(args.covalent_residue):
            raise ValueError(
                f"The amount of covalent residues must match amount of proteins. Amount: {total_proteins}."  # noqa: E501
            )
        for i, residue_text in enumerate(args.covalent_residue):
            protein = project.proteins[i]
            residues = protein.residues.find(residue_text)
            if len(residues) == 0:
                raise ValueError(
                    f"The protein {protein.title} does not contain the residue '{residue_text}'"
                )
            elif len(residues) > 1:
                raise ValueError(
                    f"The protein {protein.title} contains {len(residues)} residues that match "
                    "residue {residue_text}"
                )
            docking.system.set_covalent_residue_for_protein(protein, residues[0])

    if args.flickering_water:
        water_residues = []
        for water_residue_name in args.flickering_water:
            residues = docking.protein.residues.find(water_residue_name)
            if len(residues) == 0:
                raise ValueError(f"The protein does not contain the residue '{water_residue_name}'")
            water_residues.extend(residues)
        docking.system.flickering_waters = water_residues

    if args.docking_constraint:
        # Create constraints dictionary, where key is atom and value is (type, strength) tuple
        docking_constraints_dict = {}
        constraint_atom_used = []
        for constraint in args.docking_constraint:
            residue_name = constraint[0]
            atom_name = constraint[1]
            constraint_type_char = constraint[2]
            constraint_strength_char = constraint[3]

            residues = docking.protein.residues.find(residue_name)
            if len(residues) == 0:
                raise ValueError(f"The protein does not contain the residue '{residue_name}'")
            if len(residues) > 1:
                raise ValueError(
                    f"The protein contains {len(residues)} residues that match "
                    f"residue {residue_name}"
                )

            residue = residues[0]
            constraint_atom = None
            for atom in residue.atoms:
                if atom.name == atom_name:
                    if constraint_atom is None:
                        constraint_atom = atom
                    else:
                        raise ValueError(
                            f"The protein residue '{residue_name}' contains more than one atom called "  # noqa E203
                            f"'{atom_name}'"
                        )
            if constraint_atom is None:
                raise ValueError(
                    f"The protein residue '{residue_name}' does not contain an atom called "
                    f"'{atom_name}'"
                )

            if constraint_atom in constraint_atom_used:
                raise ValueError("Only one constraint can be added to each atom")
            constraint_atom_used.append(constraint_atom)

            if constraint_strength_char == "w":
                constraint_strength = flare.LeadFinderSystem.ConstraintStrength.Weak
            elif constraint_strength_char == "n":
                constraint_strength = flare.LeadFinderSystem.ConstraintStrength.Normal
            elif constraint_strength_char == "s":
                constraint_strength = flare.LeadFinderSystem.ConstraintStrength.Strong
            else:
                raise ValueError(
                    "Docking constraint strengths must be one of 'w' for weak, 'n' for normal or "
                    "'s' for strong"
                )

            if constraint_type_char == "h":
                constraint_type = flare.LeadFinderSystem.ConstraintType.HydrogenBond
            elif constraint_type_char == "m":
                constraint_type = flare.LeadFinderSystem.ConstraintType.Metal
            elif constraint_type_char == "s":
                constraint_type = flare.LeadFinderSystem.ConstraintType.SaltBridge
            elif constraint_type_char == "p":
                constraint_type = flare.LeadFinderSystem.ConstraintType.PiStack
            elif constraint_type_char == "c":
                constraint_type = flare.LeadFinderSystem.ConstraintType.PiCation
            else:
                raise ValueError(
                    "Docking constraint types must be one of 'h' for hydrogen-bond, 'm' for metal, "
                    "'s' for salt-bridge, 'p' for pi-pi stack, or 'c' for pi-cation"
                )

            docking_constraints_dict[constraint_atom] = (constraint_type, constraint_strength)

        docking.system.docking_constraints = docking_constraints_dict

    if args.max_constraint_penalty is not None:
        with cmdutil.CheckValue("max-constraint-penalty"):
            docking.system.maximum_constraint_penalty = args.max_constraint_penalty

    if args.frozen_torsion:
        frozen_torsions = []
        for frozen_torsion_arg in args.frozen_torsion:
            frozen_torsions.append((frozen_torsion_arg[0], int(frozen_torsion_arg[1])))
        docking.system.frozen_torsions = frozen_torsions

    if args.number_of_runs is not None:
        with cmdutil.CheckValue("number-of-runs"):
            docking.system.number_of_runs = args.number_of_runs

    if args.pool_size is not None:
        with cmdutil.CheckValue("pool-size"):
            docking.system.pool_size = args.pool_size

    if args.population_size is not None:
        with cmdutil.CheckValue("population-size"):
            docking.system.population_size = args.population_size

    if args.quality == "exhaustive":
        docking.system.quality = flare.LeadFinderSystem.Quality.ExtraPrecision
    elif args.quality == "virtual-screening":
        docking.system.quality = flare.LeadFinderSystem.Quality.VirtualScreening
    elif args.quality == "score":
        docking.system.quality = flare.LeadFinderSystem.Quality.ScoreOnlyOptimized
    elif args.quality == "score-fixed":
        docking.system.quality = flare.LeadFinderSystem.Quality.ScoreOnlyFixed
    else:
        docking.system.quality = flare.LeadFinderSystem.Quality.Normal

    # `rotate-amide-bonds` and `rotate-ester-bonds` use `action="store_true"`
    # Only use them if they are True, otherwise their False default will override the config file.
    # This means you cannot use the CLI to override a True value to False.
    if args.rotate_amide_bonds:
        docking.system.rotate_amide_bonds = args.rotate_amide_bonds

    if args.rotate_ester_bonds:
        docking.system.rotate_ester_bonds = args.rotate_ester_bonds

    if args.timeout is not None:
        with cmdutil.CheckValue("timeout"):
            docking.system.timeout = args.timeout

    if args.additional_ring_conformation_sampling:
        docking.system.additional_ring_conformation_sampling = (
            args.additional_ring_conformation_sampling
        )

    if args.template_ligand:
        role = project.roles.append("Template", "Ligand to guide the docking process")
        role.ligands.extend(
            flare.read_file(args.template_ligand, _find_format(args.template_ligand, args))
        )
        if not role.ligands:
            raise ValueError(f"file '{args.template_ligand}' does not contain any molecules")
        if len(role.ligands) > 1:
            print(
                "Warning: multiple template ligands specified, only the first template will "
                + "be used to guide the docking process",
                file=sys.stderr,
            )

        del role.ligands[1:]
        docking.system.template_ligand = role.ligands[0]

    with cmdutil.CheckValue("localengines"):
        docking.local_engine_count = args.localengines

    return project, ligands_role


def _read_batch(ligands_to_dock_role, args):
    """Yield batches of molecules read from the command line options."""
    # Read from stdin if no ligand files are given
    mol_files = args.ligands if args.ligands else ["-"]
    # If a project is set then all ligands need to be kept in memory so the project can be
    # saved
    if args.project and args.batch_size != DEFAULT_BATCH_SIZE:
        print("--batch-size will be ignored as -P/--project was given.\n", file=sys.stderr)
    batch_size = args.batch_size if not args.project else sys.maxsize

    raise_exception_on_error = not args.input_ignore_errors

    ligands_to_dock_role.ligands.clear()
    for mol_file in mol_files:
        for batch, errors in cmdutil.batch_read_files(
            [mol_file], _find_format(mol_file, args), batch_size, raise_exception_on_error
        ):
            ligands_to_dock_role.ligands.extend(batch)

            for error in errors:
                print(error, file=sys.stderr)

            if len(ligands_to_dock_role.ligands) >= batch_size:
                yield
                ligands_to_dock_role.ligands.clear()

    if ligands_to_dock_role.ligands:
        yield


def _write_results(docking, project, imported_tags, args):
    """Write the results."""
    if args.project:
        try:
            project.save(args.project)
        except Exception as err:
            print(err, file=sys.stderr)
            # print the error and continue

    # Print the result poses to stdout
    poses = []
    for ligand in docking.ligands:
        if args.log:
            log_text = cmdutil.molecule_log_as_sdf_string(ligand)
            for pose in ligand.poses:
                pose.properties["FlareDocking_Log"].value = log_text
        for i, pose in enumerate(ligand.poses, 1):
            pose.properties["Pose"].value = i
            pose.properties["Ligand Title"].value = ligand.title
        poses.extend(ligand.poses)

    file_format = args.output if args.output else "sdf"

    allowed_cresset_tags = [
        "LF dG",
        "LF VSscore",
        "LF Rank Score",
        "LF LE",
        "Pose",
        "Poses",
        "Protein",
        "Filename",
        "_cresset_fieldpoints",
        "FlareDocking_Log",
        "Ligand Title",
    ]
    list_tags_to_keep = list(imported_tags)
    list_tags_to_keep.extend(allowed_cresset_tags)

    def modify_tags(pose, tags):
        """Add/Removes SDF tags for each pose."""
        tags["Filename"] = tags["Filename_cresset"]
        tags["Protein"] = tags["Protein_cresset"]
        tags["Poses"] = str(len(pose.ligand.poses))
        tags_to_delete = [tag for tag in tags if tag not in list_tags_to_keep]
        for tag in tags_to_delete:
            del tags[tag]

        return tags

    flare.write_file(sys.stdout, poses, file_format, modify_tags)

    if args.csv:
        with open(args.csv, "w", newline="") as csv_file:
            _write_csv_file(poses, csv_file)


def _write_csv_file(poses, csv_file):
    """Write the ligand titles, pose numbers and docking scores to `csv_file`."""
    writer = csv.writer(csv_file)

    column_titles = ["Ligand Title", "Pose", "LF dG", "LF VSscore", "LF Rank Score", "LF LE"]

    # Write the table header
    writer.writerow(column_titles)

    for pose in poses:
        cells = _molecule_row_data(pose, column_titles)
        # Write the pose properties
        writer.writerow(cells)


def _molecule_row_data(molecule, column_titles):
    """Return a list containing the molecule properties for the column titles as strings."""
    cells = []
    for column in column_titles:
        data = molecule.properties[column].value
        cells.append(str(data) if data is not None else "")
    return cells


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
