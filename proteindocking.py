#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Docks one protein to another and writes the results to a directory or Flare project.

Both proteins must have dynamics trajectories associated with them. The matching
topology files must also be present alongside the protein files. Only natural amino
acid chains are used in the calculation and are automatically detected.

The output from the calculation is a group of docked poses, with names containing the
docking score. These are either written to a directory, or a Flare project can be
created containing them.

examples:
    pyflare proteindocking.py receptor.pdb ligand.pdb -o /output/path -p 10
      Docks ligand.pdb to protein.pdb and outputs 10 poses to the given path

    pyflare proteindocking.py --help
      Prints the help text including all available options and exits.
"""  # noqa: E501
import sys
import os
import argparse
import traceback

from cresset import flare

from _internal import cmdutil


def main(argv):
    """Run the protein docking process using the command line args."""
    try:
        docking = flare.ProteinDocking()

        args = _parse_args(docking, argv)
        project = _configure_process(docking, args)
        logging_context = cmdutil.LoggingContext(project.log, verbose=args.verbose)
        cmdutil.start_process(docking, None, args.verbose)
        cmdutil.wait_for_finish(docking, logging_context=logging_context)
        cmdutil.check_errors(docking)
        if not docking.is_cancelled():
            _write_results(project, args)

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(docking, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )

    parser.add_argument(
        "receptor",
        help="The protein to use as the receptor in the docking calculation.",
    )
    parser.add_argument(
        "ligand",
        help="The protein to use as the ligand in the docking calculation.",
    )

    group = parser.add_argument_group("input/output")
    group.add_argument(
        "-j",
        "--input-protein",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for reading the proteins. The default is to autodetect.",
    )
    group.add_argument(
        "-o",
        "--output-path",
        help="Write proteins to file in the given location. The default behavior is to output "
        "results alongside the input proteins.",
    )
    group.add_argument(
        "-P",
        "--project",
        help="Write a Flare project file with the results to the given file path.",
    )

    group = parser.add_argument_group("docking options")
    group.add_argument(
        "-i",
        "--iso-value",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.ProteinDocking.iso_value.__doc__),
    )
    group.add_argument(
        "-d",
        "--distance-cutoff",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.ProteinDocking.distance_cutoff.__doc__),
    )
    group.add_argument(
        "-s",
        "--num-solutions",
        type=int,
        help=cmdutil.numpy_doc_to_help(flare.ProteinDocking.num_solutions.__doc__),
    )
    group.add_argument(
        "-p",
        "--max-poses",
        type=int,
        help=cmdutil.numpy_doc_to_help(flare.ProteinDocking.max_poses.__doc__),
    )

    group = parser.add_argument_group("general options")
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )
    args = parser.parse_args(argv)

    return args


def _configure_process(docking, args):
    """Configure `proteindocking` using the command line arguments."""
    project = flare.Project()

    project.proteins.extend(flare.read_file(args.receptor, args.input_protein))
    project.proteins.extend(flare.read_file(args.ligand, args.input_protein))

    if not project.proteins:
        raise ValueError("failed to load protein(s)")
    if len(project.proteins) != 2:
        raise ValueError("two proteins must be specified")
    else:
        docking.receptor = project.proteins[0]
        docking.receptor_sequences = cmdutil.chain_types_to_sequences(docking.receptor, "protein")
        docking.ligand = project.proteins[1]
        docking.ligand_sequences = cmdutil.chain_types_to_sequences(docking.ligand, "protein")

    docking.output_role = "dock"

    # Docking Settings
    if args.iso_value is not None:
        with cmdutil.CheckValue("iso-value"):
            docking.iso_value = args.iso_value
    if args.distance_cutoff is not None:
        with cmdutil.CheckValue("distance-cutoff"):
            docking.distance_cutoff = args.distance_cutoff
    if args.num_solutions is not None:
        with cmdutil.CheckValue("num-solutions"):
            docking.num_solutions = args.num_solutions
    if args.max_poses is not None:
        with cmdutil.CheckValue("max-poses"):
            docking.max_poses = args.max_poses

    return project


def _write_results(project, args):
    """Write the results."""
    # If requested write the project file
    if args.project:
        try:
            project.save(args.project)
        except Exception as err:
            print(err, file=sys.stderr)
            # print the error and continue

    if args.output_path:
        output_path = args.output_path
    else:
        output_path = os.path.dirname(args.receptor)

    for pose in project.protein_roles["dock"].proteins:
        flare.write_file(os.path.join(output_path, pose.title + ".pdb"), [pose], "pdb")


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
