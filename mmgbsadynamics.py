#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Runs MM/GBSA on a dynamics trajectory and writes the results to stdout.

A protein must be provided that has a ligand residue and an associated dynamics trajectory.
A topology file is also required and must be in the same location as the protein. By default,
the first ligand residue in the protein will be scored. If there are multiple and a different
residue needs to be scored, its name can be provided.

The output from the calculation is the specified ligand residue, converted to a molecule and
with an additional tag containing the MM/GBSA energy.

examples:
  pyflare mmgbsadynamics.py --protein protein.pdb --residue-name ABC
    Calculates the binding free energy of the ligand residue with the name "ABC", within the
    given protein, then converts the residue to a molecule and writes it to stdout.

  pyflare mmgbsadynamics.py --help
    Prints the help text including all available options and exits.
"""  # noqa: E501
import sys
import argparse
import traceback

from cresset import flare

from _internal import cmdutil


def main(argv):
    """Run the mmgbsa dynamics process using the command line args."""
    try:
        mmgbsaDynamics = flare.MMGBSADynamics()

        # Configure the mmgbsa process with the command line arguments
        args = _parse_args(mmgbsaDynamics, argv)
        project = _configure_process(mmgbsaDynamics, args)

        logging_context = cmdutil.LoggingContext(project.log, verbose=args.verbose)

        # Start the process
        cmdutil.start_process(mmgbsaDynamics, args.broker, args.verbose)

        # Display the progress of the mmgbsa process
        cmdutil.wait_for_finish(mmgbsaDynamics, logging_context=logging_context)
        cmdutil.check_errors(mmgbsaDynamics)

        # Writes the results based on the command line arguments
        if not mmgbsaDynamics.is_cancelled():
            _write_results(project, args)

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(mmgbsaDynamics, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )

    group = parser.add_argument_group("input/output")
    group.add_argument(
        "-p",
        "--protein",
        help="The protein whose dynamics trajectory will be used in the calculation.",
    )
    group.add_argument(
        "-r",
        "--residue-name",
        help="The name of the ligand residue to be scored. If not specified, the first ligand "
        "residue in the protein will be scored.",
    )
    group.add_argument(
        "-j",
        "--input-protein",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for reading the protein. The default is to autodetect.",
    )
    group.add_argument(
        "-f",
        "--first-frame",
        type=int,
        help="Set the first frame of the trajectory to include. The default skips the first 20%% of"
        " the trajectory.",
    )
    group.add_argument(
        "-F",
        "--last-frame",
        type=int,
        help="Set the last frame of the trajectory to include. The default is the last frame.",
    )
    group.add_argument(
        "-i",
        "--score-interval",
        type=int,
        help="Set the interval of frames to include. The default is 1.",
    )

    group = parser.add_argument_group("force field")
    group.add_argument(
        "-w",
        "--solvent-model",
        choices=[
            "implicit",
            "gbn",
            "gbn2",
            "hct",
            "obc1",
            "obc2",
        ],
        help="The solvent model to use. Implicit (gbn2) solvent is used by default.",
    )

    group = parser.add_argument_group("general options")
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        default=mmgbsaDynamics.local_engine_count,
        help="Set the number of local engines to start. Use 0 for no local processing: you must specify some remote engines with -g if you do so.",  # noqa: E501
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker in the format hostname:port. Field Engines which have been registered with the Broker will be used during the calculation. The CRESSET_BROKER environment variable can be used instead of this option.",  # noqa: E501
    )
    group.add_argument(
        "-L",
        "--log",
        action="store_true",
        help="Request that the calculation log be written as one of the SDF tags for the output ligand. The calculation log is written as tag 'FlareMMGBSADynamics_Log'.",  # noqa: E501
    )
    group.add_argument(
        "-P",
        "--project",
        help="Write the Flare project file with the results to the given file path.",
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )
    args = parser.parse_args(argv)

    return args


def _configure_process(mmgbsaDynamics, args):
    """Configure `mmgbsadynamics` using the command line arguments."""

    project = flare.Project()

    project.proteins.extend(flare.read_file(args.protein, args.input_protein))

    if not project.proteins:
        raise ValueError("failed to load protein")
    if len(project.proteins) > 1:
        raise ValueError("only one protein can be specified")

    if args.residue_name:
        matches = project.proteins[0].residues.find(args.residue_name)
        if len(matches) == 0:
            raise ValueError(f"failed to find residue '{args.residue_name}'")
        if matches[0].sequence.type != flare.Sequence.Type.Ligand:
            raise ValueError(f"residue matched by '{args.residue_name}' is not a ligand")
        mmgbsaDynamics.residues = {matches[0]}
    else:
        # Use the first ligand residue
        for seq in project.proteins[0].sequences:
            if seq.type == flare.Sequence.Type.Ligand:
                mmgbsaDynamics.residues = {seq.atoms[0].residue}
                break

    if len(mmgbsaDynamics.residues) == 0:
        raise RuntimeError("no ligand residue found")

    if args.first_frame is not None or args.last_frame is not None:
        trajectory = flare.DynamicsResult(project.proteins[0])

        if args.first_frame is not None:
            if 0 < args.first_frame and args.first_frame <= trajectory.frame_count:
                mmgbsaDynamics.range_start = (args.first_frame - 1) / trajectory.frame_count
            else:
                raise ValueError(
                    "`--first-frame` must be greater than 0 and not more than the total number of frames."  # noqa: E501
                )

        if args.last_frame is not None:
            if 0 < args.last_frame and args.last_frame <= trajectory.frame_count:
                mmgbsaDynamics.range_end = args.last_frame / trajectory.frame_count
            else:
                raise ValueError(
                    "`--last-frame` must be greater than 0 and not more than the total number of frames."  # noqa: E501
                )

    if mmgbsaDynamics.range_start >= mmgbsaDynamics.range_end:
        raise ValueError(
            f"selected trajectory range is {mmgbsaDynamics.range_start}-{mmgbsaDynamics.range_end}. "  # noqa: E501
            "The beginning of the range must be before the end. "
            "Adjust the chosen range with `--first-frame` and `--last-frame`."
        )

    if args.score_interval:
        mmgbsaDynamics.score_interval = args.score_interval

    if args.solvent_model == "implicit":
        mmgbsaDynamics.solvate.model = flare.SolventModel.ImplicitGBn2
    elif args.solvent_model == "vacuum":
        mmgbsaDynamics.solvate.model = flare.SolventModel.Vacuum
    elif args.solvent_model == "gbn":
        mmgbsaDynamics.solvate.model = flare.SolventModel.ImplicitGBn
    elif args.solvent_model == "gbn2":
        mmgbsaDynamics.solvate.model = flare.SolventModel.ImplicitGBn2
    elif args.solvent_model == "hct":
        mmgbsaDynamics.solvate.model = flare.SolventModel.ImplicitHCT
    elif args.solvent_model == "obc1":
        mmgbsaDynamics.solvate.model = flare.SolventModel.ImplicitOBC1
    elif args.solvent_model == "obc2":
        mmgbsaDynamics.solvate.model = flare.SolventModel.ImplicitOBC2

    # Other Settings
    with cmdutil.CheckValue("localengines"):
        mmgbsaDynamics.local_engine_count = args.localengines

    return project


def _write_results(project, args):
    """Write the results."""
    # If requested write the project file
    if args.project:
        try:
            project.save(args.project)
        except Exception as err:
            print(err, file=sys.stderr)
            # print the error and continue

    ligand = project.ligands[0]

    if args.log:
        log_text = cmdutil.molecule_log_as_sdf_string(ligand)
        ligand.properties["FlareMMGBSADynamics_Log"].value = log_text

    list_tags_to_keep = ["FlareMMGBSADynamics_Log", "MM/GBSA dG"]

    def modify_tags(ligand, tags):
        """Add/Removes SDF tags for each ligand."""
        tags_to_delete = [tag for tag in tags if tag not in list_tags_to_keep]
        for tag in tags_to_delete:
            del tags[tag]

        return tags

    # Print the result to stdout
    flare.write_file(sys.stdout, {ligand}, "sdf", modify_tags)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
