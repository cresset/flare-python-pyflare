#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Calculates the Electrostatic Complementarity of ligands to a protein.

The results are written to stdout.

The protein should be prepared before running this calculation. Use the proteinprep.py script
to do this. For example:

  pyflare proteinprep.py protein.pdb > prep_protein.pdb
  pyflare electrostaticcomplementarity.py --protein prep_protein.pdb ligands.sdf

examples:
  pyflare electrostaticcomplementarity.py --protein protein.pdb ligands.sdf
    Calculates the electrostatic complementarity of the ligands to the protein.
    The results are written to stdout in the SDF tags "EC", "EC r" and "EC rho".

  pyflare electrostaticcomplementarity.py --help
    Prints the help text including all available options and exits.
"""
import sys
import argparse
import traceback

from cresset import flare

from _internal import cmdutil


def main(argv):
    """Run the electrostatic complementarity process using the command line args."""
    try:
        elec = flare.ElectrostaticComplementarity()

        args = _parse_args(elec, argv)
        project, imported_tags = _configure_process(elec, args)
        logging_context = cmdutil.LoggingContext(project.log, verbose=args.verbose)
        cmdutil.start_process(elec, args.broker, args.verbose)
        cmdutil.wait_for_finish(elec, logging_context=logging_context)
        cmdutil.check_errors(elec)
        if not elec.is_cancelled():
            _write_results(elec, project, imported_tags, args)

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(elec, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )
    parser.add_argument(
        "ligands",
        nargs="*",
        help="The ligands file to calculate the electrostatic complementarity for. If not given the ligands are read from stdin.",  # noqa: E501
    )

    group = parser.add_argument_group("input options")
    group.add_argument(
        "-p",
        "--protein",
        required=True,
        help="The protein to calculate the electrostatic complementarity to.",
    )
    group.add_argument(
        "-i",
        "--input",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for reading the ligands. The default is to autodetect.",
    )
    group.add_argument(
        "-j",
        "--input-protein",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for reading the protein. The default is to autodetect.",
    )

    group = parser.add_argument_group("general options")
    group.add_argument(
        "-L",
        "--log",
        action="store_true",
        help="Request that the calculation log be written as one of the SDF tags for each ligand. The calculation log is written as tag 'FlareEC_Log'.",  # noqa: E501
    )
    group.add_argument(
        "-P",
        "--project",
        help="Write the Flare project file with the results to the given file path.",
    )
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        default=elec.local_engine_count,
        help="Set the number of local engines to start. Use 0 for no local processing: you must specify some remote engines with -g if you do so.",  # noqa: E501
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker in the format hostname:port. Field Engines which have been registered with the Broker will be used during the calculation. The CRESSET_BROKER environment variable can be used instead of this option.",  # noqa: E501
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )
    args = parser.parse_args(argv)

    return args


def _configure_process(elec, args):
    """Configure `elec` using the command line arguments."""
    project = flare.Project()

    for ligand_file in args.ligands:
        project.ligands.extend(flare.read_file(ligand_file, args.input))

    if not args.ligands:
        # If the ligand is not set read it from stdin
        file_format = args.input if args.input else "sdf"
        project.ligands.extend(flare.read_file(sys.stdin, file_format))

    if not project.ligands:
        raise ValueError("no ligand molecules were read from the input")

    protein = project.proteins.append(next(flare.read_file(args.protein, args.input_protein)))
    protein.ligands = project.ligands

    imported_tags = set(project.ligands.columns.user_keys())

    elec.ligands = project.ligands

    if not project.proteins:
        raise ValueError("no protein molecules were read from the input")
    if len(project.proteins) > 1:
        raise ValueError("only one protein can be specified")

    with cmdutil.CheckValue("localengines"):
        elec.local_engine_count = args.localengines

    return project, imported_tags


def _write_results(elec, project, imported_tags, args):
    """Write the results."""
    if args.project:
        try:
            project.save(args.project)
        except Exception as err:
            print(err, file=sys.stderr)
            # print the error and continue

    if args.log:
        for ligand in elec.ligands:
            log_text = cmdutil.molecule_log_as_sdf_string(ligand)
            ligand.properties["FlareEC_Log"].value = log_text

    allowed_cresset_tags = ["EC", "EC r", "EC rho", "Filename", "FlareEC_Log"]
    list_tags_to_keep = list(imported_tags)
    list_tags_to_keep.extend(allowed_cresset_tags)

    def modify_tags(ligand, tags):
        """Add/Removes SDF tags for each ligand."""
        tags["Filename"] = tags["Filename_cresset"]
        tags_to_delete = [tag for tag in tags if tag not in list_tags_to_keep]
        for tag in tags_to_delete:
            del tags[tag]

        return tags

    # Print the results to stdout
    flare.write_file(sys.stdout, elec.ligands, "sdf", modify_tags)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
