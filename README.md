# Flare Python pyflare

Welcome to the [Flare](https://www.cresset-group.com/flare/) Python pyflare repository. This repository is a collection of free Python scripts which allows Flare (TM) functionality to be accessed through the pyflare command line interpreter.

Since Flare 5 the command line scripts are installed with Flare. They are found in the "pyflare-scripts" directory within Flare's installation directory.
These scripts are also included in this repository so that they can be updated separately from Flare.

The scripts can be run by:
> pyflare scriptname.py [options]

If you are looking for Flare extensions please visit the [flare-python-extensions repository](https://gitlab.com/cresset/flare-python-extensions).

If you need help with any of these scripts or have suggestions of your own please contact [Cresset support](https://www.cresset-group.com/about-us/contact-us/).

## Scripts Descriptions

### Absolute Binding Free Energy (abferun.py)
Runs an Absolute FEP calculation and writes the results to stdout.

The Absolute Binding Free Energy (ABFE) calculation is run on a set of ligands and a protein.
When completed the ligands are written to stdout in sdf format with tags for the predicted activity.

If the `--project` option is set then a project file will be written. If the calculation is
interrupted by pressing Ctrl+C the current progress will be saved in the specified Flare project
file. To resume the calculation, run the script with the
`--continue` option. If you want to restart the calculation from the beginning and discard any
previous results, use the --delete option. If both the project file and ligands are given then the
ligands are added to the calculation.

The FEP calculation requires GPU resources to run. It is recommended to use a Cresset Engine Broker
by setting the `--broker` option. However, on Linux local GPU resources may be used.
To use multiple GPU devices, the `--cuda-device-index` or `--opencl-device-index` options should be
specified multiple times, e.g. specifying `--cuda-device-index 0 --cuda-device-index 1`
will allow the calculation to use both GPUs 0 and 1.

Once the FEP calculation is complete and if the `--project` option was set then details about the
the results can be viewed by the fepview.py script.

*Examples:*
> pyflare abferun.py --project project.flr --protein protein.pdb ligands.sdf --broker example:9000

Starts a new calculation. The results are stored in the project and written to stdout.

> pyflare abferun.py --project project.flr --continue --broker example:9000

Continues the ABFE calculation on the FEP project within project.flr.

> pyflare abferun.py --help

Prints the help text including all available options and exits.


### Align (align.py)
Aligns ligands using their electrostatic and hydrophobic fields and writes the results to stdout.

Ligands or conformations are read in and aligned using Cresset's "field point" technology.
Field points are a way of representing molecules in terms of their surface and electrostatic
properties: positive and negative electrostatic fields, van der Waals effects and hydrophobic
effects on and near the surface of a molecule. Two molecules which both bind to a common active
site tend to make similar interactions with the protein and hence have highly similar field point
patterns. This script thus aligns molecules such that both their field point patterns and the
underlying fields are as similar as possible: this has been shown to correlate extremely well
with the bioactive alignments.


*Examples:*
> pyflare align.py reference.sdf mols.sdf > results.sdf

Reads `mols.sdf`. For each entry, generates conformers then aligns them to the
molecule (or set of molecules) loaded from `reference.sdf`.
Writes the best alignment for each molecule to 'results.sdf' in SDF format.

> pyflare align.py -X 18,a reference.sdf mols.sdf > results.sdf

Do the alignment with a pharmacophore constraint: the aligned molecule must have an acceptor
atom near atom 18 of the reference or it will get a score penalty.

> pyflare align.py reference.sdf mols.sdf -p protein.pdb -P my_project.flr > results.sdf

Do the alignment using 'protein.pdb' as a protein excluded volume. In additional to writing
the results to `results.sdf` the results are also written to `my_project.flr`.

> pyflare align.py --mcs very-permissive -S -P myproject.fqj reference.sdf mols.sdf moremols.sdf

Align the molecules in `mols.sdf` and `moremols.sdf` to the ones in 'reference.sdf'.
The alignment will be done by aligning common substructures if possible, using a very loose
matching rule (atoms match if they have the same hybridisation, ignoring element and
ring/chain labels). The aligned molecules will not be allowed to move to increase the
alignment score. The results will be written to stdout in SDF format and also to the
Flare project `myproject.fqj`.

> pyflare align.py --help

Prints the help text including all available options and exits.


### Conformation Hunt (confhunt.py)
Performs a conf hunt of each molecule using the XED force field and writes the results to stdout.

The conformational hunt process is designed to generate a highly diverse set of conformations,
all of which are minimized, in as short a space of time as possible.

algorithm:

The conf hunt first loads and converts the input molecule into XED atom types and applies the XED
charge model. The molecule is then popped into 3D (if the input coords were 2D) using a relatively
crude but fairly robust algorithm which includes a ring library to ensure reasonable starting
geometries for rings. This starting structure is then minimized. If required (or if the molecule
contains flexible rings which were not present in the ring library), a short high-temperature
dynamics run is performed to explore ring conformations. A Monte Carlo bond twist routine is
then used to generate conformers. The twist bond routine by default generates up to 2000
conformers, which are then serially minimized, filtered and stored until all have been processed
or a preset limiting number of conformations has been found. The final set of conformers is
then written to standard output.

*Examples:*
> pyflare confhunt.py molecule.sdf > molecule_confs.sdf

Perform a conformation search on the molecule in `molecule.sdf` and write the results to
`molecule_confs.sdf`.

> pyflare conf_hunt.py --help

Prints the help text including all available options and exits.


### Docking (docking.py)
Docks ligands to a protein and writes the results to stdout.

The ligands are docked to the protein in the region defined by the --grid option.
This option takes the 2 opposite corners of the region in the format
min_x,min_y,min_z,max_x,max_y,max_z. For example --grid=-3,-6.2,2,12,8.6,5.

A quick method to find the appropriate grid values to use is to open the protein structure within
Flare and pick the atoms which define the grid. Then in the Python Console run these commands which
will print the grid values:

```python
grid = flare.atom_pick.corners(flare.main_window().picked_atoms)
print(f'{grid[0][0]},{grid[0][1]},{grid[0][2]},{grid[1][0]},{grid[1][1]},{grid[1][2]}')
```

The region may also be defined by the size and location of the first molecule in a file by using
the --reference file.sdf option.

To save time spent in recalculating the grid when running the script multiple times using the same
protein docking region, the generated grid can be written to a file using the option
--write-grid grid_file and then used in subsequent runs by specifying the
--grid-from-file grid_file option.

The protein should be prepared before docking ligands. Use the proteinprep.py script
to do this. For example:

> pyflare proteinprep.py protein.pdb > prep_protein.pdb

> pyflare docking.py --protein prep_protein.pdb --grid=-3,-6.2,2,12,8.6,5 ligands.sdf


*Examples:*
> pyflare docking.py --protein protein.pdb --grid=-3,-6.2,2,12,8.6,5 ligands.sdf

Docks the ligands to the protein in the box defined by the grid.
The results are written to stdout.

> pyflare docking.py --protein protein.pdb --reference ref.sdf --project project.flr ligands.sdf

Docks the ligands to the protein in the box defined by the first molecule in ref.sdf.
The results are written to stdout and project.flr.

> pyflare docking.py --protein protein1.pdb --protein protein2.pdb --covalent-residue "A LYS 745" --covalent-residue "A LYS 553" --reference ref.sdf ligands.sdf

Docks the ligands to an ensemble of proteins in the box defined by the first molecule in ref.sdf.
Also set the covalent residues assigned to the order of the given proteins.
The results are written to stdout.

> pyflare docking.py --help

Prints the help text including all available options and exits.


### Dynamics (dynamics.py)
Runs dynamics on a molecule from a PDB file and writes output files.

Dynamics is run on the structure in the specified PDB file and output files from the dynamics run
are written to the current working directory by default. A directory for the output files to be
written to can be specified with the --directory option. The structure used in the dynamics run can
also be saved to a Flare project if the --project option is specified and the output DCD file can
then be opened in the Flare project by clicking 'Open Trajectory' in the Dynamics dock. Dynamics is
performed on GPU by default. To run on CPU instead, --openmm-use-cpu can be specified.

The protein should be prepared before running dynamics. Use the proteinprep.py script to do this.
For example:

> pyflare proteinprep.py protein.pdb > prep_protein.pdb

> pyflare dynamics.py prep_protein.pdb

*Examples:*
> pyflare dynamics.py protein_and_ligand.pdb --directory output --project proj.flr --openmm-use-cpu

Runs dynamics on the structure in the protein_and_ligand.pdb file using CPU resources, writes
output files to the directory called output and saves the structure in a Flare project called
proj.flr.

> pyflare dynamics.py --help

Prints the help text including all available options and exits.


### Electrostatic Complementarity (TM) (electrostaticcomplementarity.py)
Calculates the Electrostatic Complementarity of ligands to a protein.

The results are written to stdout.

The protein should be prepared before running this calculation. Use the proteinprep.py script
to do this. For example:

> pyflare proteinprep.py protein.pdb > prep_protein.pdb

> pyflare electrostaticcomplementarity.py --protein prep_protein.pdb ligands.sdf


*Examples:*
> pyflare electrostaticcomplementarity.py --protein protein.pdb ligands.sdf

Calculates the electrostatic complementarity of the ligands to the protein.
The results are written to stdout in the SDF tags "EC", "EC r" and "EC rho".

> pyflare electrostaticcomplementarity.py --help

Prints the help text including all available options and exits.


### Setup a FEP calculation (fepcreate.py)
Creates a FEP Project and generates the graph network for a set of ligands and a protein.

The FEP Project will be stored in a Flare project and will be written to the file specified by the
--project option.

If --project points to an existing Flare project then the ligands will be added to the project
instead of creating a new project. If the Flare project contains multiple FEP projects, then the
FEP project name must be specified by the --name option.

Likewise, the protein of an existing FEP project can be changed by setting --project to an
existing Flare project and setting the --protein option.

Once the FEP Project has been generated the FEP calculation can be run using the feprun.py script.
It is recommended to review the graph network in Flare before running the calculation.

*Examples:*
> pyflare fepcreate.py --project project.flr --protein protein.pdb ligands.sdf

Creates a FEP project containing the ligands and protein. The graph network is automatically
generated.

> pyflare fepcreate.py --help

Prints the help text including all available options and exits.


### Run a FEP calculation (feprun.py)
Runs a FEP calculation.

The FEP calculation will be run on the FEP project within the Flare project specified by --project.
The FEP project can be created by the fepcreate.py script or in Flare. The Flare project file will
be periodically updated as the calculation runs.

The FEP calculation requires GPU resources to run. It is recommended to use a Cresset Engine Broker
by setting the --broker option. However, on Linux local GPU resources may be used.
To use multiple GPU devices, the --cuda-device-index or --opencl-device-index options should be
specified multiple times, e.g. specifying '--cuda-device-index 0 --cuda-device-index 1'
will allow the calculation to use both GPUs 0 and 1.

The calculation can be interrupted at any time by pressing Ctrl+C. The current progress will be
saved in the specified Flare project file. To resume the calculation, run the script with the
--continue option. If you want to restart the calculation from the beginning and discard any
previous results, use the `--delete` option.

Once the FEP calculation is complete the results can be viewed by the fepview.py script.

*Examples:*
> pyflare feprun.py --project project.flr --broker example:9000

Runs the FEP calculation on the FEP project within project.flr.

> pyflare feprun.py --help

Prints the help text including all available options and exits.


### View FEP results (fepview.py)
Prints the results of a FEP calculation.

The results of the FEP calculation will be printed to standard output. The default is to print the
results for each node in a tab-separated table including the predicted delta g.
The data for the links can be viewed by using the --links, --cycles or --overlap options. The log
can be printed via the --log option.

If the results have only been partially completed, then the results so far will be printed. To see
if the calculation is complete use the --status option.

*Examples:*
> pyflare fepview.py --project project.flr

Prints the results of the calculation.

> pyflare fepview.py --project project.flr --name fep --links

Prints the results of the links in the FEP project called "fep".

> pyflare fepview.py --help

Prints the help text including all available options and exits.


### FieldDescriptors (fielddescriptors.py)
Generates field descriptors for aligned ligands which are used to build machine learning models.

input:
The input ligands should be in SDF format

Optionally a csv file can be read which contains the field descriptor types and coordinates by
using the -d/--descriptors argument. If this is not given then this data will be generated.

output:
The input ligands in SDF format with tags for the field descriptors.

If -w/--write-descriptors is given the field descriptor types and coordinates will be written to
a csv file. If this file is passed to -d/--descriptors in subsequent runs the types and
coordinates of the descriptors will be reused.

*Examples:*
> pyflare fielddescriptors.py -d descriptors.csv training_set.sdf

Reads the ligands in 'training_set.sdf' and calculates the descriptor types and coordinates.
The descriptor values will be added to the training_set as SDF tags and printed to stdout.
The descriptor types and coordinates will be written to descriptors.csv.

> pyflare fielddescriptors.py -w descriptors.csv test_set.sdf

Reads the ligands in 'test_set.sdf' and the descriptor types and coordinates are read from
the descriptors.csv file.
The descriptor values will be added to the test_set as SDF tags and printed to stdout.

> pyflare fielddescriptors.py --help

Prints the help text including all available options and exits.


### GIST (gist.py)
Runs GIST on a protein and optionally a ligand.

GIST is run on the structure in the specified PDB file and output surface files
from the GIST run are written to the current working directory by default. A
directory for the output files to be written to can be specified with the
--directory option. The region of the protein to run GIST on is defined by
--grid or --reference. The structure used in the GIST run can also be saved to a
Flare project if the --project option is specified. GIST is performed on GPU by
default.

To run on CPU instead, --openmm-use-cpu can be specified. The final part of the
GIST calculation is run locally so that it can access the trajectory file.
Therefore local processing cannot be disabled with --localengines 0.

The protein should be prepared before running GIST. Use the proteinprep.py
script to do this.

For example:

> pyflare proteinprep.py protein.pdb > prep_protein.pdb

> pyflare gist.py prep_protein.pdb --reference lig.sdf --ligand lig.sdf


First, prepare the protein. Then run GIST on the structure in the
protein.pdb file plus lig.sdf using GPU resources.  The grid is defined by
the size and location of the lig.sdf structure.  Writes output files to the
current working directory.

> pyflare gist.py protein_and_ligand.pdb --directory output --project proj.flr

--grid=-8.5,19.5,-35.4,0.8,32.6,-25.4 --openmm-use-cpu

Runs GIST on the structure in the protein_and_ligand.pdb file using CPU
resources.  The grid is defined by the --grid option.  Writes output files
to the directory called output and saves the structure in a Flare project
called proj.flr.

> pyflare gist.py --help

Prints the help text including all available options and exits.

see also:
https://amberhub.chpc.utah.edu/gist/


### Ligand Preparation (ligprep.py)
Ligand preparation: adds hydrogens, recharge to pH 7, pop to 3D, strip salts, file format
conversion

*Examples:*
> pyflare ligprep.py -3 -c molecule.sdf > results.sdf

Recharge the molecule in `molecule.sdf`, pop to 3D and write the result to
`results.sdf`.

> pyflare ligprep.py --help

Prints the help text including all available options and exits.


### Minimization (minimization.py)
Minimizes a protein and writes the results to stdout.

The protein should be prepared before minimizing it. Use the proteinprep.py script
to do this. For example:

> pyflare proteinprep.py protein.pdb | pyflare minimization.py


By default, the AMBER force field is used to perform the minimization. To change this, use the
--small-molecule-force-field option. Minimization for the AMBER and Open force fields are performed
on GPU by default. To run on CPU instead, --openmm-use-cpu can be specified.

If the AMBER or Open force fields are used for the small molecules then AMBER's FF14SB force field
will be used for the protein. If the XED force field is used for the small molecules then
the XED force field will also be used for the protein.

The purpose of this script is to minimize a protein or the active site of a protein.
If you wish to minimize a set of ligands within a fixed protein active site see the
minimizeligands.py script.

*Examples:*
> pyflare minimization.py --openmm-use-cpu protein.pdb

Uses the AMBER force field on the CPU to minimize the protein and write the results
to stdout.

> pyflare minimization.py --small-molecule-force-field xed --ligand ligand.sdf --radius 6 protein.pdb

Uses the XED force field to minimize the ligand within the protein and write the
results to stdout. Only the ligand atoms and protein atoms within 6A of the
ligand are allowed to move.

> pyflare minimization.py --residue LIG --radius 6 protein.pdb

Uses the AMBER force field on the GPU to minimize the residue 'LIG' within the
protein and write the results to stdout. Only the residue 'LIG' atoms and atoms
within 6A of the residue are allowed to move.

> pyflare minimization.py --help

Prints the help text including all available options and exits.


### Minimize Ligands (minimizeligands.py)
Minimizes ligands and writes the results to stdout.

If a protein is given then its structure will be included in the minimization of each ligand but
the proteins atoms will not move. The protein should be prepared, use the proteinprep.py script to
do this.

By default, the AMBER force field is used to perform the minimization. To change this, use the
--small-molecule-force-field option. Minimization for the AMBER and Open force fields are performed
on GPU by default. To run on CPU instead, --openmm-use-cpu can be specified.

If the AMBER or Open force fields are used for the small molecules then AMBER's FF14SB force field
will be used for the protein. If the XED force field is used for the small molecules then
the XED force field will also be used for the protein.

The purpose of this script is to minimize a set of ligands within a fixed protein active site.
If you wish to minimize a protein or just the active site of a protein see the minimization.py
script.

*Examples:*
> pyflare minimizeligands.py --openmm-use-cpu ligands.sdf

Uses the AMBER force field on the CPU to minimize the ligands and write the results to stdout.

> pyflare minimizeligands.py --small-molecule-force-field xed --protein protein.pdb ligands.sdf

Uses the XED force field to minimize the ligands within the protein and write the results to stdout.

> pyflare minimizeligands.py --help

Prints the help text including all available options and exits.


### MM/GBSA (mmgbsa.py)
Runs MM/GBSA on a set of ligands and writes the results to stdout.

A protein must be provided in order to create the protein-ligand complex that will be used as
input for the calculation. The protein should be prepared, use the proteinprep.py script to
do this. It is assumed that the input ligands are positioned inside the protein active site.
If they are not, run the docking.py script first.

By default, the Amber force field is used to perform the calculation. To change this, use the
--small-molecule-force-field option. Minimization for the AMBER and Open force fields are performed
on GPU by default. To run on CPU instead, --openmm-use-cpu can be specified.

The output from the calculation is the same set of the ligands that were provided as input, but
with an additional tag containing the MM/GBSA energy.

*Examples:*
> pyflare mmgbsa.py --openmm-use-cpu --protein protein.pdb ligands.sdf

Uses the Amber force field on the CPU to calculate energies for the ligands and write the
results to stdout.

> pyflare mmgbsa.py --help

Prints the help text including all available options and exits.


### MM/GBSA dynamics (mmgbsadynamics.py)
Runs MM/GBSA on a dynamics trajectory and writes the results to stdout.

A protein must be provided that has a ligand residue and an associated dynamics trajectory.
A topology file is also required and must be in the same location as the protein. By default,
the first ligand residue in the protein will be scored. If there are multiple and a different
residue needs to be scored, its name can be provided.

The output from the calculation is the specified ligand residue, converted to a molecule and
with an additional tag containing the MM/GBSA energy.

*Examples:*
> pyflare mmgbsadynamics.py --protein protein.pdb --residue-name ABC

Calculates the binding free energy of the ligand residue with the name "ABC", within the
given protein, then converts the residue to a molecule and writes it to stdout.

> pyflare mmgbsadynamics.py --help

Prints the help text including all available options and exits.


### Pop to 3D (popto3d.py)
Pops molecules to 3D, add fields and performs a quick minimization.

*Examples:*
> pyflare popto3d.py molecule.sdf > results.sdf

Pops the molecule in `molecule.sdf` to 3D and writes the results to
`results.sdf`.

> pyflare popto3d.py --help

Prints the help text including all available options and exits.


### Protein Preparation (proteinprep.py)
Prepares a protein and writes the results to stdout.

This will add hydrogen atoms, optimize hydrogen bonds, remove atomic clashes and
assign optimal protonation states.

*Examples:*
> pyflare proteinprep.py protein.pdb

Prepares the protein and prints the results to stdout.

> pyflare proteinprep.py --help

Prints the help text including all available options and exits.


### QM (qm.py)
Runs QM calculations on ligands and writes the results to stdout.

QM calculations uses Quantum Mechanics to minimize the given ligands.
There are two types of QM calculations: Ligand minimization and
electrostatic potential (ESP) calculation. ESP calculation computes
and outputs electrostatic surfaces. Only SDF file format is used for
ligand input and output.

By default it runs only on minimize mode. To switch to single-point
energy calculation, set --minimize-max-iterations to 0. To perform
both calculations, just run the script in minimize mode at first then
using that output as the input for running the script the second time
but on single-point energy mode.

The default DFT functional and basis set is different for ESP.
Instead of PBE and 3-21G, ESP would have B3LYP and 6-31G(d).

QM calculations on Conformations and Poses can be run using the ligand
minimization calculation, once the Conformation and Poses have been
exported as ligands into an SDF file format. For a given ligand in
Flare, Conformations and Poses can be exported from the Ligands table
using the 'Export Conformations' and 'Export Poses' right-click
functionalities, respectively.

*Examples:*
> pyflare qm.py --method HF ligands.sdf

Runs QM minimization calculation in minimize mode with ligands from
the ligands.sdf and minimization method of "HF", then write the
results to stdout.

> pyflare qm.py --minimize-max-iterations 0 ligands.sdf

Runs QM minimization calculation in single-point energy only with
ligands from the ligands.sdf, then write the results to stdout.

> pyflare qm.py --grid-output ccp4 --esp ligands.sdf

Runs QM ESP calculation with ligands from the ligands.sdf file then
writes the ligand results to stdout and grid file (of ESP surfaces)
to a generated file in the specified "ccp4" grid output format.

> pyflare qm.py --minimize-max-iterations 0 --grid-output grd ligands.sdf

Runs QM minimization calculation in single-point energy only with
ligands from the ligands.sdf, then write the results to stdout and
grid file (of electron density, HOMO, and LUMO surfaces) to a
generated file in the specified "grd" grid output format.

> pyflare qm.py --help

Prints the help text including all available options and exits.


### Build QSAR Model (qsarbuild.py)
Builds 3D Field QSAR, Activity Atlas and kNN models from aligned ligands.

The following types of QSAR models can be generated:

Field QSAR
Uses the electrostatic and shape properties of aligned ligands to develop quantitative structure activity relationship models. Ligands in the "Training Set" are used to derive a set of sampling points around the dataset that can be used to probe any molecule for the electrostatic potential or for the volume taken up by ligands.
The sample values are combined using partial least squares (PLS) to derive an equation that describes activity.
This 3D QSAR model can help to explain SAR data and with the best models used to predict an activity value for newly designed ligands.

Activity Atlas
An Activity Atlas model summarizes all the SAR for a series into a 3D model that can be used to inform new molecule design.
Based on Activity Miner, Activity Atlas examines all of the activity cliffs in the data set and asks a series of questions about each. Are the ligands aligned correctly? Are there plausible alternative alignments? Are there flexible groups whose alignment is unconstrained?
The answers to these questions are combined in a Bayesian analysis which combines the probability that each alignment is correct with the size of the activity cliff.
The result is a global picture of activity, highlighting those regions around the series where the SAR is clear. This model can be view in 3D using Forge or Torch.

A similar analysis is performed at the same time to determine which regions of space around the series have been explored.
Unlike traditional SAR table methods, the nature of the substitutions is implicitly accounted for as we perform the analysis in electrostatic and steric field space.
Where have we placed positive electrostatic potential? Where have we placed hydrophobicity?

This latter analysis then allows qsarscore.py to compute a 'Novelty' score for new ligands - if we made a new molecule, what would it tell us?
The ligands with the highest 'Novelty' are those with small, controlled changes.
Ideas with a low 'Novelty' don't expand our understanding of the SAR, while those with too high a value are potentially taking too bold a leap into the unknown.
Designing compounds into the middle ground allows the SAR to be efficiently explored, giving the maximum understanding with the least synthetic effort.

k Nearest Neighbor (kNN)
Generates a k Nearest Neighbor (kNN) model using similarity and activity data of aligned ligands.
The similarity between the ligands is calculated using Cresset's field similarity method or by using the 2D methods ECFP4, ECFP6, FCFP4, or FCFP6.
This model can be used with qsarscore.py to predict an activity value for newly designed ligands.

A regression or classification kNN model can be generated.

To build a machine learning model use the fielddescriptors.py script to generate the descriptors.
Then use an external machine learning program, for example scikit-learn, to generate the model.

input:
The input ligands should be read in SDF format. Other molecular file formats (such as mol2 and XED) are not useful, since QSAR Model building requires activity data and those file formats have no provision for metadata.

The ligands need to be pre-aligned for all the QSAR models except for the kNN model when using 2D similarities. align.py is a recommended tool for this.

output:
By default qsarbuild.py writes its results to a set of files in the current working directory:

output.sdf
The input ligands, with tags added for predicted activity and role (training/test set).

output.log
The log of the calculation, giving details on the model created.

output.model.csv
The model sample data in CSV format, in case you wish to use alternative statistical techniques to build a model. This file is only created for Field QSAR models.

output.flr
A Flare project file containing the ligands and generated model. This can be opened in Flare to view the model.

output.model.flr
A Flare project file containing the generated model only. This can be used with qsarbuild.py to score ligands against the model.

The prefix for these output files can be changed with the -o flag.

Note that if a project file was specified, then the base name of that project file is used as the output path.

*Examples:*
> pyflare qsarbuild.py -a My_Activity mols.sdf

Reads the ligands in 'mols.sdf'. Activity values are obtained from the 'My_Activity' tag, and are assumed to be in log units.
A Field QSAR model will be built and the results put in 'output.sdf', 'output.log', 'output.csv', 'output.flr' and 'output.model.flr'.

> pyflare qsarbuild.py -t test.sdf -r 20 -f e,h -o expt1 mols.sdf

As above, but a test set will be used consisting of all of the ligands in 'test.sdf' together with a randomly-chosen 20% of the ligands from 'mols.sdf'.
The electrostatic and hydrophobic fields will be used to build the model. The outputs will go to 'expt1.sdf' etc.

> pyflare qsarbuild.py -a My_Activity -u uM -w 0.6-0.8 mols.sdf

Reads the ligands in 'mols.sdf'. Activity values are obtained from the 'My_Activity' tag, and are in uM units.
Each molecule's similarity value will be read from their 'Sim' tag, and these will be converted to weights so that a molecule with a Sim value less than 0.6 gets a weight of 0 (it is omitted),
a molecule with a Sim value greater than 0.8 gets a weight of 1, and the weight values are linearly interpolated between 0.6 and 0.8 so that a molecule with a Sim value of 0.7 gets a weight of 0.5.

> pyflare qsarbuild.py -q kNN --method ECFP4 mols.sdf

Builds a k-Nearest-Neighbor model from the ligands in mols.sdf, using ECFP4 fingerprints. Note that the ligands in mols.sdf do not need to be aligned (or even 3D).

> pyflare qsarbuild.py -l 0 -g cebroker:9000 -a My_Activity -u uM -w 0.6-0.8 mols.sdf

As above, but no processing will be done locally: the calculation will be performed by the Cresset Engine Broker running on the machine with the hostname 'cebroker' on port '9000'.

> pyflare qsarbuild.py --help

Prints the help text including all available options and exits.


### Fit ligands to a QSAR model (qsarscore.py)
Calculates predicted data for ligands against a model.

The type of data which can be predicted depends on the type of model used.

Field QSAR
Calculates the predicted activity of each ligand and the ligand's distance to the QSAR model.

Activity Atlas
Calculates a 'Novelty' score for new ligands. Ligands with a low 'Novelty' don't expand our understanding of the SAR, while those with too high a value are potentially taking too bold a leap into the unknown. Designing compounds into the middle ground allows the SAR to be efficiently explored, giving the maximum understanding with the least synthetic effort.

k Nearest Neighbor (kNN)
Calculates the predicted activity of each ligand, the error of the prediction and the ligand's distance to the model.

input:
The input ligands should be read in SDF format.

The ligands need to be pre-aligned for all the QSAR models except for the kNN model when using 2D similarities. align.py is a recommended tool for this.

output:
By default qsarscore.py writes its results to standard output in SDF format.

*Examples:*
> pyflare qsarscore.py model.flr mols.sdf > results.sdf

Reads the (aligned!) ligands in 'mols.sdf', and calculates the predicted activity values for them against the Flare model contained in 'model.flr'. The results are written to 'results.sdf' in SDF format

> pyflare qsarscore.py -i model.flr

Prints the calculation log for the Flare model contained in 'model.flr' - this can be useful to see what ligands were used to score the model, and to see model statistics.

> pyflare qsarscore.py -o csv model.flr mols.sdf

Reads the (aligned!) ligands in 'mols.sdf', and calculates the predicted activity values for them against the Flare model contained in 'model.flr'. The results are written to standard output in CSV format.

> pyflare qsarscore.py -n 3 -x molsamps.csv model.flr mymols.sdf

Reads the (aligned!) ligands in 'mymols.sdf' and calculated the predicted activity values for them against the Flare model contained in 'model.flr', using 3 PLS components of the model. The sample values for each molecule that were used to calculate the predicted activity values are output in 'molsamps.csv' - if you derived an external model from data exported by fbuild you can use these sample values to fit these ligands to the external model.

> pyflare qsarscore.py --help

Prints the help text including all available options and exits.

flare workflow examples:
Suppose that you have a reference ligand in 'reference.sdf', its protein in 'protein.pdb', a bunch of training set ligands in 'training.sdf'. We want to align the ligands using the MCS method, score a Flare model and then score the ligands in 'othermols.sdf' against that model. We'll use an 80/20 training/test set split, done by activity.

First, align the training set:

> pyflare align.py --mcs normal -p protein.pdb reference.sdf training.sdf > aligned_training.sdf


Next, score the model - the model will be output to the project file 'output.model.flr'.

> pyflare qsarbuild.py -p 20 aligned_training.sdf


Finally, align and score 'othermols.sdf' against the model:

> pyflare align.py --mcs normal -p protein.pdb reference.sdf othermols.sdf | pyflare qsarscore.py output.model.flr > results.sdf



### 3D-RISM (rism.py)
Runs 3D-RISM on a protein and writes the results to stdout.

The PDB temperature factor field for the waters will be set to the 3D-RISM DeltaG value.

The protein should be prepared before running 3D-RISM. Use the proteinprep.py script
to do this. For example:

> pyflare proteinprep.py protein.pdb | pyflare rism.py --ligand ligand.sdf


*Examples:*
> pyflare rism.py --ligand ligand.sdf protein.pdb

Run 3D-RISM on the protein with the ligand.

> pyflare rism.py --help

Prints the help text including all available options and exits.


### Bioisoteric replacement (sparkcl.py)
A script to generate bioisosteric replacements using molecular fields

sparkcl reads in a molecule, removes a user-specified section, and then searches
for bioisosteric replacements for that section in a database of fragments.
The replacement fragments are merged into the original molecule and the results
are scored. Result molecules are scored using field and shape similarity to the
original molecule or through docking to a protein.

If more than one molecule is provided, the second and subsequent molecules are
used in scoring the results. You can change the relative weights of these
molecules using the -W option.

As an alternative to loading molecules directly, sparkcl can load a Spark project
file and run it. All other relevant settings from that project file (e.g. field
constraints and protein) will be used as well.

Output:
By default sparkcl writes its result molecules to standard output in SDF format.
Different file formats can be specified with the -o flag. In addition, a Spark
project file can be written containing the results by specifying the -P flag.

Specifying a section to replace:
Unless an existing project is supplied, sparkcl requires you to specify one
or more bonds to be broken in the starter molecule. Bonds are specified as
pairs of atoms by index (starting at 1), with the first atom in the pair being
retained, and the second atom being part of the removed section.

Replacement of a central section of a molecule can be accomplished by
specifying all of the bonds leading to that central section.

For example, given C-C-C-C-O-C, numbered 1-6 left to right, replacement of
the terminal methoxy group could be requested with '-i 4,5' (i.e. keep atom
4, and discard atom 5 and everything connected to it). Replacement of the two
central carbons could be requested with '-i 2,3 -i 5,4' (i.e. keep atoms 2
and 5, and delete atoms 3 and 4 and anything in between).

Note that:
a) You may not break a double or triple bond
b) Breaking rings is allowed, but since all of the fragments in the provided
databases are initially acyclic you can end up with rather unlikely ring
systems in the output.

You can add flags restricting the type of atom that can be connected to any
broken bond with extra arguments to the -i option. For example, in the methoxy
replacement example above, to specify that a replacement fragment must connect
through a sp3 nitrogen or oxygen, do '-i 4,5,Nsp3,O'.

See the "attachment flags" section to get a list of available attachment flags.

Alternative method for specifying a section to replace:
As an alternative to the method outlined in the previous section, you may
directly specify the list of atoms to replace with the -S flag. The list of
atoms should form a consistent connected fragment, and the bonds connecting
this fragment to the rest of the molecule must be single.

For example, given C-C-C-C-O-C, numbered 1-6 left to right, replacement of
the terminal methoxy group could be requested with '-S 5,6' (i.e. discard
atoms 5 and 6). Replacement of the two central carbons could be requested
with '-S 3,4' (i.e. delete atoms 3 and 4).

If the -S option is used there is no way to restrict the attachment atom types.

Attachment flags:
Available attachment point flags are:

Br, C, Car, Cl, Csp, Csp2, Csp3, F, Hal, I, N, Nsp2, Nsp3, O, Osp3, P, PS, S

A comma-separated list of flags can be given, which will match any of the
types. Note that the plain element types match any hybridisation e.g. 'N'
matches any nitrogen, while 'Nsp2' matches only sp2 nitrogens. The nitrogen
types include positively charged nitrogen, so 'Nsp3' includes 'Nsp3+'.

In addition to the element and hybridisation types, you can specify 'ring'
to constrain matches to ring atoms, 'ar' to constrain matches to aromatic
atoms and 'ac' to constrain matches to acyclic atoms.

Example: C,Nsp3,ring means that the fragment must connect through a carbon
or sp3 nitrogen which is in a ring.

Examples:
> pyflare sparkcl.py -i 4,5 -d mydb molecule.sdf


Reads 'molecule.sdf', breaks the bond between atoms 4 and 5 (discarding atom 5
and everything attached to it), and searches for isosteres for the removed
part in the 'mydb' database.

> pyflare sparkcl.py -i 4,5,C,Nsp3,ring -F=-Chiral -d mydb molecule.sdf


As above, but the replacement fragment must NOT be chiral, and must connect
through a carbon or an sp3 nitrogen which is in a ring.

> pyflare sparkcl.py -S 2,3,15,19 -d mydb molecule.sdf


Reads 'molecule.sdf', and searches for isosteres for the fragment consisting
of atoms 2, 3, 15, and 19. These 4 atoms must be connected.

> pyflare sparkcl.py -i 7,9 -i 14,13 -d mydb -P out molecule.mol2


Reads 'molecule.mol2' and breaks the bonds between atoms 7 and 9 and atoms 14
and 13. The section between atoms 9 and 13 is removed. Bioisosteres to the
removed section are searched for in database 'mydb'. The results will be written
to stdout in SDF format and will also be saved to the file 'out.flr'.

> pyflare sparkcl.py -k -i 4,5 -p protein.pdb -b 5.0 -d mydb -a A,GLN,85A,O,hbond,w molecule.sdf


Reads 'molecule.sdf', breaks the bond between atoms 4 and 5 (discarding atom 5
and everything attached to it), and searches for isosteres for the removed part
in the 'mydb' database. The results are scored by docking them into 'protein.pdb'
using an active site which is defined by the size and location of the molecule in
'molecule.sdf' with a 5.0A buffer. A weak hydrogen bonding constraint is added
to atom "O" in GLN 85A on chain A.

> pyflare sparkcl.py -d Test -P output.flr input.flr


A Spark search is run on the 'Test' database using the query molecule set in the
'input.flr' project. The new results are appended to any already in the project
and the result written to 'output.flr'.

Environment:
CRESSET_FF_VER
Set to '2' to force the use of XED FF version 2 in the calculations. The default
is to use XED FF version 3.

CRESSET_DDD
Set to 'true' to force the use of distance-dependent dielectric (DDD) in field
calculations. Set to 'false' to disable the use of distance-dependent dielectric
in field calculations. The default is set to 'true'.

FLARE_SPARK_DB
If a relative path is specified with the -d option, the path will be interpreted relative to the
directory pointed to by this variable.

CRESSET_BROKER
Set the location of the Cresset Engine Broker in the format hostname:port.
This can be overridden using the -g option.

Notes:
sparkcl.py uses Cresset's FieldEngine to perform the calculations. By default, sparkcl.py
will launch a number of FieldEngines equal to the number of processors on the local
machine. Use the -l and -g options to alter this behavior.


### Spark Database Generation (sparkdb.py)
A script to generate fragment databases for Spark

sparkdb reads one or more  molecule files (in XED, mol2, smi or SDF format),
breaks them into fragments, and stores the fragments into a Spark database file.

SDF files (only) can be read from stdin by giving a filename of '-'.

Example:
> pyflare sparkdb.py molecule.sdf

Reads "molecule.sdf", breaks each molecule into fragments and stores
the fragments into a database file called "user.fsd". If the database
does not exist it will be created.

> pyflare sparkdb.py --db myDatabase molecule.sdf

As above, but the database filename is "myDatabase.fsd" instead of
"user.fsd".

> pyflare sparkdb.py --db "C:\databases\myDatabase.fsd" molecule.sdf

As above, but the database at "C:\databases\myDatabase.fsd" will be
created or updated.

> pyflare sparkdb.py -c 0 -i U --db myfrags fragments.sdf

The molecules in "fragments.sdf" are assumed to be molecular fragments
with the attachment points labelled with uranium atoms. These fragments
are imported in their existing conformation (no conformation hunt is done)
and placed in the "myfrags.fsd" database.

> pyflare sparkdb.py --reagent alcohol --db alchols molecule.sdf

Creates a reagent database called "alcohols.fsd" in the standard location
using the "alcohol" script.

> pyflare sparkdb.py --merge-database source --db target

Merges the contents of the "source.fsd" database into the "target.fsd"
database.

Environment:
CRESSET_FF_VER
Set to '2' to force the use of XED FF version 2 in the calculations. The default
is to use XED FF version 3.

CRESSET_DDD
Set to 'true' to force the use of distance-dependent dielectric (DDD) in field
calculations. Set to 'false' to disable the use of distance-dependent dielectric
in field calculations. The default is set to 'true'.

FLARE_SPARK_DB
If a relative path is specified with the -d option, the path will be interpreted relative to the
directory pointed to by this variable.

CRESSET_BROKER
Set the location of the Cresset Engine Broker in the format hostname:port.
This can be overridden using the -g option.

Notes:
sparkdb.py uses Cresset's FieldEngine to perform the calculations. By default, sparkdb.py
will launch a number of FieldEngines equal to the number of processors on the local
machine. Use the -l and -g options to alter this behavior.


### Update Spark databases (sparkdbupdate.py)
A script to manage and update databases for Spark

*Examples:*
> pyflare sparkdbupdate.py VeryCommon

Install/update the "VeryCommon" database.

> pyflare sparkdbupdate.py -u

Upgrade all of the installed databases to the latest version.

environment:
FLARE_SPARK_DB
If a relative path is specified with the -d option, the path will be interpreted relative to the
directory pointed to by this variable.

notes:
Note that all Spark databases files can be freely moved and copied between installations: you can
safely download databases to one location and then move or copy them to a different location or
machine.


### Surfaces (surfaces.py)
Writes protein field surfaces to stdout.

*Examples:*
> pyflare surfaces.py protein.pdb --output grd --type pos > surface.grd

Writes the positive surface in grd format.

> pyflare surfaces.py protein.pdb --output ccp4 --type vdw > surface.ccp4

Writes the Van der Waals surface in ccp4 format.

> pyflare surfaces.py --help

Prints the help text including all available options and exits.


### Templater (templater.py)
Finds templates from two to ten active 2D or 3D ligands.

This script compares ligands using their electrostatic and hydrophobic fields in order to find
common patterns. When applied to several structurally distinct ligands with a common activity,
this script can determine the bioactive conformations and relative alignments of these ligands
without requiring any protein information. Other pharmacophore generation packages simply attempt
to generate a very crude idea of what the protein wants in terms of donor points, acceptor points
and the like: FieldTemplater attempts to provide a full picture of how the active ligands bind,
which features they use, what shape they are, and how different series can be compared.

Flare's molecular comparisons utilize Cresset's field and 'field point' technology.
Fields are a way of representing ligands in terms of their surface and electrostatic properties:
positive and negative electrostatic fields, van der Waals effects and hydrophobic effects on and
near the surface of a ligand. Field points are placed at regions where the field has a
local maximum. They summarize the most important binding regions of a ligand and can be used both
to visually compare fields of two ligands and to accurately determine the overall similarity
of the fields of two different ligands.

input:
Two to ten input ligands can be read in SDF or mol2 format. The input format should be autodetected.
The best results are obtained when three to six structurally diverse active ligands are provided.

As an alternative to loading input ligands directly, this script can load a Flare project file.
The conformations and alignments in the project will be used if present or generated if missing.
Any existing templates will be deleted. This behavior can be changed using the -d/--delete flag.
All other relevant settings from that project file (e.g. pairwise constraints) will be used as well.
If the Forge project contains more than one FieldTemplater project then the name of the
FieldTemplater project should be given on the command line after the path to the project file.
The input project is not modified by templater.py but the new project can be saved using the
-P/--project flag.

output:
The result ligands are written to standard output in SDF format.
Different file formats can be specified with the -o flag.
In addition, a Flare project file can be written containing the results by specifying the -P flag.

In the output stream, templates are written in size order, then score order.
So, when templating 5 molecules A-E, all 5-ligand templates will be written,
followed by all 4-ligand templates containing A,B,C,D, followed by all 4-molecule templates
containing A,B,C,E, and so forth. Tags are added to each molecule written containing details of
which template it is in (the 'Template', 'Member' and 'Molecule name' tags).

*Examples:*
> pyflare templater.py ligands.sdf > results.sdf

Reads 'ligands.sdf'. For each entry, generates conformers then aligns them to the conformers
of the other ligands in the 'ligands.sdf' file to generate the templates.
Writes the best templates to 'templates.sdf' in SDF format.

> pyflare templater.py -e -P project.flr molecules.sdf > templates.sdf

As above, but generates more conformers. This is best when using larger molecules with 6 or
more rotatable bonds. A Flare project is also saved containing the templates.

> pyflare templater.py -r c mol_1_conformers mol_2_conformers mol_3_conformers >

templates.sdf
Generates the templates using the conformations in the files instead of generating new
conformations.

> pyflare templater.py -d a -P Results.flr FT.flr myproject > templates.sdf

Opens the FieldTemplater project called 'myproject' in 'FT.flr'.
The templates and alignments are removed and the calculation is rerun.
The templates are written to 'templates.sdf' in SDF format and to the 'Results.fqj' Flare
project file. The 'FT.fqj' Flare project file is not changed.

> pyflare templater.py --help

Prints the help text including all available options and exits.


### WaterSwap (waterswap.py)
Runs WaterSwap on a protein and saves the results to a project file.

The protein should be prepared before running WaterSwap. Use the proteinprep.py script
to do this. For example:

> pyflare proteinprep.py protein.pdb | pyflare waterswap.py -P project.flr --residue LIG


At the end of each iteration the results are written to the project given by the
'-P/--project' option. Once the first iteration is completed this script can be stopped
via 'Ctrl+C' and restarted at a later date by using the '--restart' option.

*Examples:*
> pyflare waterswap.py -P project.flr --ligand ligand.sdf protein.pdb

Run WaterSwap on the protein with the ligand. The results are written to
project.flr.

> pyflare waterswap.py -P project.flr --residue LIG protein.pdb

Run WaterSwap on the protein. The protein residue 'LIG' is used as the ligand.
The results are written to project.flr.

> pyflare waterswap.py -P project.flr --top protein.top --crd protein.crd --top-ligand LIG

Run WaterSwap on the protein within the topology and coordinate file.
The protein residue 'LIG' is used as the ligand.
The results are written to project.flr.

> pyflare waterswap.py -P project.flr --restart restart_project.flr

Continue the WaterSwap calculation for the results stored in 'restart_project.flr'.
If the project contains more than one result then the last result will be used.
The results are written to project.flr.

> pyflare waterswap.py --help

Prints the help text including all available options and exits.

