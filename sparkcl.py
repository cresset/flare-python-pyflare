#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""A script to generate bioisosteric replacements using molecular fields

sparkcl reads in a molecule, removes a user-specified section, and then searches
for bioisosteric replacements for that section in a database of fragments.
The replacement fragments are merged into the original molecule and the results
are scored. Result molecules are scored using field and shape similarity to the
original molecule or through docking to a protein.

If more than one molecule is provided, the second and subsequent molecules are
used in scoring the results. You can change the relative weights of these
molecules using the -W option.

As an alternative to loading molecules directly, sparkcl can load a Spark project
file and run it. All other relevant settings from that project file (e.g. field
constraints and protein) will be used as well.

Output:
    By default sparkcl writes its result molecules to standard output in SDF format.
    Different file formats can be specified with the -o flag. In addition, a Spark
    project file can be written containing the results by specifying the -P flag.

Specifying a section to replace:
    Unless an existing project is supplied, sparkcl requires you to specify one
    or more bonds to be broken in the starter molecule. Bonds are specified as
    pairs of atoms by index (starting at 1), with the first atom in the pair being
    retained, and the second atom being part of the removed section.

    Replacement of a central section of a molecule can be accomplished by
    specifying all of the bonds leading to that central section.

    For example, given C-C-C-C-O-C, numbered 1-6 left to right, replacement of
    the terminal methoxy group could be requested with '-i 4,5' (i.e. keep atom
    4, and discard atom 5 and everything connected to it). Replacement of the two
    central carbons could be requested with '-i 2,3 -i 5,4' (i.e. keep atoms 2
    and 5, and delete atoms 3 and 4 and anything in between).

    Note that:
    a) You may not break a double or triple bond
    b) Breaking rings is allowed, but since all of the fragments in the provided
       databases are initially acyclic you can end up with rather unlikely ring
       systems in the output.

    You can add flags restricting the type of atom that can be connected to any
    broken bond with extra arguments to the -i option. For example, in the methoxy
    replacement example above, to specify that a replacement fragment must connect
    through a sp3 nitrogen or oxygen, do '-i 4,5,Nsp3,O'.

    See the "attachment flags" section to get a list of available attachment flags.

Alternative method for specifying a section to replace:
    As an alternative to the method outlined in the previous section, you may
    directly specify the list of atoms to replace with the -S flag. The list of
    atoms should form a consistent connected fragment, and the bonds connecting
    this fragment to the rest of the molecule must be single.

    For example, given C-C-C-C-O-C, numbered 1-6 left to right, replacement of
    the terminal methoxy group could be requested with '-S 5,6' (i.e. discard
    atoms 5 and 6). Replacement of the two central carbons could be requested
    with '-S 3,4' (i.e. delete atoms 3 and 4).

    If the -S option is used there is no way to restrict the attachment atom types.

Attachment flags:
    Available attachment point flags are:

    Br, C, Car, Cl, Csp, Csp2, Csp3, F, Hal, I, N, Nsp2, Nsp3, O, Osp3, P, PS, S

    A comma-separated list of flags can be given, which will match any of the
    types. Note that the plain element types match any hybridisation e.g. 'N'
    matches any nitrogen, while 'Nsp2' matches only sp2 nitrogens. The nitrogen
    types include positively charged nitrogen, so 'Nsp3' includes 'Nsp3+'.

    In addition to the element and hybridisation types, you can specify 'ring'
    to constrain matches to ring atoms, 'ar' to constrain matches to aromatic
    atoms and 'ac' to constrain matches to acyclic atoms.

    Example: C,Nsp3,ring means that the fragment must connect through a carbon
    or sp3 nitrogen which is in a ring.

Examples:
    pyflare sparkcl.py -i 4,5 -d mydb molecule.sdf

    Reads 'molecule.sdf', breaks the bond between atoms 4 and 5 (discarding atom 5
    and everything attached to it), and searches for isosteres for the removed
    part in the 'mydb' database.

    pyflare sparkcl.py -i 4,5,C,Nsp3,ring -F=-Chiral -d mydb molecule.sdf

    As above, but the replacement fragment must NOT be chiral, and must connect
    through a carbon or an sp3 nitrogen which is in a ring.

    pyflare sparkcl.py -S 2,3,15,19 -d mydb molecule.sdf

    Reads 'molecule.sdf', and searches for isosteres for the fragment consisting
    of atoms 2, 3, 15, and 19. These 4 atoms must be connected.

    pyflare sparkcl.py -i 7,9 -i 14,13 -d mydb -P out molecule.mol2

    Reads 'molecule.mol2' and breaks the bonds between atoms 7 and 9 and atoms 14
    and 13. The section between atoms 9 and 13 is removed. Bioisosteres to the
    removed section are searched for in database 'mydb'. The results will be written
    to stdout in SDF format and will also be saved to the file 'out.flr'.

    pyflare sparkcl.py -k -i 4,5 -p protein.pdb -b 5.0 -d mydb -a A,GLN,85A,O,hbond,w molecule.sdf

    Reads 'molecule.sdf', breaks the bond between atoms 4 and 5 (discarding atom 5
    and everything attached to it), and searches for isosteres for the removed part
    in the 'mydb' database. The results are scored by docking them into 'protein.pdb'
    using an active site which is defined by the size and location of the molecule in
    'molecule.sdf' with a 5.0A buffer. A weak hydrogen bonding constraint is added
    to atom "O" in GLN 85A on chain A.

    pyflare sparkcl.py -d Test -P output.flr input.flr

    A Spark search is run on the 'Test' database using the query molecule set in the
    'input.flr' project. The new results are appended to any already in the project
    and the result written to 'output.flr'.

Environment:
  CRESSET_FF_VER
    Set to '2' to force the use of XED FF version 2 in the calculations. The default
    is to use XED FF version 3.

  CRESSET_DDD
    Set to 'true' to force the use of distance-dependent dielectric (DDD) in field
    calculations. Set to 'false' to disable the use of distance-dependent dielectric
    in field calculations. The default is set to 'true'.

  FLARE_SPARK_DB
    If a relative path is specified with the -d option, the path will be interpreted relative to the
    directory pointed to by this variable.

  CRESSET_BROKER
    Set the location of the Cresset Engine Broker in the format hostname:port.
    This can be overridden using the -g option.

Notes:
  sparkcl.py uses Cresset's FieldEngine to perform the calculations. By default, sparkcl.py
  will launch a number of FieldEngines equal to the number of processors on the local
  machine. Use the -l and -g options to alter this behavior.
"""

import re
import os
import sys
import argparse
import pathlib
from cresset import flare
from enum import Enum
from typing import Union, Optional

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))
from _internal import cmdutil  # noqa: E402


class ResultsFate(Enum):
    REPLACE = 0
    APPEND = 1


RESULTS_FATE_MAP = {
    "r": ResultsFate.REPLACE,
    "a": ResultsFate.APPEND,
    "replace": ResultsFate.REPLACE,
    "append": ResultsFate.APPEND,
}

PROTEIN_HARDNESS_MAP = {
    "soft": flare.Align.ProteinHardness.Soft,
    "medium": flare.Align.ProteinHardness.Medium,
    "hard": flare.Align.ProteinHardness.Hard,
}

SCORING_METRIC_MAP = {
    "d": flare.Align.ScoringMetric.Dice,
    "a": flare.Align.ScoringMetric.Tanimoto,
    "t": flare.Align.ScoringMetric.Tversky,
    "dice": flare.Align.ScoringMetric.Dice,
    "tanimoto": flare.Align.ScoringMetric.Tanimoto,
    "tversky": flare.Align.ScoringMetric.Tversky,
}

DOCKING_CONSTRAINT_TYPE = {
    "h": flare.LeadFinderSystem.ConstraintType.HydrogenBond,
    "hbond": flare.LeadFinderSystem.ConstraintType.HydrogenBond,
    "m": flare.LeadFinderSystem.ConstraintType.Metal,
    "metal": flare.LeadFinderSystem.ConstraintType.Metal,
    "p": flare.LeadFinderSystem.ConstraintType.PiStack,
    "pistack": flare.LeadFinderSystem.ConstraintType.PiStack,
    "c": flare.LeadFinderSystem.ConstraintType.PiCation,
    "pication": flare.LeadFinderSystem.ConstraintType.PiCation,
    "s": flare.LeadFinderSystem.ConstraintType.SaltBridge,
    "saltbridge": flare.LeadFinderSystem.ConstraintType.SaltBridge,
}


def is_db_installed(db) -> bool:
    is_installed = True
    try:
        db_cat = flare.SparkCatalog(db)
        is_installed = db_cat.is_installed
    except ValueError:
        pass
    return is_installed


def print_databases(args):
    # List all databases available to sparkcl for number of
    # attachment points, 0 for all fragments
    if args.databases is not None:
        databases_str_list = set()
        for database_arg_str in args.databases.split(","):
            databases_str_list.add(str(os.path.dirname(pathlib.Path(database_arg_str).resolve())))
        os.environ["FLARE_SPARK_DB"] = (";" if os.name == "nt" else ":").join(databases_str_list)
        print(os.environ["FLARE_SPARK_DB"], file=sys.stderr)

    databases_list = flare.SparkDatabase.databases()
    print("Name                    Count\tPath\t", file=sys.stderr)
    frag_pos = args.listdatabases
    if frag_pos > 0:
        print(f"                     ( {frag_pos} bonds)", file=sys.stderr)
    print("==========================================================", file=sys.stderr)
    for db in databases_list:
        if is_db_installed(db):
            frag_count = db.fragment_count
            count = (
                frag_count[frag_pos] if 0 < frag_pos < len(frag_count) else db.fragment_total_count
            )
            print(f"{db.name:<20}\t{count}\t{db.url}", file=sys.stderr)
    print("\nDatabase search path:", end="", file=sys.stderr)
    print(";".join(flare.SparkDatabase.search_path()), file=sys.stderr)


def main(argv: list[str]) -> int:
    args, parser = _parse_args(argv)

    if args.listdatabases is not None:
        print_databases(args)
        return 0

    if args.flags == "help":
        # List all flags available
        for eflag in flare.SparkSearch.FragmentFlag.__members__:
            print(eflag)
        return 0

    if len(args.files) == 0:
        print("ERROR: Must at least specify a molecule file or project.", file=sys.stderr)
        return 1

    project = None
    load_project = args.files[0].endswith(".flr")
    if load_project:
        results_fate = RESULTS_FATE_MAP[args.project_results]
        project_file = args.files[0]
        project = flare.Project.load(project_file)
        if (
            args.reference_role not in project.roles
            or len(project.roles[args.reference_role].ligands) == 0
        ):
            print("ERROR: Specified project contains no reference molecules", file=sys.stderr)
            return 1
        if args.result_role not in project.roles:
            project.roles.append(args.result_role)
        if args.verbose >= 1:
            print(f"Loaded existing project from '{project_file}'", file=sys.stderr)
            results_fate_str = "replaced" if results_fate == ResultsFate.REPLACE else "appended to"
            ref_role_len = len(project.roles[args.reference_role].ligands)
            result_role_len = len(project.roles[args.result_role].ligands)
            print(
                f"Read in {ref_role_len} references and {result_role_len} results which "
                + f"will be {results_fate_str}",
                file=sys.stderr,
            )
        if results_fate == ResultsFate.REPLACE:
            for result_ligand in project.roles[args.result_role].ligands:
                project.ligands.remove(result_ligand)
    else:
        project = flare.Project()
        for file in args.files:
            project.add_molecules(flare.read_file(file))
        if len(project.ligands) == 0:
            print("ERROR: No ligands extracted from the files", file=sys.stderr)
            return 1
        if len(project.ligands) > 9:
            print("ERROR: No more than 9 reference molecules can be provided", file=sys.stderr)
            return 1
        if args.reference_role not in project.roles:
            project.roles.append(args.reference_role)
        if args.result_role not in project.roles:
            project.roles.append(args.result_role)
        reference_role = project.roles[args.reference_role]
        for ligand in project.ligands:
            ligand.role = reference_role

    # SparkStarter
    if args.selection is not None and args.indices is not None:
        print("ERROR: Cannot use -S/--selection and -i/--indices together.", file=sys.stderr)
        return 1

    reference_role = project.roles[args.reference_role]
    starter_ligand = reference_role.ligands[0]
    spark_starter = flare.SparkStarter(starter_ligand)
    rvs = spark_starter.fragment_selection

    atom_indices_list = []
    if args.selection is not None:
        atom_indices = set()
        for numstr in args.selection.split(","):
            if not numstr.isdigit():
                print("ERROR: -S indices must be a number", file=sys.stderr)
                return 1
            atom_indices.add(int(numstr))
        for bond in starter_ligand.bonds:
            atom_ip, atom_iq = bond.atoms
            # Match only if heavy atoms
            if atom_ip.atomic_number > 1 and atom_iq.atomic_number > 1:
                if atom_ip.index in atom_indices and atom_iq.index not in atom_indices:
                    rvs.append((atom_iq, atom_ip))
                elif atom_iq.index in atom_indices and atom_ip.index not in atom_indices:
                    rvs.append((atom_ip, atom_iq))
        atom_indices_list = list(atom_indices)
    elif args.indices is not None:
        for indice in args.indices:
            atom_idx1, atom_idx2, *flags_opt = indice.split(",")
            atom_idx1 = int(atom_idx1)
            atom_idx2 = int(atom_idx2)
            bond_found = False
            for bond in starter_ligand.bonds:
                atom_ip, atom_iq = bond.atoms
                if (atom_ip.index == atom_idx1 and atom_iq.index == atom_idx2) or (
                    atom_iq.index == atom_idx1 and atom_ip.index == atom_idx2
                ):
                    is_lr = atom_ip.index == atom_idx1 and atom_iq.index == atom_idx2
                    atom_left = atom_ip if is_lr else atom_iq
                    atom_right = atom_iq if is_lr else atom_ip
                    if len(flags_opt) > 0:
                        rvs.append((atom_left, atom_right, *flags_opt))
                    else:
                        rvs.append((atom_left, atom_right))
                    bond_found = True
                    break

            if not bond_found:
                print(
                    f"ERROR: Invalid atom index given: Index of {atom_idx1} and {atom_idx2}",
                    file=sys.stderr,
                )
                return 1
    if len(rvs) == 0:
        print("ERROR: -S/--selection or -i/--indices not used.", file=sys.stderr)
        return 1
    if args.verbose >= 1:
        print("Selected the following bonds for disconnection:", file=sys.stderr)
        for rv in rvs:
            print(f"Bond: {rv[0].index} {rv[1].index}", file=sys.stderr)
    try:
        if len(atom_indices_list) > 0:
            spark_starter.target_selection_indices = atom_indices_list
        spark_starter.fragment_selection = rvs
    except ValueError as valError:
        print("ERROR:", valError, file=sys.stderr)
        return 1

    spark_search = flare.SparkSearch()
    spark_search.load_preset(
        flare.SparkSearch.Preset.Accurate if args.exhaustive else flare.SparkSearch.Preset.Normal
    )
    if args.config:
        spark_search.read_config(args.config)
    spark_search.starter = spark_starter

    if not args.databases:
        print("ERROR: -d/--databases not used.", file=sys.stderr)
        return 1
    # spark_search.databases can handle filepaths and names
    spark_search.databases = args.databases.split(",")
    if args.verbose >= 3:
        for db in spark_search.databases:
            print(f'Database used: "{db.name}" "{db.url}"', file=sys.stderr)

    spark_search.role = project.roles[args.result_role]
    spark_search.references = reference_role.ligands
    if args.protein is not None:
        project.proteins.extend(flare.read_file(args.protein))
        if len(project.proteins) != 1:
            print(f"ERROR: Only one protein is allowed. {len(project.proteins)} proteins given.")
            return 1
        spark_search.protein = project.proteins[0]
        if args.protein_chain_types is not None:
            spark_search.sequences = cmdutil.chain_types_to_sequences(
                spark_search.protein, args.protein_chain_types
            )

    # Scoring Options
    if args.maxresults is not None:
        spark_search.max_results_to_keep = args.maxresults
    spark_search.score_method = (
        flare.SparkSearch.ScoreMethod.Docking
        if args.dock
        else flare.SparkSearch.ScoreMethod.LigandSimilarity
    )
    if spark_search.score_method == flare.SparkSearch.ScoreMethod.LigandSimilarity:
        # If it already been set true at this point, the config file already sets it
        if not spark_search.use_shape_scoring_first_pass:
            spark_search.use_shape_scoring_first_pass = args.use_first_pass_shape
    if args.gradient_cutoff is not None:
        spark_search.minimize_gradient_cutoff = args.gradient_cutoff
    if args.protein_hardness is not None:
        spark_search.protein_hardness = PROTEIN_HARDNESS_MAP[args.protein_hardness]
    if args.shapewt is not None:
        spark_search.shape_weight = args.shapewt

    # Constraints (Field and Pharamcophore)
    regen_field_points = (
        args.fieldconstraint is not None
        or args.ph4constraint is not None
        or len(spark_search.pharmacophore_constraints) > 0
        or len(spark_search.field_constraints) > 0
    )
    regen_constraints = False
    bkup_field_constraints = []
    for field_point, constraint_size in spark_search.field_constraints.items():
        bkup_field_constraints.append((field_point.molecule, field_point.index, constraint_size))
    if not regen_field_points:
        for ligand in reference_role.ligands:
            if len(ligand.field_points) > 0:
                ligand.field_points.clear()
                regen_field_points = True
                regen_constraints = True
    if regen_field_points:
        # Add field points first
        add_fields = flare.AddFields()
        add_fields.ligands = reference_role.ligands
        add_fields.start()
        add_fields.wait()

    if regen_constraints:
        spark_search.field_constraints.clear()
        for mol, atom_idx, constraint_size in bkup_field_constraints:
            for field_point in mol.field_points:
                if field_point.index == atom_idx:
                    spark_search.field_constraints[field_point] = constraint_size
                    break

    if args.fieldconstraint is not None:
        for fconststr in args.fieldconstraint:
            fconstarray = fconststr.split(",")
            if len(fconstarray) < 2:
                print(
                    "ERROR: --fieldconstraint must have at least 2 "
                    + "items (index,size,[reference_index]), provided "
                    + f"argument: {fconststr} does not satisfy the requirement.",
                    file=sys.stderr,
                )
                return 1
            # Item 1 and 3 are 1-based, only convert the 3rd one to 0-based, atom_index
            # is compared within index property of the Python API which is 1-based
            atom_index = int(fconstarray[0])
            constraint_size = float(fconstarray[1])

            ref_index = 0 if len(fconstarray) < 3 else (int(fconstarray[2]) - 1)
            if ref_index >= len(reference_role.ligands):
                print(
                    f"ERROR: Invalid reference index of {ref_index + 1}, "
                    + f"must be within 1 and {len(reference_role.ligands)}.",
                    file=sys.stderr,
                )
                return 1
            ref_ligand = reference_role.ligands[ref_index]

            field_point = None
            if atom_index <= 0:
                # If negative or 0, turn to positive 0-based index of the fieldpoint
                field_point = ref_ligand.field_points[-atom_index]
            else:
                # Convert atom index matching field_point
                for cmp_field_point in ref_ligand.field_points:
                    if cmp_field_point.index == atom_index:
                        field_point = cmp_field_point
                        break

            if field_point is None:
                print(f"ERROR: Invalid atom index given: {atom_index}", file=sys.stderr)
                return 1

            spark_search.field_constraints[field_point] = constraint_size

    PH4_TYPES_MAP = {
        "d": flare.Align.PharmacophoreConstraint.Donor,
        "a": flare.Align.PharmacophoreConstraint.Acceptor,
        "+": flare.Align.PharmacophoreConstraint.Cation,
        "-": flare.Align.PharmacophoreConstraint.Anion,
        "m": flare.Align.PharmacophoreConstraint.MetalBinder,
        "v": flare.Align.PharmacophoreConstraint.Covalent,
    }

    if args.ph4constraint is not None:
        for pconststr in args.ph4constraint:
            pconstarray = pconststr.split(",")
            if len(pconstarray) < 2:
                print(
                    "ERROR: --ph4constraint must have at least 2 "
                    + "items (index,type,[strength],reference_index]), provided "
                    + f"argument: {pconstarray} does not staisfy the requirement.",
                    file=sys.stderr,
                )
                return 1
            # Item 1 and 4 are 1-based, convert to 0-based
            atom_index = int(pconstarray[0]) - 1
            ph4_type_str = pconstarray[1]
            ph4_type_set = set()
            for ph4_type_char in ph4_type_str:
                if ph4_type_char not in PH4_TYPES_MAP.keys():
                    print(
                        f"ERROR: --ph4constraint type '{ph4_type_char}' "
                        + f"(from '{ph4_type_str}') is invalid. "
                        + "Must be: d,a,+,-,m, or v.",
                        file=sys.stderr,
                    )
                    return 1
                ph4_type = PH4_TYPES_MAP[ph4_type_char]
                ph4_type_set.add(ph4_type)
            strength = 10.0 if len(pconstarray) < 3 else float(pconstarray[2])
            ref_index = 0 if len(pconstarray) < 4 else (int(pconstarray[3]) - 1)
            if ref_index >= len(reference_role.ligands):
                print(
                    f"ERROR: Invalid reference index of {ref_index + 1}, "
                    + f"must be within 1 and {len(reference_role.ligands)}.",
                    file=sys.stderr,
                )
                return 1
            ref_ligand = reference_role.ligands[ref_index]
            spark_search.pharmacophore_constraints[ref_ligand.atoms[atom_index]] = (
                tuple(ph4_type_set),
                strength,
            )

    if args.weight is not None:
        weightarray = args.weight.split(",")
        ref_total = len(reference_role.ligands)
        if len(weightarray) != ref_total:
            print(
                f"ERROR: The number of weights given ({len(weightarray)}) must be equal to "
                + "the amount of reference molecules ({ref_total})"
            )
            return 1
        for i in range(0, ref_total):
            spark_search.reference_weight[reference_role.ligands[i]] = float(weightarray[i])

    if args.verbose >= 3:
        for field_point in starter_ligand.field_points:
            print("Field point:", field_point.index, field_point.size, file=sys.stderr)

        for field_point, constraint_size in spark_search.field_constraints.items():
            print("Field constraint:", field_point.index, constraint_size, file=sys.stderr)

        for atom, ph4_tuple in spark_search.pharmacophore_constraints.items():
            print("ph4 constraint:", atom, ph4_tuple, file=sys.stderr)

    if args.field_weight is not None:
        fw_array = args.field_weight.split(",")
        if len(fw_array) != 4:
            print("ERROR: --field-weight must define 4 values split by comma.")
            return 1
        spark_search.field_similarity_weighting_positive = float(fw_array[0])
        spark_search.field_similarity_weighting_negative = float(fw_array[1])
        spark_search.field_similarity_weighting_surface = float(fw_array[2])
        spark_search.field_similarity_weighting_hydrophobic = float(fw_array[3])

    if spark_search.score_method == flare.SparkSearch.ScoreMethod.LigandSimilarity:
        if args.scoring_metric is not None:
            spark_search.scoring_metric = SCORING_METRIC_MAP[args.scoring_metric]
        if (
            spark_search.scoring_metric == flare.Align.ScoringMetric.Tversky
            and args.tversky_alpha is not None
        ):
            spark_search.scoring_metric_tversky_alpha = args.tversky_alpha

    # Docking options
    if spark_search.score_method == flare.SparkSearch.ScoreMethod.Docking:
        if args.active_site_buffer is not None and args.docking_grid is not None:
            print(
                "ERROR: Cannot use -b/--active-site-buffer and --docking-grid together.",
                file=sys.stderr,
            )
            return 1

        if args.active_site_buffer is not None:
            least = None
            most = None
            for ligand in spark_search.references:
                for atom in ligand.atoms:
                    least = (
                        atom.pos
                        if least is None
                        else tuple([min(least[i], atom.pos[i]) for i in range(3)])
                    )
                    most = (
                        atom.pos
                        if most is None
                        else tuple([max(most[i], atom.pos[i]) for i in range(3)])
                    )
            bufSize = args.active_site_buffer
            least = (least[0] - bufSize, least[1] - bufSize, least[2] - bufSize)
            most = (most[0] + bufSize, most[1] + bufSize, most[2] + bufSize)
            spark_search.grid_box = (least, most)

        if args.docking_grid is not None:
            DOCKING_GRID_VALUE_ERROR_MSG = (
                "--docking-grid must be set to either 'existing', "
                + "'N1,N2,N3,N4,N5,N6', or 'LIGAND_FILE'. The ligand file must contain a molecule."
            )
            if args.docking_grid == "existing":
                spark_search.use_existing_grid = True
            else:
                grid_list = args.docking_grid.split(",")
                if len(grid_list) == 6:
                    top_left = (grid_list[0], grid_list[1], grid_list[2])
                    bottom_right = (grid_list[3], grid_list[4], grid_list[5])
                    spark_search.grid_box = (top_left, bottom_right)
                else:
                    role = project.roles.append(
                        "Docking_Ligand", "The molecule which define the region to dock to"
                    )
                    role.ligands.extend(flare.read_file(args.docking_grid))
                    if not role.ligands:
                        print(DOCKING_GRID_VALUE_ERROR_MSG, file=sys.stderr)
                        return 1
                    if len(role.ligands) > 1:
                        print(
                            "Warning: multiple docking grid ligands specified, only the first "
                            + "docking grid molecule will be used to define the region of the "
                            + "protein to dock to",
                            file=sys.stderr,
                        )
                        del role.ligands[1:]
                    spark_search.grid_box = role.ligands[0]

        if args.max_docking_constraint_penalty is not None:
            spark_search.max_constraint_penalty = args.max_docking_constraint_penalty
        if args.docking_timeout is not None:
            spark_search.docking_timeout = args.docking_timeout

        protein_constraints_array = []
        CONST_STRENGTH_MAP = {
            "w": flare.LeadFinderSystem.ConstraintStrength.Weak,
            "n": flare.LeadFinderSystem.ConstraintStrength.Normal,
            "s": flare.LeadFinderSystem.ConstraintStrength.Strong,
        }

        protein = spark_search.protein
        if args.docking_index_constraint is not None:
            for dock_index_str in args.docking_index_constraint:
                dock_index_array = dock_index_str.split(",")
                if len(dock_index_array) > 3 or len(dock_index_array) < 2:
                    print(
                        "ERROR: Read --help for info on how to use --docking-index-constraint",
                        file=sys.stderr,
                    )
                    return 1
                # atomindex is 1-based
                atomindex, atomtype, *strength_opt = dock_index_array
                if not atomindex.isdigit():
                    print(
                        "ERROR: --docking-index-constraint: "
                        + f"First item should be a number not '{atomindex}'",
                        file=sys.stderr,
                    )
                    return 1
                atomindex = int(atomindex)
                if atomindex < 1 or atomindex > len(protein.atoms):
                    print(
                        "ERROR: --docking-index-constraint: "
                        + f"Atom index given: {atomindex} is out of range (1-{len(protein.atoms)})",
                        file=sys.stderr,
                    )
                    return 1
                atomtype = atomtype.lower()

                constraint_strength = flare.LeadFinderSystem.ConstraintStrength.Normal
                if len(strength_opt) > 0:
                    strength_char = strength_opt[0].lower()
                    if strength_char not in CONST_STRENGTH_MAP.keys():
                        print("ERROR: strength must be of 'w','n', or 's'.", file=sys.stderr)
                        return 1
                    constraint_strength = CONST_STRENGTH_MAP[strength_char]

                if atomtype not in DOCKING_CONSTRAINT_TYPE.keys():
                    print(f"ERROR: {atomtype} is not a valid atomtype.", file=sys.stderr)
                    return 1

                protein_constraints_array.append(
                    (
                        protein.atoms[atomindex - 1],
                        DOCKING_CONSTRAINT_TYPE[atomtype],
                        constraint_strength,
                    )
                )

        if args.docking_atom_constraint is not None:
            RESNUM_RE = re.compile("(\\d+)([a-zA-Z])?")
            for dock_constraint_str in args.docking_atom_constraint:
                dock_atom_constraint_array = dock_constraint_str.split(",")
                if len(dock_atom_constraint_array) > 6 or len(dock_atom_constraint_array) < 5:
                    print(
                        "ERROR: Read --help for info on how to use --docking-atom-constraint",
                        file=sys.stderr,
                    )
                    return 1
                (
                    chain,
                    resname,
                    resnum_str,
                    atomname,
                    atomtype,
                    *strength_opt,
                ) = dock_atom_constraint_array
                atomtype = atomtype.lower()

                resnum_search = RESNUM_RE.search(resnum_str)
                if resnum_search is None:
                    print("ERROR: resnum should be a number", file=sys.stderr)
                    return 1
                resnum = int(resnum_search.group(1))
                insertcode = resnum_search.group(2)
                if insertcode is None:
                    insertcode = ""

                constraint_atom = None
                for residue in protein.residues:
                    if (
                        residue.chain == chain
                        and residue.name == resname
                        and residue.icode == insertcode
                        and residue.seq_num == resnum
                    ):
                        for atom in residue.atoms:
                            if atom.name == atomname:
                                constraint_atom = atom
                                break
                        break
                if constraint_atom is None:
                    print(
                        "ERROR: Cannot find atom from: "
                        + f"{chain}, {resname}, {resnum_str}, {atomname}",
                        file=sys.stderr,
                    )
                    return 1

                constraint_strength = flare.LeadFinderSystem.ConstraintStrength.Normal
                if len(strength_opt) > 0:
                    strength_char = strength_opt[0].lower()
                    if strength_char not in CONST_STRENGTH_MAP.keys():
                        print("ERROR: strength must be of 'w','n', or 's'.", file=sys.stderr)
                        return 1
                    constraint_strength = CONST_STRENGTH_MAP[strength_char]

                if atomtype not in DOCKING_CONSTRAINT_TYPE.keys():
                    print(f"ERROR: {atomtype} is not a valid atomtype.", file=sys.stderr)
                    return 1

                protein_constraints_array.append(
                    (constraint_atom, DOCKING_CONSTRAINT_TYPE[atomtype], constraint_strength)
                )

        spark_search.docking_protein_constraints = protein_constraints_array

    # Size criteria options
    # Python 3.10+ should use "int | float" type hints instead of Union
    def checkrange(
        longopt: str,
        argstr: str,
        range_min: Union[int, float] = 0,
        range_max: Union[int, float] = 0,
        require_bound: bool = True,
        allow_single: bool = True,
        is_float: bool = False,
    ) -> (Optional[Union[int, float]], Optional[Union[int, float]]):
        def checkbounds(
            longopt: str,
            numstr: str,
            range_min: Union[int, float],
            range_max: Union[int, float],
            require_bound: bool,
            is_float: bool,
        ) -> Union[int, float]:
            num = None
            if is_float:
                try:
                    num = float(numstr)
                except ValueError:
                    print(f"ERROR: {longopt} {numstr} is not a floating number")
                    exit(1)
            else:
                if not numstr.isdigit():
                    print(f"ERROR: {longopt} {numstr} is not a number")
                    exit(1)
                num = int(numstr)

            if not require_bound:
                # Skip bound checking
                return num

            if num < range_min or num > range_max:
                print(f"ERROR: {longopt} {num} is out of range ({range_min} - {range_max})")
                exit(1)
            return num

        argarray = argstr.split(",")
        if len(argarray) > 2 or len(argarray) == 0:
            print(f"ERROR: {longopt} unable to parse argument of '{argstr}'.")
            exit(1)
            return (None, None)

        if len(argarray) == 1:
            if allow_single:
                return (
                    None,
                    checkbounds(
                        longopt, argarray[0], range_min, range_max, require_bound, is_float
                    ),
                )
            else:
                print(f"ERROR: {longopt} must have a minimum and maximum defined.")
                exit(1)
                return (None, None)

        low = checkbounds(longopt, argarray[0], range_min, range_max, require_bound, is_float)
        high = checkbounds(longopt, argarray[1], range_min, range_max, require_bound, is_float)
        if low > high:
            print(
                f"ERROR: {longopt} range error minimum ({low}) should not be "
                + "higher than maximum ({high})"
            )
            exit(1)
        return (low, high)

    if args.maxnh is not None or args.maxmw is not None:
        fsc = flare.SparkFragmentSizeConstraint()
        if args.maxnh is not None:
            low, high = checkrange("--maxnh", args.maxnh, 1, 999)
            if low is not None:
                fsc.heavy_atom_min = low
            if high is not None:
                fsc.heavy_atom_max = high
        if args.maxmw is not None:
            low, high = checkrange("--maxmw", args.maxmw, 1, 9999)
            if low is not None:
                fsc.molecular_weight_min = low
            if high is not None:
                fsc.molecular_weight_max = high
        spark_search.fragment_size_constraint = fsc
    if args.maxrb is not None:
        spark_search.max_rotatable_bonds = args.maxrb

    # Filters Options
    if args.flags is not None:
        FLAGS_MAP = dict()
        for eflag_name, eflag_value in flare.SparkSearch.FragmentFlag.__members__.items():
            FLAGS_MAP[eflag_name.lower()] = eflag_value

        flags_require_list = []
        flags_exclude_list = []
        for flag_str in args.flags.strip().split(","):
            require = True
            if flag_str.startswith("+"):
                flag_str = flag_str[1:]
            elif flag_str.startswith("-"):
                flag_str = flag_str[1:]
                require = False
            flag_str = flag_str.lower()
            if flag_str not in FLAGS_MAP.keys():
                print(f"ERROR: Given flag '{flag_str}' isn't a known flag.")
                return 1
            flag_e = FLAGS_MAP[flag_str]
            if require:
                flags_require_list.append(flag_e)
            else:
                flags_exclude_list.append(flag_e)
        spark_search.filters_required_flags = flags_require_list
        spark_search.filters_excluded_flags = flags_exclude_list

    if args.filter_requires_smarts is not None:
        spark_search.filters_required_smarts = args.filter_requires_smarts

    if args.filter_excludes_smarts is not None:
        spark_search.filters_excluded_smarts = args.filter_excludes_smarts

    if args.filter_tpsa is not None:
        spark_search.require_tpsa_min, spark_search.require_tpsa_max = checkrange(
            "--filter-tpsa", args.filter_tpsa, require_bound=False, allow_single=False
        )

    if args.filter_slogp is not None:
        spark_search.require_slogp_min, spark_search.require_slogp_max = checkrange(
            "--filter-slogp",
            args.filter_slogp,
            require_bound=False,
            allow_single=False,
            is_float=True,
        )

    if args.filter_flexibility is not None:
        (
            spark_search.require_flexibility_min,
            spark_search.require_flexibility_max,
        ) = checkrange(
            "--filter-flexibility",
            args.filter_flexibility,
            require_bound=False,
            allow_single=False,
            is_float=True,
        )

    if args.experiment_name:
        spark_search.experiment_name = args.experiment_name
    spark_search.experiment_tag = args.experiment_tag

    # Engine setups
    if args.localengines is not None:
        spark_search.local_engine_count = args.localengines

    cmdutil.start_process(spark_search, args.broker, args.verbose)
    cmdutil.wait_for_finish(spark_search)

    if spark_search.errors():
        print("\n".join(spark_search.errors()), file=sys.stderr)

    if not spark_search.is_cancelled():
        if args.project:
            try:
                project.save(args.project)
            except Exception as err:
                print(err, file=sys.stderr)

        result_ligands = project.roles[args.result_role].ligands
        if args.log:
            for ligand in result_ligands:
                ligand.properties["FlareSparkCL_Log"].value = cmdutil.molecule_log_as_sdf_string(
                    ligand
                )

        if args.verbose >= 2 and project.log:
            print(project.log[-1].comment, file=sys.stderr)

        def modify_tags(ligand, tags):
            if args.no_computed_properties:
                for del_tag in [
                    "MW",
                    "#Atoms",
                    "2D sim",
                    "SlogP",
                    "TPSA",
                    "Flexibility",
                    "#RB",
                    "Rof5",
                ]:
                    del_tag_cresset = del_tag + "_cresset"
                    if del_tag_cresset in tags:
                        del tags[del_tag_cresset]
            return tags

        ref_ligands = project.roles[args.reference_role].ligands
        if args.writeref == "before":
            flare.write_file(sys.stdout, ref_ligands, args.output, modify_tags)
        flare.write_file(sys.stdout, result_ligands, args.output, modify_tags)
        if args.writeref == "after":
            flare.write_file(sys.stdout, ref_ligands, args.output, modify_tags)

    return 0


def _parse_args(argv: [str]) -> (argparse.Namespace, argparse.ArgumentParser):
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=cmdutil.WrappedNewlineFormatter,
        add_help=False,
        allow_abbrev=False,
    )

    parser.add_argument(
        "files",
        nargs="*",
        help="Molecular or project files.",
    )

    group = parser.add_argument_group("roles options")
    group.add_argument(
        "--reference-role",
        default="Reference",
        metavar="role",
        help="Set the role to use for references, defaults to 'Reference'.",
    )
    group.add_argument(
        "--result-role",
        default="Ligands",
        metavar="role",
        help="Set the role to use for results, defaults to 'Ligands'.",
    )

    group = parser.add_argument_group("database options")
    group.add_argument(
        "-d",
        "--databases",
        metavar="dbname1,dbname2,...",
        help="Search the specified databases. Use -D 0 to list available dbs. Note that "
        + "instead of giving the short database name, you can instead give a full (absolute) "
        + "path to any sparkcl database. This argument is mandatory.",
    )
    group.add_argument(
        "-D",
        "--listdatabases",
        metavar="number_of_attachment_points",
        type=int,
        help="List all of the databases available to sparkcl. The database fragment counts "
        + "listed are for fragments with the specified number of attachment points: use '0' "
        + "to get a count of all fragments.",
    )
    group.add_argument(
        "-i",
        "--indices",
        metavar="atom1,atom2[,flags]",
        action="append",
        help="Specify a bond to break: atom1 is kept, atom2 is discarded. Multiple -i options "
        + "can be given to break multiple bonds. Sparkcl will warn and exit if it can't make a "
        + "consistent selection from the bonds specified. The flags restrict the types of atoms "
        + 'that can be connected to this position. See "specifying a section to replace" for '
        + "more help on this option.",
    )
    group.add_argument(
        "-S",
        "--selection",
        metavar="a1,a2,a3,...",
        help="Specify the set of atoms to replace directly - see "
        + '"alternative method for specifying a section to replace" for more details.',
    )
    group.add_argument(
        "--project-results",
        choices=RESULTS_FATE_MAP.keys(),
        default="a",
        help="If a project file was specified as one of the inputs and it contains results, "
        + "this option allows choosing whether the results should be replaced ('r') or appended "
        + "to ('a', the default). '-P' or '--project' needs to be set to specify where the "
        + "modified project should written to.",
    )

    group = parser.add_argument_group("quality options")
    group.add_argument(
        "-e",
        "--exhaustive",
        action="store_true",
        help="Relax fragment fit criteria and check more rotamers: slower but might find more "
        + "solutions.",
    )

    group = parser.add_argument_group("output options")
    group.add_argument(
        "-o",
        "--output",
        choices=["sdf", "mol2", "xed"],
        default="sdf",
        type=str,
        help="Set the file format for the output. The default is to output SDF files.",
    )
    group.add_argument(
        "-P",
        "--project",
        metavar="project_file",
        help="Write the results to the specified file as a Flare project which can be read by the "
        + "GUI version. A '.flr' extension will be added to the filename if not specified.",
    )
    group.add_argument(
        "-w",
        "--writeref",
        choices=["before", "after"],
        help="Write out the reference molecule(s) before or after the results. The default "
        + "is not to output the reference molecule(s).",
    )
    group.add_argument(
        "-L",
        "--log",
        action="store_true",
        help="Request that the calculation log be written as one of the SDF tags for each result. "
        + "Only has an effect if the output format (-o flag) is 'sdf': this is the default. The "
        + "size of the output SDF file will get noticeably larger if this option is specified.",
    )
    group.add_argument(
        "--no-computed-properties",
        action="store_true",
        help="Request that SDF tags of computed properties (MW, #Atoms, 2D Sim, SlogP, TPSA, "
        + "Flexibility, #RB, Rof5) be excluded from the output.",
    )
    group.add_argument(
        "--experiment-name",
        help="Set the name for the Spark experiment.",
    )
    group.add_argument(
        "--experiment-tag",
        action="store_true",
        help="Set if the Spark experiment name will also be set as a tag. This will only "
        + "work with --project set.",
    )

    group = parser.add_argument_group("scoring options")
    group.add_argument(
        "-k",
        "--dock",
        action="store_true",
        help="Use docking to score the final result molecules. A protein must be provided with "
        + "the -p option when using docking. This should define the active site of the reference "
        + "molecules in the same coordinate space. The docking grid is constructed using the "
        + "protein atoms within a bounding box containing the reference molecules. The size of "
        + "the box may be adjusted by specifying a buffer using the -b option. Note that "
        + "approximate field scoring is still used in the first stage of filtering the results.",
    )
    group.add_argument(
        "-b",
        "--active-site-buffer",
        metavar="x",
        type=float,
        help="Increase the size of the bounding box enclosing the reference molecules by x "
        + "Angstroms in all directions. This bounding box is used to construct the docking grid "
        + "from the protein.",
    )
    group.add_argument(
        "--docking-grid",
        help="Set the docking grid to use, can be set to either: 'existing' "
        + "to use an existing grid, 'N1,N2,N3,N4,N5,N6' where N1-N3 is the "
        + "top-left and N4-N6 is the bottom-right of the grid box, or "
        + "'LIGAND_FILE' the ligand file in (EX: sdf format) which contains "
        + "the molecule to take the grid from.",
    )
    group.add_argument(
        "-c",
        "--docking-index-constraint",
        metavar="index,type,[strength]",
        action="append",
        help="Specifies a docking constraint on an atom from the specified protein molecule, "
        + "with the specified index (1-based). Multiple constraints can be specified by providing "
        + "multiple -c options. The strength of the constraint must be one of 'w' (weak), 'n' "
        + "(normal), or 's' (strong), and defaults to 'n' if not specified.\n"
        + "\n"
        + "Type is one of:\n"
        + "\n"
        + "'h'/'hbond'\n"
        + "Hydrogen bond (on donor or acceptor atom)\n"
        + "\n"
        + "'m'/'metal'\n"
        + "Metal interaction (on metal atom)\n"
        + "\n"
        + "'p'/'pistack'\n"
        + "Pi stacking interaction (on aromatic carbon atom)\n"
        + "\n"
        + "'+'/'pication'\n"
        + "Pi-cation interation (on aromatic carbon atom)\n"
        + "\n"
        + "'s'/'saltbridge'\n"
        + "Salt-bridge interaction (on charged atom)\n",
    )
    group.add_argument(
        "-a",
        "--docking-atom-constraint",
        metavar="chain,resname,resnum,atomname,type[,strength]",
        action="append",
        help="Specifies a docking constraint on an atom with the specified atom name from the "
        + "residue with the specified chain, name, number and insertion code in the specified "
        + "protein molecule. Multiple constraints can be specified by providing multiple -a "
        + "options. The strength of the constraint must be one of 'w' (weak), 'n' (normal), "
        + "or 's' (strong), and defaults to 'n' if not specified.\n\n"
        + "See the -c option for the allowed arguments for 'type'",
    )
    group.add_argument(
        "-C",
        "--max-docking-constraint-penalty",
        metavar="X",
        type=float,
        help="The maximum docking constraint penalty that will be tolerated. With the default "
        + "value of 1.0, docking poses that do not match the provided pharmacophore constraints "
        + "will be disallowed. To let Lead Finder produce poses that violate the constraints, "
        + "set this to a high value (e.g. 100).",
    )
    group.add_argument(
        "--docking-timeout",
        type=int,
        help="Time limit for docking each molecule (in seconds). The default is 0 which means "
        + "unlimited.",
    )
    group.add_argument(
        "-m",
        "--maxresults",
        metavar="M",
        type=int,
        help="Output at most M result molecules. The default is 500, or 1000 with the -e switch. "
        + "Setting this to 0 outputs all of the result molecules.",
    )
    group.add_argument(
        "-s",
        "--shapewt",
        metavar="S",
        type=float,
        help="Set the weight of the shape component of the scoring function. The default, 0.5, "
        + "means half shape and half fields. Does not have an effect if -k is specified. Note "
        + "that the shape score will not be used in the initial fragment scoring stage unless "
        + "--use-first-pass-shape is specified.",
    )
    group.add_argument(
        "-p",
        "--protein",
        metavar="protein_file",
        type=str,
        help="Read the first molecule in the specified file and use it as an excluded volume when "
        + "scoring fragments. If -k is specified, it will be used for docking to score the final "
        + "result molecules. Accepted file formats include mol2, PDB, SDF and XED. Note that only "
        + "the first 25K atoms from the protein will be used: trim the protein down to the "
        + "residues around the active site if it is very large.",
    )
    group.add_argument(
        "--protein-chain-types",
        metavar="protein,water,other,ligand",
        type=str.lower,
        help="Select the types of chains to include in the calculation as a comma separated "
        + "list. 'p', 'w', 'o' and 'l' can be used as a shorthand for 'protein', water', "
        + "'other', 'ligand'. The default is to include all the chains.",
    )
    group.add_argument(
        "-R",
        "--protein-hardness",
        choices=PROTEIN_HARDNESS_MAP.keys(),
        help="Set the hardness of the protein excluded volume to use: a harder protein penalises "
        + "steric clashes more. Only has an effect if -p is specified.",
    )
    group.add_argument(
        "-f",
        "--fieldconstraint",
        metavar="index,size,[reference_index]",
        action="append",
        help="Specifies a field constraint on the field point from the specified reference "
        + "molecule, with the specified index. The reference index is optional and if not "
        + "provided will default to 1 (the starter molecule). Multiple constraints can be "
        + "specified by providing multiple -f options. Note that this option is only useful "
        + "if the input molecule is read from a XED file or an SDF file exported from a "
        + "Cresset tool.\n\n"
        + "If index is a positive value, it is taken to be the index of the field point in "
        + "the full list of atoms. If it is a negative value, then the absolute value of "
        + "<index> is used as the 0-based index in the list of field points e.g. "
        + "'-f=-4,10' constrains the fifth field point in the molecule. In Spark GUI if "
        + "atom index labels are shown then the fifth field point will have "
        + 'the label "F5".',
    )
    group.add_argument(
        "-X",
        "--ph4constraint",
        metavar="index,type,[strength],[reference_index]",
        action="append",
        help="Specifies a pharmacophore constraint on an atom from the specified reference "
        + "molecule, with the specified index. The reference index is optional and if not "
        + "provided will default to 1 (the starter molecule). Multiple constraints can be "
        + "specified by providing multiple -X options. The strength of the constraint must "
        + "be between 0 and 100, and defaults to 10 if not specified. The type of constraint "
        + "is specified with either a character sequence or a number representing a "
        + "combination of bit flags. The characters for each type are 'd'=Donor, 'a'=Acceptor, "
        + "'+'=Cation, '-'=Anion, 'm'=Metal binder and 'v'=Covalent. The respective bit "
        + "flags are 1, 2, 4, 8, 16 and 32.",
    )
    group.add_argument(
        "-W",
        "--weight",
        metavar="w1,w2,w3,...",
        help="Set the relative weights of the reference molecules. The number of weights must "
        + "equal the number of reference molecules.",
    )
    group.add_argument(
        "--gradient-cutoff",
        metavar="rms",
        type=float,
        help="This value controls how much minimisation is performed on each result i.e. how "
        + "near to the bottom of the local conformational energy well do we try to get. A low "
        + "value (e.g. 0.1) minimises each result intensively, which takes a long time and can "
        + "sometimes lead to a poor sampling of conformational space, while a high value "
        + "(e.g. 1.5) often leads to a broader exploration but at the expense of the occasional "
        + "unrealistic geometry. Values of 0.25 to 1.0 are recommended. "
        + "{def. 0.7 normal, 0.5 exhaustive}",
    )
    group.add_argument(
        "--scoring-metric",
        choices=SCORING_METRIC_MAP.keys(),
        help="Set the scoring metric to use, the tversky metric requires an alpha value to be "
        + "set. The default is dice. Does not have an effect if -k is specified.",
    )
    group.add_argument(
        "--tversky-alpha",
        type=float,
        help="Set the alpha value for tversky, only if --scoring-metric is set to 'tversky'.",
    )
    group.add_argument(
        "--field-weight",
        metavar="P,N,S,H",
        help="Set weightings for the four different field types: Positive, Negative, Surface "
        + "and Hydrophobic. All four values must be specified and in the range 0 to 1.",
    )
    group.add_argument(
        "--use-first-pass-shape",
        action="store_true",
        help="Use the shape similarity (see --shapewt) in the first pass rough scoring of "
        + "fragments. This significantly slows the calculation and only slightly improves "
        + "results.",
    )

    group = parser.add_argument_group("size criteria options")
    group.add_argument(
        "-n",
        "--maxnh",
        metavar="max|min,max",
        type=str,
        help="Only search fragments with at most 'max' heavy atoms. The default limit is the "
        + "size of the removed section plus 5 atoms.",
    )
    group.add_argument(
        "-M",
        "--maxmw",
        metavar="max|min,max",
        type=str,
        help="Only search fragments with molecular weight less than 'max'. The default limit is "
        + "the size of the removed section plus 75 daltons.",
    )
    group.add_argument(
        "-r",
        "--maxrb",
        metavar="R",
        type=int,
        help="Only search fragments with at most R rotatable bonds {def. 5}",
    )

    group = parser.add_argument_group("filters options")
    group.add_argument(
        "-F",
        "--flags",
        metavar="[+-]flag1,[+-]flag2,...",
        help="Set features that either must be present or must not be present in fragments. "
        + "e.g. '-F=Ring,-S' means only consider fragments containing a ring, but no sulfurs. "
        + "Use -F help to get a list of available flags. Note that for arguments starting with "
        + "'-' you will need to use the '-F=-S' format rather than '-F -S'\n"
        + "\n"
        + "Useful flags include\n"
        + "\n"
        + "-NonRingBond\n"
        + "specifying -F=-NonRingBond will only find fragments that contain no non-ring bonds "
        + "i.e. fragments that consist of rings only with no exocyclic components\n"
        + "\n"
        + "Donor\n"
        + "fragment must contain a H-bond donor (defined as any H on a nitrogen or oxygen)\n"
        + "\n"
        + "Acceptor\n"
        + "fragment must contain a H-bond acceptor (defined as =N-, =O, -OH, or -C#N).\n"
        + "\n"
        + "-Nasties\n"
        + 'Excluding "Nasties" removes fragments which contain a "nasty" functional group '
        + "(nitroso, isocyanate, peroxide, etc)",
    )
    group.add_argument(
        "--filter-requires-smarts",
        metavar="smartsPattern",
        action="append",
        help="Only fragment which match the smartsPattern will be included in the results. "
        + "If multiple --filter-requires-smarts are set then the fragment will be included if "
        + "it matches any of the patterns.",
    )
    group.add_argument(
        "--filter-excludes-smarts",
        metavar="smartsPattern",
        action="append",
        help="Only fragment which do not match the smartsPattern will be included in the "
        + "results. If multiple --filter-excludes-smarts are set then the fragment will only "
        + "be included if it does not match any of the patterns.",
    )
    group.add_argument(
        "--filter-tpsa",
        metavar="min,max",
        help="Only molecules which have a TPSA between min and max inclusive will be "
        + "included in the results.",
    )
    group.add_argument(
        "--filter-slogp",
        metavar="min,max",
        help="Only molecules which have a SLogP between min and max inclusive will be "
        + "included in the results.",
    )
    group.add_argument(
        "--filter-flexibility",
        metavar="min,max",
        help="Only molecules which have a flexibility between min and max inclusive "
        + "will be included in the results.",
    )

    group = parser.add_argument_group("field engine options")
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        help="Set the number of local engines to start. Use 0 for no local processing: "
        + "you must specify some remote engines with -g if you do so.",
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker in the format "
        + "hostname:port. Field Engines which have been registered with the "
        + "Broker will be used during the calculation. The CRESSET_BROKER "
        + "environment variable can be used instead of this option.",
    )

    group = parser.add_argument_group("general options")
    group.add_argument(
        "--config",
        help=cmdutil.CONFIG_ARG_HELP_TEXT,
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="count",
        default=0,
        help="Print log and progress information to standard error.",
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )

    return (parser.parse_args(argv), parser)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
