#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Prepares a protein and writes the results to stdout.

This will add hydrogen atoms, optimize hydrogen bonds, remove atomic clashes and
assign optimal protonation states.

examples:
  pyflare proteinprep.py protein.pdb
    Prepares the protein and prints the results to stdout.

  pyflare proteinprep.py --help
    Prints the help text including all available options and exits.
"""
import sys
import argparse
import pathlib
import traceback

from cresset import flare

from _internal import cmdutil


def main(argv):
    """Run the protein prep process using the command line args."""
    try:
        prep = flare.ProteinPrep()
        project = flare.Project()

        args = _parse_args(prep, argv)
        _configure_process(prep, project, args)
        logging_context = cmdutil.LoggingContext(project.log, verbose=args.verbose)
        cmdutil.start_process(prep, args.broker, args.verbose)
        cmdutil.wait_for_finish(prep, logging_context=logging_context)
        cmdutil.check_errors(prep)
        if not prep.is_cancelled():
            _write_results(prep, project, args)

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(prep, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )
    # Input output settings
    parser.add_argument(
        "protein",
        nargs="?",
        help="The protein file to read. If no files are listed then the proteins are read from stdin. Only the first protein will be prepared.",  # noqa: E501
    )

    group = parser.add_argument_group("input/output")
    group.add_argument(
        "-i",
        "--input",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for reading the protein. The default is to autodetect when reading from files.",  # noqa: E501
    )
    group.add_argument(
        "-o",
        "--output",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for writing the protein. The default is 'pdb'.",
    )
    group.add_argument(
        "-P",
        "--project",
        help="Write the Flare project file with the results to the given file path.",
    )

    # Prep setting
    group = parser.add_argument_group("prep options")
    group.add_argument(
        "-p",
        "--ph",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.ProteinPrep.pH.__doc__),
    )
    group.add_argument(
        "-C",
        "--do-not-cap-chains",
        action="store_true",
        help="Do not allow capping of chains with NME and ACE.",
    )
    group.add_argument(
        "-G",
        "--do-not-fill-gaps",
        action="store_true",
        help="Do not allow 1- and 2-residue gaps in the protein to be filled.",
    )
    group.add_argument(
        "-L",
        "--keep-ligands-unchanged",
        action="store_true",
        help=cmdutil.numpy_doc_to_help(flare.ProteinPrep.keep_ligands_unchanged.__doc__, True),
    )
    group.add_argument(
        "-x",
        "--extract-ligands",
        help="Removes the ligand molecules and outputs them to the given file path.",
    )
    group.add_argument(
        "-m",
        "--disallow-small-side-chain-movements",
        action="store_true",
        help="Disallow the side chains from moving a small amount to relieve steric clashes in the input coordinates.",  # noqa: E501
    )
    group.add_argument(
        "-b",
        "--keep-atoms-from-residues-with-incomplete-backbone",
        action="store_true",
        help="Keep atoms from residues with incomplete backbone. The default is to remove all atoms from residues which have an incomplete backbone.",  # noqa: E501
    )
    group.add_argument(
        "-t",
        "--remove-posttranslational-modifications",
        action="store_true",
        help="Remove post-translational modifications of the protein (methylated lysines, glycosylations, acetylations, etc).",  # noqa: E501
    )
    group.add_argument(
        "--protonate-mode",
        choices=["all", "protein", "ligand-cofactor", "off"],
        help="Parts of the system that can change protonation/tautomerization state.",
    )
    group.add_argument(
        "--allow-flips",
        action="store_true",
        help=cmdutil.numpy_doc_to_help(flare.ProteinPrep.allow_flips.__doc__, True),
    )
    group.add_argument(
        "--residues-to-modify",
        type=str,
        help="Residues to include in Protein Prep. Selects all residues that match the provided string. See `cresset.flare.ResidueList.find` for a description of the string format.",  # noqa: E501
    )

    # Other settings
    group = parser.add_argument_group("general options")
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        default=prep.local_engine_count,
        help="Set the number of local engines to start. Use 0 for no local processing: you must specify some remote engines with -g if you do so.",  # noqa: E501
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker in the format hostname:port. Field Engines which have been registered with the Broker will be used during the calculation. The CRESSET_BROKER environment variable can be used instead of this option.",  # noqa: E501
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )
    args = parser.parse_args(argv)

    return args


def _configure_process(prep, project, args):
    """Configure `prep` using the command line arguments."""
    if args.protein:
        # Read the proteins into the project
        project.proteins.extend(flare.read_file(args.protein, args.input))
    else:
        # If there are no proteins read from stdin
        file_format = args.input if args.input else "pdb"
        project.proteins.extend(flare.read_file(sys.stdin, file_format))

    if not project.proteins:
        raise ValueError("no protein molecules were read from the input")
    if len(project.proteins) > 1:
        print(
            "The protein contains multiple models. Only the first model will be prepared.",
            file=sys.stderr,
        )
        del project.proteins[1:]

    # Setup the process
    prep.proteins = [project.proteins[0]]
    if args.do_not_cap_chains:
        prep.cap_mode = flare.ProteinPrep.CappingMode.NoCapping
    if args.do_not_fill_gaps:
        prep.fill_small_gaps = False
    prep.keep_ligands_unchanged = args.keep_ligands_unchanged
    prep.allow_small_side_chain_movements = not args.disallow_small_side_chain_movements
    prep.remove_atoms_from_residues_with_incomplete_backbone = (
        not args.keep_atoms_from_residues_with_incomplete_backbone
    )
    prep.remove_posttranslational_modifications = args.remove_posttranslational_modifications

    prep.allow_flips = args.allow_flips

    if args.protonate_mode is not None:
        with cmdutil.CheckValue("protonate-mode"):
            if args.protonate_mode == "off":
                prep.protonate_mode = flare.ProteinPrep.ProtonateMode.Off
            elif args.protonate_mode == "protein":
                prep.protonate_mode = flare.ProteinPrep.ProtonateMode.Protein
            elif args.protonate_mode == "ligand-cofactor":
                prep.protonate_mode = flare.ProteinPrep.ProtonateMode.LigandAndCofactor
            elif args.protonate_mode == "all":
                prep.protonate_mode = flare.ProteinPrep.ProtonateMode.All

    if args.residues_to_modify is not None:
        with cmdutil.CheckValue("residues-to-modify"):
            residues = prep.proteins[0].residues.find(args.residues_to_modify)
            if len(residues) == 0:
                raise ValueError(f"failed to find residues that match '{args.residues_to_modify}'.")
            if args.verbose:
                print(f"Residues to modify: {len(residues)}", file=sys.stderr)
            prep.residues_to_modify = residues

    if args.ph is not None:
        with cmdutil.CheckValue("ph"):
            prep.pH = args.ph

    with cmdutil.CheckValue("localengines"):
        prep.local_engine_count = args.localengines


def _write_results(prep, project, args):
    """Write the prep protein and any extracted ligands."""
    if args.extract_ligands:
        try:
            ligand_residues = []
            for sequence in prep.proteins[0].sequences:
                if sequence.type == flare.Sequence.Type.Ligand:
                    ligand_residues.extend(sequence)

            ligands = []
            for ligand_residue in ligand_residues:
                ligand = project.ligands.append(ligand_residue)
                prep.proteins[0].residues.remove(ligand_residue)
                ligands.append(ligand)

            suffix = pathlib.Path(args.extract_ligands).suffix
            ligand_format = suffix[1:] if suffix else "sdf"
            flare.write_file(args.extract_ligands, ligands, ligand_format)
        except Exception as err:
            print(err, file=sys.stderr)
            # print the error and continue

    if args.project:
        try:
            project.save(args.project)
        except Exception as err:
            print(err, file=sys.stderr)
            # print the error and continue

    file_format = args.output if args.output else "pdb"
    prep.proteins[0].write_file(sys.stdout, file_format)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
