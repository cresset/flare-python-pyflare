# Changelog

## 2025-01 for Flare 10.0
### Added
- Script `abferun.py` for running Absolute FEP calculations (abferun.py).
- Option `--membrane` to the protein dynamics and FEP. (dynamics.py, feprun.py)
- Option `--additional-ring-conformation-sampling` and `--timeout` to ligand docking (docking.py)
- Option `--gcncmc-sphere-center` to GIST (gist.py)
- Options `--protonate-mode`, `--allow-flips` and `--residues-to-modify` to protein prep (proteinprep.py)

## 2024-06 for Flare 9.0
### Added
- Script `mmgbsadynamics.py` script for running MM/GBSA calculations on dynamics proteins. (mmgbsadynamics.py)
- Script `sparkcl.py` script which is a drop-in replacement to the Spark sparkcl binary. (sparkcl.py)
- Script `sparkdb.py` script which is a drop-in replacement to the Spark sparkdb binary. (sparkdb.py)
- Script `sparkdbupdate.py` script which is a drop-in replacement to the Spark sparkdbupdate binary. (sparkdbupdate.py)
- Option `--membrane` to FEP calculations. (feprun.py)
- Option `--remove-posttranslational-modifications` to protein prep. (proteinprep.py)
- Option `--constrain` to the protein dynamics and minimization script. (dynamics.py, minimization.py)
- Option `--constraint-file` to load the dynamics constraint from a file. (dynamics.py, minimization.py)
- Option `--verbose` to several scripts.

### Changed
- Fixed feprun.py ignoring the solvent box shape when using the `--config` option. (feprun.py)
- Fixed dynamics.py `--gcncmc-ligand-residue` failing if a ligand was also specified. (dynamics.py)
- Fixed error in gist.py `--gcncmc-ligand-residue` that caused simulation to be shorter than it should have been. (gist.py)
- Options `--simulation-length` and `--simulation-length-delta-q` now also accepts 2 floats
  which allow different simulation lengths to be set for the bound and free structures. (feprun.py)

## 2024-01 for Flare 8.0
### Changed
- Improved help text for grid generation. (qm.py)
- Ligand preparation now removes salts from the molecule. (ligprep.py)
- Added the options for configuring GCNCMC in GIST. (gist.py)
- Option `--simulation-length-delta-q` to configure the length links are run for in nanoseconds for links where there is a charge perturbation. (feprun.py)
- Options `--openmm-temperature`, `--openmm-pressure`, `-openmm-non-bonded-method` and `--openmm-non-bonded-cutoff` added to FEP. (feprun.py)
- Option `--gcncmc-sphere-center` to set the center of the GCNCMC sphere. (feprun.py)
- The default chain types to use in a docking calculation have changed to "protein", "other" and "water". Before, only the "protein" chain was used. (docking.py)
- The default chain types to use in an alignment calculation have changed to "protein", "other" and "water". Before, the "protein" and "other" chains were used. (align.py)

## 2023-05-10 for Flare 7.0
### Added
- Script for running QM calculations. (qm.py)
- Script for ligand preparation. (ligprep.py)
- The "DFT//GFN2-xTB" level of theory may now be used when generating custom parameters. (dynamics.py, feprun.py, minimizeligands.py, minimization.py)
- Added the following options for configuring GCNCMC `--gcncmc-sampling-protocol`, `--gcncmc-sphere-buffer`, `--gcncmc-num-moves` and `--gcncmc-time-interval`. (dynamics.py, feprun.py, minimization.py)
- Option `--resume` which continues a cancelled dynamics calculation (dynamics.py)
- Option `--force-dynamics` causes high-temperature dynamics to be run regardless of whether a flexible ring was found in the molecule. (confhunt.py)
- Ensemble docking can now be performed by specifying `--protein` multiple times. (docking.py)
- Ensemble covalent docking can now be performed by specifying `--covalent-residue` multiple times. (docking.py)
- Option `--output` allows the field descriptors to be written in SDF (the default) or CSV file format. (fielddescriptors.py)
- Option `--input-ignore-errors` will cause the script to skip invalid input molecules and print a warning to `stderr`. The default behavior is to stop the script if an invalid molecule is detected. (align.py, confhunt.py, docking.py, minimizeligands.py, popto3d.py, qsarscore.py)

### Changed
- Many of the short options have been updated in the scripts.
- The order the options are listed in `--help` output has changed for all of the scripts.
- Option `--list-open-versions` now lists both the name and the path to the force field file (dynamics.py, feprun.py, minimizeligands.py, minimization.py, gist.py)
- The "Protein" SDF tag is now outputted by the docking script. This tag contains the name of the protein which the ligand was docked to. (docking.py)
- When continuing a partially calculated FEP project the `--continue` option must now be given. Previously FEP calculations could be continued without passing the option. Now the script will error if the calculation is partially complete and neither `--continue` or `--delete` is not given. (feprun.py)

## 2022-11-15 for Flare 6.1
### Added
- Script to pop molecules to 3D (popto3d.py)
- Option `--disable-local-gpu` which disables GPU calculations from running locally (dynamics.py, feprun.py, minimizeligands.py, minimization.py, gist.py)
- Option `--open-custom-parameters-level-of-theory` to set the level of theory to "ANI-2x" or "GFN2-xTB" (dynamics.py, feprun.py, minimizeligands.py, minimization.py, gist.py)

## 2022-06-14 for Flare 6.0
### Added
- Molecules in smi format can now be docked (docking.py)
- Option `--flickering-water` which allows waters to be marked as optional in docking calculations (docking.py)
- Option `--frozen-torsion` which allows torsions in a ligand to be frozen in docking calculations (docking.py)
- Options `--disable-open-custom-parameters` and `--open-custom-parameters-num-rotamers`. Calculations
which use the Open Force Field now have custom parameters for the ligand generated by default.
These options can be used to disable custom parameters or change the number of rotamers (dynamics.py, feprun.py, minimizeligands.py, minimization.py, gist.py)
- Script to generate Cresset's field descriptors which can be used with machine learning libraries (fielddescriptors.py)
- Option `--membrane` which runs a dynamics calculation with a membrane (dynamics.py)
- Option `--solvent-box-ionic-strength` to dynamics, FEP and GIST calculations (dynamics.py, feprun.py, gist.py)

### Changed
- For calculations which require a GPU if the GPU ID is not specified then the calculation will
autodetect which GPU to use.
- The `--gpu-platform` and `--gpu-device-id` command line arguments have been replaced with
`--opencl-platform-index`, `--opencl-device-index` and `--cuda-device-index`.
- The TIP4P water model has been replaced with TIP4P/2005 (dynamics.py, feprun.py, gist.py)

### Removed
- Flare has transitioned from using the Dlib library to scikit-learn for creating its machine learning models.
As a result of this the ability to build machine learning models has been removed from qsarbuild.py.
Instead, fielddescriptors.py can be used to generate field descriptors which can be used to
generate machine learning models using scikit-learn or other machine learning programs.
- The ability to generate and use custom GAFF and GAFF2 parameters. Instead, the ability to generate Open Force Field custom parameters has been added.  (dynamics.py, feprun.py, minimizeligands.py, minimization.py, gist.py, waterswap.py)

## 2021-06-28 for Flare 5.0
### Added
- Option `--quality score-fixed` which stops a pose from moving when scoring it (docking.py)
- Added support for minimizing ligands and proteins using the AMBER or Open force fields (minimizeligands.py, minimization.py)
- Added `smi` file format support for conf hunt and align (align.py, confhunt.py)
- Options `--openmm-temperature`, `--openmm-pressure`, `--openmm-non-bonded-method`, `--openmm-non-bonded-cutoff` and `--openmm-hydrogen-mass` options to dynamics and fep calculations (dynamics.py, feprun.py)
- Options `--solvent-box-shape` and `--solvent-model` to dynamics and fep calculations (dynamics.py, feprun.py)
- Option `--verbose` option which prints the calculation log to stderr (align.py, confhunt.py, docking.py, dynamics.py, electrostaticcomplementarity.py, fepcreate.py, feprun.py, minimization.py, minimizeligands.py, proteinprep.py, rism.py, waterswap.py)
- Options `--do-not-require-cycles`, `--add-intermediates` and `--minimum-link-score-for-intermediate` to control how the FEP graph is generated (fepcreate.py)
- Option `--protein-chain-types` which sets if ligand, water, protein or other chain types should be included in a calculation (align.py, docking.py, dynamics.py, fepcreate.py, minimization.py, minimizeligands.py, rism.py, waterswap.py)
- Option `--reference-weight` which sets the weight of each reference molecule being used in an alignment (align.py)
- Script for building QSAR models (qsarbuild.py)
- Script for fitting ligands to a QSAR model (qsarscore.py)
- Script for creating pharmacophores (template.py)
- Script for running a GIST calculation (gist.py)

### Changed
- Option `--docking-constraint` to support additional types of constraints (docking.py)

### Removed
- Removed support for writing the `cube` grid file format (surfaces.py)
- Removed the manywaterswap.py example

## 2020-08-17 for Flare 4.0
### Changed
 - 3D RISM calculations now use all fragments of the molecule except for water. Previously ligands, cofactors and ions were ignored (rism.py)
 - Fixed bug where ligands would fail to align if were not in the first batch (align.py)
 - Improved the error message shown when a calculation fails

## 2020-05-02 for Flare 4.0
### Added
- Option `--no-invert` which disables the inversion of achiral molecules (align.py)
- Option `--field-constraint` which allow constraints to be placed on field points (align.py)
- Added "Conformer", "Ref", "Confs", "Alns", and "Filename" SDF tags to the results (align.py)
- Added "Confs" and "Filename" SDF tags to the results (confhunt.py)
- Option `--write-grid` which saves the docking grid to a file (docking.py)
- Option `--grid-from-file` which reads the docking grid from a file (docking.py)
- Option `--csv` which writes the docking scores to the given csv file (docking.py)
- Option `--max-constraint-penalty` which sets the maximum docking constraint penalty that will be tolerated (docking.py)
- Added "Poses" and "Filename" SDF tags to the results (docking.py)
- Option `--small-molecule-force-field` which sets if the Amber or Open Force Field should be used for small molecules (dynamics.py, feprun.py, waterswap.py)
- Option `--list-open-versions` which prints the list of the versions of the Open Force Field which can be used in the calculation. (dynamics.py, feprun.py, waterswap.py)
- Option `--open-version` which sets the version of the Open Force Field should be use. (dynamics.py, feprun.py, waterswap.py)
- Option `--list-custom-parameters"` which prints the list of the custom-parameters which can be used in the calculation. (dynamics.py, feprun.py, waterswap.py)
- Option `--amber-custom-parameter"` which sets the custom-parameters which are used in the calculation. (dynamics.py, feprun.py, waterswap.py)
- Added "Filename" SDF tag to the results (electrostaticcomplementarity.py, minimizeligands.py)
- Option `--calculation-mode` which sets if the calculation should be run in benchmark or production mode. (fepcreate.py)
- Option `--max-dist-to-actives` which sets the maximum distance of a molecule to a known active. (fepcreate.py)
- Option `--star` which generates a star graph. (fepcreate.py)
- Option `--intermediate-results-directory` which will cause the intermediate results to be written to the specified directory and will not be deleted when the calculation finishes. (feprun.py)
- Option `--quick-mode` which will cause links to be calculated in only one direction. (feprun.py)
- Option `--do-not-fill-gaps` which disables the filling of 1- and 2-residue gaps. (proteinprep.py)
- Option `--keep-ligands-unchanged` which will prevent the protonation and tautomer state of the ligands from changing. (proteinprep.py)

### Changed
- Updates to support the Flare 4.0 Python API
- Changed the "Conformer#" SDF tag name to "Conformer" in results (confhunt.py)
- Option `--amber-solvate-box-buffer` has been renamed to `--solvate-box-buffer` as this option also applies to the Open Force Field. (dynamics.py, feprun.py, waterswap.py)
- fepcreate.py now defaults to production mode. In the previous version the default and only available option was benchmark mode.
- How FEP calculates the predicted activity values has changed so that parent ligands are not used in the calculation.
fepview.py has changed so that parent ligands are not printed in the table of activity values.
- Option `--cap-chains` has been replaced with `--do-not-cap-chains` as the default is to cap chains. (proteinprep.py)

### Removed
- Removed "Molecule" SDF tag from the results as the same value is in the SDF title field (confhunt.py)
- Removed "Ligand Title" SDF tag from the results as the same value is in the SDF title field (confhunt.py)

## 2020-01-30 for Flare 3.0
### Added
- Option `--disallow-small-side-chain-movements` which disallows small side chain movements when preparing proteins (proteinprep.py)
- Option `--keep-atoms-from-residues-with-incomplete-backbone` which does not delete atoms in broken residues (proteinprep.py)
- Option `--docking-constraint` which allows adding docking constraints (docking.py)
- Option `--covalent-residue` which performs covalent docking (docking.py)
- Option `--template-ligand` which performs template docking (docking.py)
- Option `--number-of-runs` which sets the number of docking runs to run (docking.py)
- Option `--batch-size` which allows ligands to be docked in batches to reduce the memory usage when docking tens of thousands of ligands (docking.py)
- Option `--amber-gaff-version` which allows the Amber GAFF version to be set (waterswap.py, rism.py)
- Script which runs dynamics on a protein (dynamics.py)
- Script which creates a FEP project (fepcreate.py)
- Script which runs a FEP calculation (feprun.py)
- Script which shows the results of a FEP calculation (fepview.py)
- Script which minimizes ligands within a protein (minimizeligands.py)
- Script which generates conformations for ligands (confhunt.py)
- Script which aligns ligands to a reference (align.py)

### Changed
- Updates to support the Flare 3.0 Python API
- The electrostatic complementarity SDF tags have been renamed from "Complementarity",
"Complementarity r", "Complementarity rho" to "EC", "EC r" and "EC rho" (electrostaticcomplementarity.py)

### Removed
- Option `--rotate-conjugated-bonds`, Flare 3.0 will autodetect the correct value for this setting (docking.py)

## 2018-07-25 for Flare 2.0
### Added
- Script which docks ligands to a protein (docking.py)
- Script which calculates ligands Electrostatic Complementarity (TM) to a protein (electrostaticcomplementarity.py)
- Example on how to split a WaterSwap job then merge the results (manywaterswap.py)
- Script to minimize a protein (minimization.py)
- Script which prepares a protein (proteinprep.py)
- Script which runs 3D-RISM on a protein (rism.py)
- Script which writes field surfaces for a protein (surfaces.py)
- Script which runs WaterSwap on a protein (waterswap.py)
