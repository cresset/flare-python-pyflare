#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Writes protein field surfaces to stdout.

examples:
  pyflare surfaces.py protein.pdb --output grd --type pos > surface.grd
    Writes the positive surface in grd format.

  pyflare surfaces.py protein.pdb --output ccp4 --type vdw > surface.ccp4
    Writes the Van der Waals surface in ccp4 format.

  pyflare surfaces.py --help
    Prints the help text including all available options and exits.
"""
import sys
import argparse
import traceback

from cresset import flare


def main(argv):
    """Run the protein prep process using the command line args."""
    try:
        project = flare.Project()

        args = parse_args(argv)
        configure_project(project, args)
        _write_results(project, args)

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def parse_args(argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )
    parser.add_argument(
        "protein",
        nargs="?",
        help="The protein file to read. If no files are listed then the proteins are read from stdin. Surfaces will only be written for the first protein.",  # noqa: E501
    )

    group = parser.add_argument_group("input/output")
    group.add_argument(
        "-i",
        "--input",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for reading the protein. The default is to autodetect when reading from files.",  # noqa: E501
    )
    group.add_argument(
        "-o",
        "--output",
        choices=["grd", "ccp4", "mgd"],
        default="ccp4",
        type=str.lower,
        help="Set the file format for writing the surface. The default is 'ccp4'.",
    )

    group = parser.add_argument_group("surface")
    group.add_argument(
        "-t",
        "--type",
        choices=["pos", "neg", "vdw", "hyd"],
        default="pos",
        type=str.lower,
        help="Set the type of surface to export.",
    )

    group = parser.add_argument_group("general options")
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )

    args = parser.parse_args(argv)

    return args


def configure_project(project, args):
    """Read the protein."""
    if args.protein:
        # Read the proteins into the project
        project.proteins.extend(flare.read_file(args.protein, args.input))
    else:
        # If there are no proteins read from stdin
        file_format = args.input if args.input else "pdb"
        project.proteins.extend(flare.read_file(sys.stdin, file_format))

    if not project.proteins:
        raise ValueError("no protein molecules were read from the input")
    if len(project.proteins) > 1:
        raise ValueError("only one protein can be specified")


def _write_results(project, args):
    """Write the surface."""
    protein = project.proteins[0]

    surface_type = None
    if args.type == "neg":
        surface_type = flare.SurfaceType.FieldNeg
    elif args.type == "vdw":
        surface_type = flare.SurfaceType.FieldVdW
    elif args.type == "hyd":
        surface_type = flare.SurfaceType.FieldHyd
    else:
        surface_type = flare.SurfaceType.FieldPos

    surface = protein.surfaces.new(surface_type, protein.sequences)

    surface_bytes = surface.write_surface_bytes(args.output)
    sys.stdout.flush()
    sys.stdout.buffer.write(surface_bytes)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
