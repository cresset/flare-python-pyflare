#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Runs GIST on a protein and optionally a ligand.

GIST is run on the structure in the specified PDB file and output surface files
from the GIST run are written to the current working directory by default. A
directory for the output files to be written to can be specified with the
--directory option. The region of the protein to run GIST on is defined by
--grid or --reference. The structure used in the GIST run can also be saved to a
Flare project if the --project option is specified. GIST is performed on GPU by
default.

To run on CPU instead, --openmm-use-cpu can be specified. The final part of the
GIST calculation is run locally so that it can access the trajectory file.
Therefore local processing cannot be disabled with --localengines 0.

The protein should be prepared before running GIST. Use the proteinprep.py
script to do this.

For example:

  pyflare proteinprep.py protein.pdb > prep_protein.pdb
  pyflare gist.py prep_protein.pdb --reference lig.sdf --ligand lig.sdf

    First, prepare the protein. Then run GIST on the structure in the
    protein.pdb file plus lig.sdf using GPU resources.  The grid is defined by
    the size and location of the lig.sdf structure.  Writes output files to the
    current working directory.

  pyflare gist.py protein_and_ligand.pdb --directory output --project proj.flr
      --grid=-8.5,19.5,-35.4,0.8,32.6,-25.4 --openmm-use-cpu

    Runs GIST on the structure in the protein_and_ligand.pdb file using CPU
    resources.  The grid is defined by the --grid option.  Writes output files
    to the directory called output and saves the structure in a Flare project
    called proj.flr.

  pyflare gist.py --help
    Prints the help text including all available options and exits.

see also:
  https://amberhub.chpc.utah.edu/gist/
"""
import sys
import os
import argparse
import traceback
import re

from cresset import flare

from _internal import cmdutil


def main(argv):
    """Run the GIST process using the command line args."""
    try:
        gist = flare.Gist()

        args = _parse_args(gist, argv)
        project = _configure_process(gist, args)
        logging_context = cmdutil.LoggingContext(project.log, verbose=args.verbose)
        cmdutil.start_process(gist, args.broker, args.verbose)
        cmdutil.wait_for_finish(gist, logging_context=logging_context)
        cmdutil.check_errors(gist)
        if not gist.is_cancelled():
            _write_results(gist, project, args)

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(gist, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )
    # Input settings
    parser.add_argument(
        "protein",
        nargs="?",
        help="A PDB file containing the protein to perform GIST "
        "on. If this is not provided, the protein is read from "
        "stdin. Only one protein should be provided.",
    )

    group = parser.add_argument_group("input")
    group.add_argument(
        "-C",
        "--protein-chain-types",
        metavar="protein,water,other,ligand",
        default="protein,water,other,ligand",
        type=str.lower,
        help="Select the types of chains to include in the calculation as a comma separated list. 'p', 'w', 'o' and 'l' can be used as a shorthand for 'protein', water', 'other', 'ligand'. The default is to include all the protein, water and other chains.",  # noqa: E501
    )
    group.add_argument(
        "-m",
        "--ligand",
        help="The optional ligand. If GCNCMC is enabled then it will be centered on this ligand.",
    )  # noqa: E501
    group.add_argument(
        "-u",  # matches --gcncmc-ligand-residue in dynamics.py
        "--ligand-residue",
        help='The optional ligand, specified using residue information, for example, "LIG" or "A LIG 15A".  If GCNCMC is enabled then it will be centered on this ligand.',  # noqa: E501
    )
    group.add_argument(
        "-i",
        "--input",
        choices=["pdb", "sdf", "mol2", "xed"],
        default="sdf",
        type=str.lower,
        help="Set the file format for reading the ligand. The default is sdf.",
    )
    group.add_argument(
        "-G",
        "--grid",
        help="The region of the protein to perform the GIST analysis on. This is defined as 2 opposite vertices in the format min_x,min_y,min_z,max_x,max_y,max_z. For example --grid=-3,-6.2,2,12,8.6,5.",  # noqa: E501
    )
    group.add_argument(
        "-r",
        "--reference",
        help="The region of the protein to perform the GIST analysis on defined by the location and size of the first molecule in the file. Note that the molecule is not actually added to the system.",  # noqa: E501
    )

    # Output settings
    group = parser.add_argument_group("output")
    group.add_argument(
        "-P",
        "--project",
        help="Write the Flare project file with the results to the given file path.",
    )
    group.add_argument(
        "-o",
        "--output",
        choices=["grd", "ccp4", "mgd", "dx"],
        default="dx",
        type=str.lower,
        help="Set the file format for writing the surface. The default is 'dx'. Due to limitation of the ccp4 file format, the grids might be up to half a grid width out.",  # noqa: E501
    )
    group.add_argument(
        "-d",
        "--directory",
        default=os.getcwd(),
        help="Write the the output surfaces to the given directory path.",
    )

    # GIST settings
    group = parser.add_argument_group("GIST")
    group.add_argument(
        "--grid-spacing",
        type=float,
        default=gist.grid_spacing,
        help=cmdutil.numpy_doc_to_help(flare.Gist.grid_spacing.__doc__, True),
    )

    # ForceField settings
    group = parser.add_argument_group("force field")
    cmdutil.add_dynamics_force_field_arguments(group, allow_open_custom_parameters=False)

    # System settings
    group = parser.add_argument_group("system options")
    group.add_argument(
        "-b",
        "--solvate-box-buffer",
        type=float,
        default=gist.solvate.box_buffer,
        help=cmdutil.numpy_doc_to_help(flare.Solvate.box_buffer.__doc__, doc_has_defaults=False),
    )

    group.add_argument(
        "-I",
        "--solvate-box-ionic-strength",
        type=float,
        default=gist.solvate.box_ionic_strength,
        help=cmdutil.numpy_doc_to_help(
            flare.Solvate.box_ionic_strength.__doc__, doc_has_defaults=False
        ),
    )

    group.add_argument(
        "-w",
        "--solvent-model",
        choices=[
            "tip3p",
            "spce",
            "opc",
            "opc3",
            "spceb",
            "tip3pfb",
            "tip4p2005",
            "tip4pd",
            "tip4pfb",
            "tip4pew",
        ],
        help="The water model to use. Explicit (tip4pew) solvent is used by " "default.",
    )

    # OpenMM settings (GPU/CPU)
    group = parser.add_argument_group("GPU/CPU")
    cmdutil.add_gpu_arguments(group, gist)
    cmdutil.add_cpu_arguments(group, gist)

    # OpenMM settings
    group = parser.add_argument_group("openmm")
    group.add_argument(
        "-s",
        "--simulation-length",
        type=float,
        default=gist.simulation_length,
        help=cmdutil.numpy_doc_to_help(flare.Gist.simulation_length.__doc__, True),
    )
    cmdutil.add_openmm_common_arguments(group, gist)
    cmdutil.add_openmm_equilibration_arguments(group, gist)
    cmdutil.add_openmm_time_step_arguments(group, gist)
    cmdutil.add_openmm_advanced_arguments(group, gist)

    # GCNCMC settings
    group = parser.add_argument_group("gcncmc arguments")
    group.add_argument(
        "--gcncmc-center-on-residue",
        help="Set a residue in the protein to use as the center of the GCNCMC "
        " sphere. If not set then GCNCMC sphere is centred on the ligand.",
    )
    group.add_argument(
        "--gcncmc-sphere-center",
        nargs=3,
        type=float,
        help="Provide 3 arguments as the x/y/z coordinates to use as the center of the GCNCMC sphere.",  # noqa: E501
    )
    cmdutil.add_gcncmc_arguments(group, gist, equilibration_only=True)

    # Other settings
    group = parser.add_argument_group("general options")
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        default=gist.local_engine_count,
        help="Set the number of local engines to start. At least 1 "
        "local engine must be set as the final part of the calculation "
        "must be run locally. "
        "Remote engines may be specified with -g.",
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker "
        "in the format hostname:port. Field Engines that have "
        "been registered with the Broker will be used during the "
        "calculation. The CRESSET_BROKER environment variable "
        "can be used instead of this option.",
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )
    args = parser.parse_args(argv)

    return args


def _configure_process(gist, args):
    """Configure `gist` using the command line arguments."""
    if args.list_open_versions:
        cmdutil.print_open_force_field_versions_and_exit()

    # Create the output directory now in case it fails
    if args.directory:
        os.makedirs(args.directory, exist_ok=True)

    project = flare.Project()

    file_format = "pdb"
    if args.protein:
        # Read the molecules into the project
        project.proteins.extend(flare.read_file(args.protein, file_format))
    else:
        # If there are no molecules read from stdin
        project.proteins.extend(flare.read_file(sys.stdin, file_format))

    if not project.proteins:
        raise ValueError("no molecules were read from the input")
    if len(project.proteins) > 1:
        raise ValueError("only one molecule can be specified")

    # Configure the grid
    if sum(map(bool, [args.reference, args.grid])) > 1:
        raise ValueError("only one of --reference and --grid should be set")

    if args.grid:
        grid_list = args.grid.split(",")
        if len(grid_list) != 6:
            raise ValueError("expected grid to be 6 float values separated by commas")
        top_left = (grid_list[0], grid_list[1], grid_list[2])
        bottom_right = (grid_list[3], grid_list[4], grid_list[5])
        gist.grid_box = (top_left, bottom_right)

    elif args.reference:
        role = project.roles.append(
            "Reference", "The molecule which defines the region to perform the GIST analysis on"
        )  # noqa: E501
        role.ligands.extend(flare.read_file(args.reference, args.input))
        if not role.ligands:
            raise ValueError(f"file '{args.reference}' does not contain any molecules")
        if len(role.ligands) > 1:
            print(
                "Warning: multiple references specified, only the first reference molecule will "
                + "be used to define the region to perform the GIST analysis on",
                file=sys.stderr,
            )

        del role.ligands[1:]
        gist.grid_box = role.ligands[0]
    else:
        raise ValueError("--reference or --grid must be set")

    gist.protein = project.proteins[0]
    gist.protein.is_prepared = True  # Suppress warnings
    gist.sequences = cmdutil.chain_types_to_sequences(gist.protein, args.protein_chain_types)

    # Load the optional ligand
    if args.ligand and args.ligand_residue:
        raise ValueError("both --ligand and --ligand-residue cannot be given")
    elif args.ligand:
        role = project.roles.append("Ligand", "The ligand to include in the GIST analysis")
        role.ligands.extend(flare.read_file(args.ligand, args.input))
        if len(role.ligands) > 1:
            raise ValueError("only one ligand can be specified")
        if len(role.ligands) == 1:
            gist.ligand = project.ligands[0]
    elif args.ligand_residue:
        gist.ligand = cmdutil.convert_residue_to_ligand(gist.protein, args.ligand_residue, "GIST")

    with cmdutil.CheckValue("grid-spacing"):
        gist.grid_spacing = args.grid_spacing

    cmdutil.configure_dynamics_force_field_process(gist, args, allow_open_custom_parameters=False)

    with cmdutil.CheckValue("solvate-box-buffer"):
        gist.solvate.box_buffer = args.solvate_box_buffer

    with cmdutil.CheckValue("solvate-box-ionic-strength"):
        gist.solvate.box_ionic_strength = args.solvate_box_ionic_strength

    cmdutil.configure_gpu_process(gist, args)
    cmdutil.configure_cpu_process(gist, args)
    cmdutil.configure_openmm_common_process(gist, args)
    cmdutil.configure_openmm_equilibration_process(gist, args)
    cmdutil.configure_openmm_time_step_process(gist, args)
    cmdutil.configure_openmm_advanced_process(gist, args)
    cmdutil.configure_gcncmc_process(gist, args)

    if gist.gcncmc.sampling_protocol != flare.DynamicsGCNCMC.SamplingProtocol.Off:
        if not args.gcncmc_center_on_residue and not args.gcncmc_sphere_center and not gist.ligand:
            raise ValueError("a ligand residue or sphere center must be specified for GCNCMC")
        if args.gcncmc_center_on_residue:
            gist.gcncmc.center_on = cmdutil.calculate_center_of_residue(
                gist.protein, args.gcncmc_center_on_residue
            )
            gist.gcncmc.sphere_buffer = (
                gist.gcncmc.sphere_buffer
                + cmdutil.calculate_radius_of_residue(gist.protein, args.gcncmc_center_on_residue)
            )
        else:
            gist.gcncmc.center_on = args.gcncmc_sphere_center

    with cmdutil.CheckValue("simulation-length"):
        gist.simulation_length = args.simulation_length

    if args.solvent_model == "tip3p":
        gist.solvate.model = flare.SolventModel.ExplicitTIP3P
    elif args.solvent_model == "spce":
        gist.solvate.model = flare.SolventModel.ExplicitSPCE
    elif args.solvent_model == "opc":
        gist.solvate.model = flare.SolventModel.ExplicitOPC
    elif args.solvent_model == "opc3":
        gist.solvate.model = flare.SolventModel.ExplicitOPC3
    elif args.solvent_model == "tip3pfb":
        gist.solvate.model = flare.SolventModel.ExplicitTIP3Pfb
    elif args.solvent_model == "spceb":
        gist.solvate.model = flare.SolventModel.ExplicitSPCEb
    elif args.solvent_model == "tip4p2005":
        gist.solvate.model = flare.SolventModel.ExplicitTIP4P2005
    elif args.solvent_model == "tip4pd":
        gist.solvate.model = flare.SolventModel.ExplicitTIP4Pd
    elif args.solvent_model == "tip4pfb":
        gist.solvate.model = flare.SolventModel.ExplicitTIP4Pfb
    elif args.solvent_model == "tip4pew":
        gist.solvate.model = flare.SolventModel.ExplicitTIP4Pew

    with cmdutil.CheckValue("localengines"):
        if args.localengines < 1:
            raise ValueError("1 or more local engines are required")
        gist.local_engine_count = args.localengines

    return project


def _write_results(gist, project, args):
    """Write the results."""
    if args.project:
        try:
            project.save(args.project)
        except Exception as err:
            print(err, file=sys.stderr)
            # print the error and continue

    protein = gist.result_protein()
    directory = args.directory if args.directory is not None else os.getcwd()
    for surface in protein.surfaces:
        filename = f"{protein.title}.{surface.name}.{args.output}"
        filename = filename.replace("Δ", "delta")
        filename = re.sub('[<|>|:|"|/|\\\\:\\?:\\*]', "_", filename)
        path = os.path.join(directory, filename)
        surface.write_surface_file(path, args.output)

    print(f"The surface files have been written to directory {directory}.")


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
