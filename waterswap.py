#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Runs WaterSwap on a protein and saves the results to a project file.

The protein should be prepared before running WaterSwap. Use the proteinprep.py script
to do this. For example:

  pyflare proteinprep.py protein.pdb | pyflare waterswap.py -P project.flr --residue LIG

At the end of each iteration the results are written to the project given by the
'-P/--project' option. Once the first iteration is completed this script can be stopped
via 'Ctrl+C' and restarted at a later date by using the '--restart' option.

examples:
  pyflare waterswap.py -P project.flr --ligand ligand.sdf protein.pdb
    Run WaterSwap on the protein with the ligand. The results are written to
    project.flr.

  pyflare waterswap.py -P project.flr --residue LIG protein.pdb
    Run WaterSwap on the protein. The protein residue 'LIG' is used as the ligand.
    The results are written to project.flr.

  pyflare waterswap.py -P project.flr --top protein.top --crd protein.crd --top-ligand LIG
    Run WaterSwap on the protein within the topology and coordinate file.
    The protein residue 'LIG' is used as the ligand.
    The results are written to project.flr.

  pyflare waterswap.py -P project.flr --restart restart_project.flr
    Continue the WaterSwap calculation for the results stored in 'restart_project.flr'.
    If the project contains more than one result then the last result will be used.
    The results are written to project.flr.

  pyflare waterswap.py --help
    Prints the help text including all available options and exits.
"""
import sys
import argparse
import time
import traceback

from cresset import flare

from _internal import cmdutil


def main(argv):
    """Run the WaterSwap process using the command line args."""
    try:
        water_swap = flare.WaterSwap()

        args = _parse_args(water_swap, argv)
        project = _configure_process(water_swap, args)
        cmdutil.start_process(water_swap, args.broker, args.verbose)
        _wait_for_finish(water_swap, project, args)
        cmdutil.check_errors(water_swap)
        _write_results(project, args)

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(water_swap, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )
    parser.add_argument(
        "protein",
        nargs="?",
        help="The protein to run WaterSwap on. If not set the protein is read from stdin. Requires --ligand or --residue to also be set.",  # noqa: E501
    )
    group = parser.add_argument_group("input/output")
    group.add_argument(
        "-C",
        "--protein-chain-types",
        metavar="protein,water,other,ligand",
        default="protein,water,other,ligand",
        type=str.lower,
        help="Select the types of chains to include in the calculation as a comma separated list. 'p', 'w', 'o' and 'l' can be used as a shorthand for 'protein', water', 'other', 'ligand'. The default is to include all the chains.",  # noqa: E501
    )
    group.add_argument(
        "-m",
        "--ligand",
        help="The ligand molecule. Requires the --protein to have been set. This is mutual exclusivity with --residue.",  # noqa: E501
    )
    group.add_argument(
        "-r",
        "--residue",
        help='The name of the residue in the protein, for example "LIG" or "A LIG 15A". This is mutual exclusivity with --ligand.',  # noqa: E501
    )
    group.add_argument(
        "-i",
        "--input",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for reading the protein. The default is to autodetect.",
    )

    group.add_argument(
        "-t",
        "--top",
        help="The topology file of the protein to run WaterSwap on. Requires --crd and --top-ligand to be set.",  # noqa: E501
    )
    group.add_argument(
        "-c",
        "--crd",
        help="The coordinate file of the protein to run WaterSwap on. Requires --top and --top-ligand to be set.",  # noqa: E501
    )
    group.add_argument(
        "-n",
        "--top-ligand",
        help="The coordinate file of the protein to run WaterSwap on. Requires --top and --top-ligand to be set.",  # noqa: E501
    )

    group.add_argument(
        "-e", "--restart", help="A Flare project which contains the project to restart."
    )

    group.add_argument(
        "-P",
        "--project",
        required=True,
        help="Write the Flare project file with the results to the given file path.",
    )

    group = parser.add_argument_group("waterswap")
    group.add_argument(
        "--concurrent-tasks",
        type=int,
        default=water_swap.concurrent_tasks,
        help=cmdutil.numpy_doc_to_help(flare.WaterSwap.concurrent_tasks.__doc__),
    )
    group.add_argument(
        "-I",
        "--iterations",
        type=int,
        default=water_swap.iterations,
        help=cmdutil.numpy_doc_to_help(flare.WaterSwap.iterations.__doc__),
    )
    group.add_argument(
        "-T",
        "--max-threads",
        type=int,
        default=water_swap.max_threads,
        help=cmdutil.numpy_doc_to_help(flare.WaterSwap.max_threads.__doc__),
    )
    group.add_argument(
        "--stop-on-converge",
        action="store_true",
        help=cmdutil.numpy_doc_to_help(flare.WaterSwap.stop_on_converge.__doc__),
    )
    group.add_argument(
        "--water-monitor-distance",
        type=float,
        default=water_swap.water_monitor_distance,
        help=cmdutil.numpy_doc_to_help(flare.WaterSwap.water_monitor_distance.__doc__),
    )

    group = parser.add_argument_group("force field")
    cmdutil.add_dynamics_force_field_arguments(group, allow_open_custom_parameters=False)

    group = parser.add_argument_group("openmm")
    group.add_argument(
        "-b",
        "--solvate-box-buffer",
        type=float,
        default=water_swap.solvate.box_buffer,
        help=cmdutil.numpy_doc_to_help(flare.Solvate.box_buffer.__doc__, doc_has_defaults=False),
    )

    cmdutil.add_openmm_common_arguments(group, water_swap)
    cmdutil.add_openmm_fep_ws_arguments(group, water_swap)

    group.add_argument(
        "--openmm-use-gpu",
        action="store_true",
        help="Use the GPU if available during the OpenMM stage",
    )

    group = parser.add_argument_group("general options")
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        default=water_swap.local_engine_count,
        help="Set the number of local engines to start. Use 0 for no local processing: you must specify some remote engines with -g if you do so.",  # noqa: E501
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker in the format hostname:port. Field Engines which have been registered with the Broker will be used during the calculation. The CRESSET_WATERSWAP_BROKER environment variable can be used instead of this option.",  # noqa: E501
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )
    args = parser.parse_args(argv)

    return args


def _configure_process(water_swap, args):
    """Configure `water_swap` using the command line arguments."""
    if args.list_open_versions:
        cmdutil.print_open_force_field_versions_and_exit()

    project = flare.Project()

    protein_args_set = args.protein or args.ligand or args.residue or args.input
    file_args_set = args.top or args.crd or args.top_ligand
    restart_set = bool(args.restart)

    if bool(protein_args_set) + bool(file_args_set) + bool(restart_set) != 1:
        raise ValueError(
            "WaterSwap must be started from either a protein file, top+crd file or a Flare project"
        )

    # Configure from reading a protein and ligand files
    if protein_args_set:
        if args.ligand and args.residue:
            raise ValueError("either --ligand or --residue must be set, not both")
        elif not (args.ligand or args.residue):
            raise ValueError("either --ligand or --residue must be set")

        if args.protein:
            project.proteins.extend(flare.read_file(args.protein, args.input))
        else:
            # If the protein is not set read it from stdin
            file_format = args.input if args.input else "pdb"
            project.proteins.extend(flare.read_file(sys.stdin, file_format))

        if not project.proteins:
            raise ValueError("no protein molecules were read from the input")
        if len(project.proteins) > 1:
            raise ValueError("only one protein can be specified")

        water_swap.system.protein = project.proteins[0]
        water_swap.system.protein.is_prepared = True  # Suppress warnings
        water_swap.system.sequences = cmdutil.chain_types_to_sequences(
            water_swap.system.protein, args.protein_chain_types
        )

        if args.ligand:
            project.ligands.extend(flare.read_file(args.ligand, args.input))
            if not project.ligands:
                raise ValueError("no ligand molecules were read from the input")
            if len(project.ligands) > 1:
                raise ValueError("only one ligand can be specified")

            water_swap.system.ligand = project.ligands[0]
        else:
            residues = water_swap.system.protein.residues.find(args.residue)
            if not residues:
                raise ValueError("protein does not contain the given residue")
            if len(residues) > 1:
                raise ValueError(
                    "protein contains {} residues which match the given residue".format(
                        len(residues)
                    )
                )

            water_swap.system.ligand = residues[0]

    # Configure crd + top file
    elif file_args_set:
        water_swap.system = flare.WaterSwapFile()

        if not (args.top and args.crd and args.top_ligand):
            raise ValueError(
                "all --top, --crd, --top-ligand must be set if one of these options is set"
            )

        water_swap.system.top_file = args.top
        water_swap.system.crd_file = args.crd
        water_swap.system.ligand_residue = args.top_ligand
        water_swap.system.project = project

    # Restart file
    else:
        project = flare.Project.load(args.restart)

        # Use the restart file from the last protein which was created by WaterSwap
        water_swap_result = None
        for protein in project.proteins:
            water_swap_result = protein.water_swap_result

        if not water_swap_result:
            raise ValueError("project does not contain any WaterSwap results")

        water_swap.system = water_swap_result

    # Common setup
    with cmdutil.CheckValue("concurrent-tasks"):
        water_swap.concurrent_tasks = args.concurrent_tasks

    with cmdutil.CheckValue("iterations"):
        water_swap.iterations = args.iterations

    with cmdutil.CheckValue("max-threads"):
        water_swap.max_threads = args.max_threads

    water_swap.stop_on_converge = args.stop_on_converge

    with cmdutil.CheckValue("water-monitor-distance"):
        water_swap.water_monitor_distance = args.water_monitor_distance

    cmdutil.configure_dynamics_force_field_process(
        water_swap, args, allow_open_custom_parameters=False
    )

    with cmdutil.CheckValue("solvate-box-buffer"):
        if args.solvate_box_buffer is not None:
            water_swap.solvate.box_buffer = args.solvate_box_buffer

    cmdutil.configure_openmm_common_process(water_swap, args)
    cmdutil.configure_openmm_fep_ws_process(water_swap, args)

    with cmdutil.CheckValue("openmm-minimize-energy-max-iterations"):
        if args.openmm_minimize_energy_max_iterations is not None:
            water_swap.openmm.minimize_energy_max_iterations = (
                args.openmm_minimize_energy_max_iterations
            )

    with cmdutil.CheckValue("openmm-minimize-energy-tolerance"):
        if args.openmm_minimize_energy_tolerance is not None:
            water_swap.openmm.minimize_energy_tolerance = args.openmm_minimize_energy_tolerance

    if args.openmm_use_gpu:
        water_swap.openmm.processor_type = flare.WaterSwapOpenMM.ProcessorType.GPU

    with cmdutil.CheckValue("localengines"):
        water_swap.local_engine_count = args.localengines

    # Save the project now so if there are file permission problems
    # we get the error now instead of after the first iteration is done
    project.save(args.project)

    return project


def _wait_for_finish(water_swap, project, args):
    """Run the WaterSwap process while printing the progress."""
    logging_context = cmdutil.LoggingContext(project.log, verbose=args.verbose)
    last_complete_iteration = 0
    clear_progress_line = False
    try:
        while water_swap.is_running():
            time.sleep(10)
            if logging_context is not None and logging_context.verbose:
                cmdutil.print_project_logs(logging_context)
            else:
                cmdutil.print_progress(water_swap)
                clear_progress_line = True

            # Save the project after every iteration in case the job is killed
            water_swap_result = water_swap.result()
            if water_swap_result and water_swap_result.iterations > last_complete_iteration:
                project.save(args.project)
                last_complete_iteration = water_swap_result.iterations
    except KeyboardInterrupt:
        # On the first Ctrl+C try to shutdown cleanly
        # On the second Ctrl+C `water_swap.wait()` will raise an exception
        # and it is allowed to kill this script
        try:
            if clear_progress_line:
                print("", file=sys.stderr)
                clear_progress_line = False
            print("Calculation cancelled, shutting down FieldEngines, please wait", file=sys.stderr)
            water_swap.cancel()
            water_swap.wait()
        finally:
            # Even if there a second Ctrl+C save the project
            project.save(args.project)

    finally:
        if clear_progress_line:
            print("", file=sys.stderr)
        # if Ctrl+C is pressed try to write out the final log files
        if logging_context is not None and logging_context.verbose:
            cmdutil.print_project_logs(logging_context)


def _write_results(project, args):
    """Write the results."""
    print(f"Saving results to '{args.project}'", file=sys.stderr)
    project.save(args.project)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
