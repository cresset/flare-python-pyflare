#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Runs dynamics on a molecule from a PDB file and writes output files.

Dynamics is run on the structure in the specified PDB file and output files from the dynamics run
are written to the current working directory by default. A directory for the output files to be
written to can be specified with the --directory option. The structure used in the dynamics run can
also be saved to a Flare project if the --project option is specified and the output DCD file can
then be opened in the Flare project by clicking 'Open Trajectory' in the Dynamics dock. Dynamics is
performed on GPU by default. To run on CPU instead, --openmm-use-cpu can be specified.

The protein should be prepared before running dynamics. Use the proteinprep.py script to do this.
For example:

  pyflare proteinprep.py protein.pdb > prep_protein.pdb
  pyflare dynamics.py prep_protein.pdb


examples:
  pyflare dynamics.py protein_and_ligand.pdb --directory output --project proj.flr --openmm-use-cpu
    Runs dynamics on the structure in the protein_and_ligand.pdb file using CPU resources, writes
    output files to the directory called output and saves the structure in a Flare project called
    proj.flr.

  pyflare dynamics.py --help
    Prints the help text including all available options and exits.
"""
import sys
import os
import argparse
import traceback

from cresset import flare

from _internal import cmdutil


def main(argv):
    """Run the dynamics process using the command line args."""
    try:
        dynamics = flare.Dynamics()

        args = _parse_args(dynamics, argv)
        project = _configure_process(dynamics, args)
        logging_context = cmdutil.LoggingContext(project.log, verbose=args.verbose)
        cmdutil.start_process(dynamics, args.broker, args.verbose)
        cmdutil.wait_for_finish(dynamics, logging_context=logging_context)
        cmdutil.check_errors(dynamics)
        # The project (if requested) is still written if the calculation was cancelled
        _write_results(project, args)

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(dynamics, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawTextHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )
    parser.add_argument(
        "molecule",
        nargs="?",
        help="A PDB file containing the molecule to perform dynamics "
        "on. If this is not provided, molecules are read from "
        "stdin. Only one molecule should be provided.",
    )

    group = parser.add_argument_group("input/output")
    group.add_argument(
        "-C",
        "--protein-chain-types",
        metavar="protein,water,other,ligand",
        default="protein,water,other,ligand",
        type=str.lower,
        help="Select the types of chains to include in the calculation as a comma separated list. 'p', 'w', 'o' and 'l' can be used as a shorthand for 'protein', water', 'other', 'ligand'. The default is to include all the chains.",  # noqa: E501
    )
    group.add_argument(
        "-P",
        "--project",
        help="Write the Flare project file with the results to the given file path.",
    )
    group.add_argument(
        "-d",
        "--directory",
        default=os.getcwd(),
        help="Write the the output DCD and top files to the given directory path.",
    )

    group = parser.add_argument_group("force fields")
    cmdutil.add_dynamics_force_field_arguments(group)

    group = parser.add_argument_group("dynamics")
    group.add_argument(
        "-b",
        "--solvate-box-buffer",
        type=float,
        # A default cannot be set as it will override the value set by --config
        help=cmdutil.numpy_doc_to_help(flare.Solvate.box_buffer.__doc__, doc_has_defaults=False),
    )

    group.add_argument(
        "-i",
        "--solvate-box-ionic-strength",
        type=float,
        help=cmdutil.numpy_doc_to_help(
            flare.Solvate.box_ionic_strength.__doc__, doc_has_defaults=False
        ),
    )

    group.add_argument(
        "-s",
        "--simulation-length",
        type=float,
        # A default cannot be set as it will override the value set by --config
        help=cmdutil.numpy_doc_to_help(flare.Dynamics.simulation_length.__doc__, True),
    )

    group.add_argument(
        "-r",
        "--resume",
        action="store_true",
        help="Request that an existing dynamics simulation is resumed. The output protein from "
        "the original calculation must be specified and must have a base name that matches "
        "all the other simulation files. The simulation length can still be modified, if "
        "desired, but other calculation settings will be ignored as they will be retrieved from "
        "the .ini file present in the directory containing the protein. Settings that determine "
        "where the calculation will be run (e.g. GPU and engine settings) are not remembered and "
        "must still be set.",
    )

    group.add_argument(
        "-w",
        "--solvent-model",
        choices=[
            "explicit",
            "implicit",
            "vacuum",
            "gbn",
            "gbn2",
            "hct",
            "obc1",
            "obc2",
            "tip3p",
            "spce",
            "opc",
            "opc3",
            "spceb",
            "tip3pfb",
            "tip4p2005",
            "tip4pd",
            "tip4pfb",
            "tip4pew",
        ],
        help="The solvent model to use. Explicit (tip3p) solvent is used by "
        "default. If implicit solvent is specified, OBC2 is the default model.",
    )

    cmdutil.add_membrane_arguments(group, dynamics)

    group.add_argument(
        "--solvent-box-shape",
        choices=[
            "octahedral",
            "orthogonal",
        ],
        help="The shape of the solvent box. Octahedral boxes (the default) are more "
        "spherical in shape and so are more efficient for most proteins.",
    )

    group = parser.add_argument_group("GPU")
    cmdutil.add_gpu_arguments(group, dynamics)

    group = parser.add_argument_group("CPU")
    cmdutil.add_cpu_arguments(group, dynamics)

    group = parser.add_argument_group("openmm arguments")
    cmdutil.add_openmm_common_arguments(group, dynamics)
    cmdutil.add_openmm_equilibration_arguments(group, dynamics)
    cmdutil.add_openmm_time_step_arguments(group, dynamics)
    cmdutil.add_openmm_advanced_arguments(group, dynamics)
    cmdutil.add_constraint_arguments(group, dynamics)

    group = parser.add_argument_group("gcncmc arguments")
    cmdutil.add_gcncmc_arguments(group, dynamics)
    group.add_argument(
        "-u",  # matches --ligand-residue in gist.py
        "--gcncmc-ligand-residue",
        help='The ligand for GCNCMC, the geometrical center of which defines the center of the sampling sphere. This can be specified using residue information, for example, "LIG" or "A LIG 15A"',  # noqa: E501
    )
    group.add_argument(
        "--gcncmc-sphere-center",
        nargs=3,
        type=float,
        help="Provide 3 arguments as the x/y/z coordinates to use as the center of the GCNCMC sphere.",  # noqa: E501
    )

    group = parser.add_argument_group("general options")
    group.add_argument(
        "--config",
        help=cmdutil.CONFIG_ARG_HELP_TEXT,
    )
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        default=dynamics.local_engine_count,
        help="Set the number of local engines to start. Use 0 for no "
        "local processing - you must specify some remote engines "
        "with -g if you do so.",
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker "
        "in the format hostname:port. Field Engines that have "
        "been registered with the Broker will be used during the "
        "calculation. The CRESSET_BROKER environment variable "
        "can be used instead of this option.",
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )
    args = parser.parse_args(argv)

    return args


def _configure_process(dynamics, args):
    """Configure `dynamics` using the command line arguments."""
    if args.list_open_versions:
        cmdutil.print_open_force_field_versions_and_exit()

    project = flare.Project()

    # Load the config file first. Any other settings given will override the config file.
    if args.config:
        dynamics.read_config(args.config)

    file_format = "pdb"
    if args.molecule:
        # Read the molecules into the project
        project.proteins.extend(flare.read_file(args.molecule, file_format))
    else:
        # If there are no molecules read from stdin
        project.proteins.extend(flare.read_file(sys.stdin, file_format))

    if not project.proteins:
        raise ValueError("no molecules were read from the input")
    if len(project.proteins) > 1:
        raise ValueError("only one molecule can be specified")

    dynamics.protein = project.proteins[0]
    dynamics.protein.is_prepared = True  # Suppress warnings
    dynamics.sequences = cmdutil.chain_types_to_sequences(
        dynamics.protein, args.protein_chain_types
    )

    dynamics.output_path = args.directory

    if args.resume:
        dynamics.resume = True
        if dynamics.protein is None:
            raise ValueError(
                "the output protein from the original calculation must be provided when resuming"
                + " dynamics"
            )
        if not os.path.exists(dynamics.output_path):
            raise ValueError(
                "a valid output path to the directory containing the dynamics output files must"
                + " be provided"
            )

    cmdutil.configure_dynamics_force_field_process(dynamics, args)

    if args.solvate_box_buffer is not None:
        with cmdutil.CheckValue("solvate-box-buffer"):
            dynamics.solvate.box_buffer = args.solvate_box_buffer

    with cmdutil.CheckValue("solvate-box-ionic-strength"):
        if args.solvate_box_ionic_strength is not None:
            dynamics.solvate.box_ionic_strength = args.solvate_box_ionic_strength

    cmdutil.configure_gpu_process(dynamics, args)
    cmdutil.configure_cpu_process(dynamics, args)
    cmdutil.configure_openmm_common_process(dynamics, args)
    cmdutil.configure_openmm_equilibration_process(dynamics, args)
    cmdutil.configure_openmm_time_step_process(dynamics, args)
    cmdutil.configure_openmm_advanced_process(dynamics, args)
    cmdutil.configure_constraint_process(dynamics, args)

    cmdutil.configure_gcncmc_process(dynamics, args)
    if dynamics.gcncmc.sampling_protocol != flare.DynamicsGCNCMC.SamplingProtocol.Off:
        if not args.gcncmc_sphere_center and not args.gcncmc_ligand_residue:
            raise ValueError("a ligand residue or sphere center must be specified for GCNCMC")
        else:
            if args.gcncmc_ligand_residue:
                dynamics.gcncmc.center_on = cmdutil.calculate_center_of_residue(
                    dynamics.protein, args.gcncmc_ligand_residue
                )
                dynamics.gcncmc.sphere_buffer = (
                    dynamics.gcncmc.sphere_buffer
                    + cmdutil.calculate_radius_of_residue(
                        dynamics.protein, args.gcncmc_ligand_residue
                    )
                )
            else:
                dynamics.gcncmc.center_on = args.gcncmc_sphere_center

    if args.simulation_length is not None:
        with cmdutil.CheckValue("simulation-length"):
            dynamics.simulation_length = args.simulation_length

    if args.solvent_model == "explicit":
        dynamics.solvate.model = flare.SolventModel.Explicit
    elif args.solvent_model == "implicit":
        dynamics.solvate.model = flare.SolventModel.ImplicitGBn2
    elif args.solvent_model == "vacuum":
        dynamics.solvate.model = flare.SolventModel.Vacuum
    elif args.solvent_model == "tip3p":
        dynamics.solvate.model = flare.SolventModel.ExplicitTIP3P
    elif args.solvent_model == "spce":
        dynamics.solvate.model = flare.SolventModel.ExplicitSPCE
    elif args.solvent_model == "opc":
        dynamics.solvate.model = flare.SolventModel.ExplicitOPC
    elif args.solvent_model == "opc3":
        dynamics.solvate.model = flare.SolventModel.ExplicitOPC3
    elif args.solvent_model == "tip3pfb":
        dynamics.solvate.model = flare.SolventModel.ExplicitTIP3Pfb
    elif args.solvent_model == "spceb":
        dynamics.solvate.model = flare.SolventModel.ExplicitSPCEb
    elif args.solvent_model == "tip4p2005":
        dynamics.solvate.model = flare.SolventModel.ExplicitTIP4P2005
    elif args.solvent_model == "tip4pd":
        dynamics.solvate.model = flare.SolventModel.ExplicitTIP4Pd
    elif args.solvent_model == "tip4pfb":
        dynamics.solvate.model = flare.SolventModel.ExplicitTIP4Pfb
    elif args.solvent_model == "tip4pew":
        dynamics.solvate.model = flare.SolventModel.ExplicitTIP4Pew
    elif args.solvent_model == "gbn":
        dynamics.solvate.model = flare.SolventModel.ImplicitGBn
    elif args.solvent_model == "gbn2":
        dynamics.solvate.model = flare.SolventModel.ImplicitGBn2
    elif args.solvent_model == "hct":
        dynamics.solvate.model = flare.SolventModel.ImplicitHCT
    elif args.solvent_model == "obc1":
        dynamics.solvate.model = flare.SolventModel.ImplicitOBC1
    elif args.solvent_model == "obc2":
        dynamics.solvate.model = flare.SolventModel.ImplicitOBC2

    cmdutil.configure_membrane_process(dynamics, args)

    # If a membrane is set them the solvate model must be set to its default of explicit tip3p.
    # This is checked when cmdutil.start_process calls dynamics.setup_errors() and therefore
    # is not checked here

    if args.solvent_box_shape == "octahedral":
        dynamics.solvate.box_shape = flare.AmberSolvationBox.Octahedral
    elif args.solvent_box_shape == "orthogonal":
        dynamics.solvate.box_shape = flare.AmberSolvationBox.Orthogonal
    elif args.solvent_box_shape is None and args.membrane is not None:
        # Membrane requires an orthogonal box but the default is octahedral.
        dynamics.solvate.box_shape = flare.AmberSolvationBox.Orthogonal

    with cmdutil.CheckValue("localengines"):
        dynamics.local_engine_count = args.localengines

    return project


def _write_results(project, args):
    """Write the results."""
    if args.project:
        try:
            project.save(args.project)
            print(
                f"The trajectory file in directory {args.directory} can be opened in the Flare "
                f"project {args.project} by clicking 'Open Trajectory' in the Dynamics dock."
            )
        except Exception as err:
            print(err, file=sys.stderr)
            # print the error and continue
    else:
        print(
            f"Output files from the dynamics run have been written to directory "
            f"{args.directory}."
        )


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
