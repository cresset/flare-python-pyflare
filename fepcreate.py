#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Creates a FEP Project and generates the graph network for a set of ligands and a protein.

The FEP Project will be stored in a Flare project and will be written to the file specified by the
--project option.

If --project points to an existing Flare project then the ligands will be added to the project
instead of creating a new project. If the Flare project contains multiple FEP projects, then the
FEP project name must be specified by the --name option.

Likewise, the protein of an existing FEP project can be changed by setting --project to an
existing Flare project and setting the --protein option.

Once the FEP Project has been generated the FEP calculation can be run using the feprun.py script.
It is recommended to review the graph network in Flare before running the calculation.

examples:
  pyflare fepcreate.py --project project.flr --protein protein.pdb ligands.sdf
    Creates a FEP project containing the ligands and protein. The graph network is automatically
    generated.

  pyflare fepcreate.py --help
    Prints the help text including all available options and exits.
"""
import sys
import argparse
import os
import traceback

from cresset import flare

from _internal import cmdutil


def main(argv):
    """Add molecules to a FEP project and generate the graph network."""
    try:
        fep_generate_links = flare.FepGenerateLinks()

        args = _parse_args(fep_generate_links, argv)
        project, fep_project = _create_update_project(fep_generate_links, args)
        _configure_process(fep_generate_links, fep_project, args)
        logging_context = cmdutil.LoggingContext(fep_project.log, verbose=args.verbose)
        cmdutil.start_process(fep_generate_links, args.broker, args.verbose)
        cmdutil.wait_for_finish(fep_generate_links, logging_context=logging_context)
        cmdutil.check_errors(fep_generate_links)
        if not fep_generate_links.is_cancelled():
            _write_results(project, fep_project, args)

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(fep_generate_links, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )
    parser.add_argument(
        "ligands",
        nargs="*",
        help="The ligands to add to the FEP project. All ligands should sit in the protein active site and have the same total formal charge. This is required when creating a new FEP project and may be used to add ligands to existing FEP projects.",  # noqa: E501
    )

    group = parser.add_argument_group("input/output")
    group.add_argument(
        "-i",
        "--input",
        choices=["pdb", "sdf", "mol2", "xed"],
        help="Set the file format for reading the ligands. The default is to autodetect.",
    )
    group.add_argument(
        "-p",
        "--protein",
        help="The protein which will be used by the FEP calculation. The protein should have been prepared. This is required when creating a new FEP project and may be used to change the protein of existing FEP projects.",  # noqa: E501
    )
    group.add_argument(
        "-j",
        "--input-protein",
        choices=["pdb", "sdf", "mol2", "xed"],
        help="Set the file format for reading the protein. The default is to autodetect.",
    )
    group.add_argument(
        "-C",
        "--protein-chain-types",
        metavar="protein,water,other,ligand",
        default="protein,water,other,ligand",
        type=str.lower,
        help="Select the types of chains to include in the calculation as a comma separated list. 'p', 'w', 'o' and 'l' can be used as a shorthand for 'protein', water', 'other', 'ligand'. The default is to include all the chains.",  # noqa: E501
    )
    group.add_argument(
        "-P",
        "--project",
        required=True,
        help="The Flare project which the FEP project will be written to. If the Flare project exists and contains the named FEP project, then the FEP project is edited instead of being created.",  # noqa: E501
    )

    group = parser.add_argument_group("FEP options")
    group.add_argument(
        "-a",
        "--activity",
        help="The SDF tag of the ligands containing the experimental activity values in DeltaG (kcal/mol) units.",  # noqa: E501
    )
    group.add_argument(
        "-n",
        "--name",
        help="The name of the FEP project to create or add molecules to. If not specified and the Flare project contains one FEP project then that project is edited, otherwise the name is autogenerated.",  # noqa: E501
    )
    group.add_argument(
        "-d",
        "--delete",
        action="store_true",
        help="When adding ligands to an existing FEP project, allow the deletion of existing links during graph network generation. If calculations have been run previously, then data may be lost as a consequence of link deletion.",  # noqa: E501
    )
    group.add_argument(
        "-m",
        "--calculation-mode",
        choices=["benchmark", "production"],
        type=str.lower,
        help="The calculation mode. In benchmark mode, any known experimental activities are not allowed to influence the calculation. This is generally what you want when benchmarking, but not when doing production FEP runs. In production mode, the FEP network is adjusted to prefer direct links to molecules with known activity, and the final computation of ligand dG values uses the experimental activities directly. The default calculation mode for a new project is production mode.",  # noqa: E501
    )
    group.add_argument(
        "-3",
        "--max-3d-coords-dist",
        type=float,
        default=fep_generate_links.max_3d_coords_dist,
        help=cmdutil.numpy_doc_to_help(flare.FepGenerateLinks.max_3d_coords_dist.__doc__, True),
    )
    group.add_argument(
        "-A",
        "--max-dist-to-actives",
        type=int,
        default=fep_generate_links.max_dist_to_actives,
        help=cmdutil.numpy_doc_to_help(flare.FepGenerateLinks.max_dist_to_actives.__doc__, True),
    )
    group.add_argument(
        "-D",
        "--max-graph-diameter",
        type=int,
        default=fep_generate_links.max_graph_diameter,
        help=cmdutil.numpy_doc_to_help(flare.FepGenerateLinks.max_graph_diameter.__doc__, True),
    )
    group.add_argument(
        "--star",
        help="Generate a star graph instead of a normal graph, using the ligand with the specified title as the center of the star. This option is useful for exploring how changes to a molecule affect activity.",  # noqa: E501
    )
    group.add_argument(
        "--do-not-use-3d-coords",
        action="store_true",
        default=not fep_generate_links.use_3d_coords,
        help="If set the mappings will ignore 3D coordinates and use the topology instead",
    )
    group.add_argument(
        "--do-not-require-cycles",
        action="store_true",
        default=not fep_generate_links.require_cycles,
        help="If set, then the calculation will not try to ensure that all molecules are part of a cycle",  # noqa: E501
    )
    group.add_argument(
        "-I",
        "--add-intermediates",
        action="store_true",
        default=fep_generate_links.add_intermediates,
        help=cmdutil.numpy_doc_to_help(flare.FepGenerateLinks.add_intermediates.__doc__, True),
    )
    group.add_argument(
        "-M",
        "--minimum-link-score-for-intermediate",
        type=float,
        default=fep_generate_links.minimum_link_score_for_intermediate,
        help=cmdutil.numpy_doc_to_help(
            flare.FepGenerateLinks.minimum_link_score_for_intermediate.__doc__, True
        ),
    )

    group = parser.add_argument_group("general options")
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        default=fep_generate_links.local_engine_count,
        help="Set the number of local engines to start. Use 0 for no local processing: you must specify some remote engines with -g if you do so.",  # noqa: E501
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker in the format hostname:port. Field Engines which have been registered with the Broker will be used during the calculation. The CRESSET_BROKER environment variable can be used instead of this option.",  # noqa: E501
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )
    args = parser.parse_args(argv)

    return args


def _create_update_project(fep_generate_links, args):
    """Create or update the fep project using the command line arguments.

    Returns
    -------
    tuple(cresset.flare.Project, cresset.flare.FepProject)
        The Flare and Fep project.
    """
    # Create the projects
    project = None
    if os.path.isfile(args.project):
        project = flare.Project.load(args.project)
    else:
        project = flare.Project()

    fep_project = None
    if args.name:
        fep_projects = [proj for proj in project.fep_projects if proj.name == args.name]
        if not fep_projects:
            fep_project = project.fep_projects.new()
            fep_project.name = args.name
        elif len(fep_projects) == 1:
            fep_project = fep_projects[0]
            fep_generate_links.calculation_mode = fep_project.calculation_mode
        else:
            raise ValueError("multiple fep projects with the same name detected")
    else:
        if not project.fep_projects:
            fep_project = project.fep_projects.new()
        elif len(project.fep_projects) == 1:
            fep_project = project.fep_projects[0]
            fep_generate_links.calculation_mode = fep_project.calculation_mode
        else:
            names = (
                '"' + '", "'.join([fep_project.name for fep_project in project.fep_projects]) + '"'
            )
            raise ValueError(
                "multiple fep projects detected, "
                + "specify a fep project name with --name. "
                + f"The names of the fep projects are {names}."
            )

    # Load Ligands
    new_ligands = []
    for ligand_file in args.ligands:
        new_ligands.extend(project.ligands.extend(flare.read_file(ligand_file, args.input)))

    if not args.ligands:
        # If the ligand is not set read it from stdin
        file_format = args.input if args.input else "sdf"
        new_ligands.extend(project.ligands.extend(flare.read_file(sys.stdin, file_format)))

    if not fep_project.nodes and not new_ligands:
        raise ValueError("fep project contains no ligands")

    new_nodes = fep_project.nodes.new(new_ligands)
    if args.activity:
        for node in new_nodes:
            ligand = node.source_ligand()
            if args.activity in ligand.properties:
                node.experimental_delta_g = ligand.properties[args.activity].value

    # Load protein
    new_proteins = []
    if args.protein:
        new_proteins.extend(
            project.proteins.extend(flare.read_file(args.protein, args.input_protein))
        )

    if len(new_proteins) > 1:
        raise ValueError(
            f"only one protein can be set, file {args.protein} "
            + f"contains {len(new_proteins)} proteins"
        )

    if new_proteins:
        sequences = cmdutil.chain_types_to_sequences(new_proteins[0], args.protein_chain_types)
        fep_project.set_protein(new_proteins[0], sequences)

    return (project, fep_project)


def _configure_process(fep_generate_links, fep_project, args):
    """Configure `fep_generate_links` calculation using the command line arguments."""
    fep_generate_links.fep_project = fep_project

    with cmdutil.CheckValue("max-3d-coords-dist"):
        fep_generate_links.max_3d_coords_dist = args.max_3d_coords_dist

    with cmdutil.CheckValue("max-dist-to-actives"):
        fep_generate_links.max_dist_to_actives = args.max_dist_to_actives

    with cmdutil.CheckValue("max-graph-diameter"):
        fep_generate_links.max_graph_diameter = args.max_graph_diameter

    if args.star:
        star_node = None
        for node in fep_generate_links.fep_project.nodes:
            if node.title == args.star:
                star_node = node
        if not star_node:
            raise ValueError("star graph center ligand with specified title does not exist")
        fep_generate_links.star_center = star_node
        fep_generate_links.type = flare.FepGenerateLinks.Type.Star

    fep_generate_links.use_3d_coords = not args.do_not_use_3d_coords
    fep_generate_links.require_cycles = not args.do_not_require_cycles
    fep_generate_links.add_intermediates = args.add_intermediates
    fep_generate_links.minimum_link_score_for_intermediate = (
        args.minimum_link_score_for_intermediate
    )

    if args.calculation_mode == "benchmark":
        fep_generate_links.calculation_mode = flare.FepGenerateLinks.CalculationMode.Benchmark
    elif args.calculation_mode == "production":
        fep_generate_links.calculation_mode = flare.FepGenerateLinks.CalculationMode.Production

    if args.delete:
        fep_generate_links.mode = flare.FepGenerateLinks.Mode.AddAndRemove
    else:
        fep_generate_links.mode = flare.FepGenerateLinks.Mode.Add

    with cmdutil.CheckValue("localengines"):
        fep_generate_links.local_engine_count = args.localengines


def _write_results(project, fep_project, args):
    """Write the results."""
    try:
        tmp_file = args.project + ".tmp"
        project.save(tmp_file)
        if os.path.exists(args.project):
            os.remove(args.project)
        os.rename(tmp_file, args.project)

        script_dir = os.path.join(os.path.dirname(sys.argv[0]), "feprun.py")
        print(
            f"Generated network with {len(fep_project.nodes)} ligands "
            + f"and {len(fep_project.links)} links."
        )
        print("Please inspect the network in Flare then run the FEP calculation using:")
        print(
            f'"{sys.executable}" "{script_dir}" '
            + f'--project "{args.project}" --name "{fep_project.name}"'
        )
    except Exception as err:
        print(err, file=sys.stderr)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
