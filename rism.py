#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Runs 3D-RISM on a protein and writes the results to stdout.

The PDB temperature factor field for the waters will be set to the 3D-RISM DeltaG value.

The protein should be prepared before running 3D-RISM. Use the proteinprep.py script
to do this. For example:

  pyflare proteinprep.py protein.pdb | pyflare rism.py --ligand ligand.sdf

examples:
  pyflare rism.py --ligand ligand.sdf protein.pdb
    Run 3D-RISM on the protein with the ligand.

  pyflare rism.py --help
    Prints the help text including all available options and exits.
"""
import sys
import argparse
import traceback

from cresset import flare

from _internal import cmdutil


CHARGE_HANDLING_ENUM_MAP = {
    "dont-neutralize": flare.Rism.ChargeHandling.DontNeutralize,
    "neutralize-counterions": flare.Rism.ChargeHandling.NeutralizeCounterions,
    "neutralize-evenly": flare.Rism.ChargeHandling.NeutralizeEvenly,
}


def main(argv):
    """Run the 3D-RISM process using the command line args."""
    try:
        rism = flare.Rism()

        args = _parse_args(rism, argv)
        project = _configure_process(rism, args)
        logging_context = cmdutil.LoggingContext(project.log, verbose=args.verbose)
        cmdutil.start_process(rism, args.broker, args.verbose)
        cmdutil.wait_for_finish(rism, logging_context=logging_context)
        cmdutil.check_errors(rism)
        if not rism.is_cancelled():
            _write_results(rism, project, args)

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(rism, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )
    # Input output settings
    parser.add_argument(
        "protein",
        nargs="?",
        help="The protein file to run 3D-RISM on. If not given the protein is read from stdin.",
    )
    group = parser.add_argument_group("input/output")
    group.add_argument(
        "-C",
        "--protein-chain-types",
        metavar="protein,water,other,ligand",
        default="protein",
        type=str.lower,
        help="Select the types of chains to include in the calculation as a comma separated list. 'p', 'w', 'o' and 'l' can be used as a shorthand for 'protein', water', 'other', 'ligand'. The default is the 'protein' chain.",  # noqa: E501
    )
    group.add_argument("-m", "--ligand", help="The ligand molecule.")
    group.add_argument(
        "-i",
        "--input",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for reading the protein. The default is to autodetect.",
    )
    group.add_argument(
        "-o",
        "--output",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for writing the protein. The default is 'pdb'.",
    )
    group.add_argument(
        "-P",
        "--project",
        help="Write the Flare project file with the results to the given file path.",
    )
    # 3D RISM  settings
    group = parser.add_argument_group("3d rism")
    group.add_argument(
        "-c",
        "--charge-handling",
        choices=CHARGE_HANDLING_ENUM_MAP.keys(),
        type=str.lower,
        help=cmdutil.numpy_doc_to_help(flare.Rism.charge_handling.__doc__, True),
    )
    group.add_argument(
        "--charge-method",
        choices=["gasteiger", "am1-bcc"],
        type=str.lower,
        help=cmdutil.numpy_doc_to_help(flare.Rism.charge_method.__doc__, True),
    )
    group.add_argument(
        "--convergence-tolerance",
        type=float,
        default=rism.convergence_tolerance,
        help=cmdutil.numpy_doc_to_help(flare.Rism.convergence_tolerance.__doc__),
    )
    group.add_argument(
        "-f",
        "--force-field",
        choices=["xed", "amber"],
        help=cmdutil.numpy_doc_to_help(flare.Rism.force_field.__doc__, True),
    )
    group.add_argument(
        "--gaff-version",
        choices=["gaff", "gaff2"],
        type=str.lower,
        help=cmdutil.numpy_doc_to_help(flare.Rism.gaff_version.__doc__, True),
    )
    group.add_argument(
        "-b",
        "--grid-border-width",
        type=float,
        default=rism.grid_border_width,
        help=cmdutil.numpy_doc_to_help(flare.Rism.grid_border_width.__doc__),
    )
    group.add_argument(
        "-G",
        "--grid-spacing",
        type=float,
        default=rism.grid_spacing,
        help=cmdutil.numpy_doc_to_help(flare.Rism.grid_spacing.__doc__),
    )
    group.add_argument(
        "--max-iterations",
        type=int,
        default=rism.max_iterations,
        help=cmdutil.numpy_doc_to_help(flare.Rism.max_iterations.__doc__),
    )
    group.add_argument(
        "-T",
        "--max-threads",
        type=int,
        default=rism.max_threads,
        help=cmdutil.numpy_doc_to_help(flare.Rism.max_threads.__doc__),
    )
    # Other settings
    group = parser.add_argument_group("general options")
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        default=rism.local_engine_count,
        help="Set the number of local engines to start. Use 0 for no local processing: you must specify some remote engines with -g if you do so.",  # noqa: E501
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker in the format hostname:port. Field Engines which have been registered with the Broker will be used during the calculation. The CRESSET_BROKER environment variable can be used instead of this option.",  # noqa: E501
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )
    args = parser.parse_args(argv)

    return args


def _configure_process(rism, args):
    """Configure `rism` using the command line arguments."""
    project = flare.Project()

    if args.protein:
        project.proteins.extend(flare.read_file(args.protein, args.input))
    else:
        # If the protein is not set read it from stdin
        file_format = args.input if args.input else "pdb"
        project.proteins.extend(flare.read_file(sys.stdin, file_format))

    if not project.proteins:
        raise ValueError("no protein molecules were read from the input")
    if len(project.proteins) > 1:
        raise ValueError("only one protein can be specified")

    rism.protein = project.proteins[0]
    rism.protein.is_prepared = True  # Suppress warnings
    rism.sequences = cmdutil.chain_types_to_sequences(rism.protein, args.protein_chain_types)

    if args.ligand:
        project.ligands.extend(flare.read_file(args.ligand, args.input))
        if not project.ligands:
            raise ValueError("no ligand molecules were read from the input")
        if len(project.ligands) > 1:
            raise ValueError("only one ligand can be specified")

        rism.ligand = project.ligands[0]

    if args.charge_handling in CHARGE_HANDLING_ENUM_MAP:
        rism.charge_handling = CHARGE_HANDLING_ENUM_MAP[args.charge_handling]

    if args.force_field == "amber":
        rism.force_field = flare.Rism.ForceField.Amber
    elif args.force_field == "xed":
        rism.force_field = flare.Rism.ForceField.Xed

    if rism.force_field == flare.Rism.ForceField.Amber:
        if args.charge_method == "am1-bcc":
            rism.charge_method = flare.Rism.ChargeMethod.AM1BCC
        elif args.charge_method == "gasteiger":
            rism.charge_method = flare.Rism.ChargeMethod.Gasteiger

        if args.gaff_version == "gaff2":
            rism.gaff_version = flare.Rism.GaffVersion.GAFF2
        elif args.gaff_version == "gaff":
            rism.gaff_version = flare.Rism.GaffVersion.GAFF

    with cmdutil.CheckValue("convergence-tolerance"):
        rism.convergence_tolerance = args.convergence_tolerance

    with cmdutil.CheckValue("convergence-tolerance"):
        rism.convergence_tolerance = args.convergence_tolerance

    with cmdutil.CheckValue("grid-border-width"):
        rism.grid_border_width = args.grid_border_width

    with cmdutil.CheckValue("grid-spacing"):
        rism.grid_spacing = args.grid_spacing

    with cmdutil.CheckValue("max-iterations"):
        rism.max_iterations = args.max_iterations

    with cmdutil.CheckValue("max-threads"):
        rism.max_threads = args.max_threads

    with cmdutil.CheckValue("localengines"):
        rism.local_engine_count = args.localengines

    return project


def _write_results(rism, project, args):
    """Write the results."""
    # Save the project before changing any RISM water tf property
    if args.project:
        try:
            project.save(args.project)
        except Exception as err:
            print(err, file=sys.stderr)
            # print the error and continue

    # Set tf property for the water molecules created by 3D-RISM to their deltaG value
    with rism.protein.batch_edit():
        for atom in rism.protein.atoms:
            delta_g = rism.protein.rism_result.atom_delta_g(atom)
            if delta_g is not None:
                atom.tf = delta_g

    # Print the result protein to stdout
    file_format = args.output if args.output else "pdb"
    rism.protein.write_file(sys.stdout, file_format)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
