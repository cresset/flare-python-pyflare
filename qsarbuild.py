#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Builds 3D Field QSAR, Activity Atlas and kNN models from aligned ligands.

The following types of QSAR models can be generated:

Field QSAR
    Uses the electrostatic and shape properties of aligned ligands to develop quantitative structure activity relationship models. Ligands in the "Training Set" are used to derive a set of sampling points around the dataset that can be used to probe any molecule for the electrostatic potential or for the volume taken up by ligands.
    The sample values are combined using partial least squares (PLS) to derive an equation that describes activity.
    This 3D QSAR model can help to explain SAR data and with the best models used to predict an activity value for newly designed ligands.

Activity Atlas
    An Activity Atlas model summarizes all the SAR for a series into a 3D model that can be used to inform new molecule design.
    Based on Activity Miner, Activity Atlas examines all of the activity cliffs in the data set and asks a series of questions about each. Are the ligands aligned correctly? Are there plausible alternative alignments? Are there flexible groups whose alignment is unconstrained?
    The answers to these questions are combined in a Bayesian analysis which combines the probability that each alignment is correct with the size of the activity cliff.
    The result is a global picture of activity, highlighting those regions around the series where the SAR is clear. This model can be view in 3D using Forge or Torch.

    A similar analysis is performed at the same time to determine which regions of space around the series have been explored.
    Unlike traditional SAR table methods, the nature of the substitutions is implicitly accounted for as we perform the analysis in electrostatic and steric field space.
    Where have we placed positive electrostatic potential? Where have we placed hydrophobicity?

    This latter analysis then allows qsarscore.py to compute a 'Novelty' score for new ligands - if we made a new molecule, what would it tell us?
    The ligands with the highest 'Novelty' are those with small, controlled changes.
    Ideas with a low 'Novelty' don't expand our understanding of the SAR, while those with too high a value are potentially taking too bold a leap into the unknown.
    Designing compounds into the middle ground allows the SAR to be efficiently explored, giving the maximum understanding with the least synthetic effort.

k Nearest Neighbor (kNN)
    Generates a k Nearest Neighbor (kNN) model using similarity and activity data of aligned ligands.
    The similarity between the ligands is calculated using Cresset's field similarity method or by using the 2D methods ECFP4, ECFP6, FCFP4, or FCFP6.
    This model can be used with qsarscore.py to predict an activity value for newly designed ligands.

    A regression or classification kNN model can be generated.

To build a machine learning model use the fielddescriptors.py script to generate the descriptors.
Then use an external machine learning program, for example scikit-learn, to generate the model.

input:
  The input ligands should be read in SDF format. Other molecular file formats (such as mol2 and XED) are not useful, since QSAR Model building requires activity data and those file formats have no provision for metadata.

  The ligands need to be pre-aligned for all the QSAR models except for the kNN model when using 2D similarities. align.py is a recommended tool for this.

output:
  By default qsarbuild.py writes its results to a set of files in the current working directory:

    output.sdf
      The input ligands, with tags added for predicted activity and role (training/test set).

    output.log
      The log of the calculation, giving details on the model created.

    output.model.csv
      The model sample data in CSV format, in case you wish to use alternative statistical techniques to build a model. This file is only created for Field QSAR models.

    output.flr
      A Flare project file containing the ligands and generated model. This can be opened in Flare to view the model.

    output.model.flr
      A Flare project file containing the generated model only. This can be used with qsarbuild.py to score ligands against the model.

  The prefix for these output files can be changed with the -o flag.

  Note that if a project file was specified, then the base name of that project file is used as the output path.

examples:
  pyflare qsarbuild.py -a My_Activity mols.sdf
    Reads the ligands in 'mols.sdf'. Activity values are obtained from the 'My_Activity' tag, and are assumed to be in log units.
    A Field QSAR model will be built and the results put in 'output.sdf', 'output.log', 'output.csv', 'output.flr' and 'output.model.flr'.

  pyflare qsarbuild.py -t test.sdf -r 20 -f e,h -o expt1 mols.sdf
    As above, but a test set will be used consisting of all of the ligands in 'test.sdf' together with a randomly-chosen 20% of the ligands from 'mols.sdf'.
    The electrostatic and hydrophobic fields will be used to build the model. The outputs will go to 'expt1.sdf' etc.

  pyflare qsarbuild.py -a My_Activity -u uM -w 0.6-0.8 mols.sdf
    Reads the ligands in 'mols.sdf'. Activity values are obtained from the 'My_Activity' tag, and are in uM units.
    Each molecule's similarity value will be read from their 'Sim' tag, and these will be converted to weights so that a molecule with a Sim value less than 0.6 gets a weight of 0 (it is omitted),
    a molecule with a Sim value greater than 0.8 gets a weight of 1, and the weight values are linearly interpolated between 0.6 and 0.8 so that a molecule with a Sim value of 0.7 gets a weight of 0.5.

  pyflare qsarbuild.py -q kNN --method ECFP4 mols.sdf
    Builds a k-Nearest-Neighbor model from the ligands in mols.sdf, using ECFP4 fingerprints. Note that the ligands in mols.sdf do not need to be aligned (or even 3D).

  pyflare qsarbuild.py -l 0 -g cebroker:9000 -a My_Activity -u uM -w 0.6-0.8 mols.sdf
    As above, but no processing will be done locally: the calculation will be performed by the Cresset Engine Broker running on the machine with the hostname 'cebroker' on port '9000'.

  pyflare qsarbuild.py --help
    Prints the help text including all available options and exits.
"""  # noqa: E501
import sys
import os
import re
import csv
import random
import math
import argparse
import traceback

from cresset import flare

from _internal import cmdutil


def main(argv):
    """Builds a QSAR model using the command line args."""
    try:
        build = flare.BuildQSARModel()

        args = _parse_args(build, argv)
        project, imported_tags = _configure_process(build, args)
        logging_context = cmdutil.LoggingContext(project.log, verbose=args.verbose)
        cmdutil.start_process(build, args.broker, args.verbose)
        cmdutil.wait_for_finish(build, logging_context=logging_context)

        if build.created_qsar_model() is None:
            cmdutil.check_errors(build)
        elif build.errors():
            # If a model is built and there is an error still write the output files
            print("\n".join(build.errors()), file=sys.stderr)

        if not build.is_cancelled():
            _write_results(build, project, imported_tags, args)

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(build, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )
    # Input settings
    parser.add_argument(
        "trainingset",
        nargs="+",
        help="The ligand files to use as a training set. If not given the ligands are read from stdin.",  # noqa: E501
    )
    # Output settings
    group = parser.add_argument_group("output")
    group.add_argument(
        "-o",
        "--output",
        help="""Set the base file name/path for the output files. The default is \"output\" which causes the results to be written to the current working directory with the name \"output\".
For example, -o /home/username/build/foobar will output these files:
/home/username/build/foobar.sdf
/home/username/build/foobar.log
/home/username/build/foobar.model.csv (Only created for Field QSAR models)
/home/username/build/foobar.fqj
/home/username/build/foobar.model.fqj""",  # noqa: E501
    )
    group.add_argument(
        "--surface",
        choices=["ccp4", "grd", "mgd", "dx"],
        type=str.lower,
        help="Writes the Activity Atlas surfaces to the output directory",
    )

    # Common settings
    group = parser.add_argument_group("qsar options")
    group.add_argument(
        "-q",
        "--qsar",
        choices=[
            "field",
            "pls",
            "f",
            "activityatlas",
            "a",
            "knn",
            "k",
            "knn-c",
        ],
        default="field",
        type=str.lower,
        help="""Specifies which type of model to create.

Models with the suffix "-C" are classification models. For these models the activity data must be integer values which represents the class and there must not be more than 10 classes.

"PLS" is a alias for the "Field" model.
"f" is a alias for the "Field" model.
"a" is a alias for the "ActivityAtlas" model.
"k" is a alias for the "kNN" model.
""",  # noqa: E501
    )
    group.add_argument(
        "-a",
        "--activity",
        help="Specify the tag in the input SDF file containing the activity data. If not specified, looks for a tag containing any of the (case insensitive) strings activity|ic50|pic50|ec50|ki|kd|pki|pkd. If no activity data is found this script will abort.\n\nFor regression models the activity values should be floating point numbers. For classification models activity values should be integer values which represent the class and there must not be more than 10 classes.",  # noqa: E501
    )
    group.add_argument(
        "-u",
        "--units",
        metavar="autodetect|p|M|mM|uM|nM|l",
        choices=["autodetect", "p", "m", "mm", "um", "nm", "l"],
        type=str.lower,
        help="Specify that the input activity values require log-transforming, and give their units. The default is to assume that the activity values are already log- transformed (i.e. that they are pKi values, not Ki values). The l argument specifies that the activity values are -pKi (i.e. log Ki values).\n\nThis option is not used for classification models.",  # noqa: E501
    )
    group.add_argument(
        "-t",
        "--testset",
        action="append",
        help="Load in ligands from the specified file to use in the test set. Multiple -t options can be given. This option is not allowed for Activity Atlas models.",  # noqa: E501
    )
    group.add_argument(
        "-p",
        "--partition",
        metavar="P",
        type=int,
        help="Partition the input data set so that P%% of the molecules are moved from the training set to the test set. The partitioning is activity-stratified: for a random partition, use -r instead.",  # noqa: E501
    )
    group.add_argument(
        "-r",
        "--random-partition",
        metavar="P",
        type=int,
        help="As -p, but the partitioning is done by random pick, rather than activity-stratified.",  # noqa: E501
    )
    # Field QSAR settings
    group = parser.add_argument_group("field qsar")
    group.add_argument(
        "-f",
        "--fields",
        metavar="e,s,h,v",
        type=str.lower,
        help="Specifies which fields to use for the QSAR calculation. The available fields are 'e'lectrostatic, 's'teric (i.e. van der Waals), 'h'ydrophobic, 'v'olume (i.e. volume indicator field, -1 inside, 0 outside). The default is 'e,v'.",  # noqa: E501
    )
    group.add_argument(
        "-m",
        "--mindist",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.BuildQSARModel.min_distance.__doc__),
    )
    group.add_argument(
        "-M",
        "--leave-many-out",
        metavar="P,R",
        help="Uses the leave-many-out validation method instead of the default leave-one-out. P is the percentage of the ligand from the training set used as the validation data, while the remaining ligands serve as the training data. R is the number of times to repeat the operation.",  # noqa: E501
    )
    group.add_argument(
        "-s",
        "--scrambles",
        type=int,
        help=cmdutil.numpy_doc_to_help(flare.BuildQSARModel.scrambles.__doc__),
    )
    group.add_argument(
        "-w",
        "--weight-by-similarity",
        metavar="minS-maxS",
        help="Weight the ligands according to their 'Similarity_cresset' tag (or other tag specified with the similarity-tag option) value, such that similarity values less than Smin get a weight of 0, and similarity values greater than SMax get a weight of 1.\nFor example, -w 0.6-0.8 means that ligands whose similarity values are less than 0.6 get a weight of 0, those with a similarity greater than 0.8 get a weight of 1, and those with a similarity of 0.7 get a weight of 0.5.",  # noqa: E501
    )
    group.add_argument(
        "-W",
        "--weightmode",
        metavar="linear|quadratic",
        choices=["linear", "l", "quadratic", "q"],
        type=str.lower,
        help="If weighting ligands by similarity (with -w), this option controls how the weight values ramp between 0 and 1. In linear mode (the default), the weight values increase linearly, while in quadratic mode the weight values used are squared (e.g. with -w 0.6-0.8, a ligand with a similarity value of 0.7 would get a weight of 0.5 in linear mode, and 0.25 in quadratic mode).\nIf -w is not otherwise specified, implies -w 0-1",  # noqa: E501
    )
    # Activity Atlas settings
    group = parser.add_argument_group("activity atlas")
    group.add_argument(
        "--grid-spacing",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.BuildQSARModel.aa_grid_spacing.__doc__),
    )
    group.add_argument(
        "--disparity-range",
        metavar="minDisparity-maxDisparity",
        help="Ligand pairs whose disparity is less than the minimum value will be excluded from the analysis. Pairs whose disparity is greater than the maximum will be treated as though they had the maximum value.\nFor example, if the range is 5.0-20.0, then pairs with a disparity less than 5.0 are ignored, and those with a disparity of greater than 20.0 treated as though the disparity value was 20.0.\nIf not specified then disparity range is automatically calculated based on the ligands in the Training Set.",  # noqa: E501
    )
    group.add_argument(
        "--activity-range",
        metavar="minActivity-maxActivity",
        help="Ligands whose activity is less than the minimum value will be treated as 'inactive'. Those with more than the maximum value are 'fully active'.\nFor example, if the range is 6.0-8.0, then any ligand with an activity of less than 6.0 is inactive, any ligand with an activity of more than 8.0 is fully active, and any ligand in between is partially active.\nIf not specified then activity range is automatically calculated based on the ligands in the Training Set.",  # noqa: E501
    )
    group.add_argument(
        "--similarity-range",
        metavar="minSim-maxSim",
        help="Set the similarity thresholds to use when deciding whether a ligand is correctly aligned. Alignments whose similarity value is less than the lower threshold are not trusted at all and are excluded from the calculation. Those with similarity values above the upper threshold are completely trusted and are assumed to be correct. The ones in between are partially trusted.\nFor example, if the range is 0.6-0.8, then any alignment with a similarity score less than 0.6 will be excluded. Alignments with a similarity of 0.8 or higher are assumed to be correct. An alignment with a similarity score of 0.7 (half way between the thresholds) is assumed to have a 50%% chance of being correct.\nIf not specified then similarity range is automatically calculated based on the ligands in the Training Set.",  # noqa: E501
    )
    group.add_argument(
        "--similarity-tag",
        metavar="tag",
        help="Set the SDF tag used to retrieve the similarity score for a ligand. This should be set when using SDF files produced by other tools and so will not have the Sim tag.",  # noqa: E501
    )
    group.add_argument(
        "-S",
        "--shapewt",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.BuildQSARModel.shape_weight.__doc__, True),
    )
    group.add_argument(
        "--optimize-alignment",
        action="store_true",
        help=cmdutil.numpy_doc_to_help(flare.BuildQSARModel.optimize_alignment.__doc__, True),
    )
    group.add_argument(
        "--molecules-explore-count",
        metavar="numMols",
        type=int,
        help=cmdutil.numpy_doc_to_help(flare.BuildQSARModel.aa_ligands_explore_count.__doc__, True),
    )
    group.add_argument(
        "--activity-atlas-algorithm",
        metavar="weighted-sum|sum",
        choices=["weighted-sum", "w", "sum", "s"],
        type=str.lower,
        help="Set the Activity Atlas algorithm to use. sum was used in Forge 10.5 and earlier versions, weighted-sum is an improved algorithm that is less susceptible to outliers.\nThe default is weighted-sum.",  # noqa: E501
    )
    # kNN Settings
    group = parser.add_argument_group("knn")
    group.add_argument(
        "--max-k-components",
        metavar="maxK",
        type=int,
        help=cmdutil.numpy_doc_to_help(flare.BuildQSARModel.max_k_components.__doc__, True),
    )
    group.add_argument(
        "--method",
        metavar="field|ECFP4|ECFP6|FCFP4|FCFP6",
        choices=["field", "ecfp4", "ecfp6", "fcfp4", "fcfp6"],
        type=str.lower,
        help="The method for finding the similarity between the molecules. The default value 'field' uses Cresset's field similarity method. ECFP4, ECFP6, FCFP4, or FCFP6 can be specified in which case the respective 2D similarity method is used. If a 2D method is chosen then the input molecules need not have 3D conformations or be aligned.",  # noqa: E501
    )
    group.add_argument(
        "--weighting-scheme",
        metavar="Automatic|NoWeighting|InverseDistance|Exponential|Exponential2|Exponential3|MFT11|MFT12|MFT13|MFT14|MFT21|MFT22|MFT23|MFT24|MFT31|MFT32|MFT33|MFT34|MFT41|MFT42|MFT43|MFT44",  # noqa: E501
        choices=[
            "automatic",
            "noweighting",
            "inversedistance",
            "exponential",
            "exponential2",
            "exponential3",
            "mft11",
            "mft12",
            "mft13",
            "mft14",
            "mft21",
            "mft22",
            "mft23",
            "mft24",
            "mft31",
            "mft32",
            "mft33",
            "mft34",
            "mft41",
            "mft42",
            "mft43",
            "mft44",
        ],
        type=str.lower,
        help="The weighting method to use when averaging the activities of the closest neighbours. In 'Automatic' mode, all of the weighting options are tried and the one that provides the best q^2 for regression models or informedness for classification models is chosen.\nOnly the options Automatic|NoWeighting|InverseDistance|Exponential|Exponential2|Exponential3 are allowed for a classification model.",  # noqa: E501
    )
    # Other settings
    group = parser.add_argument_group("general options")
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        default=build.local_engine_count,
        help="Set the number of local engines to start. Use 0 for no local processing: you must specify some remote engines with -g if you do so.",  # noqa: E501
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker in the format hostname:port. Field Engines which have been registered with the Broker will be used during the calculation. The CRESSET_BROKER environment variable can be used instead of this option.",  # noqa: E501
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )
    args = parser.parse_args(argv)

    return args


def _configure_process(build, args):
    """Configure `build` using the command line arguments."""
    project = flare.Project()

    is_classification_model = False

    # Set the type of model to build
    if args.qsar == "field" or args.qsar == "pls" or args.qsar == "f":
        build.type = flare.QSARModelType.Field
    elif args.qsar == "activityatlas" or args.qsar == "a":
        build.type = flare.QSARModelType.ActivityAtlas
    elif args.qsar == "knn" or args.qsar == "k":
        build.type = flare.QSARModelType.KNN_Regression
    elif args.qsar == "knn-c":
        build.type = flare.QSARModelType.KNN_Classification
        is_classification_model = True

    # Read the training
    training_role = project.roles.append("Training Set")
    for file in args.trainingset:
        training_role.ligands.extend(flare.read_file(file, "sdf"))

    # Read the optional test set
    test_role = project.roles.append("Test Set")
    if args.testset is not None:
        _verify_model_type(
            build.type,
            "--testset",
            [
                flare.QSARModelType.Field,
                flare.QSARModelType.KNN_Classification,
                flare.QSARModelType.KNN_Regression,
            ],
        )
        for file in args.testset:
            test_role.ligands.extend(flare.read_file(file, "sdf"))

    # Set the "Sim" column to the column containing the similarity value.
    # This is done before creating the poses so the sim values are copied to the pose
    if args.similarity_tag is not None:
        _verify_model_type(
            build.type,
            "--similarity-tag",
            [flare.QSARModelType.Field, flare.QSARModelType.ActivityAtlas],
        )
        if args.similarity_tag not in project.ligands.columns:
            raise ValueError(f'--similarity-tag "{args.similarity_tag}", tag was not found')

        for ligand in project.ligands:
            ligand.properties["Sim"].value = ligand.properties[args.similarity_tag].value

    elif build.type == flare.QSARModelType.ActivityAtlas:
        # Activity Atlas requires a "Sim" column
        # Create an empty "Sim" column. All poses will get the default similarity value
        if "Sim" not in project.ligands.columns:
            project.ligands.columns.append("Sim")

    # Mark all ligands as aligned, unless a 2D model is being created
    if (
        args.method == "field"
        or args.method is None
        or build.type
        not in (
            flare.QSARModelType.KNN_Classification,
            flare.QSARModelType.KNN_Regression,
        )
    ):
        for ligand in project.ligands:
            ligand.poses.append(ligand)

    imported_tags = set(project.ligands.columns.user_keys())

    activity_column = None
    if args.activity is not None:
        if args.activity not in imported_tags:
            raise ValueError(f'ligands do not contain a tag called "{args.activity}"')
        project.activity.columns.new(args.activity)
        activity_column = args.activity
    else:
        # If the activity column was auto-detected then the primary activity will be set to it
        project.activity.auto_detect_columns(training_role.ligands)
        activity_column = project.activity.primary

    if activity_column is None:
        raise ValueError(
            "no activity column was set. -a/--activity can be used to set the activity column"
        )

    if is_classification_model:
        if args.units is not None:
            raise ValueError("the activity units cannot be changed for classification models")
        project.activity.columns[activity_column].units = flare.ActivityUnit.StringToCategory

        # For classification we require the activity values are all integers and that
        # there are not more then 10 categories.
        classes = set()
        for ligand in training_role.ligands:
            activity = ligand.properties[activity_column].value
            if not _is_int(activity):
                raise ValueError("classification models require all activity data to be integers")
            classes.add(int(activity))

        if len(classes) > 10:
            raise ValueError("no more then 10 classes are allowed for classification models")

        categories = []
        for clazz in classes:
            categories.append((str(clazz), clazz))
        project.activity.columns[activity_column].string_to_category = categories

    # Regression models
    elif args.units == "p" or args.units is None:
        project.activity.columns[activity_column].units = flare.ActivityUnit.pKi
    elif args.units == "l":
        project.activity.columns[activity_column].units = flare.ActivityUnit.logKi
    elif args.units == "m":
        project.activity.columns[activity_column].units = flare.ActivityUnit.M
    elif args.units == "mm":
        project.activity.columns[activity_column].units = flare.ActivityUnit.mM
    elif args.units == "um":
        project.activity.columns[activity_column].units = flare.ActivityUnit.uM
    elif args.units == "nm":
        project.activity.columns[activity_column].units = flare.ActivityUnit.nM
    elif args.units == "autodetect":
        # Let Flare auto-detected the units
        pass
    else:
        raise ValueError(f'invalid --units value "{args.units}"')

    build.activity_column = activity_column

    # This setting requires the Activity data to be set.
    if args.partition is not None and args.random_partition is not None:
        raise ValueError("both --partition and --random-partition cannot be set")

    if args.partition is not None:
        _verify_model_type(
            build.type,
            "--partition",
            [
                flare.QSARModelType.Field,
                flare.QSARModelType.KNN_Classification,
                flare.QSARModelType.KNN_Regression,
            ],
        )
        if args.partition < 0 or args.partition > 100:
            raise ValueError("--partition must be between 0 and 100")
        _partition_activity_stratify(training_role, test_role, args.partition, activity_column)

    if args.random_partition is not None:
        _verify_model_type(
            build.type,
            "--random-partition",
            [
                flare.QSARModelType.Field,
                flare.QSARModelType.KNN_Classification,
                flare.QSARModelType.KNN_Regression,
            ],
        )
        if args.random_partition < 0 or args.random_partition > 100:
            raise ValueError("--random-partition must be between 0 and 100")
        _partition_random(training_role, test_role, args.random_partition)

    build.training_set_role = training_role
    if len(test_role.ligands) > 0:
        build.test_set_role = test_role

    # Field QSAR settings
    if args.fields is not None:
        _verify_model_type(
            build.type,
            "--fields",
            [
                flare.QSARModelType.Field,
            ],
        )

        fields = set(args.fields.split(","))

        with cmdutil.CheckValue("fields"):
            build.electrostatics_descriptors_enabled = "e" in fields
            build.steric_descriptors_enabled = "s" in fields
            build.hydrophobics_descriptors_enabled = "h" in fields
            build.volume_descriptors_enabled = "v" in fields

    if args.mindist is not None:
        _verify_model_type(
            build.type,
            "--mindist",
            [flare.QSARModelType.Field],
        )

        with cmdutil.CheckValue("mindist"):
            build.min_distance = args.mindist

    if args.leave_many_out is not None:
        _verify_model_type(build.type, "--leave-many-out", [flare.QSARModelType.Field])

        m = re.search(r"(\d+),(\d+)", args.leave_many_out)
        if m is None:
            raise ValueError(
                f'format of --leave-many-output value is invalid. Found "{args.leave_many_out}"',
                'expected "P,R"',
            )

        with cmdutil.CheckValue("leave-many-out"):
            build.cross_validation = flare.BuildQSARModel.CrossValidationType.LeaveManyOut
            build.leave_many_out_percentage = int(m.group(1))
            build.leave_many_out_repeats = int(m.group(2))

    if args.scrambles is not None:
        _verify_model_type(build.type, "--scrambles", [flare.QSARModelType.Field])
        with cmdutil.CheckValue("scrambles"):
            build.scrambles = args.scrambles

    if args.weight_by_similarity is not None:
        _verify_model_type(build.type, "--weight-by-similarity", [flare.QSARModelType.Field])

        m = re.search(r"([0-9.]+)-([0-9.]+)", args.weight_by_similarity)
        if m is None:
            raise ValueError(
                "format of --weight-by-similarity value is invalid. "
                f'Found "{args.weight_by_similarity}", expected "minS-maxS"'
            )

        if "Sim" not in project.ligands.columns:
            raise ValueError(
                '--weight-by-similarity requires the ligands to have a "Sim" tag or the tag '
                "containing the similarity data should be specified by --similarity-tag <tag>"
            )

        with cmdutil.CheckValue("weight-by-similarity"):
            build.field_weight_enabled = True
            build.field_weight_similarity_min = float(m.group(1))
            build.field_weight_similarity_max = float(m.group(2))
            if build.field_weight_similarity_max < build.field_weight_similarity_min:
                raise ValueError("--weight-by-similarity minS must be <= maxS")

    if args.weightmode is not None:
        _verify_model_type(build.type, "--weightmode", [flare.QSARModelType.Field])

        if "Sim" not in project.ligands.columns:
            raise ValueError(
                '--weightmode requires the ligands to have a "Sim" tag or the tag '
                "containing the similarity data should be specified by --similarity-tag <tag>"
            )

        with cmdutil.CheckValue("weightmode"):
            build.field_weight_enabled = True
            if args.weightmode.startswith("l"):
                build.field_weight_power = flare.BuildQSARModel.Power.Linear
            elif args.weightmode.startswith("q"):
                build.field_weight_power = flare.BuildQSARModel.Power.Quadratic
            else:
                raise ValueError(f'invalid value passed to --weightmode. Found "{args.weightmode}"')

    # Activity Atlas QSAR settings
    if args.grid_spacing is not None:
        _verify_model_type(build.type, "--grid-spacing", [flare.QSARModelType.ActivityAtlas])
        with cmdutil.CheckValue("grid-spacing"):
            build.aa_grid_spacing = args.grid_spacing

    if args.disparity_range is not None:
        _verify_model_type(build.type, "--disparity-range", [flare.QSARModelType.ActivityAtlas])

        m = re.search(r"([0-9.]+)-([0-9.]+)", args.disparity_range)
        if m is None:
            raise ValueError(
                "format of --disparity-range value is invalid. "
                f'Found "{args.disparity_range}", expected "minDisparity-maxDisparity"'
            )

        with cmdutil.CheckValue("disparity-range"):
            build.aa_auto_calculate_disparity_range = False
            build.aa_disparity_min = float(m.group(1))
            build.aa_disparity_max = float(m.group(2))
            if build.aa_disparity_max < build.aa_disparity_min:
                raise ValueError("--disparity-range minDisparity must be <= maxDisparity")

    if args.activity_range is not None:
        _verify_model_type(build.type, "--activity-range", [flare.QSARModelType.ActivityAtlas])

        m = re.search(r"([0-9.]+)-([0-9.]+)", args.activity_range)
        if m is None:
            raise ValueError(
                "format of --activity-range value is invalid. "
                f'Found "{args.activity_range}", expected "minActivity-maxActivity"'
            )

        with cmdutil.CheckValue("activity-range"):
            build.aa_auto_calculate_activity_range = False
            build.aa_activity_min = float(m.group(1))
            build.aa_activity_max = float(m.group(2))
            if build.aa_activity_max < build.aa_activity_min:
                raise ValueError("--activity-range minActivity must be <= maxActivity")

    if args.similarity_range is not None:
        _verify_model_type(build.type, "--similarity-range", [flare.QSARModelType.ActivityAtlas])

        m = re.search(r"([0-9.]+)-([0-9.]+)", args.similarity_range)
        if m is None:
            raise ValueError(
                "format of --similarity-range value is invalid. "
                f'Found "{args.similarity_range}", expected "minSim-maxSim"'
            )

        with cmdutil.CheckValue("similarity-range"):
            build.aa_auto_calculate_similarity_range = False
            build.aa_similarity_min = float(m.group(1))
            build.aa_similarity_max = float(m.group(2))
            if build.aa_similarity_max < build.aa_similarity_min:
                raise ValueError("--similarity-range minSim must be <= maxSim")

    if args.shapewt is not None:
        _verify_model_type(
            build.type,
            "--shapewt",
            [
                flare.QSARModelType.ActivityAtlas,
                flare.QSARModelType.KNN_Classification,
                flare.QSARModelType.KNN_Regression,
            ],
        )
        with cmdutil.CheckValue("shapewt"):
            build.shape_weight = args.shapewt

    if args.optimize_alignment:
        _verify_model_type(
            build.type,
            "--optimize-alignment",
            [
                flare.QSARModelType.ActivityAtlas,
                flare.QSARModelType.KNN_Classification,
                flare.QSARModelType.KNN_Regression,
            ],
        )
        with cmdutil.CheckValue("optimize-alignment"):
            build.optimize_alignment = args.optimize_alignment

    if args.molecules_explore_count is not None:
        _verify_model_type(
            build.type, "--molecules-explore-count", [flare.QSARModelType.ActivityAtlas]
        )
        with cmdutil.CheckValue("molecules-explore-count"):
            build.aa_ligands_explore_count = args.molecules_explore_count

    if args.activity_atlas_algorithm is not None:
        _verify_model_type(
            build.type, "--activity-atlas-algorithm", [flare.QSARModelType.ActivityAtlas]
        )
        with cmdutil.CheckValue("activity-atlas-algorithm"):
            if args.activity_atlas_algorithm.startswith("w"):
                build.aa_algorithm = flare.BuildQSARModel.ActivityAtlasAlgorithm.WeightedSum
            else:
                build.aa_algorithm = flare.BuildQSARModel.ActivityAtlasAlgorithm.Sum

    # kNN Settings
    if args.max_k_components is not None:
        _verify_model_type(
            build.type,
            "--max-k-components",
            [flare.QSARModelType.KNN_Classification, flare.QSARModelType.KNN_Regression],
        )
        with cmdutil.CheckValue("max-k-components"):
            build.max_k_components = args.max_k_components

    if args.method is not None:
        _verify_model_type(
            build.type,
            "--method",
            [flare.QSARModelType.KNN_Classification, flare.QSARModelType.KNN_Regression],
        )
        with cmdutil.CheckValue("method"):
            if args.method == "field":
                build.knn_method = flare.SimilarityType.Field
            elif args.method == "ecfp4":
                build.knn_method = flare.SimilarityType.ECFP4
            elif args.method == "ecfp6":
                build.knn_method = flare.SimilarityType.ECFP6
            elif args.method == "fcfp4":
                build.knn_method = flare.SimilarityType.FCFP4
            elif args.method == "fcfp6":
                build.knn_method = flare.SimilarityType.FCFP6
            else:
                raise ValueError(f"invalid --method {args.method}")

    if args.weighting_scheme is not None:
        _verify_model_type(
            build.type,
            "--weighting-scheme",
            [flare.QSARModelType.KNN_Classification, flare.QSARModelType.KNN_Regression],
        )
        with cmdutil.CheckValue("weighting-scheme"):
            if args.weighting_scheme == "automatic":
                build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.Automatic
            elif args.weighting_scheme == "noweighting":
                build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.NoWeighting
            elif args.weighting_scheme == "inversedistance":
                build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.InverseDistance
            elif args.weighting_scheme == "exponential":
                build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.Exponential
            elif args.weighting_scheme == "exponential2":
                build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.Exponential2
            elif args.weighting_scheme == "exponential3":
                build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.Exponential3
            else:
                _verify_model_type(
                    build.type, "--weighting-scheme", [flare.QSARModelType.KNN_Regression]
                )
                if args.weighting_scheme == "mft11":
                    build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.MFT11
                elif args.weighting_scheme == "mft12":
                    build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.MFT12
                elif args.weighting_scheme == "mft13":
                    build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.MFT13
                elif args.weighting_scheme == "mft14":
                    build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.MFT14
                elif args.weighting_scheme == "mft21":
                    build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.MFT21
                elif args.weighting_scheme == "mft22":
                    build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.MFT22
                elif args.weighting_scheme == "mft23":
                    build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.MFT23
                elif args.weighting_scheme == "mft24":
                    build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.MFT24
                elif args.weighting_scheme == "mft31":
                    build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.MFT31
                elif args.weighting_scheme == "mft32":
                    build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.MFT32
                elif args.weighting_scheme == "mft33":
                    build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.MFT33
                elif args.weighting_scheme == "mft34":
                    build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.MFT34
                elif args.weighting_scheme == "mft41":
                    build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.MFT41
                elif args.weighting_scheme == "mft42":
                    build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.MFT42
                elif args.weighting_scheme == "mft43":
                    build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.MFT43
                elif args.weighting_scheme == "mft44":
                    build.knn_weighting_scheme = flare.BuildQSARModel.KnnWeightingScheme.MFT44
                else:
                    raise ValueError(f"invalid --weighting-scheme {args.weighting_scheme}")

    with cmdutil.CheckValue("localengines"):
        build.local_engine_count = args.localengines

    return (project, imported_tags)


def _is_int(value):
    """Return true if value is an integer."""
    try:
        return float(value).is_integer()
    except ValueError:
        return False


def _verify_model_type(type, opt, allowed_types):
    """Raises an error if `type` is not in `allowed_types`."""
    if type not in allowed_types:
        allowed_types_string_list = []
        for allowed_type in allowed_types[:-1]:
            allowed_types_string_list.append(str(allowed_type).split(".")[1])
        allowed_types_string = ", ".join(allowed_types_string_list)
        if allowed_types_string:
            allowed_types_string += " or "
        allowed_types_string += str(allowed_types[-1]).split(".")[1]

        type_str = str(type).split(".")[1]
        raise ValueError(
            f"the {allowed_types_string} option {opt} cannot be used when building a "
            f"{type_str} model"
        )


def _write_results(build, project, imported_tags, args):
    """Write the results."""
    # Print the result to the output directory

    output_dir = os.getcwd()
    output_prefix = "output"
    if args.output is not None:
        head, tail = os.path.split(args.output)
        if head:
            output_dir = head
        if tail:
            output_prefix = tail

    print(f'QSAR model written to directory: "{output_dir}"')
    os.makedirs(output_dir, exist_ok=True)

    # Keep the filename
    imported_tags.add("Filename_cresset")

    # Write the ligands to SDF
    def remove_noninput_tags(ligand, tags):
        """Filter out tags that weren't in the input file."""
        # Only print the SDF tags which were either in the input files or were created by
        # building the model
        tags_to_delete = [
            tag
            for tag in tags
            if tag not in imported_tags
            and re.match(f".*\\(.*{build.activity_column}\\).*", tag) is None
        ]
        for tag in tags_to_delete:
            del tags[tag]

        # Remove the _cresset from the end of the tags
        cresset_postfix = "_cresset"
        for tag in list(tags.keys()):
            if tag.endswith(cresset_postfix):
                tags[tag[: len(tag) - len(cresset_postfix)]] = tags[tag]
                del tags[tag]

        # Add the role name as a tag
        tags["Role"] = ligand.role.name

        return tags

    flare.write_file(
        os.path.join(output_dir, output_prefix + ".sdf"),
        project.ligands,
        "sdf",
        remove_noninput_tags,
    )

    qsar_model = build.created_qsar_model()

    # Write the QSAR log
    with open(os.path.join(output_dir, output_prefix + ".log"), "w") as file:
        file.write(qsar_model.log)

    # Write the CSV file
    if qsar_model.type == flare.QSARModelType.Field:
        _write_displayed_qsar_model_data_to_csv(
            qsar_model, os.path.join(output_dir, output_prefix + ".model.csv")
        )

    # Write the project
    project.save(os.path.join(output_dir, output_prefix + ".flr"))

    # Write the project with the ligands deleted to reduce the project file size
    project.roles.clear()
    # Also remove the columns as they contain no data.
    # But keep the activity column as that contains the configuration of the activity units
    for column in project.ligands.columns.user_keys():
        if column != qsar_model.source_activity_column:
            del project.ligands.columns[column]

    project.save(os.path.join(output_dir, output_prefix + ".model.flr"))

    # Write the ctivity Atlas surfaces if requested.
    if args.surface is not None and qsar_model.type == flare.QSARModelType.ActivityAtlas:
        surface_dir = os.path.join(output_dir, output_prefix + ".surfaces")
        os.makedirs(surface_dir, exist_ok=True)
        flare.write_grid_file(
            os.path.join(surface_dir, "Activity Cliff Summary of Electrostatics." + args.surface),
            [qsar_model.aa_activity_cliff_summary_of_electrostatics_grid],
        )
        flare.write_grid_file(
            os.path.join(surface_dir, "Activity Cliff Summary of Hydrophobics." + args.surface),
            [qsar_model.aa_activity_cliff_summary_of_hydrophobics_grid],
        )
        flare.write_grid_file(
            os.path.join(surface_dir, "Activity Cliff Summary of Shape." + args.surface),
            [qsar_model.aa_activity_cliff_summary_of_shape_grid],
        )
        flare.write_grid_file(
            os.path.join(surface_dir, "Average Electrostatics of Actives." + args.surface),
            [qsar_model.aa_average_electrostatics_of_actives_grid],
        )
        flare.write_grid_file(
            os.path.join(surface_dir, "Average Hydrophobics of Actives." + args.surface),
            [qsar_model.aa_average_hydrophobics_of_actives_grid],
        )
        flare.write_grid_file(
            os.path.join(surface_dir, "Average Shape of Actives." + args.surface),
            [qsar_model.aa_average_shape_of_actives_grid],
        )
        flare.write_grid_file(
            os.path.join(surface_dir, "Regions Explored in Hydrophobics." + args.surface),
            [qsar_model.aa_regions_explored_in_hydrophobics_grid],
        )
        flare.write_grid_file(
            os.path.join(
                surface_dir, "Regions Explored in Negative Electrostatics." + args.surface
            ),
            [qsar_model.aa_regions_explored_in_negative_electrostatics_grid],
        )
        flare.write_grid_file(
            os.path.join(
                surface_dir, "Regions Explored in Positive Electrostatics." + args.surface
            ),
            [qsar_model.aa_regions_explored_in_positive_electrostatics_grid],
        )
        flare.write_grid_file(
            os.path.join(surface_dir, "Shape Explored." + args.surface),
            [qsar_model.aa_shape_explored_grid],
        )


def _write_displayed_qsar_model_data_to_csv(qsar_model, file_path):
    """Write the QSAR model data to `file_path`."""
    with open(file_path, "w", newline="") as csvfile:
        writer = csv.writer(csvfile)

        field_descriptors_header = []
        for i, type in enumerate(qsar_model.descriptor_types, 1):
            field_descriptors_header.append(_qsar_model_descriptor_type_to_char(type) + str(i))

        descriptors_header = list(field_descriptors_header)
        descriptors_header.extend(qsar_model.column_descriptor_names)

        # Write descriptor matrix
        writer.writerow(["Type", "Molecule name", "SMILES", "Activity", *descriptors_header])
        for (ligand1, field_descriptors), (ligand2, column_descriptors) in zip(
            qsar_model.descriptors, qsar_model.column_descriptors
        ):
            assert ligand1 == ligand2
            title = ligand1.title if ligand1 is not None else ""
            smiles = ligand1.smiles() if ligand1 is not None else ""
            activity = (
                str(ligand1.properties[qsar_model.activity_column].value)
                if ligand1 is not None
                else ""
            )
            xy_matrix_row = ["Data matrix", title, smiles, activity]
            for value in field_descriptors:
                xy_matrix_row.append(str(value))
            for value in column_descriptors:
                xy_matrix_row.append(str(value))
            writer.writerow(xy_matrix_row)

        writer.writerow([])

        # Write the coefficients if the model has components
        if qsar_model.max_components > 0:
            writer.writerow(["Equation labels", "", "", "Intercept", *field_descriptors_header])
            coefficients_row = ["Equation coefficients", "", "", qsar_model.equation_intercept]
            for value in qsar_model.equation_coefficients:
                coefficients_row.append(str(value))
            writer.writerow(coefficients_row)
            writer.writerow([])

        # Write the coordinates of the where the descriptors field values were sampled from
        writer.writerow(["Sample co-ordinate labels", "", "", "", *field_descriptors_header])
        sample_x_row = ["Sample_X", "", "", ""]
        sample_y_row = ["Sample_Y", "", "", ""]
        sample_z_row = ["Sample_Z", "", "", ""]

        for x, y, z in qsar_model.sample_coordinates:
            sample_x_row.append(str(x))
            sample_y_row.append(str(y))
            sample_z_row.append(str(z))

        writer.writerow(sample_x_row)
        writer.writerow(sample_y_row)
        writer.writerow(sample_z_row)
        writer.writerow([])

        # Write the notes
        if qsar_model.notes:
            writer.writerow(["Notes", "", qsar_model.notes, ""])


def _qsar_model_descriptor_type_to_char(type):
    """Convert a descriptor to a 1 char str."""
    if type == flare.QSARModel.DescriptorType.Electrostatics:
        return "E"
    if type == flare.QSARModel.DescriptorType.VanDerWaals:
        return "S"
    if type == flare.QSARModel.DescriptorType.Hydrophobics:
        return "H"
    if type == flare.QSARModel.DescriptorType.Volume:
        return "V"
    return "?"


def _partition_activity_stratify(training_role, test_role, amount, activity_column):
    """Splits ligands between the training and test set based on activity."""
    ligands = list(training_role.ligands) + list(test_role.ligands)
    original_index = {}
    for index, ligand in enumerate(ligands):
        original_index[ligand] = index

    num_test_ligands = (amount / 100) * len(ligands)
    sample_increment = len(ligands) / num_test_ligands
    next = sample_increment / 2

    def sort_activity(ligand):
        activity = -math.inf
        try:
            activity = ligand.properties[activity_column].value
        except TypeError:
            pass
        return (activity, ligand.title)

    ligands.sort(key=sort_activity)

    training_ligands = []
    test_ligands = []
    for index, ligand in enumerate(ligands):
        if index + 1 > next:
            next += sample_increment
            test_ligands.append(ligand)
        else:
            training_ligands.append(ligand)

    # Sort each list so the ligands are in the same order as the input.
    training_ligands.sort(key=lambda ligand: original_index[ligand])
    test_ligands.sort(key=lambda ligand: original_index[ligand])

    training_role.ligands.move(0, training_ligands)
    test_role.ligands.move(0, test_ligands)


def _partition_random(training_role, test_role, amount):
    """Splits ligands between the training and test set randomly."""
    ligands = list(training_role.ligands) + list(test_role.ligands)
    original_index = {}
    for index, ligand in enumerate(ligands):
        original_index[ligand] = index

    random.shuffle(ligands)

    num_test_ligands = int((amount / 100) * len(ligands))
    num_training_ligands = len(ligands) - num_test_ligands

    training_ligands = ligands[0:num_training_ligands]
    test_ligands = ligands[num_training_ligands:]

    # Sort each list so the ligands are in the same order as the input.
    training_ligands.sort(key=lambda ligand: original_index[ligand])
    test_ligands.sort(key=lambda ligand: original_index[ligand])

    training_role.ligands.move(0, training_ligands)
    test_role.ligands.move(0, test_ligands)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
