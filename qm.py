#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Runs QM calculations on ligands and writes the results to stdout.

QM calculations uses Quantum Mechanics to minimize the given ligands.
There are two types of QM calculations: Ligand minimization and
electrostatic potential (ESP) calculation. ESP calculation computes
and outputs electrostatic surfaces. Only SDF file format is used for
ligand input and output.

By default it runs only on minimize mode. To switch to single-point
energy calculation, set --minimize-max-iterations to 0. To perform
both calculations, just run the script in minimize mode at first then
using that output as the input for running the script the second time
but on single-point energy mode.

The default DFT functional and basis set is different for ESP.
Instead of PBE and 3-21G, ESP would have B3LYP and 6-31G(d).

QM calculations on Conformations and Poses can be run using the ligand
minimization calculation, once the Conformation and Poses have been
exported as ligands into an SDF file format. For a given ligand in
Flare, Conformations and Poses can be exported from the Ligands table
using the 'Export Conformations' and 'Export Poses' right-click
functionalities, respectively.

examples:
  pyflare qm.py --method HF ligands.sdf
    Runs QM minimization calculation in minimize mode with ligands from
    the ligands.sdf and minimization method of "HF", then write the
    results to stdout.

  pyflare qm.py --minimize-max-iterations 0 ligands.sdf
    Runs QM minimization calculation in single-point energy only with
    ligands from the ligands.sdf, then write the results to stdout.

  pyflare qm.py --grid-output ccp4 --esp ligands.sdf
    Runs QM ESP calculation with ligands from the ligands.sdf file then
    writes the ligand results to stdout and grid file (of ESP surfaces)
    to a generated file in the specified "ccp4" grid output format.

  pyflare qm.py --minimize-max-iterations 0 --grid-output grd ligands.sdf
    Runs QM minimization calculation in single-point energy only with
    ligands from the ligands.sdf, then write the results to stdout and
    grid file (of electron density, HOMO, and LUMO surfaces) to a
    generated file in the specified "grd" grid output format.

  pyflare qm.py --help
    Prints the help text including all available options and exits.
"""
import sys
import argparse
import pathlib
from datetime import datetime
from cresset import flare
from _internal import cmdutil

GRID_OUTPUT_CHOICES = ["grd", "ccp4", "mgd", "dx"]

BASIS_SET_CHOICES = [
    "3-21G",
    "6-31G",
    "6-31G(d)",
    "6-31+G(d)",
    "6-311G(d)",
    "6-311G(d,p)",
    "6-311+G(d)",
    "6-311+G(d,p)",
    "6-311+G(2d,2p)",
    "def2-TZVP",
    "def2-TZVPP",
    "def2-QZVP",
    "def2-QZVPP",
    "cc-pVDZ",
    "cc-pVTZ",
    "cc-pVQZ",
    "aug-cc-pVDZ",
    "aug-cc-pVTZ",
    "aug-cc-pVQZ",
]

DFT_FUNCTIONAL_ENUM_MAP = {
    "PBE": flare.QM.DftFunctional.PBE,
    "B3LYP": flare.QM.DftFunctional.B3LYP,
    "wB97X-D": flare.QM.DftFunctional.WB97XD,
    "M06-2X": flare.QM.DftFunctional.M062X,
}

METHOD_ENUM_MAP = {
    "DFT": flare.QM.Method.DFT,
    "HF": flare.QM.Method.HF,
    "CCSD": flare.QM.Method.CCSD,
    "MP2": flare.QM.Method.MP2,
    "GFN2-xTB": flare.QM.Method.XTB,
}

TOLERANCE_ENUM_MAP = {
    "Loose": flare.QM.Tolerance.Loose,
    "Normal": flare.QM.Tolerance.Normal,
    "Tight": flare.QM.Tolerance.Tight,
    "VeryTight": flare.QM.Tolerance.VeryTight,
}

CONVERGENCE_ENUM_MAP = {
    "Loose": flare.QM.Convergence.Loose,
    "Medium": flare.QM.Convergence.Medium,
    "Tight": flare.QM.Convergence.Tight,
}

PCM_ENUM_MAP = {
    "IEFPCM": flare.QM.Pcm.IEFPCM,
    "CPCM": flare.QM.Pcm.CPCM,
}

SOLVENT_ENUM_MAP = {
    "Vacuum": flare.QM.Solvent.Vacuum,
    "Water": flare.QM.Solvent.Water,
    "Dimethylsulfoxide": flare.QM.Solvent.Dimethylsulfoxide,
    "Nitromethane": flare.QM.Solvent.Nitromethane,
    "Acetonitrile": flare.QM.Solvent.Acetonitrile,
    "Methanol": flare.QM.Solvent.Methanol,
    "Ethanol": flare.QM.Solvent.Ethanol,
    "Acetone": flare.QM.Solvent.Acetone,
    "1,2-Dichloroethane": flare.QM.Solvent.N1c2Dichloroethane,
    "Methylenechloride": flare.QM.Solvent.Methylenechloride,
    "Tetrahydrofurane": flare.QM.Solvent.Tetrahydrofuran,
    "Tetrahydrofuran": flare.QM.Solvent.Tetrahydrofuran,
    "Aniline": flare.QM.Solvent.Aniline,
    "Chlorobenzene": flare.QM.Solvent.Chlorobenzene,
    "Chloroform": flare.QM.Solvent.Chloroform,
    "Toluene": flare.QM.Solvent.Toluene,
    "1,4-Dioxane": flare.QM.Solvent.N1c4Dioxane,
    "Benzene": flare.QM.Solvent.Benzene,
    "Carbon Tetrachloride": flare.QM.Solvent.CarbonTetrachloride,
    "Cyclohexane": flare.QM.Solvent.Cyclohexane,
    "N-heptane": flare.QM.Solvent.Nheptane,
}

DISPERSIONCORRECTION_ENUM_MAP = {
    "Auto": flare.QM.DispersionCorrection.Auto,
    "False": False,
    "True": True,
}


def main(argv: [str]) -> int:
    args, parser = _parse_args(argv)

    if args.esp and args.grid_output is None:
        parser.print_usage()
        print("qm.py: error: ESP requires --grid-output to be specified.", file=sys.stderr)
        return 1

    project = flare.Project()
    if args.ligands is not None:
        for ligands_filepath in args.ligands:
            project.ligands.extend(flare.read_file(ligands_filepath, "sdf"))
    else:
        project.ligands.extend(flare.read_file(sys.stdin, "sdf"))

    logging_context = cmdutil.LoggingContext(project.log, verbose=args.verbose)

    qm = flare.QM()
    qm.esp_mode = args.esp is True
    # If ESP, just set a different default
    if qm.esp_mode:
        qm.dft_functional = flare.QM.DftFunctional.B3LYP
        qm.dispersion_correction = True
        qm.solvent = flare.QM.Solvent.Vacuum
        qm.basis_set = "6-31G(d)"

    ligands = []
    for ligand in project.ligands:
        ligands.append(ligand)
    qm.ligands = ligands

    qm = _configure_process(qm, args)

    cmdutil.start_process(qm, args.broker, args.verbose)
    cmdutil.wait_for_finish(qm, logging_context=logging_context)
    cmdutil.check_errors(qm)

    if qm.errors():
        print("\n".join(qm.errors()), file=sys.stderr)

    if not qm.is_cancelled():
        # Write project/QM result file
        if args.project:
            try:
                project.save(args.project)
            except Exception as err:
                print(err, file=sys.stderr)

        if args.log:
            for ligand in qm.ligands:
                ligand.properties["FlareQM_Log"].value = cmdutil.molecule_log_as_sdf_string(ligand)

        if args.grid_output:
            now_datetime_str = datetime.now().strftime("%Y%m%d_%H%M%S")
            SURFACE_FILE_PROP = "Surface_Files"

            if args.grid_output_directory:
                # If directory specified yet doesn't exists, create it
                pathlib.Path(args.grid_output_directory).mkdir(parents=True, exist_ok=True)

            for ligand in qm.ligands:
                # Only mgd can do multiple of grids, so just do one surface file for mgd, but
                # other surface file types do multiples
                if args.grid_output == "mgd":
                    surface_file = ligand.title + "_surfaces_" + now_datetime_str + ".mgd"
                    surface_file = surface_file.replace(" ", "_")
                    ligand.properties[SURFACE_FILE_PROP].value = surface_file
                    grids = []
                    for surface in ligand.extra_surfaces:
                        grids.append(surface.grid())
                    if args.grid_output_directory:
                        surface_file = str(
                            pathlib.PurePath(args.grid_output_directory, surface_file)
                        )
                    flare.write_grid_file(surface_file, grids, "mgd")
                else:
                    surface_files_property = []
                    for surface in ligand.extra_surfaces:
                        surface_file = (
                            ligand.title
                            + "_surface_"
                            + surface.name
                            + "_"
                            + now_datetime_str
                            + "."
                            + args.grid_output
                        )
                        surface_file = surface_file.replace(" ", "_")
                        if args.grid_output_directory:
                            surface_file = str(
                                pathlib.PurePath(args.grid_output_directory, surface_file)
                            )
                        flare.write_grid_file(surface_file, [surface.grid()], args.grid_output)
                        surface_files_property.append(surface_file)
                    ligand.properties[SURFACE_FILE_PROP].value = "\n".join(surface_files_property)

        flare.write_file(sys.stdout, qm.ligands, "sdf")

    return 0


def _parse_args(argv: [str]) -> (argparse.Namespace, argparse.ArgumentParser):
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )

    NO_ESP_MSG = "Not used for ESP calculation."
    LINUX_ONLY_MSG = "Only available on Linux locally or through the broker."

    parser.add_argument(
        "ligands",
        nargs="*",
        help="The ligands file to run QM minimization calculation on. If not given "
        + "the ligands are read from stdin.",
    )

    group = parser.add_argument_group("common settings")
    group.add_argument(
        "-m",
        "--method",
        choices=METHOD_ENUM_MAP.keys(),
        help="Set the method used for QM.",
    )
    group.add_argument(
        "-t",
        "--dft-functional",
        help="Set the DFT functional from a pre-defined set ("
        + ",".join(DFT_FUNCTIONAL_ENUM_MAP.keys())
        + ") or a custom string.",
    )
    group.add_argument(
        "-c",
        "--dispersion-correction",
        choices=DISPERSIONCORRECTION_ENUM_MAP.keys(),
        help="Disable/enable dispersion correction or let QM automatically set.",
    )
    group.add_argument(
        "-b",
        "--basis-set",
        choices=BASIS_SET_CHOICES,
        help="Specify the basis set to use.",
    )
    group.add_argument(
        "-S",
        "--solvent",
        choices=SOLVENT_ENUM_MAP.keys(),
        help="Set the solvation model. " + LINUX_ONLY_MSG,
    )

    group = parser.add_argument_group("minimization only settings")
    group.add_argument(
        "-T",
        "--minimize-tolerance",
        choices=TOLERANCE_ENUM_MAP.keys(),
        help="Set the tolerance criteria to use in minimization. Tighter criteria "
        + "will take more iterations but will return a structure closer to the "
        + "bottom of the potential energy well. "
        + NO_ESP_MSG,
    )
    group.add_argument(
        "-I",
        "--minimize-max-iterations",
        type=int,
        help="Set the minimize maximum iterations to do before giving up. " + NO_ESP_MSG,
    )

    group = parser.add_argument_group("single-point energy only settings")
    group.add_argument(
        "--pcm-method",
        choices=PCM_ENUM_MAP.keys(),
        help="Set the energy PCM method to use. " + LINUX_ONLY_MSG,
    )
    group.add_argument(
        "-C",
        "--scf-convergence",
        choices=CONVERGENCE_ENUM_MAP.keys(),
        help="Set the covergence criteria to use in the SCF.",
    )
    group.add_argument(
        "--scf-max-iterations",
        type=int,
        help="Set the SCF maximum iterations to do before giving up.",
    )

    group = parser.add_argument_group("grid output settings")
    group.add_argument(
        "--grid-output",
        choices=GRID_OUTPUT_CHOICES,
        help="Set the file output for writing the PSI4 electron density/HOMO/LUMO,"
        + " or ESP surfaces in cube file format.",
    )
    group.add_argument(
        "--grid-output-directory",
        help="Set the directory path where the grid files should output to.",
    )

    group = parser.add_argument_group("ESP")
    group.add_argument(
        "--esp",
        action="store_true",
        help="Runs an electric-static potential calculation. Requires --grid-output.",
    )
    group.add_argument(
        "--esp-grid-spacing",
        type=float,
        help="ESP grid spacing used to calculate ESP grid in angstrom. Only used for ESP.",
    )

    group = parser.add_argument_group("resource")
    group.add_argument(
        "--max-threads",
        type=int,
        help="Set the number of maximum threads. Setting 0 or not setting will automatically "
        + "determine the maximum amount of threads used.",
    )
    group.add_argument(
        "-M",
        "--max-memory",
        type=float,
        help="Set the amount of maximum memory in GB. Ideally as much as possible, "
        + "up to 80%% of total system memory. Setting 0 will automatically determine "
        + "the maximum amount of memory used, otherwise 4GB is the default.",
    )

    group = parser.add_argument_group("general options")
    group.add_argument(
        "-L",
        "--log",
        action="store_true",
        help="Request that the calculation log be written as one of the SDF tags for "
        + "each ligand. The calculation log is written as tag 'FlareQM_Log'.",
    )
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        help="Set the number of local engines to start. Use 0 for no local processing: "
        + "you must specify some remote engines with -g if you do so.",
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker in the format "
        + "hostname:port. Field Engines which have been registered with the "
        + "Broker will be used during the calculation. The CRESSET_BROKER "
        + "environment variable can be used instead of this option.",
    )
    group.add_argument(
        "-P",
        "--project",
        help="Write the Flare project file with the results to the given file path.",
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )

    return (parser.parse_args(argv), parser)


def _configure_process(qm: flare.QM, args: argparse.Namespace) -> flare.QM:
    # Common
    if args.method in METHOD_ENUM_MAP:
        qm.method = METHOD_ENUM_MAP[args.method]
    if args.dft_functional:
        if args.dft_functional in DFT_FUNCTIONAL_ENUM_MAP:
            qm.dft_functional = DFT_FUNCTIONAL_ENUM_MAP[args.dft_functional]
        else:
            qm.dft_functional = args.dft_functional
    if args.dispersion_correction in DISPERSIONCORRECTION_ENUM_MAP:
        qm.dispersion_correction = DISPERSIONCORRECTION_ENUM_MAP[args.dispersion_correction]
    if args.basis_set:
        qm.basis_set = args.basis_set

    # Minimize only
    if not args.esp:
        if args.minimize_tolerance in TOLERANCE_ENUM_MAP:
            qm.minimize_tolerance = TOLERANCE_ENUM_MAP[args.minimize_tolerance]
        if args.minimize_max_iterations is not None:
            qm.minimize_max_iterations = args.minimize_max_iterations

    # Energy only
    if args.pcm_method in PCM_ENUM_MAP:
        qm.energy_pcm_method = PCM_ENUM_MAP[args.pcm_method]
    if args.solvent in SOLVENT_ENUM_MAP:
        qm.solvent = SOLVENT_ENUM_MAP[args.solvent]

    if args.scf_convergence in CONVERGENCE_ENUM_MAP:
        qm.scf_convergence = CONVERGENCE_ENUM_MAP[args.scf_convergence]
    if args.scf_max_iterations is not None:
        qm.scf_max_iterations = args.scf_max_iterations

    # ESP only
    if args.esp_grid_spacing and args.esp:
        qm.esp_grid_spacing = args.esp_grid_spacing

    # Resources
    if args.max_threads is not None:
        qm.max_threads = args.max_threads
    if args.max_memory is not None:
        qm.max_memory = args.max_memory
    if args.localengines is not None:
        qm.local_engine_count = args.localengines

    return qm


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
