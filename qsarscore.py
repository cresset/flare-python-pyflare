#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Calculates predicted data for ligands against a model.

The type of data which can be predicted depends on the type of model used.

Field QSAR
    Calculates the predicted activity of each ligand and the ligand's distance to the QSAR model.

Activity Atlas
    Calculates a 'Novelty' score for new ligands. Ligands with a low 'Novelty' don't expand our understanding of the SAR, while those with too high a value are potentially taking too bold a leap into the unknown. Designing compounds into the middle ground allows the SAR to be efficiently explored, giving the maximum understanding with the least synthetic effort.

k Nearest Neighbor (kNN)
    Calculates the predicted activity of each ligand, the error of the prediction and the ligand's distance to the model.

input:
  The input ligands should be read in SDF format.

  The ligands need to be pre-aligned for all the QSAR models except for the kNN model when using 2D similarities. align.py is a recommended tool for this.

output:
  By default qsarscore.py writes its results to standard output in SDF format.

examples:
  pyflare qsarscore.py model.flr mols.sdf > results.sdf
    Reads the (aligned!) ligands in 'mols.sdf', and calculates the predicted activity values for them against the Flare model contained in 'model.flr'. The results are written to 'results.sdf' in SDF format

  pyflare qsarscore.py -i model.flr
    Prints the calculation log for the Flare model contained in 'model.flr' - this can be useful to see what ligands were used to score the model, and to see model statistics.

  pyflare qsarscore.py -o csv model.flr mols.sdf
    Reads the (aligned!) ligands in 'mols.sdf', and calculates the predicted activity values for them against the Flare model contained in 'model.flr'. The results are written to standard output in CSV format.

  pyflare qsarscore.py -n 3 -x molsamps.csv model.flr mymols.sdf
    Reads the (aligned!) ligands in 'mymols.sdf' and calculated the predicted activity values for them against the Flare model contained in 'model.flr', using 3 PLS components of the model. The sample values for each molecule that were used to calculate the predicted activity values are output in 'molsamps.csv' - if you derived an external model from data exported by fbuild you can use these sample values to fit these ligands to the external model.

  pyflare qsarscore.py --help
    Prints the help text including all available options and exits.

flare workflow examples:
  Suppose that you have a reference ligand in 'reference.sdf', its protein in 'protein.pdb', a bunch of training set ligands in 'training.sdf'. We want to align the ligands using the MCS method, score a Flare model and then score the ligands in 'othermols.sdf' against that model. We'll use an 80/20 training/test set split, done by activity.

  First, align the training set:

  pyflare align.py --mcs normal -p protein.pdb reference.sdf training.sdf > aligned_training.sdf

Next, score the model - the model will be output to the project file 'output.model.flr'.

  pyflare qsarbuild.py -p 20 aligned_training.sdf

Finally, align and score 'othermols.sdf' against the model:

  pyflare align.py --mcs normal -p protein.pdb reference.sdf othermols.sdf | pyflare qsarscore.py output.model.flr > results.sdf
"""  # noqa: E501
import sys
import re
import csv
import argparse
import collections
import traceback

from cresset import flare

from _internal import cmdutil

DEFAULT_BATCH_SIZE = 5000


def main(argv):
    """Builds a QSAR model using the command line args."""
    try:
        score = flare.UpdateQSARModelPredictions()
        args = _parse_args(score, argv)

        project = flare.Project.load(args.project[0])

        qsar_model = project.qsar_models[0] if project.qsar_models else None

        if args.modeltype:
            if args.info:
                raise ValueError("--modeltype and --info cannot both be used")
            if args.statistics:
                raise ValueError("--modeltype and --statistics cannot both be used")
            _print_model_type(qsar_model)
        elif args.info:
            if args.statistics:
                raise ValueError("--info and --statistics cannot both be used")
            _print_info(qsar_model)

        else:
            if len(project.qsar_models) == 0:
                raise ValueError("the project does not contain any QSAR models")
            if len(project.qsar_models) > 1:
                raise ValueError(
                    f"the Flare project contains {len(project.qsar_models)} QSAR models, "
                    "only projects with one model can be used"
                )

            elif args.statistics:
                _print_statistics(qsar_model)
            else:
                _score_ligands(score, project, qsar_model, args)

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _score_ligands(score, project, qsar_model, args):
    """Score the ligands using the QSAR model and write the results to stdout."""
    prediction_role = _configure_process(score, project, qsar_model, args)

    if args.numcomponents is not None:
        qsar_model.num_components = args.numcomponents

    imported_tags = set()

    # The file object to output the samples to if --export is given
    sample_file = None

    # When --output csv is set then this contains the columns which are written to the csv file.
    # This is set processing the first batch and reused for all other batches
    csv_headers = None

    try:
        if args.export is not None:
            sample_file = _create_samples_file(args.export, qsar_model)

        # Read each batch in turn and process them, this is to reduce memory usage by not holding
        # all ligands in memory at the same time
        any_ligands_read = False
        for i, _ in enumerate(_read_batch_into_role(args, prediction_role, qsar_model), 1):
            any_ligands_read = True

            # The outputted SDF file should contain tags for
            # 1) The calculated QSAR Scores
            # 2) the columns which were created from SDF tags when importing ligands are
            # added to imported_tags. The Loaded project may already contain columns. They should
            # only be outputted if any ligands that have been read had data for those columns.
            for column in project.ligands.columns.user_keys():
                if column not in imported_tags:
                    for ligand in prediction_role.ligands:
                        if (
                            ligand.properties[column].value is not None
                            and ligand.properties[column].value != ""
                        ):
                            imported_tags.add(column)
                            break

            score.ligands = prediction_role.ligands

            # Run the calculation, only kNN models require the calculation to be run.
            # Its a no-op for other model types
            cmdutil.start_process(score, args.broker, args.verbose)
            cmdutil.wait_for_finish(score, prefix=f"Batch {i}, ")

            # If there are errors don't stop, write the results and continue on to the next batch.
            if score.errors():
                print("\n".join(score.errors()), file=sys.stderr)

            csv_headers = _write_results(
                score,
                project,
                qsar_model,
                prediction_role,
                imported_tags,
                csv_headers,
                sample_file,
                args,
            )

    finally:
        if sample_file is not None:
            sample_file.close()

    if not any_ligands_read:
        raise ValueError("no ligands were read from the input")


def _parse_args(score, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )
    # Input settings
    parser.add_argument(
        "project",
        nargs=1,
        help="The Flare project containing the QSAR model.",
    )
    parser.add_argument(
        "ligands",
        nargs="*",
        help="The ligand to score. If not given the ligands are read from stdin.",
    )
    group = parser.add_argument_group("input")
    group.add_argument(
        "-Z",
        "--input-ignore-errors",
        action="store_true",
        help="By default, the calculation stops when errors are encountered in the input molecules. However, by setting this option the calculation will continue processing the remaining molecules and log the errors to standard error (stderr).",  # noqa: E501
    )
    # Output settings
    group = parser.add_argument_group("output")
    group.add_argument(
        "-o",
        "--output",
        choices=["sdf", "csv"],
        default="sdf",
        help="Set the format of the output data: either an SDF stream containing tags for the predicted activity, or a CSV file holding the molecule title, the predicted activity and other data.",  # noqa: E501
    )
    group.add_argument(
        "-x",
        "--export",
        metavar="samples.csv",
        help="Export the field sample data to the specified file. This is useful if you have built your own QSAR model from the model data exported by the qsarbuild.py script. Only Field QSAR models contain field sample data.",  # noqa: E501
    )

    # Common settings
    group = parser.add_argument_group("qsar settings")
    group.add_argument(
        "-i",
        "--info",
        action="store_true",
        help="Print information on the model to standard error and then exit.",  # noqa: E501
    )
    group.add_argument(
        "--modeltype",
        action="store_true",
        help="Print the type of the model to standard error and then exit.",  # noqa: E501
    )
    group.add_argument(
        "--statistics",
        action="store_true",
        help="Print the model statistics to standard error in CSV format and then exit. Only Field QSAR and kNN can print their statistics.",  # noqa: E501
    )
    group.add_argument(
        "-n",
        "--numcomponents",
        metavar="C",
        type=int,
        help="Field QSAR - Use C PLS components when fitting compounds to the model.\nk Nearest Neighbour (kNN) - Use k components when fitting compounds to the model.\nActivity Atlas, - This option is ignored for these models.\n\nIf not specified, then the default number of components for the model will be used.",  # noqa: E501
    )

    # Other settings
    group = parser.add_argument_group("general options")
    group.add_argument(
        "--batch-size",
        type=int,
        default=DEFAULT_BATCH_SIZE,
        help="Limit the amount of ligands which are read into memory. Smaller values will reduce memory usage but will reduce performance by a small amount.",  # noqa: E501
    )
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        default=score.local_engine_count,
        help="Set the number of local engines to start. Use 0 for no local processing: you must specify some remote engines with -g if you do so.",  # noqa: E501
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker in the format hostname:port. Field Engines which have been registered with the Broker will be used during the calculation. The CRESSET_BROKER environment variable can be used instead of this option.",  # noqa: E501
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )

    args = parser.parse_args(argv)

    return args


def _configure_process(score, project, qsar_model, args):
    """Configure `score` using the command line arguments."""
    if len(project.qsar_models) == 0:
        raise ValueError("the Flare project does not contain any QSAR models")
    if len(project.qsar_models) > 1:
        raise ValueError(
            f"the Flare project contains {len(project.qsar_models)} QSAR models, "
            "only projects with one model can be used"
        )

    qsar_model = project.qsar_models[0]
    score.qsar_models = [qsar_model]

    prediction_role = project.roles.append("Prediction Set")

    with cmdutil.CheckValue("localengines"):
        score.local_engine_count = args.localengines

    return prediction_role


def _write_results(
    score, project, qsar_model, prediction_role, imported_tags, csv_headers, sample_file, args
):
    """Write the results."""

    # Keep the filename column, SDF has _cresset appended, csv does not
    imported_tags.add("Filename_cresset")
    imported_tags.add("Filename")

    # Write the ligands to SDF
    def remove_noninput_tags(ligand, tags):
        """Filter out tags that weren't in the input file."""
        # Only print the SDF tags which were either in the input files or were created by
        # scoring with the the model
        tags_to_delete = [
            tag
            for tag in tags
            if tag not in imported_tags
            and re.match(f".*\\(.*{qsar_model.source_activity_column}\\).*", tag) is None
        ]
        for tag in tags_to_delete:
            del tags[tag]

        # Remove the _cresset from the end of the tags
        cresset_postfix = "_cresset"
        for tag in list(tags.keys()):
            if tag.endswith(cresset_postfix):
                tags[tag[: len(tag) - len(cresset_postfix)]] = tags[tag]
                del tags[tag]

        return tags

    if args.output == "sdf":
        flare.write_file(sys.stdout, prediction_role.ligands, "sdf", remove_noninput_tags)
    elif args.output == "csv":
        if csv_headers is None:
            csv_headers = _find_columns_to_export_to_csv(project, remove_noninput_tags)
            _write_results_header_to_csv(sys.stdout, csv_headers)
        _write_results_to_csv(
            sys.stdout, prediction_role.ligands, csv_headers, remove_noninput_tags
        )
    else:
        raise ValueError(f"invalid --output option {args.output}")

    _write_samples(sample_file, qsar_model, prediction_role.ligands)
    return csv_headers


def _find_columns_to_export_to_csv(project, remove_tags_func):
    """Return the names of the columns to include in the CSV export."""
    tags = collections.OrderedDict()
    tags["Filename"] = None
    for column in project.ligands.columns.user_keys():
        tags[column] = None

    # Use the same function as SDF output to filter out the columns/tags that are not needed.
    tags = remove_tags_func(None, tags)

    return list(tags.keys())


def _csv_writer(file):
    """Workaround `writerow` writing an extra "\r\n" on windows when outputting to the console"""
    if (file == sys.stdout or file == sys.stderr) and sys.platform.startswith("win"):
        return csv.writer(file, lineterminator="\n")
    else:
        return csv.writer(file)


def _write_results_header_to_csv(file, csv_headers):
    """Write the header to `file` in CSV format."""
    writer = _csv_writer(file)
    writer.writerow(["Title", "Structure", *csv_headers])


def _write_results_to_csv(file, ligands, csv_headers, remove_tags_func):
    """Write the ligands to `file` in CSV format."""
    writer = _csv_writer(file)

    for ligand in ligands:
        tags = collections.defaultdict()
        # ligand.properties.items() should not be used here as if a built in column has the same
        # name as a user column then it will return the value of the built in column.
        # ligand.properties[key] returns the user column if there is a
        # built in column with the same name
        for key in ligand.properties.keys():
            tags[key] = ligand.properties[key].value

        # Use the same function as SDF output to filter out the columns/tags that are not needed.
        tags = remove_tags_func(ligand, tags)

        csv_values = []
        for header in csv_headers:
            csv_values.append(tags.get(header, ""))

        writer.writerow([ligand.title, ligand.smiles(), *csv_values])


def _read_batch_into_role(args, role, qsar):
    """Yield when the `role` ligands is updated to a new batch."""
    # Read from stdin if a file is not given
    mol_files = args.ligands if len(args.ligands) > 0 else ["-"]

    raise_exception_on_error = not args.input_ignore_errors

    role.ligands.clear()
    for mol_file in mol_files:
        for batch, errors in cmdutil.batch_read_files(
            [mol_file], "sdf", args.batch_size, raise_exception_on_error
        ):
            for item in batch:
                role.ligands.append(item)

            for error in errors:
                print(error, file=sys.stderr)

            if qsar.method is None or qsar.method == flare.SimilarityType.Field:
                for ligand in role.ligands:
                    ligand.poses.append(ligand)

            if len(role.ligands) >= args.batch_size:
                yield
                role.ligands.clear()

    if role.ligands:
        yield


def _print_info(qsar_model):
    """Print the QSAR model log to stdout."""
    if qsar_model is None:
        print("The project does not contain any QSAR models")
    else:
        print(qsar_model.log)


def _print_model_type(qsar_model):
    """Print the QSAR model type to stdout."""
    if qsar_model is None:
        print("None")
    elif qsar_model.type == flare.QSARModelType.ActivityAtlas:
        # Add a space to the model name
        print("Activity Atlas")
    else:
        # For the other model types the string of the enum value is used with _ converted to a space
        print(str(qsar_model.type).split(".")[-1].replace("_", " "))


def _print_statistics(qsar_model):
    """Print the QSAR model statistics to stdout in csv format."""
    if qsar_model.type == flare.QSARModelType.Field:
        _print_field_statistics(qsar_model)
    elif qsar_model.type == flare.QSARModelType.KNN_Regression:
        _print_knn_regression_statistics(qsar_model)
    elif qsar_model.type == flare.QSARModelType.KNN_Classification:
        _print_knn_classification_statistics(qsar_model)
    else:
        print("The QSAR Model does not contain any statistics", file=sys.stderr)


def _format(num, precision=3):
    """Rounds `num` and converts it to a str."""
    return f"{num:.{precision}f}" if num is not None else ""


def _print_field_statistics(qsar_model):
    """Print the Field QSAR model statistics to stdout in csv format."""
    writer = _csv_writer(sys.stdout)

    writer.writerow(["Comps", "R^2", "Q^2", "RMSE", "RMSEpred", "Tau", "Tau-pred"])

    for i in range(qsar_model.max_components + 1):
        r_squared = qsar_model.r_squared(i)
        q_squared = qsar_model.q_squared(i)
        rmse = qsar_model.rmse(i)
        rmse_cv = qsar_model.rmse_cv(i)
        tau = qsar_model.tau(i)
        tau_cv = qsar_model.tau_cv(i)

        comps = str(i)
        if i == qsar_model.optimal_components:
            comps += "*"

        writer.writerow(
            [
                comps,
                _format(r_squared),
                _format(q_squared),
                _format(rmse),
                _format(rmse_cv),
                _format(tau),
                _format(tau_cv),
            ]
        )


def _print_knn_regression_statistics(qsar_model):
    """Print the kNN regression QSAR model statistics to stdout in csv format."""
    writer = _csv_writer(sys.stdout)

    writer.writerow(["K", "Distance Avg", "Distance Std. dev.", "Q^2", "RMSEpred", "Tau-pred"])

    for i in range(qsar_model.max_components + 1):
        distance_mean = qsar_model.distance_mean(i)
        distance_std_dev = qsar_model.distance_std_dev(i)
        q_squared = qsar_model.q_squared(i)
        rmse_cv = qsar_model.rmse_cv(i)
        tau_cv = qsar_model.tau_cv(i)

        comps = str(i)
        if i == qsar_model.optimal_components:
            comps += "*"

        writer.writerow(
            [
                comps,
                _format(distance_mean, 2),
                _format(distance_std_dev, 2),
                _format(q_squared),
                _format(rmse_cv),
                _format(tau_cv),
            ]
        )


def _print_knn_classification_statistics(qsar_model):
    """Print the kNN classification QSAR model statistics to stdout in csv format."""
    writer = _csv_writer(sys.stdout)

    writer.writerow(
        ["K", "Distance Avg", "Distance Std. dev.", "Informedness", "F1", "Precision", "Recall"]
    )

    for i in range(qsar_model.max_components + 1):
        distance_mean = qsar_model.distance_mean(i)
        distance_std_dev = qsar_model.distance_std_dev(i)
        informedness = qsar_model.informedness(i)
        f1 = qsar_model.f1(i)
        precision = qsar_model.precision(i)
        recall = qsar_model.recall(i)

        comps = str(i)
        if i == qsar_model.optimal_components:
            comps += "*"

        writer.writerow(
            [
                comps,
                _format(distance_mean, 2),
                _format(distance_std_dev, 2),
                _format(informedness),
                _format(f1),
                _format(precision),
                _format(recall),
            ]
        )


def _create_samples_file(file_path, qsar_model):
    """Create a CSV file for the molecule sample data and writes the header to it.

    None is returned if the QSAR model does not support sample dta.
    """
    if qsar_model.type == flare.QSARModelType.Field:
        sample_file = open(file_path, "w", newline="")
        writer = _csv_writer(sample_file)
        descriptors_header = []
        for i, type in enumerate(qsar_model.descriptor_types, 1):
            descriptors_header.append(_qsar_model_descriptor_type_to_char(type) + str(i))
        writer.writerow(["Title", *descriptors_header])
        return sample_file
    else:
        print("The QSAR model does not contain sample data", file=sys.stderr)
        return None


def _write_samples(sample_file, qsar_model, ligands):
    """Writes the ligand sample data to `sample_file` in CSV format."""
    if sample_file is not None:
        writer = _csv_writer(sample_file)
        for ligand in ligands:
            samples = qsar_model.calculate_samples(ligand)
            writer.writerow([ligand.title, *samples])


def _qsar_model_descriptor_type_to_char(type):
    """Convert a descriptor to a 1 char str."""
    if type == flare.QSARModel.DescriptorType.Electrostatics:
        return "E"
    if type == flare.QSARModel.DescriptorType.VanDerWaals:
        return "S"
    if type == flare.QSARModel.DescriptorType.Hydrophobics:
        return "H"
    if type == flare.QSARModel.DescriptorType.Volume:
        return "V"
    return "?"


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
