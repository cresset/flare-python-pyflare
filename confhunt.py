#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Performs a conf hunt of each molecule using the XED force field and writes the results to stdout.

The conformational hunt process is designed to generate a highly diverse set of conformations,
all of which are minimized, in as short a space of time as possible.

algorithm:

The conf hunt first loads and converts the input molecule into XED atom types and applies the XED
charge model. The molecule is then popped into 3D (if the input coords were 2D) using a relatively
crude but fairly robust algorithm which includes a ring library to ensure reasonable starting
geometries for rings. This starting structure is then minimized. If required (or if the molecule
contains flexible rings which were not present in the ring library), a short high-temperature
dynamics run is performed to explore ring conformations. A Monte Carlo bond twist routine is
then used to generate conformers. The twist bond routine by default generates up to 2000
conformers, which are then serially minimized, filtered and stored until all have been processed
or a preset limiting number of conformations has been found. The final set of conformers is
then written to standard output.

examples:
  pyflare confhunt.py molecule.sdf > molecule_confs.sdf
    Perform a conformation search on the molecule in `molecule.sdf` and write the results to
    `molecule_confs.sdf`.

  pyflare conf_hunt.py --help
    Prints the help text including all available options and exits.
"""
import sys
import argparse
import traceback

from cresset import flare

from _internal import cmdutil

DEFAULT_BATCH_SIZE = 10000


def main(argv):
    """Run the confhunt process using the command line args."""
    try:
        conf_hunt = flare.ConformationHunt()

        args = _parse_args(conf_hunt, argv)
        project = _configure_process(conf_hunt, args)

        imported_tags = set()

        logging_context = cmdutil.LoggingContext(project.log, verbose=args.verbose)

        # Read each batch in turn and process them
        any_ligands_read = False
        for i, batch in enumerate(_read_batch(project, args), 1):
            any_ligands_read = True

            imported_tags = imported_tags.union(set(project.ligands.columns.user_keys()))

            conf_hunt.ligands = project.ligands

            # Run the calculation
            cmdutil.start_process(conf_hunt, args.broker, args.verbose)
            cmdutil.wait_for_finish(
                conf_hunt, prefix=f"Batch {i}, ", logging_context=logging_context
            )

            # If there are errors don't stop, write the results and continue on to the next batch.
            # As if a few conf hunts failed we still want to print the results for all other
            # successful conf hunts.
            if conf_hunt.errors():
                print("\n".join(conf_hunt.errors()), file=sys.stderr)

            if not conf_hunt.is_cancelled():
                _write_results(conf_hunt, project, imported_tags, args)

        if not any_ligands_read:
            raise ValueError("no ligands were read from the input")

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(conf_hunt, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )
    # Input settings
    parser.add_argument(
        "ligands",
        nargs="*",
        help="The ligand files to conf hunt. If not given the ligands are read from stdin.",
    )

    group = parser.add_argument_group("input")
    group.add_argument(
        "-i",
        "--input",
        choices=["auto", "pdb", "sdf", "mol2", "smi", "xed"],
        default="auto",
        type=str.lower,
        help="Set the file format for reading the ligands. The default is to autodetect when reading from files, or to assume SDF files when reading from standard input.",  # noqa: E501
    )
    group.add_argument(
        "-Z",
        "--input-ignore-errors",
        action="store_true",
        help="By default, the calculation stops when errors are encountered in the input molecules. However, by setting this option the calculation will continue processing the remaining molecules and log the errors to standard error (stderr).",  # noqa: E501
    )
    group.add_argument(
        "--batch-size",
        type=int,
        default=DEFAULT_BATCH_SIZE,
        help="Limit the number of molecules which are read into memory. Smaller values will reduce memory usage but will reduce performance by a small amount. If --project/-P is set then this option is ignored.",  # noqa: E501
    )
    # Output settings
    group = parser.add_argument_group("output")
    group.add_argument(
        "-o",
        "--output",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for writing the molecules. The default is 'sdf'.",
    )
    group.add_argument(
        "-L",
        "--log",
        action="store_true",
        help="Request that the calculation log be written as one of the SDF tags for each alignment. Only has an effect if '-o sdf' (the default) is active. The calculation log is written as tag 'FlareAlign_Log'.",  # noqa: E501
    )
    group.add_argument(
        "-P",
        "--project",
        help="Write the Flare project file with the results to the given file path.",
    )

    # Speed setting
    group = parser.add_argument_group("speed")
    group.add_argument(
        "-q",
        "--quick",
        action="store_true",
        help="Reduces the number of conformations which are generated.",
    )
    group.add_argument(
        "-e",
        "--exhaustive",
        action="store_true",
        help="Increases the number of conformations which are generated.",
    )
    # Conformation settings
    # Do not set defaults for these settings as they will override the speed settings
    group = parser.add_argument_group("conformation")
    group.add_argument(
        "--secondary-amides",
        choices=["trans", "spin", "input", "t", "s", "i"],
        type=str.lower,
        help="Specify how the conformation hunter is to handle amides. The 'trans' option forces all secondary amides to adopt the trans geometry. The 'input' option will leave secondary amides in the geometry (cis/trans) that they were in in the input file. Note that this means that if the input structure was drawn cis then you will get only cis amide conformations! The 'spin' option allows the amide bond to spin, so you will get both cis and trans amide conformations. This option does not affect ureas, urethanes, thioamides etc., only normal acyclic secondary amides. The default is 'trans'.",  # noqa: E501
    )
    group.add_argument(
        "-E",
        "--energy-window",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.ConformationHunt.energy_window.__doc__, True),
    )
    group.add_argument(
        "-F",
        "--filter",
        type=float,
        help=cmdutil.numpy_doc_to_help(
            flare.ConformationHunt.filter_duplicate_conformers_at_rms.__doc__, True
        ),
    )
    group.add_argument(
        "--gradient-cutoff",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.ConformationHunt.gradient_cutoff.__doc__, True),
    )
    group.add_argument(
        "-m",
        "--maxconfs",
        type=int,
        help=cmdutil.numpy_doc_to_help(flare.ConformationHunt.max_conformations.__doc__, True),
    )
    group.add_argument(
        "-D",
        "--dynamics-runs",
        type=int,
        help=cmdutil.numpy_doc_to_help(flare.ConformationHunt.num_dynamics_runs.__doc__, True),
    )
    group.add_argument(
        "-f",
        "--force-dynamics",
        action="store_true",
        help=cmdutil.numpy_doc_to_help(flare.ConformationHunt.force_dynamics.__doc__, True),
    )
    group.add_argument(
        "-b",
        "--remove-boats",
        action="store_true",
        help=cmdutil.numpy_doc_to_help(flare.ConformationHunt.remove_boats.__doc__, True),
    )
    group.add_argument(
        "--coulombics",
        action="store_false",
        help="Use full coulombics and van der Waals interactions during the conformer generation. The default is to neglect coulombics and attractive vdW forces, which usually gives a better conformer population but may ignore internal hydrogen bonds.",  # noqa: E501
    )
    # Other settings
    group = parser.add_argument_group("general options")
    group.add_argument(
        "--config",
        help=cmdutil.CONFIG_ARG_HELP_TEXT,
    )
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        default=conf_hunt.local_engine_count,
        help="Set the number of local engines to start. Use 0 for no local processing: you must specify some remote engines with -g if you do so.",  # noqa: E501
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker in the format hostname:port. Field Engines which have been registered with the Broker will be used during the calculation. The CRESSET_BROKER environment variable can be used instead of this option.",  # noqa: E501
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )
    args = parser.parse_args(argv)

    return args


def _configure_process(conf_hunt, args):
    """Configure `confhunt` using the command line arguments."""
    project = flare.Project()

    # Load the config file first. Any other settings given will override the config file.
    if args.config:
        conf_hunt.read_config(args.config)

    if args.quick:
        conf_hunt.load_conformation_hunt_preset(flare.ConformationHunt.ConformationHuntPreset.Quick)
    elif args.exhaustive:
        conf_hunt.load_conformation_hunt_preset(
            flare.ConformationHunt.ConformationHuntPreset.Accurate
        )
    else:
        conf_hunt.load_conformation_hunt_preset(
            flare.ConformationHunt.ConformationHuntPreset.Normal
        )

    if args.secondary_amides:
        if args.secondary_amides.startswith("t"):
            conf_hunt.amide_handling = flare.ConformationHunt.Amide.ForceTrans
        elif args.secondary_amides.startswith("s"):
            conf_hunt.amide_handling = flare.ConformationHunt.Amide.Spin
        elif args.secondary_amides.startswith("i"):
            conf_hunt.amide_handling = flare.ConformationHunt.Amide.Input
        else:
            raise ValueError("invalid value for --secondary-amides")

    # If the user has not passed this value on the command line then do not change it
    # as it may have been updated by loading a preset
    if args.energy_window:
        with cmdutil.CheckValue("energy-window"):
            conf_hunt.energy_window = args.energy_window

    if args.filter:
        with cmdutil.CheckValue("filter"):
            conf_hunt.filter_duplicate_conformers_at_rms = args.filter

    if args.gradient_cutoff:
        with cmdutil.CheckValue("gradient-cutoff"):
            conf_hunt.gradient_cutoff = args.gradient_cutoff

    if args.maxconfs:
        with cmdutil.CheckValue("maxconfs"):
            conf_hunt.max_conformations = args.maxconfs

    if args.dynamics_runs:
        with cmdutil.CheckValue("dynamics-runs"):
            conf_hunt.num_dynamics_runs = args.dynamics_runs

    with cmdutil.CheckValue("force-dynamics"):
        conf_hunt.force_dynamics = args.force_dynamics

    with cmdutil.CheckValue("remove-boats"):
        conf_hunt.remove_boats = args.remove_boats

    conf_hunt.disable_coulombic_and_attractive_vdw_forces = args.coulombics

    with cmdutil.CheckValue("localengines"):
        conf_hunt.local_engine_count = args.localengines

    return project


def _read_batch(project, args):
    """Yield batches of molecules read from the command line options."""
    # Read from stdin if no ligand files are given
    mol_files = args.ligands if args.ligands else ["-"]
    # If a project is set then all ligands need to be kept in memory so the project can be
    # saved
    if args.project and args.batch_size != DEFAULT_BATCH_SIZE:
        print("--batch-size will be ignored as -P/--project was given.\n", file=sys.stderr)
    batch_size = args.batch_size if not args.project else sys.maxsize

    raise_exception_on_error = not args.input_ignore_errors

    project.ligands.clear()
    for mol_file in mol_files:
        # If reading from stdin and the file format is not given assume "sdf" format
        input = args.input if args.input != "auto" else None
        if mol_file == "-" and input is None:
            input = "sdf"

        for batch, errors in cmdutil.batch_read_files(
            [mol_file], input, batch_size, raise_exception_on_error
        ):
            project.ligands.extend(batch)
            for error in errors:
                print(error, file=sys.stderr)

            if len(project.ligands) >= batch_size:
                yield
                project.ligands.clear()

    if len(project.ligands) > 0:
        yield


def _write_results(conf_hunt, project, imported_tags, args):
    """Write the results."""
    if args.project:
        try:
            project.save(args.project)
        except Exception as err:
            print(err, file=sys.stderr)
            # print the error and continue

    # Print the result confs to stdout
    confs = []
    for ligand in conf_hunt.ligands:
        log_text = cmdutil.molecule_log_as_sdf_string(ligand) if args.log else None

        for i, conf in enumerate(ligand.conformations, 1):
            pose = ligand.poses.append(conf)
            pose.properties["Conformer"].value = i
            pose.properties["Energy"].value = round(conf.energy, 3)
            if args.log:
                pose.properties["FlareConfHunt_Log"].value = log_text
            confs.append(pose)

    file_format = args.output if args.output else "sdf"

    allowed_cresset_tags = [
        "Conformer",
        "Energy",
        "_cresset_fieldpoints",
        "Confs",
        "Filename",
        "FlareConfHunt_Log",
    ]
    list_tags_to_keep = list(imported_tags)
    list_tags_to_keep.extend(allowed_cresset_tags)

    def modify_tags(conf, tags):
        """Add/Removes SDF tags for each conformation."""
        tags["Confs"] = str(len(conf.ligand.conformations))
        tags["Filename"] = tags["Filename_cresset"]
        tags_to_delete = [tag for tag in tags if tag not in list_tags_to_keep]
        for tag in tags_to_delete:
            del tags[tag]

        return tags

    flare.write_file(sys.stdout, confs, file_format, modify_tags)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
