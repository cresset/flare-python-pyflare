#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Runs an Absolute FEP calculation and writes the results to stdout.

The Absolute Binding Free Energy (ABFE) calculation is run on a set of ligands and a protein.
When completed the ligands are written to stdout in sdf format with tags for the predicted activity.

If the `--project` option is set then a project file will be written. If the calculation is
interrupted by pressing Ctrl+C the current progress will be saved in the specified Flare project
file. To resume the calculation, run the script with the
--continue option. If you want to restart the calculation from the beginning and discard any
previous results, use the --delete option. If both the project file and ligands are given then the
ligands are added to the calculation.

The FEP calculation requires GPU resources to run. It is recommended to use a Cresset Engine Broker
by setting the --broker option. However, on Linux local GPU resources may be used.
To use multiple GPU devices, the --cuda-device-index or --opencl-device-index options should be
specified multiple times, e.g. specifying '--cuda-device-index 0 --cuda-device-index 1'
will allow the calculation to use both GPUs 0 and 1.

Once the FEP calculation is complete and if the `--project` option was set then details about the
the results can be viewed by the fepview.py script.

examples:
  pyflare abferun.py --project project.flr --protein protein.pdb ligands.sdf --broker example:9000
    Starts a new calculation. The results are stored in the project and written to stdout.

  pyflare abferun.py --project project.flr --continue --broker example:9000
    Continues the ABFE calculation on the FEP project within project.flr.

  pyflare abferun.py --help
    Prints the help text including all available options and exits.
"""
import sys
import argparse
import traceback

from cresset import flare

from _internal import cmdutil
from _internal import feputil


def main(argv):
    """Run the Absolute FEP calculation on a project file."""
    try:
        fep = flare.AbsoluteFep()

        args = _parse_args(fep, argv)
        project, fep_project = _configure_process(fep, args)
        cmdutil.start_process(fep, args.broker, args.verbose)
        feputil.wait_for_finish(fep, project, fep_project, args)
        cmdutil.check_errors(fep)
        _write_results(project, fep_project, args)

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(fep, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )

    parser.add_argument(
        "ligands",
        nargs="*",
        help="The ligands to add to the ABFE project. All ligands should sit in the protein active site. This is required when creating a new ABFE project and may be used to add ligands to existing ABFE projects.",  # noqa: E501
    )

    group = parser.add_argument_group("input")
    group.add_argument(
        "-i",
        "--input",
        choices=["pdb", "sdf", "mol2", "xed"],
        help="Set the file format for reading the ligands. The default is to autodetect.",
    )
    group.add_argument(
        "-p",
        "--protein",
        help="The protein which will be used by the ABFE calculation. The protein should have been prepared. This is required when creating a new ABFE project and may be used to change the protein of existing ABFE projects.",  # noqa: E501
    )
    group.add_argument(
        "-j",
        "--input-protein",
        choices=["pdb", "sdf", "mol2", "xed"],
        help="Set the file format for reading the protein. The default is to autodetect.",
    )
    group.add_argument(
        "-C",
        "--protein-chain-types",
        metavar="protein,water,other,ligand",
        default="protein,water,other,ligand",
        type=str.lower,
        help="Select the types of chains to include in the calculation as a comma separated list. 'p', 'w', 'o' and 'l' can be used as a shorthand for 'protein', water', 'other', 'ligand'. The default is to include all the chains.",  # noqa: E501
    )
    group.add_argument(
        "-a",
        "--activity",
        help="The SDF tag of the ligands containing the experimental activity values in DeltaG (kcal/mol) units.",  # noqa: E501
    )
    feputil.add_input_arguments(group, fep)

    group = parser.add_argument_group("force field")
    cmdutil.add_dynamics_force_field_arguments(group)

    group = parser.add_argument_group("FEP options")
    feputil.add_fep_arguments(group, fep)
    group.add_argument(
        "-LDB",
        "--lambda-windows-discharge-bound",
        type=int,
        help=cmdutil.numpy_doc_to_help(fep.__class__.lambda_windows_discharge_bound.__doc__, True),
    )
    group.add_argument(
        "-LDF",
        "--lambda-windows-discharge-free",
        type=int,
        help=cmdutil.numpy_doc_to_help(fep.__class__.lambda_windows_discharge_free.__doc__, True),
    )
    group.add_argument(
        "-LVB",
        "--lambda-windows-vanish-bound",
        type=int,
        help=cmdutil.numpy_doc_to_help(fep.__class__.lambda_windows_vanish_bound.__doc__, True),
    )
    group.add_argument(
        "-LVF",
        "--lambda-windows-vanish-free",
        type=int,
        help=cmdutil.numpy_doc_to_help(fep.__class__.lambda_windows_vanish_free.__doc__, True),
    )
    group.add_argument(
        "-LRB",
        "--lambda-windows-restrain-bound",
        type=int,
        help=cmdutil.numpy_doc_to_help(fep.__class__.lambda_windows_restrain_bound.__doc__, True),
    )
    group.add_argument(
        "-LIS",
        "--lambda-windows-intensive-sampling",
        action="store_true",
        help=cmdutil.numpy_doc_to_help(
            fep.__class__.lambda_windows_intensive_sampling.__doc__, True
        ),
    )
    group.add_argument(
        "--boresch-restraints-min-distance",
        type=float,
        help=cmdutil.numpy_doc_to_help(fep.__class__.boresch_restraints_min_distance.__doc__, True),
    )
    group.add_argument(
        "--boresch-restraints-max-distance",
        type=float,
        help=cmdutil.numpy_doc_to_help(fep.__class__.boresch_restraints_max_distance.__doc__, True),
    )
    group.add_argument(
        "--boresch-restraints-production-time",
        type=float,
        help=cmdutil.numpy_doc_to_help(
            fep.__class__.boresch_restraints_production_time.__doc__, True
        ),
    )

    group = parser.add_argument_group("GPU")
    cmdutil.add_gpu_arguments(group, fep)

    group = parser.add_argument_group("openmm")
    cmdutil.add_openmm_common_arguments(group, fep)
    cmdutil.add_openmm_time_step_arguments(group, fep)
    cmdutil.add_openmm_fep_ws_arguments(group, fep)
    cmdutil.add_openmm_advanced_arguments(group, fep, process_is_fep=True)

    group = parser.add_argument_group("gcncmc arguments")
    cmdutil.add_gcncmc_arguments(group, fep)

    group = parser.add_argument_group("general options")
    feputil.add_general_arguments(group, fep)

    args = parser.parse_args(argv)

    return args


def _configure_process(fep, args):
    """Configure `fep` calculation using the command line arguments."""

    project = feputil.configure_process(fep, args)

    if not fep.fep_project.is_calculation_started(feputil.calculation_type(fep)):
        if args.lambda_windows_discharge_bound is not None:
            with cmdutil.CheckValue("lambda-windows-discharge-bound"):
                fep.lambda_windows_discharge_bound = args.lambda_windows_discharge_bound
        if args.lambda_windows_discharge_free is not None:
            with cmdutil.CheckValue("lambda-windows-discharge-free"):
                fep.lambda_windows_discharge_free = args.lambda_windows_discharge_free
        if args.lambda_windows_vanish_bound is not None:
            with cmdutil.CheckValue("lambda-windows-vanish-bound"):
                fep.lambda_windows_vanish_bound = args.lambda_windows_vanish_bound
        if args.lambda_windows_vanish_free is not None:
            with cmdutil.CheckValue("lambda-windows-vanish-free"):
                fep.lambda_windows_vanish_free = args.lambda_windows_vanish_free
        if args.lambda_windows_restrain_bound is not None:
            with cmdutil.CheckValue("lambda-windows-restrain-bound"):
                fep.lambda_windows_restrain_bound = args.lambda_windows_restrain_bound
        if args.lambda_windows_intensive_sampling:
            fep.lambda_windows_intensive_sampling = True

        if args.boresch_restraints_min_distance is not None:
            with cmdutil.CheckValue("boresch-restraints-min-distance"):
                fep.boresch_restraints_min_distance = args.boresch_restraints_min_distance
        if args.boresch_restraints_max_distance is not None:
            with cmdutil.CheckValue("boresch-restraints-max-distance"):
                fep.boresch_restraints_max_distance = args.boresch_restraints_max_distance
        if args.boresch_restraints_production_time is not None:
            with cmdutil.CheckValue("boresch-restraints-production-time"):
                fep.boresch_restraints_production_time = args.boresch_restraints_production_time

    cmdutil.configure_gpu_process(fep, args)

    # Load Ligands
    new_ligands = []
    for ligand_file in args.ligands:
        new_ligands.extend(project.ligands.extend(flare.read_file(ligand_file, args.input)))

    if not args.ligands and len(fep.fep_project.nodes) == 0:
        # If the ligand is not and there are no ligands in the project set read it from stdin
        file_format = args.input if args.input else "sdf"
        new_ligands.extend(project.ligands.extend(flare.read_file(sys.stdin, file_format)))

    if not fep.fep_project.nodes and not new_ligands:
        raise ValueError("fep project contains no ligands")

    new_nodes = fep.fep_project.nodes.new(new_ligands)

    # Add the absolute link
    for node in new_nodes:
        node.is_absolute_enabled = True

    if new_nodes:
        fep.fep_project.layout_graph()

    if args.activity:
        for node in new_nodes:
            ligand = node.source_ligand()
            if args.activity in ligand.properties:
                node.experimental_delta_g = ligand.properties[args.activity].value

    # Load protein
    new_proteins = []
    if args.protein:
        new_proteins.extend(
            project.proteins.extend(flare.read_file(args.protein, args.input_protein))
        )

    if len(new_proteins) > 1:
        raise ValueError(
            f"only one protein can be set, file {args.protein} "
            + f"contains {len(new_proteins)} proteins"
        )

    if new_proteins:
        sequences = cmdutil.chain_types_to_sequences(new_proteins[0], args.protein_chain_types)
        fep.fep_project.set_protein(new_proteins[0], sequences)

    feputil.save_project(project, args)

    return (project, fep.fep_project)


def _write_results(project, fep_project, args):
    """Write the results."""
    feputil.save_project(project, args)

    structures = []
    node_map = {}

    for node in fep_project.nodes:
        if node.is_absolute_enabled:
            structure = node.initial_structure()[0]
            structures.append(structure)
            node_map[structure] = node

    def modify_tags(initial_structure, tags):
        """Add/Remove SDF tags for each node."""
        node = node_map[initial_structure]
        activity, error = node.predicted_delta_g(flare.FepCalculationType.Absolute)

        if node.experimental_delta_g is not None:
            tags["Experimental Activity"] = str(round(node.experimental_delta_g, 3))
        if activity is not None:
            tags["Absolute Predicted Activity"] = str(round(activity, 3))
        if error is not None:
            tags["Absolute Predicted Activity Error"] = str(round(error, 3))

        return tags

    flare.write_file(sys.stdout, structures, "sdf", modify_tags)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
