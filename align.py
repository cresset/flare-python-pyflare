#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Aligns ligands using their electrostatic and hydrophobic fields and writes the results to stdout.

Ligands or conformations are read in and aligned using Cresset's "field point" technology.
Field points are a way of representing molecules in terms of their surface and electrostatic
properties: positive and negative electrostatic fields, van der Waals effects and hydrophobic
effects on and near the surface of a molecule. Two molecules which both bind to a common active
site tend to make similar interactions with the protein and hence have highly similar field point
patterns. This script thus aligns molecules such that both their field point patterns and the
underlying fields are as similar as possible: this has been shown to correlate extremely well
with the bioactive alignments.


examples:
  pyflare align.py reference.sdf mols.sdf > results.sdf
    Reads `mols.sdf`. For each entry, generates conformers then aligns them to the
    molecule (or set of molecules) loaded from `reference.sdf`.
    Writes the best alignment for each molecule to 'results.sdf' in SDF format.

  pyflare align.py -X 18,a reference.sdf mols.sdf > results.sdf
    Do the alignment with a pharmacophore constraint: the aligned molecule must have an acceptor
    atom near atom 18 of the reference or it will get a score penalty.

  pyflare align.py reference.sdf mols.sdf -p protein.pdb -P my_project.flr > results.sdf
    Do the alignment using 'protein.pdb' as a protein excluded volume. In additional to writing
    the results to `results.sdf` the results are also written to `my_project.flr`.

  pyflare align.py --mcs very-permissive -S -P myproject.fqj reference.sdf mols.sdf moremols.sdf
    Align the molecules in `mols.sdf` and `moremols.sdf` to the ones in 'reference.sdf'.
    The alignment will be done by aligning common substructures if possible, using a very loose
    matching rule (atoms match if they have the same hybridisation, ignoring element and
    ring/chain labels). The aligned molecules will not be allowed to move to increase the
    alignment score. The results will be written to stdout in SDF format and also to the
    Flare project `myproject.fqj`.

  pyflare align.py --help
    Prints the help text including all available options and exits.
"""
import sys
import argparse
import traceback

from cresset import flare
from rdkit import Chem

from _internal import cmdutil

DEFAULT_BATCH_SIZE = 5000


def main(argv):
    """Run the align process using the command line args."""
    try:
        align = flare.Align()

        args = _parse_args(align, argv)
        project, ref_role, mols_role = _configure_process(align, args)

        imported_tags = set()

        logging_context = cmdutil.LoggingContext(project.log, verbose=args.verbose)

        # Read each batch in turn and process them
        any_ligands_read = False
        for i, _ in enumerate(_read_batch_into_role(project, args, mols_role), 1):
            any_ligands_read = True

            imported_tags = imported_tags.union(set(project.ligands.columns.user_keys()))

            align.ligands = mols_role.ligands

            # Run the calculation
            cmdutil.start_process(align, args.broker, args.verbose)
            cmdutil.wait_for_finish(align, prefix=f"Batch {i}, ", logging_context=logging_context)

            # If there are errors don't stop, write the results and continue on to the next batch.
            # As if a few conf hunts failed we still want to print the results for all other
            # successful conf hunts.
            if align.errors():
                print("\n".join(align.errors()), file=sys.stderr)

            if not align.is_cancelled():
                _write_results(align, project, imported_tags, ref_role, args)

        if not any_ligands_read:
            raise ValueError("no ligands were read from the input")

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(align, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )
    # Input settings
    parser.add_argument(
        "ligands",
        nargs="+",
        help="The ligand files to conf hunt. If not given the ligands are read from stdin.",
    )

    group = parser.add_argument_group("input")
    group.add_argument(
        "-r",
        "--readmode",
        choices=["autodetect", "molecules", "conformers", "fixed_confs", "u", "m", "c", "f"],
        default="autodetect",
        type=str.lower,
        help="Set how the database molecules should be read. In 'm'olecules mode, every entry is assumed to be a separate molecule that will require conformer generation. In 'c'onformers mode, each input file is assumed to hold multiple conformers of the same molecule i.e. one molecule per file, possibly as multiple confs. In 'f'ixed mode, each entry is assumed to be a separate molecule in a fixed conformation: it will require alignment but not conformer generation. The default, a'u'todetect, uses heuristics to decide whether each entry is a new molecule or a conformation of the preceding molecule.",  # noqa: E501
    )
    group.add_argument(
        "-i",
        "--input",
        choices=["auto", "pdb", "sdf", "mol2", "smi", "xed"],
        default="auto",
        type=str.lower,
        help="Set the file format for reading the ligands. The default is to autodetect when reading from files, or to assume SDF files when reading from standard input.",  # noqa: E501
    )
    group.add_argument(
        "-Z",
        "--input-ignore-errors",
        action="store_true",
        help="By default, the calculation stops when errors are encountered in the input molecules. However, by setting this option the calculation will continue processing the remaining molecules and log the errors to standard error (stderr).",  # noqa: E501
    )
    group.add_argument(
        "-p",
        "--protein",
        help="Read the first molecule in the specified file and use it as an excluded volume when scoring alignments.",  # noqa: E501
    )
    group.add_argument(
        "-j",
        "--input-protein",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for reading the protein. The default is to autodetect.",
    )
    group.add_argument(
        "--protein-chain-types",
        metavar="protein,water,other,ligand",
        default="protein,water,other",
        type=str.lower,
        help="Select the types of chains to include in the calculation as a comma separated list. 'p', 'w', 'o' and 'l' can be used as a shorthand for 'protein', water', 'other', 'ligand'. The default is 'protein', 'water', and 'other' chains.",  # noqa: E501
    )
    group.add_argument(
        "--batch-size",
        type=int,
        default=DEFAULT_BATCH_SIZE,
        help="Limit the number of molecules which are read into memory. Smaller values will reduce memory usage but will reduce performance by a small amount. If --project/-P is set then this option is ignored.",  # noqa: E501
    )
    # Output settings
    group = parser.add_argument_group("output")
    group.add_argument(
        "-o",
        "--output",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for writing the molecules. The default is 'sdf'.",
    )
    group.add_argument(
        "-L",
        "--log",
        action="store_true",
        help="Request that the calculation log be written as one of the SDF tags for each alignment. Only has an effect if '-o sdf' (the default) is active. The calculation log is written as tag 'FlareAlign_Log'.",  # noqa: E501
    )
    group.add_argument(
        "-P",
        "--project",
        help="Write the Flare project file with the results to the given file path.",
    )
    group.add_argument(
        "-w",
        "--writeref",
        type=str.lower,
        choices=["before", "after"],
        help="Write out the reference molecule(s) before or after the aligned molecules. The default (if this option is not specified) is not to write the reference molecule(s).",  # noqa: E501
    )
    # Speed setting
    group = parser.add_argument_group("speed")
    group.add_argument(
        "-q",
        "--quick",
        action="store_true",
        help="Reduces the number of conformations which are generated.",
    )
    group.add_argument(
        "-e",
        "--exhaustive",
        action="store_true",
        help="Increases the number of conformations which are generated.",
    )
    # Conformation settings
    # Do not set defaults for these settings as they will override the speed settings
    group = parser.add_argument_group("conformation")
    group.add_argument(
        "--secondary-amides",
        choices=["trans", "spin", "input", "t", "s", "i"],
        type=str.lower,
        help="Specify how the conformation hunter is to handle amides. The 'trans' option forces all secondary amides to adopt the trans geometry. The 'input' option will leave secondary amides in the geometry (cis/trans) that they were in in the input file. Note that this means that if the input structure was drawn cis then you will get only cis amide conformations! The 'spin' option allows the amide bond to spin, so you will get both cis and trans amide conformations. This option does not affect ureas, urethanes, thioamides etc., only normal acyclic secondary amides. The default is 'trans'.",  # noqa: E501
    )
    group.add_argument(
        "-E",
        "--energy-window",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.Align.energy_window.__doc__, True),
    )
    group.add_argument(
        "-F",
        "--filter",
        type=float,
        help=cmdutil.numpy_doc_to_help(
            flare.Align.filter_duplicate_conformers_at_rms.__doc__, True
        ),
    )
    group.add_argument(
        "--gradient-cutoff",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.Align.gradient_cutoff.__doc__, True),
    )
    group.add_argument(
        "-m",
        "--maxconfs",
        type=int,
        help=cmdutil.numpy_doc_to_help(flare.Align.max_conformations.__doc__, True),
    )
    group.add_argument(
        "-D",
        "--dynamics-runs",
        type=int,
        help=cmdutil.numpy_doc_to_help(flare.Align.num_dynamics_runs.__doc__, True),
    )
    group.add_argument(
        "-b",
        "--remove-boats",
        action="store_true",
        help=cmdutil.numpy_doc_to_help(flare.Align.remove_boats.__doc__, True),
    )
    group.add_argument(
        "--coulombics",
        action="store_false",
        help="Use full coulombics and van der Waals interactions during the conformer generation. The default is to neglect coulombics and attractive vdW forces, which usually gives a better conformer population but may ignore internal hydrogen bonds.",  # noqa: E501
    )
    group.add_argument(
        "-I",
        "--immobile",
        action="store_true",
        help="Do not generate conformations or allow molecules to move, just score molecules in their input positions. The input molecules must be 3D.",  # noqa: E501
    )
    group.add_argument(
        "-k",
        "--keep-all-confs",
        action="store_true",
        help="By default, only conformers that are used in alignments are kept. If this option is set, all conformers will be kept. This only has an effect if a project file is being written with the -P option.",  # noqa: E501
    )
    group.add_argument(
        "-n",
        "--no-invert",
        action="store_true",
        help="Do not allow inversion of achiral moving molecules.",
    )
    # Alignment settings
    # Do not set defaults for these settings as they will override the speed settings
    group = parser.add_argument_group("alignment")
    group.add_argument(
        "--reference-weight",
        metavar="ref1,ref2,...,refX",
        help="Set the relative weight for each reference ligand. For example if --reference-weights 1,2 is set then the score for the results will be based one third on the similarity to the first reference and two thirds on the second reference. A weight must be set for each reference ligand.",  # noqa: E501
    )
    group.add_argument(
        "--field-weight",
        metavar="P,N,S,H",
        help="Set weightings for the four different field types: Positive, Negative, Surface and Hydrophobic. All four values must be specified and in the range 0 to 1. For example --field-weight 0.5,0.5,0.1,0.1",  # noqa: E501
    )
    group.add_argument(
        "-f",
        "--field-constraint",
        action="append",
        help='Specifies a field constraint on the field point from the specified reference molecule, with the specified index. The format of the argument is "index,size[,reference]". The index argument is the index of the field point in the list of field points (i.e. the first field point is index 1). The reference index is optional and will default to the first reference molecule provided. Multiple -f options can be given to add multiple constraints. Note that this option is only useful if the input files already contain field points (i.e. XED format files, or SDF files with field point data exported from Cresset applications).',  # noqa: E501
    )
    group.add_argument(
        "-a",
        "--alignments",
        type=int,
        default=1,  # Ignore the value in the presets and instead output 1 by default
        help="How many alignments to output for each molecule",  # noqa: E501
    )
    group.add_argument(
        "-M",
        "--mcs",
        choices=["normal", "permissive", "very-permissive", "n", "p", "v"],
        type=str.lower,
        help="Generate conformers and alignments by performing a maximum-common-substructure match to the references, and generating conformers by moving only the atoms outside the MCS. You need to specify the MCS matching rule to use: 'normal' means match on element+hybridisation, 'permissive' means match on hybridisation only, and 'very-permissive' matches on hybridisation only and allows ring and chain atoms to match each other. You can use 'n'/'p'/'v' as shortcuts for the arguments.",  # noqa: E501
    )
    group.add_argument(
        "-S",
        "--mcs-static",
        action="store_true",
        help="Specify that once an MCS alignment has been performed, the aligned molecule may not move. The default is to allow the aligned molecule to move slightly as a rigid body to maximize the alignment score. Only has an effect if -M is specified.",  # noqa: E501
    )
    group.add_argument(
        "--mcs-smarts", help=cmdutil.numpy_doc_to_help(flare.Align.mcs_smarts.__doc__, True)
    )
    group.add_argument(
        "--mcs-require-full-ring-match",
        action="store_true",
        help=cmdutil.numpy_doc_to_help(flare.Align.mcs_require_full_ring_match.__doc__, True),
    )
    group.add_argument(
        "-X",
        "--ph4constraint",
        metavar="index,type,[strength[,reference]]",
        action="append",
        help="Specifies a pharmacophore constraint on an atom from the specified reference molecule, with the specified index. The reference index is optional and will default to the first reference molecule provided. Multiple -X options can be given to add multiple constraints. The strength of the constraint must be between 0 and 100, and defaults to 10 if not specified. The type of constraint is specified with either a character sequence or a number representing a combination of bit flags. The characters for each type are 'd'=Donor, 'a'=Acceptor, '+'=Cation, '-'=Anion, 'm'=Metal binder and 'v'=Covalent. The respective bit flags are 1, 2, 4, 8, 16 and 32.",  # noqa: E501
    )
    group.add_argument(
        "-R",
        "--protein-hardness",
        choices=["soft", "medium", "hard"],
        type=str.lower,
        help="Set the hardness of the protein excluded volume to use: a harder protein penalises steric clashes more. Only has an effect if -p is specified.",  # noqa: E501
    )
    group.add_argument(
        "-x",
        "--max-score",
        action="store_true",
        help="If there is more than one reference, the default behavior is to calculate the score of each alignment as the average of the scores to each reference. If this option is specified, then the score for an alignment is the single highest score to any of the reference molecules instead. If there is only one reference then this option does nothing.",  # noqa: E501
    )
    group.add_argument(
        "--scoring-metric",
        metavar="dice|tanimoto|tversky,alpha",
        type=str.lower,
        help="Set the scoring metric to use, the tversky metric requires an alpha value to be set. The default is dice. You can use 'd'/'a'/'t,alpha' as shortcuts for the arguments",  # noqa: E501
    )
    group.add_argument(
        "-s",
        "--shapewt",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.Align.shape_weight.__doc__, True),
    )
    group.add_argument(
        "-W",
        "--weightAtoB",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.Align.weight_a_to_b.__doc__, True),
    )
    # Other settings
    group = parser.add_argument_group("general options")
    group.add_argument(
        "--config",
        help=cmdutil.CONFIG_ARG_HELP_TEXT,
    )
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        default=align.local_engine_count,
        help="Set the number of local engines to start. Use 0 for no local processing: you must specify some remote engines with -g if you do so.",  # noqa: E501
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker in the format hostname:port. Field Engines which have been registered with the Broker will be used during the calculation. The CRESSET_BROKER environment variable can be used instead of this option.",  # noqa: E501
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )
    args = parser.parse_args(argv)

    return args


def _configure_process(align, args):
    """Configure `align` using the command line arguments."""
    project = flare.Project()

    # Select preset before anything else.
    if args.quick:
        align.load_conformation_hunt_preset(flare.Align.ConformationHuntPreset.Quick)
        align.load_alignment_preset(flare.Align.AlignPreset.Quick)
    elif args.exhaustive:
        align.load_conformation_hunt_preset(flare.Align.ConformationHuntPreset.Accurate)
        align.load_alignment_preset(flare.Align.AlignPreset.Normal)
    else:
        align.load_conformation_hunt_preset(flare.Align.ConformationHuntPreset.Normal)
        align.load_alignment_preset(flare.Align.AlignPreset.Normal)

    # Modify the preset using the config, then all other settings will override the config file.
    if args.config:
        align.read_config(args.config)

    # Use constraints added via SDF tags
    align.ignore_project_constraints = False

    # First file contains the references
    reference_file = args.ligands[0]

    ref_role = project.roles.append("References")

    ref_role.ligands.extend(flare.read_file(reference_file, _find_format(reference_file, args)))

    if not (1 <= len(ref_role.ligands) <= 9):
        raise ValueError(f"expected 1 to 9 reference molecules, found {len(ref_role.ligands)}")

    for reference in ref_role.ligands:
        if not reference.field_points:
            reference.add_fields()

    align.references = ref_role.ligands

    mols_role = project.roles.append("Ligands")

    if args.protein:
        project.proteins.extend(flare.read_file(args.protein, args.input_protein))
        if not project.proteins:
            raise ValueError(f"no proteins found in {args.protein}")
        align.protein = project.proteins[0]
        align.sequences = cmdutil.chain_types_to_sequences(align.protein, args.protein_chain_types)

    # Conformation Settings
    if args.secondary_amides:
        if args.secondary_amides.startswith("t"):
            align.amide_handling = flare.Align.Amide.ForceTrans
        elif args.secondary_amides.startswith("s"):
            align.amide_handling = flare.Align.Amide.Spin
        elif args.secondary_amides.startswith("i"):
            align.amide_handling = flare.Align.Amide.Input
        else:
            raise ValueError("invalid value for --secondary-amides")

    # If the user has not passed this value on the command line then do not change it
    # as it may have been updated by loading a preset
    if args.energy_window:
        with cmdutil.CheckValue("energy-window"):
            align.energy_window = args.energy_window

    if args.filter:
        with cmdutil.CheckValue("filter"):
            align.filter_duplicate_conformers_at_rms = args.filter

    if args.gradient_cutoff:
        with cmdutil.CheckValue("gradient-cutoff"):
            align.gradient_cutoff = args.gradient_cutoff

    if args.maxconfs:
        with cmdutil.CheckValue("maxconfs"):
            align.max_conformations = args.maxconfs

    if args.dynamics_runs:
        with cmdutil.CheckValue("dynamics-runs"):
            align.num_dynamics_runs = args.dynamics_runs

    if args.remove_boats:
        with cmdutil.CheckValue("remove-boats"):
            align.remove_boats = args.remove_boats

    align.disable_coulombic_and_attractive_vdw_forces = args.coulombics

    # Align Settings
    if args.reference_weight:
        _configure_reference_weight(align, args.reference_weight)

    if args.field_weight:
        _configure_field_weight(align, args.field_weight)

    if args.field_constraint:
        _configure_field_constraints(align, args.field_constraint)

    with cmdutil.CheckValue("alignments"):
        align.max_alignments = args.alignments

    if args.mcs:
        if args.mcs.startswith("n"):
            align.mcs_mode = flare.Align.McsMode.Normal
        elif args.mcs.startswith("p"):
            align.mcs_mode = flare.Align.McsMode.Permissive
        elif args.mcs.startswith("v"):
            align.mcs_mode = flare.Align.McsMode.VeryPermissive
        else:
            raise ValueError("invalid value for --mcs")

    align.mcs_allow_conformations_to_move = not args.mcs_static
    align.mcs_require_full_ring_match = args.mcs_require_full_ring_match

    if args.mcs_smarts:
        align.mcs_smarts = args.mcs_smarts

    if args.ph4constraint:
        _configure_ph4_constraints(align, args.ph4constraint)

    if args.protein_hardness:
        if args.protein_hardness.startswith("s"):
            align.protein_hardness = flare.Align.ProteinHardness.Soft
        elif args.protein_hardness.startswith("m"):
            align.protein_hardness = flare.Align.ProteinHardness.Medium
        elif args.protein_hardness.startswith("h"):
            align.protein_hardness = flare.Align.ProteinHardness.Hard
        else:
            raise ValueError("invalid value for --protein-hardness")

    if args.max_score:
        align.reference_weight_mode = flare.Align.ReferenceScoringMethod.Maximum

    if args.scoring_metric:
        if args.scoring_metric.startswith("d"):
            align.scoring_metric = flare.Align.ScoringMetric.Dice
        elif args.scoring_metric.startswith("ta") or args.scoring_metric == "a":
            align.scoring_metric = flare.Align.ScoringMetric.Tanimoto
        elif args.scoring_metric.startswith("t"):
            align.scoring_metric = flare.Align.ScoringMetric.Tversky
            tokens = args.scoring_metric.split(",")
            try:
                align.scoring_metric_tversky_alpha = float(tokens[1])
            except IndexError:
                raise ValueError("--scoring-metric tversky alpha value not set")
            except ValueError:
                raise ValueError("--scoring-metric tversky alpha value is not a float")

        else:
            raise ValueError("invalid value for --scoring-metric")

    # If the user has not passed this value on the command line then do not change it
    # as it may have been updated by loading a preset
    if args.shapewt:
        with cmdutil.CheckValue("shapewt"):
            align.shape_weight = args.shapewt

    if args.weightAtoB:
        with cmdutil.CheckValue("weightAtoB"):
            align.weight_a_to_b = args.weightAtoB

    align.score_only = args.immobile

    if args.no_invert:
        align.invert_achiral_conformations = not args.no_invert

    with cmdutil.CheckValue("localengines"):
        align.local_engine_count = args.localengines

    return (project, ref_role, mols_role)


def _configure_reference_weight(align, value):
    tokens = value.split(",")
    if len(tokens) != len(align.references):
        raise ValueError(
            f"expected {len(align.references):} reference weights, one for each reference ligand, "
            + f"found {value}"
        )
    weights = []
    for item in tokens:
        try:
            weights.append(float(item))
        except ValueError:
            raise ValueError(
                f"expected reference weight values to be between 0.0 and 100.0, found {value}"
            )
        if weights[-1] < 0 or weights[-1] > 100:
            raise ValueError(
                f"expected reference weight values to be between 0.0 and 100.0, found {value}"
            )

    assert len(align.references) == len(weights)
    for ligand, weight in zip(align.references, weights):
        align.reference_weight[ligand] = weight


def _configure_field_weight(align, value):
    tokens = value.split(",")
    if len(tokens) != 4:
        raise ValueError(f"expected 4 field weights, found {value}")
    weights = []
    for item in tokens:
        try:
            weights.append(float(item))
        except ValueError:
            raise ValueError(f"expected weight values to be between 0.0 and 1.0, found {value}")
        if weights[-1] < 0 or weights[-1] > 1:
            raise ValueError(f"expected weight values to be between 0.0 and 1.0, found {value}")
    align.field_similarity_weight_pos = weights[0]
    align.field_similarity_weight_neg = weights[1]
    align.field_similarity_weight_surface = weights[2]
    align.field_similarity_weight_hyd = weights[3]


def _configure_field_constraints(align, constraints):
    """Parse the field constraints and add them to `align`."""
    for constraint in constraints:
        tokens = constraint.split(",")
        if 2 <= len(tokens) <= 3:
            index = 0
            size = 0
            reference_index = 0

            try:
                index = int(tokens[0])
            except ValueError:
                raise ValueError(f"field constraint '{constraint}' index is not an integer")

            try:
                size = float(tokens[1])
            except ValueError:
                raise ValueError(f"field constraint '{constraint}' size is not an float")

            try:
                reference_index = int(tokens[2]) if len(tokens) >= 3 else 1
            except ValueError:
                raise ValueError(f"field constraint '{constraint}' reference is not an integer")

            if reference_index <= 0 or reference_index > len(align.references):
                raise ValueError(
                    f"field constraint '{constraint}' has an invalid reference - "
                    f"expected a value between 1 and {len(align.references)} found "
                    f"{reference_index})"
                )

            reference = align.references[reference_index - 1]
            if not reference.field_points:
                raise ValueError(
                    f"field constraint '{constraint}' cannot be added to a reference which "
                    "does not contain any "
                    f"field points {reference.title} {reference.field_points}"
                )

            if index <= 0 or index > len(reference.field_points):
                raise ValueError(
                    f"field constraint '{constraint}' has an invalid field point index - "
                    f"expected a value between 1 and "
                    f"{len(reference.field_points)}, found {index})"
                )

            field_point = reference.field_points[index - 1]
            align.field_constraints[field_point] = size


def _configure_ph4_constraints(align, constraints):
    """Parse the ph4 constraints and add them to `align`."""
    for constraint in constraints:
        tokens = constraint.split(",")
        if 2 <= len(tokens) <= 4:
            index = 0
            strength = 10.0
            reference_index = 0

            try:
                index = int(tokens[0])
            except ValueError:
                raise ValueError(f"pharmacophore constraint '{constraint}' index is not an integer")

            try:
                strength = float(tokens[2]) if len(tokens) >= 3 else 10.0
            except ValueError:
                raise ValueError(
                    f"pharmacophore constraint '{constraint}' strength is not an float"
                )

            try:
                reference_index = int(tokens[3]) if len(tokens) >= 4 else 1
            except ValueError:
                raise ValueError(
                    f"pharmacophore constraint '{constraint}' reference is not an integer"
                )

            types = []
            try:
                bit_value = int(tokens[1])
                if bit_value & 1:
                    types.append(flare.Align.PharmacophoreConstraint.Donor)
                if bit_value & 2:
                    types.append(flare.Align.PharmacophoreConstraint.Acceptor)
                if bit_value & 4:
                    types.append(flare.Align.PharmacophoreConstraint.Cation)
                if bit_value & 8:
                    types.append(flare.Align.PharmacophoreConstraint.Anion)
                if bit_value & 16:
                    types.append(flare.Align.PharmacophoreConstraint.MetalBinder)
                if bit_value & 32:
                    types.append(flare.Align.PharmacophoreConstraint.Covalent)
            except ValueError:
                for char in tokens[1]:
                    if char.lower() == "d":
                        types.append(flare.Align.PharmacophoreConstraint.Donor)
                    elif char.lower() == "a":
                        types.append(flare.Align.PharmacophoreConstraint.Acceptor)
                    elif char == "+":
                        types.append(flare.Align.PharmacophoreConstraint.Cation)
                    elif char == "-":
                        types.append(flare.Align.PharmacophoreConstraint.Anion)
                    elif char.lower() == "m":
                        types.append(flare.Align.PharmacophoreConstraint.MetalBinder)
                    elif char.lower() == "v":
                        types.append(flare.Align.PharmacophoreConstraint.Covalent)
                    else:
                        raise ValueError(
                            f"pharmacophore constraint '{constraint}' type is not valid"
                        )

            if reference_index <= 0 or reference_index > len(align.references):
                raise ValueError(
                    f"pharmacophore constraint '{constraint}' has an invalid reference, "
                    + "expected a value between 1 and {len(align.references)} found "
                    + "{reference_index})"
                )

            reference = align.references[reference_index - 1]

            if index <= 0 or index > len(reference.atoms):
                raise ValueError(
                    f"pharmacophore constraint '{constraint}' has an invalid atom index, "
                    + "expected a value between 1 and {len(reference.atoms())} found {index})"
                )

            atom = reference.atoms[index - 1]
            align.pharmacophore_constraints[atom] = (types, strength)


def _read_batch_into_role(project, args, role):
    """Yield when the `role` ligands is updated to a new batch."""
    # Read from stdin if only the reference file is given
    mol_files = args.ligands[1:] if len(args.ligands) > 1 else ["-"]
    # If a project is set then all ligands need to be kept in memory so the project can be
    # saved
    if args.project and args.batch_size != DEFAULT_BATCH_SIZE:
        print("--batch-size will be ignored as -P/--project was given.\n", file=sys.stderr)
    batch_size = args.batch_size if not args.project else sys.maxsize

    raise_exception_on_error = not args.input_ignore_errors

    if args.immobile:  # Ignore read mode as we are scoring alignments
        for _ in _read_batch_into_role_poses(
            mol_files, role, args, batch_size, raise_exception_on_error
        ):
            yield
    elif args.readmode == "autodetect" or args.readmode == "u":
        for _ in _read_batch_into_role_autodetect(
            mol_files, role, args, batch_size, raise_exception_on_error
        ):
            yield
    elif args.readmode.startswith("c"):  # Each file contains 1 molecule with a number of confs
        for _ in _read_batch_into_role_conformations(
            mol_files, project, role, args, batch_size, raise_exception_on_error
        ):
            yield
    else:  # Each structure is either a molecule or a molecule in a fixed conformation
        for _ in _read_batch_into_role_molecules(
            mol_files, role, args, batch_size, raise_exception_on_error
        ):
            yield


def _find_format(mol_file, args):
    # If reading from stdin and the file format is not given assume "sdf" format
    input = args.input if args.input != "auto" else None
    if mol_file == "-" and input is None:
        input = "sdf"
    return input


def _read_batch_into_role_autodetect(mol_files, role, args, batch_size, raise_exception_on_error):
    """Yield when the `role` ligands is updated to a new batch.

    For each structure if its a conformation of the previous structure then it is added as a
    conformation, otherwise it is added as a new ligand.
    """
    # As conformations will span over batches we have to read into the next batch to check
    # a molecules has all its confs. So the batch size is reduced to reduce memory
    read_batch_size = max(10, batch_size / 10) if batch_size < sys.maxsize else batch_size

    last_mol_smiles = None

    for mol_file in mol_files:
        for batch, errors in cmdutil.batch_read_files(
            [mol_file], _find_format(mol_file, args), read_batch_size, raise_exception_on_error
        ):
            for error in errors:
                print(error, file=sys.stderr)

            for item in batch:
                role.ligands.append(item)
                smiles = Chem.MolToSmiles(role.ligands[-1].to_rdmol(), isomericSmiles=False)

                if last_mol_smiles == smiles:
                    # The mol structure is the same as the last structure
                    role.ligands[-2].conformations.append(role.ligands[-1])
                    del role.ligands[-1]
                else:
                    # New molecule, if we have gone over the batch_size, yield
                    last_mol_smiles = smiles
                    if len(role.ligands) > batch_size:
                        # The molecule that has just been added will not have its confs. Leave
                        # it out of this batch and add it to the next batch
                        del role.ligands[-1]
                        yield
                        role.ligands.clear()
                        role.ligands.append(item)
    if role.ligands:
        yield


def _read_batch_into_role_conformations(
    mol_files, project, role, args, batch_size, raise_exception_on_error
):
    """Yield when the `role` ligands is updated to a new batch.

    Each file contains the conformations for 1 molecule.
    """
    role.ligands.clear()
    for file_name in mol_files:
        structures = []
        errors = None if raise_exception_on_error else []
        with cmdutil._read_molecules(
            file_name, _find_format(file_name, args), batch_size, errors
        ) as molecules:
            for molecule in molecules:
                structures.append(molecule)

            if not raise_exception_on_error:
                for error in errors:
                    print(error, file=sys.stderr)

        if structures:
            ligand = role.ligands.append(structures[0])
            ligand.conformations.extend(structures)

        if len(role.ligands) >= batch_size:
            yield
            role.ligands.clear()
    if role.ligands:
        yield


def _read_batch_into_role_molecules(mol_files, role, args, batch_size, raise_exception_on_error):
    """Yield when the `role` ligands is updated to a new batch.

    Each structure is either a structure molecule or a molecule in a fixed conformation.
    """
    role.ligands.clear()
    for mol_file in mol_files:
        for batch, errors in cmdutil.batch_read_files(
            [mol_file], _find_format(mol_file, args), batch_size, raise_exception_on_error
        ):
            for error in errors:
                print(error, file=sys.stderr)

            for item in batch:
                role.ligands.append(item)
            if args.readmode.startswith(
                "f"
            ):  # Each structure is a molecule in a fixed conformation
                for ligand in role.ligands:
                    ligand.conformations.append(ligand)

            if len(role.ligands) >= batch_size:
                yield
                role.ligands.clear()

    if role.ligands:
        yield


def _read_batch_into_role_poses(mol_files, role, args, batch_size, raise_exception_on_error):
    """Yield when the `role` ligands is updated to a new batch.

    Each structure is an alignment.
    """
    role.ligands.clear()
    for mol_file in mol_files:
        for batch, errors in cmdutil.batch_read_files(
            [mol_file], _find_format(mol_file, args), batch_size, raise_exception_on_error
        ):
            for error in errors:
                print(error, file=sys.stderr)

            for item in batch:
                role.ligands.append(item)
            for ligand in role.ligands:
                ligand.poses.append(ligand)

            if len(role.ligands) >= batch_size:
                yield
                role.ligands.clear()

    if role.ligands:
        yield


def _write_results(align, project, imported_tags, ref_role, args):
    """Write the results."""
    # Print the result poses to stdout
    poses = []
    for ligand in align.ligands:
        if args.log:
            log_text = cmdutil.molecule_log_as_sdf_string(ligand)
        for i, pose in enumerate(ligand.poses, 1):
            pose.properties["Alignment"].value = i

            conformer_number = "n/a"
            if pose.conformation is not None:
                try:
                    conformer_number = ligand.conformations.index(pose.conformation) + 1
                except ValueError:
                    conformer_number = "-"
            pose.properties["Conformer"].value = conformer_number

            if len(align.references) == 1:
                reference_name = align.references[0].title
            else:
                reference_name = "n/a"
            # Cannot write Ref tag directly as it is a Flare column,
            # value cannot be set in modify_tags as align cannot be passed to it,
            # so set Ref to MyRef in modify_tags
            pose.user_data["MyRef"] = reference_name

            pose.properties["Alns"].value = len(ligand.poses)

            if args.log:
                pose.properties["FlareAlign_Log"].value = log_text

        poses.extend(ligand.poses)

    file_format = args.output if args.output else "sdf"

    allowed_cresset_tags = [
        "Sim",
        "FSim",
        "FScore",
        "FScore+P",
        "SSim",
        "SScore",
        "SScore+P",
        "Excl Vol Pen",
        "Field Penalty",
        "Alignment",
        "_cresset_fieldpoints",
        "Transformation",
        "Conformer",
        "Ref",
        "Confs",
        "Alns",
        "Filename",
        "FlareAlign_Log",
    ]
    list_tags_to_keep = list(imported_tags)
    list_tags_to_keep.extend(allowed_cresset_tags)

    def remove_noninput_tags(ligand, tags):
        """Filter out tags that weren't in the input file."""
        tags_to_delete = [tag for tag in tags if tag not in imported_tags]
        for tag in tags_to_delete:
            del tags[tag]

        return tags

    def modify_tags(pose, tags):
        """Add/Removes SDF tags for each pose."""
        tags["Ref"] = pose.user_data["MyRef"]

        tags["Confs"] = str(len(pose.ligand.conformations))

        tags["Filename"] = tags["Filename_cresset"]
        tags_to_delete = [tag for tag in tags if tag not in list_tags_to_keep]
        for tag in tags_to_delete:
            del tags[tag]

        return tags

    if args.writeref == "before":
        flare.write_file(sys.stdout, ref_role.ligands, file_format, remove_noninput_tags)

    flare.write_file(sys.stdout, poses, file_format, modify_tags)

    if args.writeref == "after":
        flare.write_file(sys.stdout, ref_role.ligands, file_format, remove_noninput_tags)

    if args.project:
        try:
            # Remove the log column so it does not show up in the project
            del project.ligands.columns["FlareAlign_Log"]
        except KeyError:
            pass

        # Delete unused conformations to free up memory and reduce the project file size
        # This is done after writing the results to stdout so the "Confs_cresset" sdf tag has
        # the correct value
        if not args.keep_all_confs:
            for ligand in align.ligands:
                ligand.conformations.clear()

        try:
            project.save(args.project)
        except Exception as err:
            print(err, file=sys.stderr)
            # print the error and continue


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
