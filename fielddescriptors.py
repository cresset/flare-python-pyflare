#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Generates field descriptors for aligned ligands which are used to build machine learning models.

input:
  The input ligands should be in SDF format

  Optionally a csv file can be read which contains the field descriptor types and coordinates by
  using the -d/--descriptors argument. If this is not given then this data will be generated.

output:
  The input ligands in SDF format with tags for the field descriptors.

  If -w/--write-descriptors is given the field descriptor types and coordinates will be written to
  a csv file. If this file is passed to -d/--descriptors in subsequent runs the types and
  coordinates of the descriptors will be reused.

examples:
  pyflare fielddescriptors.py -d descriptors.csv training_set.sdf
    Reads the ligands in 'training_set.sdf' and calculates the descriptor types and coordinates.
    The descriptor values will be added to the training_set as SDF tags and printed to stdout.
    The descriptor types and coordinates will be written to descriptors.csv.

  pyflare fielddescriptors.py -w descriptors.csv test_set.sdf
    Reads the ligands in 'test_set.sdf' and the descriptor types and coordinates are read from
    the descriptors.csv file.
    The descriptor values will be added to the test_set as SDF tags and printed to stdout.

  pyflare fielddescriptors.py --help
    Prints the help text including all available options and exits.
"""
import sys
import csv
import argparse
import traceback

from cresset import flare

from _internal import cmdutil


def main(argv):
    try:
        args = _parse_args(argv)

        # Load the ligands
        project = flare.Project()
        for file in args.ligands:
            project.ligands.extend(flare.read_file(file, "sdf"))

        # Calculate or read the descriptors
        descriptors = None
        if args.descriptors is None:
            descriptors = _generate_field_descriptors(args, project.ligands)
        else:
            descriptors = _parse_field_descriptors(args.descriptors)

        # Add the descriptors values to the ligands
        descriptors.add_as_columns(project.ligands)

        # Write the descriptors
        if args.write_descriptors is not None:
            _write_field_descriptors(args.write_descriptors, descriptors)

        # Write the ligands with the field descriptors as SDF tags
        custom_tags = set(project.ligands.columns.user_keys())

        def modify_tags(pose, tags):
            """Removes SDF tags which were not in the input or are not field descriptors."""
            tags_to_delete = [tag for tag in tags if tag not in custom_tags]
            for tag in tags_to_delete:
                del tags[tag]
            return tags

        if args.output == "sdf":
            flare.write_file(sys.stdout, project.ligands, "sdf", modify_tags)
        elif args.output == "csv":
            writer = csv.writer(sys.stdout)
            writer.writerow(["Title"] + descriptors.column_names)
            for ligand in project.ligands:
                writer.writerow(
                    [ligand.title] + [ligand.properties[c].value for c in descriptors.column_names]
                )

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )

    group = parser.add_argument_group("input/output")
    # Input settings
    group.add_argument(
        "ligands",
        nargs="+",
        help="The ligand files to use. If not given the ligands are read from stdin.",  # noqa: E501
    )
    group.add_argument(
        "-d",
        "--descriptors",
        help="CSV file containing the types and coordinates of the descriptors. If not given then this data will be generated",  # noqa: E501
    )
    # Output settings
    group.add_argument(
        "-w",
        "--write-descriptors",
        help="Writes a CSV file containing the types and coordinates of the descriptors. This may be passed to -w/--write-descriptors in subsequent runs to reuse the descriptors.",  # noqa: E501
    )
    group.add_argument(
        "-o",
        "--output",
        choices=["csv", "sdf"],
        type=str.lower,
        default="sdf",
        help="Set the format for writing the output. The default is 'sdf'.",
    )

    group = parser.add_argument_group("field descriptors")
    # Field descriptors settings
    group.add_argument(
        "-f",
        "--fields",
        metavar="e,s,h,v",
        type=str.lower,
        help="Specifies which fields to use for the QSAR calculation. The available fields are 'e'lectrostatic, 's'teric (i.e. van der Waals), 'h'ydrophobic or 'v'olume (i.e. volume indicator field, -1 inside, 0 outside). The default is 'e,v'.",  # noqa: E501
    )
    group.add_argument(
        "-m",
        "--mindist",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.GenerateFieldDescriptors.min_distance.__doc__),
    )

    group = parser.add_argument_group("general options")
    # Other settings
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )
    args = parser.parse_args(argv)

    return args


def _generate_field_descriptors(args, ligands):
    """Generates the field descriptors"""
    gen = flare.GenerateFieldDescriptors()
    gen.ligands = ligands

    if args.fields is not None:
        fields = set(args.fields.split(","))
        with cmdutil.CheckValue("fields"):
            gen.electrostatics_descriptors_enabled = "e" in fields
            gen.steric_descriptors_enabled = "s" in fields
            gen.hydrophobics_descriptors_enabled = "h" in fields
            gen.volume_descriptors_enabled = "v" in fields

    if args.mindist is not None:
        with cmdutil.CheckValue("mindist"):
            gen.min_distance = args.mindist

    descriptors = gen.run()
    return descriptors


def _parse_field_descriptors(csv_file_path):
    headings = []
    sample_x = []
    sample_y = []
    sample_z = []

    with open(csv_file_path, newline="") as csv_file:
        reader = csv.reader(csv_file)
        for row in reader:
            if len(row) > 0 and row[0] == "Sample co-ordinate labels":
                for cell in row[1:]:
                    headings.append(cell)
            elif len(row) > 0 and row[0] == "Sample_X":
                for cell in row[1:]:
                    sample_x.append(float(cell))
            elif len(row) > 0 and row[0] == "Sample_Y":
                for cell in row[1:]:
                    sample_y.append(float(cell))
            elif len(row) > 0 and row[0] == "Sample_Z":
                for cell in row[1:]:
                    sample_z.append(float(cell))

    if len(headings) == 0:
        raise ValueError(f'failed to read any descriptors from "{csv_file_path}"')

    if (
        len(headings) != len(sample_x)
        or len(headings) != len(sample_y)
        or len(headings) != len(sample_z)
    ):
        raise ValueError(f'failed to parse descriptors from "{csv_file_path}"')

    types = flare.FieldDescriptors.types_from_column_names(headings)
    coordinates = []
    for x, y, z in zip(sample_x, sample_y, sample_z):
        coordinates.append((x, y, z))

    return flare.FieldDescriptors(types, coordinates)


def _write_field_descriptors(csv_file_path, descriptors):
    with open(csv_file_path, "w", newline="") as csv_file:
        writer = csv.writer(csv_file)

        # Write descriptor matrix
        writer.writerow(["Sample co-ordinate labels", *descriptors.column_names])
        sample_x_row = ["Sample_X"]
        sample_y_row = ["Sample_Y"]
        sample_z_row = ["Sample_Z"]

        for x, y, z in descriptors.coordinates:
            sample_x_row.append(str(round(x, 3)))
            sample_y_row.append(str(round(y, 3)))
            sample_z_row.append(str(round(z, 3)))

        writer.writerow(sample_x_row)
        writer.writerow(sample_y_row)
        writer.writerow(sample_z_row)
        writer.writerow([])


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
