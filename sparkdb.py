#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""A script to generate fragment databases for Spark

sparkdb reads one or more  molecule files (in XED, mol2, smi or SDF format),
breaks them into fragments, and stores the fragments into a Spark database file.

SDF files (only) can be read from stdin by giving a filename of '-'.

Example:
  pyflare sparkdb.py molecule.sdf
    Reads "molecule.sdf", breaks each molecule into fragments and stores
    the fragments into a database file called "user.fsd". If the database
    does not exist it will be created.

  pyflare sparkdb.py --db myDatabase molecule.sdf
    As above, but the database filename is "myDatabase.fsd" instead of
    "user.fsd".

  pyflare sparkdb.py --db "C:\\databases\\myDatabase.fsd" molecule.sdf
    As above, but the database at "C:\\databases\\myDatabase.fsd" will be
    created or updated.

  pyflare sparkdb.py -c 0 -i U --db myfrags fragments.sdf
    The molecules in "fragments.sdf" are assumed to be molecular fragments
    with the attachment points labelled with uranium atoms. These fragments
    are imported in their existing conformation (no conformation hunt is done)
    and placed in the "myfrags.fsd" database.

  pyflare sparkdb.py --reagent alcohol --db alchols molecule.sdf
    Creates a reagent database called "alcohols.fsd" in the standard location
    using the "alcohol" script.

  pyflare sparkdb.py --merge-database source --db target
    Merges the contents of the "source.fsd" database into the "target.fsd"
    database.

Environment:
  CRESSET_FF_VER
    Set to '2' to force the use of XED FF version 2 in the calculations. The default
    is to use XED FF version 3.

  CRESSET_DDD
    Set to 'true' to force the use of distance-dependent dielectric (DDD) in field
    calculations. Set to 'false' to disable the use of distance-dependent dielectric
    in field calculations. The default is set to 'true'.

  FLARE_SPARK_DB
    If a relative path is specified with the -d option, the path will be interpreted relative to the
    directory pointed to by this variable.

  CRESSET_BROKER
    Set the location of the Cresset Engine Broker in the format hostname:port.
    This can be overridden using the -g option.

Notes:
  sparkdb.py uses Cresset's FieldEngine to perform the calculations. By default, sparkdb.py
  will launch a number of FieldEngines equal to the number of processors on the local
  machine. Use the -l and -g options to alter this behavior.
"""
import os
import sys
import argparse
import tempfile
import shutil
from cresset import flare

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)), ".."))
from _internal import cmdutil  # noqa: E402


def main(argv: [str]) -> int:
    """Run the sparkdb process"""
    args, parser = _parse_args(argv)

    if args.merge_database is not None:
        try:
            flare.SparkDatabaseGenerator.merge_databases(args.db, args.merge_database)
            print(f"Merged database '{args.merge_database}' into '{args.db}'")
        except ValueError as value_error:
            print(value_error)
            return 1
        return 0
    if args.reagent == "list":
        transforms = flare.SparkDatabaseGenerator.fragment_reagent_transforms()
        padding = 0
        for name in transforms.keys():
            padding = len(name) if len(name) > padding else padding
        padding = padding + (padding % 8) - 1
        print("Name".ljust(padding), "Description")
        term_width, _ = shutil.get_terminal_size((80, 20))
        print("=" * term_width)
        for name, description in transforms.items():
            print(name.ljust(padding), description)
        print("\nReagents are searched for in:")
        for directory in flare.SparkDatabaseGenerator.fragment_reagent_directories():
            print(f'"{directory}"')
        print("\nOther reagent processing scripts can be specified by providing their full path.")
        return 0

    if not args.files:
        parser.error("the following arguments are required: files")

    dbgen = flare.SparkDatabaseGenerator()
    # Set the quality options first (argparse exclusive arguments)
    if args.quick:
        dbgen.load_preset(flare.SparkDatabaseGenerator.Preset.Quick)
    elif args.exhaustive:
        dbgen.load_preset(flare.SparkDatabaseGenerator.Preset.Exhaustive)

    # Option/main group
    dbgen.files = args.files
    dbgen.name = args.db
    if args.verbose and os.path.isfile(args.db):
        print(
            "Calculation parameters have been loaded from the existing database.", file=sys.stderr
        )

    if args.title is not None:
        dbgen.title_tag = args.title
    extra_tags_list = ["", ""]
    if args.extratag is not None:
        extra_tags_list[0] = args.extratag
    if args.extratag2 is not None:
        extra_tags_list[1] = args.extratag2
    dbgen.extra_tags = extra_tags_list
    if args.description is not None:
        dbgen.description = args.description
    if args.category is not None:
        dbgen.category = args.category
    if args.subcategory is not None:
        dbgen.sub_category = args.subcategory
    if args.filter is not None:
        dbgen.ignore_existing = args.filter.split(",")

    # Fragmentation Method (argparse exclusive arguments)
    if args.importelement is not None:
        dbgen.fragment_mode = flare.SparkDatabaseGenerator.ImportMode.Fragment
        try:
            dbgen.fragment_attachment_point = (
                int(args.importelement) if args.importelement.isdigit() else str(args.importelement)
            )
        except ValueError as value_error:
            print(value_error)
            return 1
    elif args.reagent is not None or args.all_reagents:
        dbgen.fragment_mode = flare.SparkDatabaseGenerator.ImportMode.Reagent
        dbgen.fragment_reagent = "ALL_REAGENTS" if args.all_reagents else args.reagent

    # Fragmentation Settings
    if args.maxattach is not None:
        dbgen.fragment_max_points = args.maxattach
    dbgen.fragment_rings_only = args.ringrequired
    dbgen.fragment_process_already_seen = args.alreadyseen
    dbgen.strict_chirality = args.strict_chirality
    if args.undefined_chiral_centers is not None:
        dbgen.max_undefined_chiral_centers = args.undefined_chiral_centers

    # Fragmentation Size Options
    dbgen.fragment_max_heavy_count = args.maxnh
    dbgen.fragment_max_molecular_weight = args.maxmw
    dbgen.fragment_max_rotatable_bonds = args.maxrb
    dbgen.fragment_rotatable_bond_as_heavy_atoms = args.balance

    # Conformation Hunt Options
    if args.confs == 0:
        dbgen.confhunt = False
    else:
        dbgen.confhunt = True
        if args.confs is not None:
            dbgen.max_conformations = args.confs
    if args.dynamics_runs is not None:
        dbgen.num_dynamics_runs = args.dynamics_runs
    if args.gradient_cutoff is not None:
        dbgen.gradient_cutoff = args.gradient_cutoff
    if args.energy_window is not None:
        dbgen.energy_window = args.energy_window
    if args.filter_confs is not None:
        dbgen.filter_duplicate_conformers_at_rms = args.filter_confs
    if args.secondary_amides is not None:
        if args.secondary_amides in ("trans", "t"):
            dbgen.amide_handling = flare.Align.Amide.ForceTrans
        elif args.secondary_amides in ("spin", "s"):
            dbgen.amide_handling = flare.Align.Amide.Spin
        elif args.secondary_amides in ("input", "i"):
            dbgen.amide_handling = flare.Align.Amide.Input
        else:
            print("Error: unrecognised argument to --secondary-amides", file=sys.stderr)
            sys.exit(1)
    dbgen.disable_coulombic_and_attractive_vdw_forces = not args.coulombics
    dbgen.remove_boats = not args.allow_boats

    if args.verbose:
        # Note that the log file gets appended to, so should be unique
        log_file_name = os.path.join(
            tempfile.gettempdir(), "sparkdb_pyflare_{}.log".format(os.getpid())
        )
        dbgen.log_file = log_file_name

    # Engine setups
    if args.localengines is not None:
        dbgen.local_engine_count = args.localengines

    cmdutil.start_process(dbgen, args.broker, args.verbose)

    for result_db in dbgen.results:
        print("Database created at:", result_db.url)

    cmdutil.wait_for_finish(dbgen)
    cmdutil.check_errors(dbgen)

    if dbgen.errors():
        print("\n".join(dbgen.errors()), file=sys.stderr)

    if args.verbose:
        with open(dbgen.log_file, "r", encoding="utf-8") as f:
            for line in f:
                print(line, end="")
        try:
            os.remove(dbgen.log_file)
        except OSError:
            pass

        if not dbgen.is_cancelled():
            for result_path, result_rejections in dbgen.rejections.items():
                for rejection in result_rejections:
                    print(f"{result_path}: {rejection}")
    return 0


def _parse_args(argv: [str]) -> (argparse.Namespace, argparse.ArgumentParser):
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=cmdutil.WrappedNewlineFormatter,
        add_help=False,
        allow_abbrev=False,
    )

    parser.add_argument(
        "files",
        nargs="*",
        help="The files to take the molecules from.",
    )

    group = parser.add_argument_group("options")
    group.add_argument(
        "-d",
        "--db",
        default="user",
        help="Create/append to database 'dbname' (default: 'user')\n"
        + "If the database name is a relative path then the database file will be created "
        + "in $FLARE_SPARK_DB/equivalent."
        + "If the database name is an absolute path, then that path will be used for the "
        + "database file."
        + 'In either case, if the name does not end with ".fsd" then that suffix will be added.',
    )
    group.add_argument(
        "--merge-database",
        metavar="DATABASE",
        help="If this is specified, then the contents of the specified database are merged "
        + "into the database specified with the -d option. All other arguments will be ignored.",
    )
    group.add_argument(
        "-P",
        "--title",
        help="The name of the SDF tag which contains the molecule title. If this option is "
        + "missing then the SDF title field is used. This data is used when looking up the "
        + "parent structure for each result.",
    )
    group.add_argument(
        "--extratag",
        help="The name of an extra SDF tag containing information about the molecule. This "
        + "data is used when looking up the parent structure for each result.",
    )
    group.add_argument(
        "--extratag2",
        help="As --extratag, specifies a second auxiliary tag to store.",
    )
    group.add_argument(
        "-D",
        "--description",
        default="Custom database",
        help='Set the description string of the database (default "Custom database")',
    )
    group.add_argument(
        "-s",
        "--category",
        help="When selecting databases in Spark this database will be listed under the given "
        + "category. If this is not set then the database will appear in the 'Uncategorized' "
        + "section.",
    )
    group.add_argument(
        "-S",
        "--subcategory",
        help="When selecting databases in Spark this database will be listed under the given "
        + "subcategory. If this is not set then the database will appear directly under its "
        + "category.",
    )
    group.add_argument(
        "-f",
        "--filter",
        metavar="DB1,DB2,DB3,...",
        help="Skip fragments and molecules that are already present in the specified databases. "
        + "The databases can be given as relative paths or as absolute paths: see the -d option "
        + "for details on how these are interpreted.",
    )

    group = parser.add_argument_group("quality options")
    exclusive_group = group.add_mutually_exclusive_group()
    exclusive_group.add_argument(
        "-q",
        "--quick",
        action="store_true",
        help="Take shortcuts in the conformer generation: faster but fewer confs.",
    )
    exclusive_group.add_argument(
        "-e",
        "--exhaustive",
        action="store_true",
        help="Take a long time in the conformer generation: slower and more confs. Sets -c 30 and "
        + "--gradient-cutoff 0.3, as well as changing various internal settings relating to the "
        + "conformation hunt.",
    )

    group = parser.add_argument_group("fragmentation method")
    exclusive_group = group.add_mutually_exclusive_group()
    exclusive_group.add_argument(
        "-i",
        "--importelement",
        metavar="ATOMIC_NUMBER/ELEMENT_NAME",
        help="This flag specified that the input files do not consist of molecules to be "
        + "fragmented, but are instead a pre-existing fragment library. Fragment connection "
        + "points must be labelled with a particular element: this option specifies the "
        + "element (either by atomic number or by element name, "
        + 'so iron can be specified either as 26 or as "Fe"). Any molecule with no such '
        + "marked atoms is ignored.",
    )
    exclusive_group.add_argument(
        "-R",
        "--reagent",
        metavar="SCRIPTNAME",
        help="Reagent import: preprocess input files through the specified reagent-processing "
        + "script. "
        + "A set of example scripts are in the 'data/spark-db-reagents' subdirectory of the "
        + "installation. "
        + "If the script name is given, for example 'thiolS', it is looked for in that directory; "
        + "alternatively a full (absolute) path can be given to a script file."
        + "\n\n"
        + "Run with '-R list' to list available reagent types.",
    )
    exclusive_group.add_argument(
        "-A",
        "--all-reagents",
        action="store_true",
        help="As -R, but creates multiple databases, one for each reagent type. "
        + "The reagent name is added to the database name.",
    )

    group = parser.add_argument_group("fragmentation settings")
    group.add_argument(
        "-t",
        "--maxattach",
        metavar="A",
        type=int,
        help="Only keep fragments with at most A attachment points (defaults to 4).",
    )
    group.add_argument(
        "-k",
        "--strict-chirality",
        action="store_true",
        help="By default sparkdb assumes that all input molecules are racemic, "
        "and will process then in such a way that both enantiomers of any "
        "generated fragment will be searched. If it is desired that "
        "chirality is strictly preserved, so that an 'R' chiral center can "
        "only appear in that particular configuration in the results, then "
        "specify this option. This is most useful for reagent databases, "
        "where the enantiomer of a chiral reagent may not be available. ",
    )
    group.add_argument(
        "-u",
        "--undefined-chiral-centers",
        metavar="U",
        type=int,
        help="If an input molecule has more than U undefined chiral "
        "centres, it will be discarded and no fragments will be generated. "
        "This prevents databases from being filled with pointless fragments "
        "coming from highly-chiral molecules such as sugars. The default is "
        "3.",
    )
    group.add_argument(
        "-r",
        "--ringrequired",
        action="store_true",
        help="Only keep fragments which contain at least one ring atom.",
    )
    group.add_argument(
        "-a",
        "--alreadyseen",
        action="store_true",
        help="Process molecules that have already been seen. Useful when re-fragmenting "
        + "a set of molecules with different options.",
    )

    group = parser.add_argument_group("fragmentation size options")
    group.add_argument(
        "-n",
        "--maxnh",
        metavar="N",
        default=15,
        type=int,
        help="Only keep fragments with at most N heavy atoms (defaults to 15)",
    )
    group.add_argument(
        "-m",
        "--maxmw",
        metavar="W",
        default=250,
        type=int,
        help="Only keep fragments with molecular weight less than W (defaults to 250)",
    )
    group.add_argument(
        "-b",
        "--maxrb",
        metavar="B",
        default=5,
        type=int,
        help="Only keep fragments with at most B rotatable bonds (defaults to 5)",
    )
    group.add_argument(
        "-B",
        "--balance",
        metavar="B",
        default=0,
        type=int,
        help="One rotatable bond counts as this many heavy atoms when checking the "
        + "maximum fragment heavy atom count (defaults to 0)",
    )

    group = parser.add_argument_group("conformation hunt options")
    group.add_argument(
        "-c",
        "--confs",
        metavar="C",
        type=int,
        help="Generate at most C conformers per fragment. If C is set to 0 then "
        + "the input conformations will be assumed to be in 3D and the input "
        + "coordinates will be kept for each fragment. (defaults to 20, 30 with -e)",
    )
    group.add_argument(
        "--dynamics-runs",
        type=int,
        help=cmdutil.numpy_doc_to_help(
            flare.SparkDatabaseGenerator.num_dynamics_runs.__doc__, True
        ),
    )
    group.add_argument(
        "--gradient-cutoff",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.SparkDatabaseGenerator.gradient_cutoff.__doc__, True),
    )
    group.add_argument(
        "-E",
        "--energy-window",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.SparkDatabaseGenerator.energy_window.__doc__, True),
    )
    group.add_argument(
        "-F",
        "--filter-confs",
        type=float,
        help=cmdutil.numpy_doc_to_help(
            flare.SparkDatabaseGenerator.filter_duplicate_conformers_at_rms.__doc__, True
        ),
    )
    group.add_argument(
        "--secondary-amides",
        choices=["trans", "spin", "input", "t", "s", "i"],
        type=str.lower,
        help="Specify how the conformation hunter is to handle amides. The 'trans' option forces all secondary amides to adopt the trans geometry. The 'input' option will leave secondary amides in the geometry (cis/trans) that they were in in the input file. Note that this means that if the input structure was drawn cis then you will get only cis amide conformations! The 'spin' option allows the amide bond to spin, so you will get both cis and trans amide conformations. This option does not affect ureas, urethanes, thioamides etc., only normal acyclic secondary amides. The default is 'trans'.",  # noqa: E501
    )
    group.add_argument(
        "--coulombics",
        action="store_true",
        help="Use full coulombics and van der Waals interactions during the conformer generation. The default is to neglect coulombics and attractive vdW forces, which usually gives a better conformer population but may ignore internal hydrogen bonds.",  # noqa: E501
    )
    group.add_argument(
        "--allow-boats",
        action="store_true",
        help=cmdutil.numpy_doc_to_help(flare.SparkDatabaseGenerator.remove_boats.__doc__, True),
    )

    group = parser.add_argument_group("field engine options")
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        help="Set the number of local engines to start. Use 0 for no local processing: "
        + "you must specify some remote engines with -g if you do so.",
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker in the format "
        + "hostname:port. Field Engines which have been registered with the "
        + "Broker will be used during the calculation. The CRESSET_BROKER "
        + "environment variable can be used instead of this option.",
    )

    group = parser.add_argument_group("general options")
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print log and progress information to standard error.",
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )

    return (parser.parse_args(argv), parser)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
