#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Finds templates from two to ten active 2D or 3D ligands.

This script compares ligands using their electrostatic and hydrophobic fields in order to find
common patterns. When applied to several structurally distinct ligands with a common activity,
this script can determine the bioactive conformations and relative alignments of these ligands
without requiring any protein information. Other pharmacophore generation packages simply attempt
to generate a very crude idea of what the protein wants in terms of donor points, acceptor points
and the like: FieldTemplater attempts to provide a full picture of how the active ligands bind,
which features they use, what shape they are, and how different series can be compared.

Flare's molecular comparisons utilize Cresset's field and 'field point' technology.
Fields are a way of representing ligands in terms of their surface and electrostatic properties:
positive and negative electrostatic fields, van der Waals effects and hydrophobic effects on and
near the surface of a ligand. Field points are placed at regions where the field has a
local maximum. They summarize the most important binding regions of a ligand and can be used both
to visually compare fields of two ligands and to accurately determine the overall similarity
of the fields of two different ligands.

input:
Two to ten input ligands can be read in SDF or mol2 format. The input format should be autodetected.
The best results are obtained when three to six structurally diverse active ligands are provided.

As an alternative to loading input ligands directly, this script can load a Flare project file.
The conformations and alignments in the project will be used if present or generated if missing.
Any existing templates will be deleted. This behavior can be changed using the -d/--delete flag.
All other relevant settings from that project file (e.g. pairwise constraints) will be used as well.
If the Forge project contains more than one FieldTemplater project then the name of the
FieldTemplater project should be given on the command line after the path to the project file.
The input project is not modified by templater.py but the new project can be saved using the
-P/--project flag.

output:
The result ligands are written to standard output in SDF format.
Different file formats can be specified with the -o flag.
In addition, a Flare project file can be written containing the results by specifying the -P flag.

In the output stream, templates are written in size order, then score order.
So, when templating 5 molecules A-E, all 5-ligand templates will be written,
followed by all 4-ligand templates containing A,B,C,D, followed by all 4-molecule templates
containing A,B,C,E, and so forth. Tags are added to each molecule written containing details of
which template it is in (the 'Template', 'Member' and 'Molecule name' tags).

examples:
  pyflare templater.py ligands.sdf > results.sdf
    Reads 'ligands.sdf'. For each entry, generates conformers then aligns them to the conformers
    of the other ligands in the 'ligands.sdf' file to generate the templates.
    Writes the best templates to 'templates.sdf' in SDF format.

  pyflare templater.py -e -P project.flr molecules.sdf > templates.sdf
    As above, but generates more conformers. This is best when using larger molecules with 6 or
    more rotatable bonds. A Flare project is also saved containing the templates.

  pyflare templater.py -r c mol_1_conformers mol_2_conformers mol_3_conformers >
  templates.sdf
    Generates the templates using the conformations in the files instead of generating new
    conformations.

  pyflare templater.py -d a -P Results.flr FT.flr myproject > templates.sdf
    Opens the FieldTemplater project called 'myproject' in 'FT.flr'.
    The templates and alignments are removed and the calculation is rerun.
    The templates are written to 'templates.sdf' in SDF format and to the 'Results.fqj' Flare
    project file. The 'FT.fqj' Flare project file is not changed.

  pyflare templater.py --help
    Prints the help text including all available options and exits.
"""
import sys
import argparse
import traceback
import re

from cresset import flare
from rdkit import Chem

from _internal import cmdutil

DEFAULT_BATCH_SIZE = 5000


def main(argv):
    """Run the template process using the command line args."""
    try:
        template = flare.FieldTemplater()

        args = _parse_args(template, argv)
        project, ft_project = _configure_process(template, args)

        # Run the calculation
        cmdutil.start_process(template, args.broker, args.verbose)
        cmdutil.wait_for_finish(template)

        cmdutil.check_errors(template)
        if not template.is_cancelled():
            _write_results(project, ft_project, args)

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(template, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )
    # Input settings
    parser.add_argument(
        "ligands_or_project",
        nargs="+",
        help="The ligand files or Flare project to use. If not given the ligands are read from stdin. If a Flare project file is loaded the optional second arg is the name of the FieldTemplater project to load.",  # noqa: E501
    )

    group = parser.add_argument_group("input/output")
    group.add_argument(
        "-r",
        "--readmode",
        choices=["autodetect", "molecules", "conformers", "isomer", "u", "m", "c", "i"],
        default="autodetect",
        type=str.lower,
        help="Set how the ligands should be read. In 'm'olecules mode, every entry is assumed to be a separate molecule that will require conformer generation. In 'c'onformers mode, each input file is assumed to hold multiple conformers of the same molecule i.e. one molecule per file, possibly as multiple confs. In 'f'ixed mode, each entry is assumed to be a separate molecule in a fixed conformation: it will require alignment but not conformer generation. The default, a'u'todetect, uses heuristics to decide whether each entry is a new molecule or a conformation of the preceding molecule.",  # noqa: E501"
    )
    group.add_argument(
        "-i",
        "--input",
        choices=["pdb", "sdf", "mol2", "xed", "smi"],
        default="sdf",
        type=str.lower,
        help="Set the file format for reading the ligands. The default is sdf.",
    )
    group.add_argument(
        "-d",
        "--delete",
        choices=["conformations", "alignments", "templates", "none", "c", "a", "t", "n"],
        default="templates",
        type=str.lower,
        help="Set which results in a project should be deleted and rerun. This option is only valid if a Flare project is used as input.",  # noqa: E501
    )
    # Output settings
    group.add_argument(
        "-o",
        "--output",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for writing the molecules. The default is 'sdf'.",
    )
    group.add_argument(
        "-P",
        "--project",
        help="Write the Flare project file with the results to the given file path.",
    )
    # Speed setting
    group = parser.add_argument_group("speed")
    group.add_argument(
        "-q",
        "--quick",
        action="store_true",
        help="Take shortcuts in the conformer generation and alignment: faster but possibly poorer templates.",  # noqa: E501
    )
    group.add_argument(
        "-e",
        "--exhaustive",
        action="store_true",
        help="Generate more conformers. This is best for molecules with 6 or more rotatable bonds, but turning this option on may dramatically increase run time.",  # noqa: E501
    )
    # Conformation settings
    # Do not set defaults for these settings as they will override the speed settings
    group = parser.add_argument_group("conformation")
    group.add_argument(
        "--secondary-amides",
        choices=["trans", "spin", "input", "t", "s", "i"],
        type=str.lower,
        help="Specify how the conformation hunter is to handle amides. The 'trans' option forces all secondary amides to adopt the trans geometry. The 'input' option will leave secondary amides in the geometry (cis/trans) that they were in in the input file. Note that this means that if the input structure was drawn cis then you will get only cis amide conformations! The 'spin' option allows the amide bond to spin, so you will get both cis and trans amide conformations. This option does not affect ureas, urethanes, thioamides etc., only normal acyclic secondary amides. The default is 'trans'.",  # noqa: E501
    )
    group.add_argument(
        "-E",
        "--energy-window",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.FieldTemplater.energy_window.__doc__, True),
    )
    group.add_argument(
        "-F",
        "--filter",
        type=float,
        help=cmdutil.numpy_doc_to_help(
            flare.FieldTemplater.filter_duplicate_conformers_at_rms.__doc__, True
        ),
    )
    group.add_argument(
        "--gradient-cutoff",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.FieldTemplater.gradient_cutoff.__doc__, True),
    )
    group.add_argument(
        "-m",
        "--maxconfs",
        type=int,
        help=cmdutil.numpy_doc_to_help(flare.FieldTemplater.max_conformations.__doc__, True),
    )
    group.add_argument(
        "-D",
        "--dynamics-runs",
        type=int,
        help=cmdutil.numpy_doc_to_help(flare.FieldTemplater.num_dynamics_runs.__doc__, True),
    )
    group.add_argument(
        "-b",
        "--do-not-remove-boats",
        action="store_false",
        help=cmdutil.numpy_doc_to_help(flare.FieldTemplater.remove_boats.__doc__, True),
    )
    group.add_argument(
        "--coulombics",
        action="store_false",
        help="Use full coulombics and van der Waals interactions during the conformer generation. The default is to neglect coulombics and attractive vdW forces, which usually gives a better conformer population but may ignore internal hydrogen bonds.",  # noqa: E501
    )
    # Alignment settings
    # Do not set defaults for these settings as they will override the speed settings
    group = parser.add_argument_group("alignment")
    group.add_argument(
        "-s",
        "--shapewt",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.FieldTemplater.shape_weight.__doc__, True),
    )
    group.add_argument(
        "-I",
        "--invert",
        action="store_true",
        help="""Allow conformers of achiral molecules to be inverted if that gives a better alignment.
Chiral molecules are never inverted.
This must be set if the imported conformations were exported from Flare, Forge or Torch as the conformer populations filter out mirror image conformations.
""",  # noqa: E501
    )
    group.add_argument(
        "-f",
        "--atomconstraint",
        metavar="m1,a1,m2,a2,d1[,d2][,s]",
        action="append",
        help="""Specifies an intermolecular atom constraint on the specified ligands (m1,m2) and atoms (a1,a2), with strength s. Multiple constraints can be specified by providing multiple -f options.

Constraints require certain pairs of atoms to be within certain distance ranges of each other. Each constraint specifies a pair of atoms in two different ligands and either a single number (which constrains the specified atoms to be that distance from each other), or two numbers (which constrains the specified atoms to be within the range d1 to d2 angstroms from each other), optionally followed by a third specifying the constraint strength. It is recommended (although not required) that constraints only be specified between heavy atoms, not hydrogens.

Atom numbers are specified starting from 1. Molecule numbers m1 and m2 refer to the order of files specified on the command line (so running fieldtemplater on A.sdf, B.sdf and C.sdf, molecule 1 refers to A, molecule 2 to B and so on).

Constraint examples:
-f 1,1,2,5,2.0
Constrains atom 1 on molecule 1 and atom 5 on molecule 2 to be 2.0A away from each other, constraint strength 10 A^-2 (the default).
-f 1,1,2,5,2.0,8.0
Constrains atom 1 on molecule 1 and atom 5 on molecule 2 to be between 2.0A and 8.0A from each other, constraint strength 10 A^-2.
-f 3,1,4,5,2,8,5
Constrains atom 1 on molecule 3 and atom 5 on molecule 4 to be 2.0-8.0A from each other, constraint strength 5 A^-2.
""",  # noqa: E501
    )
    # Templating settings
    # Do not set defaults for these settings as they will override the speed settings
    group = parser.add_argument_group("templating")
    group.add_argument(
        "-t",
        "--molecules-per-template",
        metavar="min,max",
        help="Sets the minimum and maximum number of ligands which are needed to form a template. With 8 input ligands set this to 5,7 to only find templates with 5, 6 or 7 ligands. The default is to allow any number of ligands greater than 2.",  # noqa: E501
    )
    group.add_argument(
        "-M",
        "--maxcomparisons",
        type=int,
        help=cmdutil.numpy_doc_to_help(flare.FieldTemplater.max_comparisons_per_pair.__doc__, True),
    )
    group.add_argument(
        "-x",
        "--max-score",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.FieldTemplater.max_score_delta_per_pair.__doc__, True),
    )
    group.add_argument(
        "-T",
        "--min-link-density",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.FieldTemplater.min_link_density.__doc__, True),
    )
    group.add_argument(
        "-R",
        "--template-filter",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.FieldTemplater.filter_duplicate_rms.__doc__, True),
    )
    # Other settings
    group = parser.add_argument_group("general options")
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        default=template.local_engine_count,
        help="Set the number of local engines to start. Use 0 for no local processing: you must specify some remote engines with -g if you do so.",  # noqa: E501
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker in the format hostname:port. Field Engines which have been registered with the Broker will be used during the calculation. The CRESSET_BROKER environment variable can be used instead of this option.",  # noqa: E501
    )
    group.add_argument(
        "-L",
        "--log",
        action="store_true",
        help="Request that the calculation log be written as one of the SDF tags for each alignment. Only has an effect if '-o sdf' (the default) is active. The calculation log is written as tag 'FlareAlign_Log'.",  # noqa: E501
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )

    args = parser.parse_args(argv)

    return args


def _configure_process(template, args):
    """Configure `template` using the command line arguments."""

    # Load the ligands or a Flare project
    project = None
    ft_project = None
    if args.ligands_or_project[0].endswith(".flr"):
        # Load the project from a file
        project = flare.Project.load(args.ligands_or_project[0])

        if len(args.ligands_or_project) > 1:
            # A FieldTemplater project name has been specified, find and load it
            ft_project_name = args.ligands_or_project[1]
            ft_projects = [proj for proj in project.ft_projects if proj.name == ft_project_name]
            if not ft_projects:
                raise ValueError(f"no FieldTemplater projects found called {ft_project_name}")
            elif len(ft_projects) == 1:
                ft_project = ft_projects[0]
            else:
                raise ValueError("multiple FieldTemplater projects with the same name detected")
        elif not project.ft_projects:
            # Flare project contains no FieldTemplater projects
            raise ValueError("no FieldTemplater projects found")
        elif len(project.ft_projects) == 1:
            # Flare project contains one FieldTemplater projects
            ft_project = project.ft_projects[0]
        else:
            # Flare project contains a number of FieldTemplater projects,
            # ask which one should be used
            names = '"' + '", "'.join([ft_project.name for ft_project in project.ft_projects]) + '"'
            raise ValueError(
                "multiple FieldTemplater projects detected, "
                + "specify a FieldTemplater project name on the command line "
                + "after the Flare project path. "
                + f"The names of the FieldTemplater projects are {names}."
            )
    else:
        # Load the molecules from files, create a FieldTemplater project and add the
        # molecules to them
        project = flare.Project()
        ft_project = project.ft_projects.new()

        _read_molecules(project, ft_project, args)

    if len(ft_project.inputs) <= 1:
        raise ValueError("2 or more ligands are required")

    template.ft_project = ft_project

    if args.delete:
        if args.delete.startswith("c"):
            template.delete_type = flare.FieldTemplater.DeleteMode.Conformations
        elif args.delete.startswith("a"):
            template.delete_type = flare.FieldTemplater.DeleteMode.Alignments
        elif args.delete.startswith("t"):
            template.delete_type = flare.FieldTemplater.DeleteMode.Templates
        elif args.delete.startswith("n"):
            template.delete_type = flare.FieldTemplater.DeleteMode.KeepAll
        else:
            raise ValueError(f"invalid --delete value {args.delete}")

    # Speed Settings
    if args.quick:
        template.load_conformation_hunt_preset(flare.FieldTemplater.ConformationHuntPreset.Quick)
        template.load_alignment_preset(flare.FieldTemplater.AlignPreset.Quick)
        template.load_templating_preset(flare.FieldTemplater.TemplatingPreset.Quick)
    elif args.exhaustive:
        template.load_conformation_hunt_preset(
            flare.FieldTemplater.ConformationHuntPreset.NormalLargeMols
        )
        template.load_alignment_preset(flare.FieldTemplater.AlignPreset.Normal)
        template.load_templating_preset(flare.FieldTemplater.TemplatingPreset.NormalLargeMols)
    else:
        template.load_conformation_hunt_preset(flare.FieldTemplater.ConformationHuntPreset.Normal)
        template.load_alignment_preset(flare.FieldTemplater.AlignPreset.Normal)
        template.load_templating_preset(flare.FieldTemplater.TemplatingPreset.Normal)

    # Conformation Settings
    if args.secondary_amides:
        if args.secondary_amides.startswith("t"):
            template.amide_handling = flare.FieldTemplater.Amide.ForceTrans
        elif args.secondary_amides.startswith("s"):
            template.amide_handling = flare.FieldTemplater.Amide.Spin
        elif args.secondary_amides.startswith("i"):
            template.amide_handling = flare.FieldTemplater.Amide.Input
        else:
            raise ValueError("invalid value for --secondary-amides")

    # If the user has not passed this value on the command line then do not change it
    # as it may have been updated by loading a preset
    if args.energy_window:
        with cmdutil.CheckValue("energy-window"):
            template.energy_window = args.energy_window

    if args.filter:
        with cmdutil.CheckValue("filter"):
            template.filter_duplicate_conformers_at_rms = args.filter

    if args.gradient_cutoff:
        with cmdutil.CheckValue("gradient-cutoff"):
            template.gradient_cutoff = args.gradient_cutoff

    if args.maxconfs:
        with cmdutil.CheckValue("maxconfs"):
            template.max_conformations = args.maxconfs

    if args.dynamics_runs:
        with cmdutil.CheckValue("dynamics-runs"):
            template.num_dynamics_runs = args.dynamics_runs

    with cmdutil.CheckValue("do-not-remove-boats"):
        template.remove_boats = args.do_not_remove_boats

    template.disable_coulombic_and_attractive_vdw_forces = args.coulombics

    # Align Settings
    if args.atomconstraint:
        _configure_atom_constraints(ft_project, args.atomconstraint)

    # If the user has not passed this value on the command line then do not change it
    # as it may have been updated by loading a preset
    if args.shapewt:
        with cmdutil.CheckValue("shapewt"):
            template.shape_weight = args.shapewt

    with cmdutil.CheckValue("invert"):
        template.invert_achiral_conformations = args.invert

    # Template Settings
    if args.molecules_per_template:
        with cmdutil.CheckValue("molecules-per-template"):
            tokens = args.molecules_per_template.split(",")
            if len(tokens) != 2:
                raise ValueError(f"expected 'min,max' found {args.molecules_per_template}")
            template.min_molecules_per_template = int(tokens[0])
            template.max_molecules_per_template = int(tokens[1])

    if args.maxcomparisons:
        with cmdutil.CheckValue("maxcomparisons"):
            template.max_comparisons_per_pair = args.maxcomparisons

    if args.max_score:
        with cmdutil.CheckValue("max-score"):
            template.max_score_delta_per_pair = args.max_score

    if args.min_link_density:
        with cmdutil.CheckValue("min-link-density"):
            template.min_link_density = args.min_link_density

    if args.template_filter:
        with cmdutil.CheckValue("template-filter"):
            template.filter_duplicate_rms = args.template_filter

    # Other settings

    with cmdutil.CheckValue("localengines"):
        template.local_engine_count = args.localengines

    return (project, ft_project)


def _configure_atom_constraints(ft_project, constraints):
    """Parse the ph4 constraints and add them to `template`."""
    for constraint in constraints:
        tokens = constraint.split(",")
        if 5 <= len(tokens) <= 7:
            input_1_index = 0
            input_2_index = 0
            atom_1_index = 0
            atom_2_index = 0
            min_dist = 0.0
            max_dist = 0.0
            strength = 0.0

            try:
                input_1_index = int(tokens[0])
            except ValueError:
                raise ValueError(f"constraint '{constraint}' 'm1' is not an integer")

            try:
                atom_1_index = int(tokens[1])
            except ValueError:
                raise ValueError(f"constraint '{constraint}' 'a1' is not an integer")

            try:
                input_2_index = int(tokens[2])
            except ValueError:
                raise ValueError(f"constraint '{constraint}' 'm2' is not an integer")

            try:
                atom_2_index = int(tokens[3])
            except ValueError:
                raise ValueError(f"constraint '{constraint}' 'a2' is not an integer")

            try:
                min_dist = float(tokens[4])
            except ValueError:
                raise ValueError(f"constraint '{constraint}' 'd1' is not an float")

            try:
                max_dist = float(tokens[5]) if len(tokens) >= 6 else min_dist
            except ValueError:
                raise ValueError(f"constraint '{constraint}' 'd2' is not an float")

            try:
                strength = float(tokens[6]) if len(tokens) >= 7 else 10.0
            except ValueError:
                raise ValueError(f"constraint '{constraint}' strength is not an float")

            input_1 = None
            input_2 = None
            atom_1 = None
            atom_2 = None

            if input_1_index < 1 or input_1_index > len(ft_project.inputs):
                raise ValueError(
                    f"constraint '{constraint}' 'm1' must be a integer between 1 and {len(ft_project.inputs)} inclusive"  # noqa: E501
                )
            else:
                input_1 = ft_project.inputs[input_1_index - 1]

            if atom_1_index < 1 or atom_1_index > len(input_1.isomers[0].atoms):
                raise ValueError(
                    f"constraint '{constraint}' 'a1' must be a integer between 1 and {len(input_1.isomers[0].atoms)} inclusive"  # noqa: E501
                )
            else:
                atom_1 = input_1.isomers[0].atoms[atom_1_index - 1]

            if input_2_index < 1 or input_2_index > len(ft_project.inputs):
                raise ValueError(
                    f"constraint '{constraint}' 'm2' must be a integer between 1 and {len(ft_project.inputs)} inclusive"  # noqa: E501
                )
            else:
                input_2 = ft_project.inputs[input_2_index - 1]

            if atom_2_index < 1 or atom_2_index > len(input_2.isomers[0].atoms):
                raise ValueError(
                    f"constraint '{constraint}' 'a2' must be a integer between 1 and {len(input_1.isomers[0].atoms)} inclusive"  # noqa: E501
                )
            else:
                atom_2 = input_2.isomers[0].atoms[atom_2_index - 1]

            if input_1 == input_2:
                raise ValueError(
                    f"constraint '{constraint}' 'm1' and 'm2' cannot be the same ligand"
                )

            if min_dist < 0:
                raise ValueError(f"constraint '{constraint}' 'd1' must be a positive float")

            if max_dist < 0:
                raise ValueError(f"constraint '{constraint}' 'd2' must be a positive float")

            if min_dist > max_dist:
                raise ValueError(f"constraint '{constraint}' 'd1' must be smaller than 'd2'")

            ft_project.add_distance_constraint(atom_1, atom_2, min_dist, max_dist, strength)


def _read_molecules(project, ft_project, args):
    """Reads the input ligands."""
    # Read from stdin if only the reference file is given
    mol_files = args.ligands_or_project if len(args.ligands_or_project) >= 1 else ["-"]

    if args.readmode == "autodetect" or args.readmode == "u":
        _read_molecules_autodetect(mol_files, ft_project, args)
    elif args.readmode.startswith("c"):  # Each file contains 1 molecule with a number of confs
        _read_molecules_conformations(mol_files, ft_project, args)
    elif args.readmode.startswith("i"):  # Each file contains 1 molecule with a number of isomers
        _read_molecules_isomers(mol_files, ft_project, args)
    else:
        _read_molecules_molecules(mol_files, ft_project, args)


def _read_molecules_autodetect(mol_files, ft_project, args):
    """Reads the molecules in autodetect mode.

    For each structure if its a conformation of the previous structure then it is added as a
    conformation, otherwise it is added as a new ligand.
    """
    for file_name in mol_files:
        last_mol_smiles = None
        for structure in flare.read_file(file_name, args.input):
            ft_project.inputs.append(structure)
            smiles = Chem.MolToSmiles(
                ft_project.inputs[-1].isomers[0].to_rdmol(), isomericSmiles=False
            )

            if last_mol_smiles == smiles:
                # The mol structure is the same as the last structure
                ft_project.inputs[-2].isomers[0].conformations.append(
                    ft_project.inputs[-1].isomers[0]
                )
                del ft_project.inputs[-1]
            else:
                # New molecule
                last_mol_smiles = smiles


def _read_molecules_conformations(mol_files, ft_project, args):
    """Reads the molecules in conformation mode.

    Each file contains the conformations for 1 molecule.
    """
    for file_name in mol_files:
        structures = list(flare.read_file(file_name, args.input))
        if structures:
            input = ft_project.inputs.append(structures[0])
            input.isomers[0].conformations.extend(structures)


def _read_molecules_isomers(mol_files, ft_project, args):
    """Reads the molecules in isomer mode.

    Each file contains the isomers for 1 molecule.
    """
    for file_name in mol_files:
        structures = list(flare.read_file(file_name, args.input))
        if structures:
            input = ft_project.inputs.append(structures[0])
            input.isomers.extend(structures[1:])
            assert len(input.isomers) == len(structures)


def _read_molecules_molecules(mol_files, ft_project, args):
    """Reads the molecules in molecule mode.

    Each file contains individual molecules.
    """
    for file_name in mol_files:
        ft_project.inputs.extend(flare.read_file(file_name, args.input))


def _write_results(project, ft_project, args):
    """Write the results."""

    if not ft_project.templates:
        print("No templates were generated", file=sys.stderr)

    # Print the result templates to stdout
    structures = []
    structures_data = []
    for i, template in enumerate(ft_project.templates, 1):
        for j, member in enumerate(template.members, 1):
            structures.append(member)
            isomer = member.conformation.isomer
            isomer_index = isomer.input.isomers.index(isomer) + 1
            structures_data.append((member, template, i, j, isomer_index))

    file_format = args.output if args.output else "sdf"

    tags_to_keep = set()
    tags_to_keep.add("_cresset_fieldpoints")

    # Keep track of the extra data which needs to be written with each template
    # `flare.write_file` will write the structures in the same order as the input
    structures_data_index = 0

    def modify_tags(member, tags):
        """Add/Removes SDF tags for each member of the template."""
        nonlocal structures_data_index
        (expected_member, template, template_index, member_index, isomer_index) = structures_data[
            structures_data_index
        ]
        structures_data_index += 1
        assert member == expected_member

        isomer = member.conformation.isomer
        input = isomer.input

        tags_to_delete = [tag for tag in tags if tag not in tags_to_keep]
        for tag in tags_to_delete:
            del tags[tag]

        for key, val in isomer.properties.items():
            tags[key] = str(val)

        tags["Template"] = str(template_index)
        tags["Member"] = str(member_index)
        tags["Isomer"] = str(isomer_index)

        tags["Similarity"] = str(round(template.sim_score, 3))
        tags["Field Similarity"] = str(round(template.field_sim_score, 3))
        tags["Raw Field Score"] = str(round(template.raw_field_sim_score, 3))
        tags["Penalised Field Score"] = str(round(template.penalised_field_sim_score, 3))
        tags["Shape Similarity"] = str(round(template.shape_sim_score, 3))
        tags["Raw Shape Score"] = str(round(template.raw_shape_sim_score, 3))
        tags["Penalised Shape Score"] = str(round(template.penalised_shape_sim_score, 3))
        tags["Atom Distance Constraint Penalty"] = str(
            round(template.atom_distance_constraint_penalty, 3)
        )
        tags["Template Pairwise RMS Similarity"] = str(round(template.pairwise_rms_similarity, 3))
        tags["Template RMS Angle Deviation"] = str(round(template.rms_angle_deviation, 3))
        tags["Template RMS Distance Deviation"] = str(round(template.rms_distance_deviation, 3))
        tags["Template Density"] = str(round(template.density, 3))

        if args.log:
            # Replace blank lines with a '.' to avoid breaking the SDF file format
            tags["Molecule_Log"] = re.sub(r"(?m)^\s*$\n?", ".\n", input.log.strip())
            tags["Template_Log"] = re.sub(r"(?m)^\s*$\n?", ".\n", template.log.strip())

        return tags

    flare.write_file(sys.stdout, structures, file_format, modify_tags)

    if args.project:
        try:
            project.save(args.project)
        except Exception as err:
            print(err, file=sys.stderr)
            # print the error and continue


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
