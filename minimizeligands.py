#!/usr/bin/env pyflare
# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Minimizes ligands and writes the results to stdout.

If a protein is given then its structure will be included in the minimization of each ligand but
the proteins atoms will not move. The protein should be prepared, use the proteinprep.py script to
do this.

By default, the AMBER force field is used to perform the minimization. To change this, use the
--small-molecule-force-field option. Minimization for the AMBER and Open force fields are performed
on GPU by default. To run on CPU instead, --openmm-use-cpu can be specified.

If the AMBER or Open force fields are used for the small molecules then AMBER's FF14SB force field
will be used for the protein. If the XED force field is used for the small molecules then
the XED force field will also be used for the protein.

The purpose of this script is to minimize a set of ligands within a fixed protein active site.
If you wish to minimize a protein or just the active site of a protein see the minimization.py
script.

examples:
  pyflare minimizeligands.py --openmm-use-cpu ligands.sdf
    Uses the AMBER force field on the CPU to minimize the ligands and write the results to stdout.

  pyflare minimizeligands.py --small-molecule-force-field xed --protein protein.pdb ligands.sdf
    Uses the XED force field to minimize the ligands within the protein and write the results to stdout.

  pyflare minimizeligands.py --help
    Prints the help text including all available options and exits.
"""  # noqa: E501
import sys
import argparse
import traceback

from cresset import flare

from _internal import cmdutil

DEFAULT_BATCH_SIZE = 10000


def main(argv):
    """Run the minimization process using the command line args."""
    try:
        minimization = flare.Minimization()

        # Configure the minimization process with the command line arguments
        args = _parse_args(minimization, argv)
        project = _configure_process(minimization, args)

        imported_tags = set()

        logging_context = cmdutil.LoggingContext(project.log, verbose=args.verbose)

        # Read each batch in turn and process them
        any_ligands_read = False
        for i, batch in enumerate(_read_batch(project, args), 1):
            any_ligands_read = True

            imported_tags = imported_tags.union(set(project.ligands.columns.user_keys()))

            minimization.ligands = project.ligands
            minimization.atoms = flare.atom_pick.expand_to_molecules(project.ligands)

            # Start the process
            cmdutil.start_process(minimization, args.broker, args.verbose)

            # Display the progress of the minimization process
            cmdutil.wait_for_finish(
                minimization, prefix=f"Batch {i}, ", logging_context=logging_context
            )

            # If there are errors don't stop, write the results and continue on to the next batch.
            # As if a few ligands failed to minimize we still want to print the results for all
            # other successfully ligands.
            if minimization.errors():
                print("\n".join(minimization.errors()), file=sys.stderr)

            # Writes the results based on the command line arguments
            if not minimization.is_cancelled():
                _write_results(project, imported_tags, args)

        if not any_ligands_read:
            raise ValueError("no ligand molecules were read from the input")

    except Exception as err:
        print(f"\nError: {err}", file=sys.stderr)
        print("\n---- Exception Information For Developers ----\n", file=sys.stderr)
        traceback.print_exc()
        return -1
    return 0


def _parse_args(minimization, argv):
    """Parse the command line arguments."""
    parser = argparse.ArgumentParser(
        description=__doc__,
        formatter_class=argparse.RawDescriptionHelpFormatter,
        add_help=False,
        allow_abbrev=False,
    )
    parser.add_argument(
        "ligands",
        nargs="*",
        help="The ligand files to minimize. If not given the ligands are read from stdin.",
    )

    group = parser.add_argument_group("input/output")
    group.add_argument(
        "-p", "--protein", help="The protein structure that will be included in the minimization."
    )
    group.add_argument(
        "-C",
        "--protein-chain-types",
        metavar="protein,water,other,ligand",
        default="protein,water,other,ligand",
        type=str.lower,
        help="Select the types of chains to include in the calculation as a comma separated list. 'p', 'w', 'o' and 'l' can be used as a shorthand for 'protein', water', 'other', 'ligand'. The default is to include all the chains.",  # noqa: E501
    )
    group.add_argument(
        "-i",
        "--input",
        choices=["sdf", "xed"],
        default="sdf",
        type=str.lower,
        help="Set the file format for reading the ligands. The default is 'sdf'.",
    )
    group.add_argument(
        "-Z",
        "--input-ignore-errors",
        action="store_true",
        help="By default, the calculation stops when errors are encountered in the input molecules. However, by setting this option the calculation will continue processing the remaining molecules and log the errors to standard error (stderr).",  # noqa: E501
    )
    group.add_argument(
        "-j",
        "--input-protein",
        choices=["pdb", "sdf", "mol2", "xed"],
        type=str.lower,
        help="Set the file format for reading the protein. The default is to autodetect.",
    )
    group.add_argument(
        "-o",
        "--output",
        choices=["sdf", "xed"],
        default="sdf",
        type=str.lower,
        help="Set the file format for writing the molecules. The default is 'sdf'.",
    )

    # OpenMM
    group = parser.add_argument_group("force field")
    cmdutil.add_dynamics_force_field_arguments(group, allow_xed=True)

    group = parser.add_argument_group("GPU/CPU")
    cmdutil.add_gpu_arguments(group, minimization)
    cmdutil.add_cpu_arguments(group, minimization)

    group = parser.add_argument_group("openmm")
    group.add_argument(
        "-w",
        "--solvent-model",
        choices=[
            "implicit",
            "vacuum",
            "gbn",
            "gbn2",
            "hct",
            "obc1",
            "obc2",
        ],
        help="The solvent model to use. Implicit (obc2) solvent is used by default.",
    )

    # OpenMM & XED
    group.add_argument(
        "-R",
        "--gradient-cutoff",
        type=float,
        # No default as the value is different for OpenMM and XED
        help=cmdutil.numpy_doc_to_help(flare.Minimization.gradient_cutoff.__doc__),
    )
    group.add_argument(
        "-I",
        "--max-iterations",
        type=int,
        # No default as the value is different for OpenMM and XED
        help=cmdutil.numpy_doc_to_help(flare.Minimization.max_iterations.__doc__),
    )

    group = parser.add_argument_group("general options")
    group.add_argument(
        "--batch-size",
        type=int,
        default=DEFAULT_BATCH_SIZE,
        help="Limit the number of molecules which are read into memory. Smaller values will reduce memory usage but will reduce performance by a small amount. If --project/-P is set then this option is ignored.",  # noqa: E501
    )
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        default=minimization.local_engine_count,
        help="Set the number of local engines to start. Use 0 for no local processing: you must specify some remote engines with -g if you do so.",  # noqa: E501
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker in the format hostname:port. Field Engines which have been registered with the Broker will be used during the calculation. The CRESSET_BROKER environment variable can be used instead of this option.",  # noqa: E501
    )
    group.add_argument(
        "-L",
        "--log",
        action="store_true",
        help="Request that the calculation log be written as one of the SDF tags for each ligand. Only has an effect if '-o sdf' (the default) is active. The calculation log is written as tag 'FlareMinimize_Log'.",  # noqa: E501
    )
    group.add_argument(
        "-P",
        "--project",
        help="Write the Flare project file with the results to the given file path.",
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )
    args = parser.parse_args(argv)

    return args


def _configure_process(minimization, args):
    """Configure `minimization` using the command line arguments."""
    if args.list_open_versions:
        cmdutil.print_open_force_field_versions_and_exit()

    project = flare.Project()

    if args.protein:
        project.proteins.extend(flare.read_file(args.protein, args.input_protein))

        if not project.proteins:
            raise ValueError("failed to load protein")
        if len(project.proteins) > 1:
            raise ValueError("only one protein can be specified")
        else:
            minimization.protein = project.proteins[0]
            minimization.protein.is_prepared = True  # Suppress warnings
            minimization.sequences = cmdutil.chain_types_to_sequences(
                minimization.protein, args.protein_chain_types
            )

    # Amber / OpenFF Settings
    cmdutil.configure_dynamics_force_field_process(minimization, args)

    # OpenMM Settings
    cmdutil.configure_gpu_process(minimization, args)
    if args.openmm_use_cpu:
        minimization.processor_type = flare.Minimization.ProcessorType.CPU
    else:
        minimization.processor_type = flare.Minimization.ProcessorType.GPU

    if args.solvent_model == "implicit":
        minimization.solvate.model = flare.SolventModel.ImplicitGBn2
    elif args.solvent_model == "vacuum":
        minimization.solvate.model = flare.SolventModel.Vacuum
    elif args.solvent_model == "gbn":
        minimization.solvate.model = flare.SolventModel.ImplicitGBn
    elif args.solvent_model == "gbn2":
        minimization.solvate.model = flare.SolventModel.ImplicitGBn2
    elif args.solvent_model == "hct":
        minimization.solvate.model = flare.SolventModel.ImplicitHCT
    elif args.solvent_model == "obc1":
        minimization.solvate.model = flare.SolventModel.ImplicitOBC1
    elif args.solvent_model == "obc2":
        minimization.solvate.model = flare.SolventModel.ImplicitOBC2

    # OpenMM & XED Settings
    if args.gradient_cutoff is not None:
        with cmdutil.CheckValue("gradient-cutoff"):
            minimization.gradient_cutoff = args.gradient_cutoff

    if args.max_iterations is not None:
        with cmdutil.CheckValue("max-iterations"):
            minimization.max_iterations = args.max_iterations

    # Other Settings
    with cmdutil.CheckValue("localengines"):
        minimization.local_engine_count = args.localengines

    return project


def _read_batch(project, args):
    """Yield batches of molecules read from the command line options."""
    # Read from stdin if no ligand files are given
    mol_files = args.ligands if args.ligands else ["-"]
    # If a project is set then all ligands need to be kept in memory so the project can be
    # saved
    if args.project and args.batch_size != DEFAULT_BATCH_SIZE:
        print("--batch-size will be ignored as -P/--project was given.\n", file=sys.stderr)
    batch_size = args.batch_size if not args.project else sys.maxsize

    raise_exception_on_error = not args.input_ignore_errors

    project.ligands.clear()
    for mol_file in mol_files:
        for batch, errors in cmdutil.batch_read_files(
            [mol_file], args.input, batch_size, raise_exception_on_error
        ):
            project.ligands.extend(batch)

            for error in errors:
                print(error, file=sys.stderr)

            if len(project.ligands) >= batch_size:
                yield
                project.ligands.clear()

    if project.ligands:
        yield


def _write_results(project, imported_tags, args):
    """Write the results."""
    # If requested write the project file
    if args.project:
        try:
            project.save(args.project)
        except Exception as err:
            print(err, file=sys.stderr)
            # print the error and continue

    if args.log:
        for ligand in project.ligands:
            log_text = cmdutil.molecule_log_as_sdf_string(ligand)
            ligand.properties["FlareMinimize_Log"].value = log_text

    allowed_cresset_tags = ["Filename", "FlareMinimize_Log"]
    list_tags_to_keep = list(imported_tags)
    list_tags_to_keep.extend(allowed_cresset_tags)

    def modify_tags(ligand, tags):
        """Add/Removes SDF tags for each ligand."""
        tags["Filename"] = tags["Filename_cresset"]

        tags_to_delete = [tag for tag in tags if tag not in list_tags_to_keep]
        for tag in tags_to_delete:
            del tags[tag]

        return tags

    # Print the result to stdout
    flare.write_file(sys.stdout, project.ligands, args.output, modify_tags)


if __name__ == "__main__":
    sys.exit(main(sys.argv[1:]))
