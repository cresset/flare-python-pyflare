# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
"""Helper classes and methods for Cresset's command line programs."""
import re
import os
import sys
import time
import math
import argparse
import textwrap

from contextlib import contextmanager
from cresset import flare


CONFIG_ARG_HELP_TEXT = "Read the calculation configuration from a config file. If a parameter is specified in both the config file and on the command line then the value in the config file is ignored. This option only works properly with config files created by the export calculation feature from the calculation start drop-down in Flare."  # noqa: E501


def numpy_doc_to_help(doc, remove_defaults=False, doc_has_defaults=True):
    """Convert numpy style property doc into a format suitable for argparse."""
    if doc_has_defaults:
        pattern = re.compile(r".*: ([\S\s]*)([\n ]Default.*)")
    else:
        pattern = re.compile(r".*: ([\S\s]*)")
    match = pattern.match(doc)
    help_text = match.group(1)
    if not remove_defaults and doc_has_defaults:
        help_text += match.group(2)

    help_text = help_text.replace(":attr:", "")
    help_text = help_text.replace(":class:", "")
    help_text = help_text.replace(":meth:", "")
    help_text = help_text.replace(":func:", "")
    # argparse uses format string so % needs to be escaped
    help_text = help_text.replace("%", "%%")

    return help_text


class SetBrokerEnv:
    """Set the broker to `value` if it is not None.

    When exiting the with block the broker is restored to its previous value.
    """

    def __init__(self, value):
        self._value = value
        self._old_value = None

    def __enter__(self):
        if self._value is not None:
            self._old_value = os.environ.get("CRESSET_BROKER", None)
            os.environ["CRESSET_BROKER"] = self._value
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        if self._value is not None:
            if self._old_value is not None:
                os.environ["CRESSET_BROKER"] = self._old_value
            elif "CRESSET_BROKER" in os.environ:
                del os.environ["CRESSET_BROKER"]


def start_process(process, broker, verbose):
    """Start the process.

    Parameters
    ----------
    broker : str or None
        Set `broker` as the broker to use for the calculation.
        `broker` should be in the format "hostname:post"
    """
    with SetBrokerEnv(broker):
        if process.setup_errors():
            raise Exception("\n".join(process.setup_errors()))

        if verbose:
            print(process.process_config(), file=sys.stderr)

        warnings = process.setup_warnings()
        if warnings:
            print("Warnings:", file=sys.stderr)

            for warning in warnings:
                print(warning, file=sys.stderr)

        process.start()


def print_progress(process, prefix=""):
    """Print the progress of the process."""
    progress_stages = process.progress()
    current_stage_index = None
    for i, value in enumerate(progress_stages):
        stage, progress = value
        if progress < 100:
            current_stage_index = i
            break

    stage_string = ""
    if len(progress_stages) > 1:
        stage_string = (
            "(stages " + "/".join(["{}".format(round(p[1], 1)) for p in progress_stages]) + "%)"
        )

    if current_stage_index is not None:
        stage, progress = progress_stages[current_stage_index]
        prog_str = "{} {} stage ({}/{}): {}% {}{}\r".format(
            prefix,
            stage,
            (current_stage_index + 1),
            len(progress_stages),
            round(progress, 1),
            stage_string,
            " " * 30,
        )
        sys.stderr.write(prog_str)
    else:
        sys.stderr.write("{}{}%{}\r".format(prefix, round(process.total_progress(), 1), " " * 60))


class LoggingContext:
    """Keep track of which logs in `cresset.flare.ProjectLogList` have been printed."""

    def __init__(self, project_log, verbose, first=0):
        self.project_log = project_log
        self.verbose = verbose
        self.next = first


def wait_for_finish(process, prefix="", verbose=False, logging_context=None):
    """Run the process while printing the progress."""
    clear_progress_line = False
    try:
        while process.is_running():
            if logging_context is not None and logging_context.verbose:
                print_project_logs(logging_context)
            else:
                print_progress(process, prefix=prefix)
                clear_progress_line = True
            time.sleep(0.5)
    except KeyboardInterrupt:
        # On the first Ctrl+C try to shutdown cleanly
        # On the second Ctrl+C `process.wait()` will raise an exception
        # and it is allowed to kill this script
        if clear_progress_line:
            print("", file=sys.stderr)
            clear_progress_line = False
        print("Calculation cancelled, shutting down FieldEngines, please wait", file=sys.stderr)
        process.cancel()
        process.wait()
    finally:
        if clear_progress_line:
            print("", file=sys.stderr)
        # if Ctrl+C is pressed try to write out the final log files
        if logging_context is not None and logging_context.verbose:
            print_project_logs(logging_context)


def print_project_logs(logging_context):
    """Print all the logs starting at index `first` to stderr."""
    last = len(logging_context.project_log)
    for i in range(logging_context.next, last):
        log = logging_context.project_log[i]
        header = log.datetime.strftime("%Y-%m-%d %H:%M:%S") + " - " + log.header
        print(header, file=sys.stderr)
        if log.summary:
            print(log.summary, file=sys.stderr)
        print("-" * max(len(header), len(log.summary)), file=sys.stderr)
        if log.comment:
            print(log.comment, file=sys.stderr)
        print("#" * 79, file=sys.stderr)
    # Flush in case the script is killed
    # But only flush if a log was printed
    if logging_context.next < last:
        sys.stderr.flush()
    logging_context.next = last


def check_errors(process):
    """Raise an exception if there were any errors while the process was running."""
    if process.errors():
        raise Exception("\n".join(process.errors()))


class CheckValue:
    """Change ValueError messages to include command line option name."""

    def __init__(self, name):
        self._name = name

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_value, exc_traceback):
        if exc_type == ValueError and exc_value.args:
            exc_value.args = (exc_value.args[0].replace("value", self._name, 1),)


@contextmanager
def _open_file_or_stdin(name):
    if name == "-":
        yield sys.stdin.buffer
    else:
        with open(name, "rb") as file:
            yield file


@contextmanager
def _read_molecules(name, fformat, size, errors):
    """Yield the molecules in the file.

    Parameters
    ----------
    files : str
        List of files to read. If one of the file name is "-" then stdin will be read.
    fformat : str
        The format of the file, e.g. sdf, smi
    size : int
        The number of molecules to read in each batch
    errors : list(str) or None
        If a list then errors while reading the file will be added to the list instead
        of raising an exception.

    Yields
    ------
    list(cresset.flare.Ligand)
        The molecules in the batch.
    """
    if size >= 2147483647:
        # Batch is too large, read the whole file
        size = -1
    yield flare.read_file(name, fformat, batch_size=int(size), errors=errors)


def batch_read_files(files, fformat, size, raise_exception_on_error):
    """Yield a list of molecules from the files up to `size` items.

    Parameters
    ----------
    files : str
        List of files to read. If one of the file name is "-" then stdin will be read.
    fformat : str
        The format of the file, e.g. sdf, smi
    size : int
        The number of molecules to read in each batch
    raise_exception_on_error : bool
        If true and the file contains an broken molecule then an exception is raised.
        Otherwise the errors are stored in a list and returned.

    Yields
    ------
    list(cresset.flare.Ligand)
        The molecules in the batch.
    list(str)
        If raise_exception_on_error is False then the list contains
        errors raised when reading the molecules.
    """
    batch = []
    batch_errors = []
    for name in files:
        errors = None if raise_exception_on_error else []
        with _read_molecules(name, fformat, size, errors) as molecules:
            for molecule in molecules:
                batch.append(molecule)
                if not raise_exception_on_error:
                    batch_errors.extend(errors)
                    errors.clear()

                if len(batch) >= size:
                    yield batch, batch_errors
                    batch = []
                    batch_errors = []
    if batch:
        yield batch, batch_errors


def molecule_log_as_sdf_string(molecule):
    """Return the molecule log as a string."""
    log_text = ""
    for log in molecule.logs:
        log_text += log.header + "\n"
        log_text += "~" * len(log.header) + "\n"
        if log.summary:
            log_text += log.summary + "\n"
        log_text += log.comment + "\n\n"
    # Replace blank lines with a '.' to avoid breaking the SDF file format
    log_text = re.sub(r"(?m)^\s*$\n?", ".\n", log_text.strip())
    return log_text


def chain_types_to_sequences(protein, chain_types_str):
    """Return the protein sequences who types are in chain_types_str."""
    str_to_types = {}
    str_to_types["protein"] = flare.Sequence.Type.Natural
    str_to_types["p"] = flare.Sequence.Type.Natural
    str_to_types["water"] = flare.Sequence.Type.Water
    str_to_types["w"] = flare.Sequence.Type.Water
    str_to_types["other"] = flare.Sequence.Type.Other
    str_to_types["o"] = flare.Sequence.Type.Other
    str_to_types["ligand"] = flare.Sequence.Type.Ligand
    str_to_types["l"] = flare.Sequence.Type.Ligand

    chain_types = set()
    for cstr in chain_types_str.split(","):
        try:
            chain_types.add(str_to_types[cstr])
        except KeyError as exc:
            raise ValueError(f"invalid chain type '{cstr}'") from exc

    sequences = [sequence for sequence in protein.sequences if sequence.type in chain_types]
    return sequences


def map_versions_to_parameter_files(parameter_files):
    """Return a dict of parameter version numbers to the parameter files"""
    version_to_file_map = {}
    version_from_file_map = {}
    pattern = re.compile(r"^.*openff_unconstrained-(\d+\.\d+\.\d+).offxml.*$", re.IGNORECASE)
    for parameter_file in parameter_files:
        match = pattern.match(parameter_file)
        if match:
            version_to_file_map[match.group(1)] = parameter_file
            version_from_file_map[parameter_file] = match.group(1)
    return version_to_file_map, version_from_file_map


def print_open_force_field_versions_and_exit():
    """Print the available Open Force Field versions and exits."""
    parameter_files = flare.OpenForceField.available_parameter_files()
    _, version_from_file_map = map_versions_to_parameter_files(parameter_files)
    print("Available pre-installed versions of the Open Force Field:")
    print("Version\t File path")
    for parameter_file in parameter_files:
        version = version_from_file_map.get(parameter_file, "")
        print(f"{version: <7}\t {parameter_file}")

    sys.exit(0)


def add_dynamics_force_field_arguments(parser, allow_xed=False, allow_open_custom_parameters=True):
    """Add the arguments used by calculations which use dynamics."""
    if allow_xed:
        parser.add_argument(
            "-f",
            "--small-molecule-force-field",
            choices=["amber", "open", "xed"],
            type=str.lower,
            help="The small molecule force field to use - either the Open Force Field, the AMBER force field (the default) or the XED force field.",  # noqa: E501
        )
    else:
        parser.add_argument(
            "-f",
            "--small-molecule-force-field",
            choices=["amber", "open"],
            type=str.lower,
            help="The small molecule force field to use - either the Open Force Field or the AMBER force field (the default).",  # noqa: E501
        )
    parser.add_argument(
        "--list-open-versions",
        action="store_true",
        help="Print a list of the available pre-installed Open Force Field versions and the path to the force field file.",  # noqa: E501
    )
    parser.add_argument(
        "--open-version",
        help='The version of the Open Force Field to use. To use one of the pre-installed parameter files, specify one of the versions listed by option --list-open-versions. Otherwise, an absolute file path can be specified, which must be accessible from all machines running the calculation. More information on the parameter files, as well as new parameter files, can be found at https://github.com/openforcefield/openforcefields. In this context, the "unconstrained" parameter files should be used. The default is to use the latest version of the Open Force Field.',  # noqa: E501
    )
    if allow_open_custom_parameters:
        parser.add_argument(
            "-U",
            "--enable-open-custom-parameters",
            action="store_true",
            help="Enable the generation of custom parameters for small molecules.",
        )
        parser.add_argument(
            "--open-custom-parameters-num-rotamers",
            type=int,
            default=flare.OpenForceField().custom_parameters_num_rotamers,
            help=numpy_doc_to_help(
                flare.OpenForceField.custom_parameters_num_rotamers.__doc__, True
            ),
        )
        parser.add_argument(
            "-y",
            "--open-custom-parameters-level-of-theory",
            default=flare.OpenForceField().custom_parameters_level_of_theory,
            choices=["ANI-2x", "GFN2-xTB", "DFT//GFN2-xTB"],
            help=numpy_doc_to_help(
                flare.OpenForceField.custom_parameters_level_of_theory.__doc__, True
            ),
        )

    parser.add_argument(
        "--amber-charge-method",
        choices=["gasteiger", "am1-bcc"],
        type=str.lower,
        help="The method used to compute partial charges when using the AMBER force field for small molecules.",  # noqa: E501
    )
    parser.add_argument(
        "--amber-gaff-version",
        choices=["gaff", "gaff2"],
        type=str.lower,
        help="The GAFF version applied when using the AMBER force field for small molecules.",
    )


def configure_dynamics_force_field_process(process, args, allow_open_custom_parameters=True):
    """Configure the process using the args set in `add_dynamics_force_field_arguments`."""
    if args.small_molecule_force_field == "amber":
        process.small_molecule_force_field = flare.AmberForceField()

        if args.amber_charge_method == "am1-bcc":
            process.small_molecule_force_field.charge_method = flare.AmberChargeMethod.AM1_BCC
        elif args.amber_charge_method == "gasteiger":
            process.small_molecule_force_field.charge_method = flare.AmberChargeMethod.Gasteiger

        if args.amber_gaff_version == "gaff":
            process.small_molecule_force_field.gaff_version = flare.AmberGaffVersion.GAFF
        elif args.amber_gaff_version == "gaff2":
            process.small_molecule_force_field.gaff_version = flare.AmberGaffVersion.GAFF2

    elif args.small_molecule_force_field == "open":
        # Verify that none of the AMBER options habe been set
        if args.amber_charge_method is not None:
            raise ValueError(
                "the --amber-charge-method option should only be used with the AMBER Force Field - the Open Force Field has been selected"  # noqa: E501
            )
        if args.amber_gaff_version is not None:
            raise ValueError(
                "the --amber-gaff-version option should only be used with the AMBER Force Field - the Open Force Field has been selected"  # noqa: E501
            )

        parameter_files = flare.OpenForceField.available_parameter_files()
        if args.open_version:
            version_to_file_map, _ = map_versions_to_parameter_files(parameter_files)
            if args.open_version in version_to_file_map:
                # User has given a version number, load the file which matches that version
                parameter_file = version_to_file_map[args.open_version]
            else:
                # User has given a file path to load
                parameter_file = args.open_version
            process.small_molecule_force_field = flare.OpenForceField(parameter_file)
        else:
            # Load the latest version of the forcefield.
            if len(parameter_files) >= 1:
                process.small_molecule_force_field = flare.OpenForceField(parameter_files[0])
            else:
                raise ValueError(
                    "no open force field files found, please specify the file with --open-version"
                )

        if allow_open_custom_parameters:
            process.small_molecule_force_field.custom_parameters_enabled = (
                args.enable_open_custom_parameters
            )
            process.small_molecule_force_field.custom_parameters_num_rotamers = (
                args.open_custom_parameters_num_rotamers
            )
            process.small_molecule_force_field.custom_parameters_level_of_theory = (
                args.open_custom_parameters_level_of_theory
            )

    elif args.small_molecule_force_field == "xed":
        process.small_molecule_force_field = flare.XedForceField()


def add_gpu_arguments(parser, process):
    """Add the arguments used by GPU calculations."""
    parser.add_argument(
        "--opencl-device-index",
        action="append",
        type=int,
        help="Indexes for local OpenCL devices that the calculation should "
        "be run on. If not set then the fastest CUDA or OpenCL device will be used. "
        "Note the CUDA_VISIBLE_DEVICES environment variable may affect the devices indexes. "
        "If the Cresset Engine Broker has been "
        "configured, this option is ignored and the calculation "
        "is instead performed on remote machines. This option can be "
        "specified multiple times to set multiple device index.",
    )
    parser.add_argument(
        "--opencl-platform-index",
        type=int,
        default=0,
        help="The index of the OpenCL platform which should be used. "
        ". If the Cresset Engine Broker has been "
        "configured, this option is ignored and the calculation "
        "is instead performed on remote machines. This option can be "
        "specified multiple times to set multiple device IDs.",
    )
    parser.add_argument(
        "--cuda-device-index",
        action="append",
        type=int,
        help="Indexes for local CUDA devices that the calculation should "
        "be run on. If not set then the fastest CUDA or OpenCL device will be used. "
        "Note the CUDA_VISIBLE_DEVICES environment variable may affect the devices indexes. "
        "If the Cresset Engine Broker has been "
        "configured, this option is ignored and the calculation "
        "is instead performed on remote machines. This option can be "
        "specified multiple times to set multiple device index.",
    )
    parser.add_argument(
        "--disable-local-gpu",
        action="store_true",
        help="Disable running GPU calculations on the local GPU.",
    )


def configure_gpu_process(process, args):
    """Configure the process using the args set in `add_gpu_arguments`."""
    active_args = [
        args.opencl_device_index is not None,
        args.cuda_device_index is not None,
        args.disable_local_gpu,
    ]
    if active_args.count(True) > 1:
        raise ValueError(
            "the options --opencl-device-index, --cuda-device-index, "
            "and --disable-local-gpu cannot be set together"
        )

    if args.opencl_device_index:
        process.gpu_devices = args.opencl_device_index
        process.gpu_opencl_platform = args.opencl_platform_index
        process.gpu_platform = flare.GpuPlatform.OpenCL
    elif args.cuda_device_index:
        process.gpu_devices = args.cuda_device_index
        process.gpu_platform = flare.GpuPlatform.CUDA
    elif args.disable_local_gpu:
        process.gpu_platform = flare.GpuPlatform.Disabled
    else:
        process.gpu_platform = flare.GpuPlatform.Any


def add_cpu_arguments(parser, process):
    """Add the arguments used by calculations which can use GPU or CPU."""
    parser.add_argument(
        "--openmm-use-cpu",
        action="store_true",
        help="Use CPU resources for the calculations, rather than the "
        "default of processing on GPU.",
    )


def configure_cpu_process(process, args):
    """Configure the process using the args set in `add_cpu_arguments`."""
    if args.openmm_use_cpu:
        process.openmm.processor_type = process.openmm.__class__.ProcessorType.CPU
    else:
        process.openmm.processor_type = process.openmm.__class__.ProcessorType.GPU


def add_membrane_arguments(parser, process):
    """Add the membrane argument."""
    parser.add_argument(
        "-m",
        "--membrane",
        choices=[
            "popc",
            "pope",
            "dlpc",
            "dlpe",
            "dmpc",
            "dopc",
            "dppc",
        ],
        help="The type of membrane to add. If set the solvent model must be left at its default"
        "value of explicit (tip3p) solvent. The membrane will be added in the xy plane and "
        + "centred on z=0.",
    )


def configure_membrane_process(process, args):
    """Configure the process using the args set in `add_membrane_arguments`."""
    if args.membrane == "popc":
        process.solvate.membrane = flare.MembraneType.MembranePOPC
    elif args.membrane == "pope":
        process.solvate.membrane = flare.MembraneType.MembranePOPE
    elif args.membrane == "dlpc":
        process.solvate.membrane = flare.MembraneType.MembraneDLPC
    elif args.membrane == "dlpe":
        process.solvate.membrane = flare.MembraneType.MembraneDLPE
    elif args.membrane == "dmpc":
        process.solvate.membrane = flare.MembraneType.MembraneDMPC
    elif args.membrane == "dopc":
        process.solvate.membrane = flare.MembraneType.MembraneDOPC
    elif args.membrane == "dppc":
        process.solvate.membrane = flare.MembraneType.MembraneDPPC


def add_openmm_common_arguments(parser, process):
    """Add the arguments used by calculations which use openMM."""
    parser.add_argument(
        "--openmm-minimize-energy-max-iterations",
        type=int,
        help=numpy_doc_to_help(
            process.openmm.__class__.minimize_energy_max_iterations.__doc__, True
        ),
    )

    parser.add_argument(
        "--openmm-minimize-energy-tolerance",
        type=float,
        help=numpy_doc_to_help(process.openmm.__class__.minimize_energy_tolerance.__doc__, True),
    )


def configure_openmm_common_process(process, args):
    """Configure the process using the args set in `add_openmm_common_arguments`."""
    with CheckValue("openmm-minimize-energy-max-iterations"):
        if args.openmm_minimize_energy_max_iterations is not None:
            process.openmm.minimize_energy_max_iterations = (
                args.openmm_minimize_energy_max_iterations
            )

    with CheckValue("openmm-minimize-energy-tolerance"):
        if args.openmm_minimize_energy_tolerance is not None:
            process.openmm.minimize_energy_tolerance = args.openmm_minimize_energy_tolerance


def add_openmm_equilibration_arguments(parser, process):
    """Add the arguments used by calculations which use openMM."""
    parser.add_argument(
        "-e",
        "--openmm-equilibration-time",
        type=int,
        help=numpy_doc_to_help(process.openmm.__class__.equilibration_time.__doc__, True),
    )

    parser.add_argument(
        "--openmm-equilibration-protocol",
        type=str,
        help=numpy_doc_to_help(process.openmm.__class__.equilibration_protocol.__doc__, True),
    )


def configure_openmm_equilibration_process(process, args):
    """Configure the process using the args set in `add_openmm_equilibration_arguments`."""
    with CheckValue("openmm-equilibration-time"):
        if args.openmm_equilibration_time is not None:
            process.openmm.equilibration_time = args.openmm_equilibration_time

    with CheckValue("openmm-equilibration-protocol"):
        if args.openmm_equilibration_protocol is not None:
            process.openmm.equilibration_protocol = args.openmm_equilibration_protocol


def add_openmm_time_step_arguments(parser, process):
    """Add the arguments used by calculations which use openMM."""
    parser.add_argument(
        "-S",
        "--openmm-time-step",
        type=float,
        help=numpy_doc_to_help(process.openmm.__class__.time_step.__doc__, True),
    )
    parser.add_argument(
        "--openmm-hydrogen-mass",
        type=float,
        help=numpy_doc_to_help(process.openmm.__class__.hydrogen_mass.__doc__, True),
    )


def configure_openmm_time_step_process(process, args):
    """Configure the process using the args set in `add_openmm_time_step_arguments`."""
    with CheckValue("openmm-time-step"):
        if args.openmm_time_step is not None:
            process.openmm.time_step = args.openmm_time_step

    with CheckValue("openmm-hydrogen-mass"):
        if args.openmm_hydrogen_mass is not None:
            process.openmm.hydrogen_mass = args.openmm_hydrogen_mass


def add_openmm_fep_ws_arguments(parser, process):
    """Add the arguments used by calculations which use openMM."""
    parser.add_argument(
        "--openmm-dynamics-time",
        type=int,
        help=numpy_doc_to_help(process.openmm.__class__.dynamics_time.__doc__),
    )


def configure_openmm_fep_ws_process(process, args):
    """Configure the process using the args set in `add_openmm_fep_arguments`."""
    with CheckValue("openmm-dynamics-time"):
        if args.openmm_dynamics_time is not None:
            process.openmm.dynamics_time = args.openmm_dynamics_time


def add_openmm_advanced_arguments(parser, process, process_is_fep=False):
    """Add the arguments used by calculations which use openMM."""
    non_bonded_method_choices = ["no-cutoff", "cutoff", "pme"]
    if process_is_fep:
        non_bonded_method_choices = ["cutoff", "pme"]
    else:
        parser.add_argument(
            "--openmm-reporting-frequency",
            type=int,
            help=numpy_doc_to_help(process.openmm.__class__.reporting_frequency.__doc__, True),
        )
    parser.add_argument(
        "-t",
        "--openmm-temperature",
        type=float,
        help=numpy_doc_to_help(process.openmm.__class__.temperature.__doc__, True),
    )
    parser.add_argument(
        "--openmm-pressure",
        type=float,
        help=numpy_doc_to_help(process.openmm.__class__.pressure.__doc__, True),
    )
    parser.add_argument(
        "--openmm-non-bonded-method",
        choices=non_bonded_method_choices,
        type=str.lower,
        help=numpy_doc_to_help(process.openmm.__class__.non_bonded_method.__doc__, True),
    )
    parser.add_argument(
        "--openmm-non-bonded-cutoff",
        type=float,
        help=numpy_doc_to_help(process.openmm.__class__.non_bonded_cutoff.__doc__, True),
    )


def configure_openmm_advanced_process(process, args, process_is_fep=False):
    """Configure the process using the args set in `add_dynamics_openmm_arguments`."""
    if not process_is_fep:
        with CheckValue("openmm-reporting-frequency"):
            if args.openmm_reporting_frequency is not None:
                process.openmm.reporting_frequency = args.openmm_reporting_frequency

    with CheckValue("openmm-temperature"):
        if args.openmm_temperature is not None:
            process.openmm.temperature = args.openmm_temperature

    with CheckValue("openmm-pressure"):
        if args.openmm_pressure is not None:
            process.openmm.pressure = args.openmm_pressure

    if args.openmm_non_bonded_method == "no-cutoff":
        process.openmm.non_bonded_method = flare.DynamicsOpenMM.NonBondedMethod.NoCutoff
    elif args.openmm_non_bonded_method == "cutoff":
        process.openmm.non_bonded_method = flare.DynamicsOpenMM.NonBondedMethod.Cutoff
    elif args.openmm_non_bonded_method == "pme":
        process.openmm.non_bonded_method = flare.DynamicsOpenMM.NonBondedMethod.PME

    with CheckValue("openmm-non-bonded-cutoff"):
        if args.openmm_non_bonded_cutoff is not None:
            process.openmm.non_bonded_cutoff = args.openmm_non_bonded_cutoff


def add_constraint_arguments(parser, process):
    """Add the arguments that constrain atoms."""
    parser.add_argument(
        "--constrain",
        choices=["ligand", "protein", "backbone"],
        nargs="+",
        type=str.lower,
        help="Atoms to be constrained",
    )

    parser.add_argument(
        "--constraint-strength", type=float, help="Strength of the constraints (in kcal/mol/A^2)"
    )

    parser.add_argument(
        "--constraint-width",
        type=float,
        default=0.0,
        help="Flat-bottom width of the constraints (in A)",
    )

    parser.add_argument(
        "--constraint-file",
        type=str,
        help="""File containing a list of dynamics constraints.
Each constraint is defined by a header, followed by a line for each atom in the constraint.

Available constraints:
- Position POS, 1 atom
    2 parameters: flat bottom half width (in A), strength (in kcal/mol/A^2)
- Distance: DIS, 2 atoms
    3 parameters: flat bottom half width (in A), strength (in kcal/mol/A^2), distance (in A)
- Angle: ANG, 3 atoms
    3 parameters: flat bottom half width (in degrees), strength (in kcal/mol/rad^2), angle (in degrees)
- Torsion: TOR, 4 atoms
    3 parameters: flat bottom half width (in degrees), strength (in kcal/mol/rad^2), angle (in degrees)

The file follows a fixed width format. In the header, the first three characters define
the constraint type. Each parameter is allocated six characters, with a single space between
each parameter.

Atoms are specified in the standard PDB format. An ATOM record may be directly copied
from a PDB file. However, atoms are identified using only the atom name, residue name,
chain ID, residue number, and insertion code. These are the only required fields.

Example with two constraints:
```
POS   0.00   5.00
             C16 5KR A 201
DIS   0.00   5.00  18.32
             CZ  ARG A 169
             OH  TYR A 179
```
More extensive examples can be generated by Flare using 'Export Calculation` with constraints.
""",  # noqa: E501
    )


def find_residue(protein: flare.Protein, chain_id: str, res_name: str, res_seq: int, i_code: str):
    """Find a single residue in the protein."""
    residue_string = f"{chain_id} {res_name} {res_seq}{i_code}"
    residues = protein.residues.find(residue_string)
    if len(residues) != 1:
        raise ValueError(f"Unable to find `{residue_string}` in {protein}")
    return residues[0]


def find_atom(residue: flare.Residue, atom_name: str):
    """Find an atom in a residue with the given name."""
    for atom in residue.atoms:
        if atom.name == atom_name:
            return atom
    raise ValueError(f"Unable to find `{atom_name}` in {residue}")


def parse_atom(protein: flare.Protein, atom: str):
    """Parse the PDB record for an atom and find it in the `protein`."""
    # Match standard PDB format
    name = atom[12:16].strip()
    res_name = atom[17:20].strip()
    chain_id = atom[21]
    res_seq = int(atom[22:26].strip())
    i_code = atom[26]

    residue = find_residue(protein, chain_id, res_name, res_seq, i_code)
    return find_atom(residue, name)


def parse_position_constraint(protein: flare.Protein, header: str, atom: str):
    """Parse a position constraint from the constraint file."""
    width = float(header[4:10].strip())
    strength = float(header[11:17].strip())
    return (parse_atom(protein, atom), strength, width)


def parse_distance_constraint(protein: flare.Protein, header: str, atom1: str, atom2: str):
    """Parse a distance constraint from the constraint file."""
    width = float(header[4:10].strip())
    strength = float(header[11:17].strip())
    distance = float(header[18:24].strip())

    return (
        parse_atom(protein, atom1),
        parse_atom(protein, atom2),
        distance,
        strength,
        width,
    )


def parse_angle_constraint(protein: flare.Protein, header: str, atom1: str, atom2: str, atom3: str):
    """Parse an angle constraint from the constraint file."""
    width = float(header[4:10].strip())
    strength = float(header[11:17].strip())
    angle = float(header[18:24].strip())

    return (
        parse_atom(protein, atom1),
        parse_atom(protein, atom2),
        parse_atom(protein, atom3),
        angle,
        strength,
        width,
    )


def parse_torsion_constraint(
    protein: flare.Protein, header: str, atom1: str, atom2: str, atom3: str, atom4: str
):
    """Parse a torsion constraint from the constraint file."""
    width = float(header[4:10].strip())
    strength = float(header[11:17].strip())
    angle = float(header[18:24].strip())

    return (
        parse_atom(protein, atom1),
        parse_atom(protein, atom2),
        parse_atom(protein, atom3),
        parse_atom(protein, atom4),
        angle,
        strength,
        width,
    )


def read_constraints(protein: flare.Protein, constraint_file: str):
    """Generate all of the constraints recorded in `constraint_file`."""
    with open(constraint_file) as file:
        current_line = 0
        try:
            for header in file:
                current_line += 1
                type = header[0:3]
                if type == "POS":
                    current_line += 1
                    yield parse_position_constraint(protein, header, file.readline())
                elif type == "DIS":
                    current_line += 2
                    yield parse_distance_constraint(
                        protein, header, file.readline(), file.readline()
                    )
                elif type == "ANG":
                    current_line += 3
                    yield parse_angle_constraint(
                        protein, header, file.readline(), file.readline(), file.readline()
                    )
                elif type == "TOR":
                    current_line += 4
                    yield parse_torsion_constraint(
                        protein,
                        header,
                        file.readline(),
                        file.readline(),
                        file.readline(),
                        file.readline(),
                    )
                else:
                    raise Exception(f"Constraint type not recognized: {type}")
        except Exception as e:
            raise Exception(f"Failed to parse constraint file on line {current_line}.") from e


def configure_constraint_process(process, args):
    """Configure the process using the args set in `add_constraint_arguments`."""

    if args.constrain is not None:
        if args.constraint_strength is None:
            raise ValueError(
                "in addition to --constrain, --constraint-strength must be provided to set the constraint strength"  # noqa: E501
            )

        def setup_constraints(atoms):
            return [(atom, args.constraint_strength, args.constraint_width) for atom in atoms]

        if "ligand" in args.constrain:
            if isinstance(process, flare.Dynamics):
                for sequence in process.protein.sequences:
                    if sequence.type == flare.Sequence.Type.Ligand:
                        process.openmm.constrained_atoms += setup_constraints(sequence.atoms)
            elif isinstance(process, flare.Minimization):
                ligand_atoms = [atom for lig in process.ligands for atom in lig.atoms]
                process.openmm.constrained_atoms += setup_constraints(ligand_atoms)

        if "protein" in args.constrain:
            for sequence in process.protein.sequences:
                if sequence.type == flare.Sequence.Type.Natural:
                    process.openmm.constrained_atoms += setup_constraints(sequence.atoms)

        # Only add backbone constraints if the protein is not constrained
        elif "backbone" in args.constrain:
            backbone_atoms = [atom for atom in process.protein.atoms if atom.is_backbone()]
            process.openmm.constrained_atoms += setup_constraints(backbone_atoms)

    if args.constraint_file is not None:
        for constraint in read_constraints(process.protein, args.constraint_file):
            if len(constraint) == 3:  # Position constraint, 1 atom, 2 parameters
                process.openmm.constrained_atoms += [constraint]
            elif len(constraint) == 5:  # Distance constraint, 2 atoms, 3 parameters
                process.openmm.constrained_distances += [constraint]
            elif len(constraint) == 6:  # Angle constraint, 3 atoms, 3 parameters
                process.openmm.constrained_angles += [constraint]
            elif len(constraint) == 7:  # Torsion constraint, 4 atoms, 3 parameters
                process.openmm.constrained_torsions += [constraint]
            else:
                raise ValueError(f"Constraint not recognized: {constraint}")

    if args.verbose:
        natoms = len(process.openmm.constrained_atoms)
        ndistances = len(process.openmm.constrained_distances)
        nangles = len(process.openmm.constrained_angles)
        ntorsions = len(process.openmm.constrained_torsions)
        if natoms > 0:
            print(f"Position constraints: {natoms}", file=sys.stderr)
        if ndistances > 0:
            print(f"Distance constraints: {ndistances}", file=sys.stderr)
        if nangles > 0:
            print(f"Angle constraints: {nangles}", file=sys.stderr)
        if ntorsions > 0:
            print(f"Torsion constraints: {ntorsions}", file=sys.stderr)


def add_gcncmc_arguments(parser, process, equilibration_only=False):
    """Add the arguments used by calculations which use GCNCMC."""
    protocol_choices = ["off", "equil"]
    if not equilibration_only:
        protocol_choices.append("on")

    parser.add_argument(
        "-N",
        "--gcncmc-sampling-protocol",
        choices=protocol_choices,
        type=str.lower,
        help=numpy_doc_to_help(process.gcncmc.__class__.sampling_protocol.__doc__, True),
    )
    parser.add_argument(
        "--gcncmc-sphere-buffer",
        type=float,
        help=numpy_doc_to_help(process.gcncmc.__class__.sphere_buffer.__doc__, True),
    )
    parser.add_argument(
        "--gcncmc-num-moves",
        type=int,
        help=numpy_doc_to_help(process.gcncmc.__class__.num_moves.__doc__, True),
    )
    parser.add_argument(
        "--gcncmc-time-interval",
        type=float,
        help=numpy_doc_to_help(process.gcncmc.__class__.time_interval.__doc__, True),
    )


def configure_gcncmc_process(process, args):
    """Configure the process using the args set in `add_gcncmc_arguments`."""
    if args.gcncmc_sampling_protocol == "off":
        process.gcncmc.sampling_protocol = process.gcncmc.__class__.SamplingProtocol.Off
    elif args.gcncmc_sampling_protocol == "equil":
        process.gcncmc.sampling_protocol = (
            process.gcncmc.__class__.SamplingProtocol.EquilibrationOnly
        )  # noqa: E501
    elif args.gcncmc_sampling_protocol == "on":
        process.gcncmc.sampling_protocol = process.gcncmc.__class__.SamplingProtocol.On

    with CheckValue("gcncmc-sphere-buffer"):
        if args.gcncmc_sphere_buffer is not None:
            process.gcncmc.sphere_buffer = args.gcncmc_sphere_buffer

    with CheckValue("gcncmc-num-moves"):
        if args.gcncmc_num_moves is not None:
            process.gcncmc.num_moves = args.gcncmc_num_moves

    with CheckValue("gcncmc-time-interval"):
        if args.gcncmc_time_interval is not None:
            process.gcncmc.time_interval = args.gcncmc_time_interval


def find_single_residue(protein, residue_name):
    """Find a residue with the given name"""
    residues = protein.residues.find(residue_name)
    if not residues:
        raise ValueError(f'protein does not contain any residues called "{residue_name}"')
    if len(residues) > 1:
        raise ValueError(
            f'protein contains {len(residues)} residues which match the residue called "{residue_name}"'  # noqa: E501
        )
    return residues[0]


def convert_residue_to_ligand(protein, residue_name, calculation_name):
    """Remove the residue from the protein and adds it as a ligand.

    The ligand is added to the same project as the protein and returned.
    """
    ligand_residue = find_single_residue(protein, residue_name)
    if ligand_residue.previous() is not None or ligand_residue.next() is not None:
        raise ValueError(
            f"ligand residue specified for {calculation_name} is connected to other residues"
        )
    ligand = protein.project.ligands.insert(0, ligand_residue)
    if not ligand:
        raise ValueError(
            "unable to insert ligand residue into the ligand list for the project"
        )  # noqa: E501
    protein.residues.remove(ligand_residue)
    return ligand


def calculate_center_of_residue(protein, residue_name):
    """Return the [x, y, z] coords of the center of the given residue name."""
    residue = find_single_residue(protein, residue_name)

    # Calculate the center of the residue
    centroid = [0.0, 0.0, 0.0]
    count = 0
    for atom in residue.atoms:
        if atom.atomic_number >= 1:
            centroid[0] += atom.pos[0]
            centroid[1] += atom.pos[1]
            centroid[2] += atom.pos[2]
            count += 1
    if count > 0:
        centroid[0] /= count
        centroid[1] /= count
        centroid[2] /= count

    return centroid


def calculate_radius_of_residue(protein, residue_name):
    """Return the radius of the residue based on its center."""
    residue = find_single_residue(protein, residue_name)

    # Calculate the center of the residue
    center = calculate_center_of_residue(protein, residue_name)
    radius2 = 0
    for atom in residue.atoms:
        if atom.atomic_number >= 1:
            dist2 = (
                (atom.pos[0] - center[0]) ** 2
                + (atom.pos[1] - center[1]) ** 2
                + (atom.pos[2] - center[2]) ** 2
            )
            if dist2 > radius2:
                radius2 = dist2
    return math.sqrt(radius2)


class DescriptionWrappedNewlineFormatter(argparse.HelpFormatter):
    """Custom argparse formatter with more sensible behavior"""

    def _fill_text(self, text, width, indent):
        # Strip the indent from the original python definition that plagues most of us.
        text = textwrap.dedent(text)
        text = textwrap.indent(text, indent)  # Apply any requested indent.
        text = text.splitlines()  # Make a list of lines
        text = [textwrap.fill(line, width) for line in text]  # Wrap each line
        text = "\n".join(text)  # Join the lines again
        return text


class WrappedNewlineFormatter(DescriptionWrappedNewlineFormatter):
    """An argparse formatter that:
    * preserves newlines (like argparse.RawTextHelpFormatter),
    * removes leading indent and applies reasonable text wrapping
    * (like DescriptionWrappedNewlineFormatter),
    * applies to all help text (description, arguments, epilogue).
    """

    def _split_lines(self, text, width):
        # Allow multiline strings to have common leading indentation.
        text = textwrap.dedent(text)
        text = text.splitlines()
        lines = []
        for line in text:
            wrapped_lines = textwrap.fill(line, width).splitlines()
            lines.extend(subline for subline in wrapped_lines)
            if line:
                lines.append("")  # Preserve line breaks.
        return lines
