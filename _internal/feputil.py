# -*- coding: utf-8 -*-
# Copyright (C) 2025 Cresset Biomolecular Discovery Ltd.
# Released under CC-BY-SA 4.0 (https://creativecommons.org/licenses/by-sa/4.0/).
# Originally downloaded from https://gitlab.com/cresset
import sys
import argparse
import os
import time

from cresset import flare

from . import cmdutil


def float_or_two_floats(arg):
    try:
        return float(arg)
    except ValueError:
        values = arg.split(",")
        if len(values) != 2:
            raise argparse.ArgumentTypeError(
                "Argument must be a single float or two comma-separated floats"
            )
        return tuple(map(float, values))


def calculation_type(fep):
    calc_type = (
        flare.FepCalculationType.Relative
        if isinstance(fep, flare.Fep)
        else flare.FepCalculationType.Absolute
    )
    return calc_type


def _requires_input_project(fep):
    return calculation_type(fep) == flare.FepCalculationType.Relative


def add_input_arguments(group, fep):
    group.add_argument(
        "-P",
        "--project",
        required=_requires_input_project(fep),
        help="The Flare project containing the FEP project to run the calculation on.",
    )
    group.add_argument(
        "-n",
        "--name",
        help="The name of the FEP project to run the calculation on. If not specified and the Flare project contains one FEP project then that project is used.",  # noqa: E501
    )
    group.add_argument(
        "-c",
        "--continue",
        dest="continue_calculation",
        action="store_true",
        help="Continue the calculation from its current state. If any of the calculation setting options are given, then they may be ignored.",  # noqa: E501
    )
    group.add_argument(
        "-d",
        "--delete",
        action="store_true",
        help="Deletes the existing results and restarts the calculation.",  # noqa: E501
    )


def add_fep_arguments(group, fep):
    group.add_argument(
        "-D",
        "--intermediate-results-directory",
        help=cmdutil.numpy_doc_to_help(fep.__class__.intermediate_results_directory.__doc__),
    )
    group.add_argument(
        "-L",
        "--lambda-windows",
        type=int,
        help=cmdutil.numpy_doc_to_help(fep.__class__.lambda_windows.__doc__, True),
    )
    group.add_argument(
        "-s",
        "--simulation-length",
        type=float_or_two_floats,
        help="The simulation length in nanoseconds. Enter two comma-separated values "
        + "for free and bound states respectively, or a single value to use in both states.",
    )
    group.add_argument(
        "-b",
        "--solvate-box-buffer",
        type=float,
        help=cmdutil.numpy_doc_to_help(flare.Solvate.box_buffer.__doc__, doc_has_defaults=False),
    )
    group.add_argument(
        "--solvate-box-ionic-strength",
        type=float,
        help=cmdutil.numpy_doc_to_help(
            flare.Solvate.box_ionic_strength.__doc__, doc_has_defaults=False
        ),
    )
    group.add_argument(
        "-w",
        "--solvent-model",
        choices=[
            "explicit",
            "tip3p",
            "spce",
            "opc",
            "opc3",
            "spceb",
            "tip3pfb",
            "tip4p2005",
            "tip4pd",
            "tip4pfb",
            "tip4pew",
        ],
        help="The solvent model to use. Explicit (tip3p) solvent is used by default.",
    )
    cmdutil.add_membrane_arguments(group, fep)
    group.add_argument(
        "--solvent-box-shape",
        choices=[
            "octahedral",
            "orthogonal",
        ],
        help="The shape of the solvent box. Octahedral boxes (the default) are more "
        "spherical in shape and so are more efficient for most proteins.",
    )


def add_general_arguments(group, fep):
    group.add_argument(
        "--config",
        help=cmdutil.CONFIG_ARG_HELP_TEXT,
    )
    group.add_argument(
        "-l",
        "--localengines",
        type=int,
        default=fep.local_engine_count,
        help="Set the number of local engines to start. Use 0 for no local processing: you must specify some remote engines with -g if you do so.",  # noqa: E501
    )
    group.add_argument(
        "-g",
        "--broker",
        help="Set the hostname and port of the Cresset Engine Broker in the format hostname:port. Field Engines which have been registered with the Broker will be used during the calculation. The CRESSET_BROKER environment variable can be used instead of this option.",  # noqa: E501
    )
    group.add_argument(
        "-h",
        "--help",
        action="help",
        default=argparse.SUPPRESS,
        help="Show this help message and exit.",
    )
    group.add_argument(
        "-v",
        "--verbose",
        action="store_true",
        help="Print the log to stderr instead of progress information.",
    )


def configure_process(fep, args):
    calc_type = calculation_type(fep)

    if args.list_open_versions:
        cmdutil.print_open_force_field_versions_and_exit()

    # Load the config file first. Any other settings given will override the config file.
    if args.config:
        fep.read_config(args.config)

    project = (
        flare.Project.load(args.project)
        if _requires_input_project(fep) or (args.project and os.path.isfile(args.project))
        else flare.Project()
    )
    fep.fep_project = _load_project(fep, project, args)

    if args.intermediate_results_directory:
        fep.intermediate_results_directory = os.path.abspath(args.intermediate_results_directory)

    if args.simulation_length:
        if isinstance(args.simulation_length, float):
            fep.simulation_length_free = float(args.simulation_length)
            fep.simulation_length_bound = float(args.simulation_length)
        else:
            fep.simulation_length_free = args.simulation_length[0]
            fep.simulation_length_bound = args.simulation_length[1]

    if not fep.fep_project.is_calculation_started(calc_type):
        if args.lambda_windows is not None:
            with cmdutil.CheckValue("lambda_windows"):
                fep.lambda_windows = args.lambda_windows

        if args.solvent_model == "explicit":
            fep.solvate.model = flare.SolventModel.Explicit
        elif args.solvent_model == "tip3p":
            fep.solvate.model = flare.SolventModel.ExplicitTIP3P
        elif args.solvent_model == "spce":
            fep.solvate.model = flare.SolventModel.ExplicitSPCE
        elif args.solvent_model == "opc":
            fep.solvate.model = flare.SolventModel.ExplicitOPC
        elif args.solvent_model == "opc3":
            fep.solvate.model = flare.SolventModel.ExplicitOPC3
        elif args.solvent_model == "tip3pfb":
            fep.solvate.model = flare.SolventModel.ExplicitTIP3Pfb
        elif args.solvent_model == "spceb":
            fep.solvate.model = flare.SolventModel.ExplicitSPCEb
        elif args.solvent_model == "tip4p2005":
            fep.solvate.model = flare.SolventModel.ExplicitTIP4P2005
        elif args.solvent_model == "tip4pd":
            fep.solvate.model = flare.SolventModel.ExplicitTIP4Pd
        elif args.solvent_model == "tip4pfb":
            fep.solvate.model = flare.SolventModel.ExplicitTIP4Pfb
        elif args.solvent_model == "tip4pew":
            fep.solvate.model = flare.SolventModel.ExplicitTIP4Pew

        cmdutil.configure_membrane_process(fep, args)

        # If a membrane is set them the solvate model must be set to its default of explicit tip3p.
        # This is checked when cmdutil.start_process calls dynamics.setup_errors() and therefore
        # is not checked here

        if args.solvent_box_shape == "octahedral":
            fep.solvate.box_shape = flare.AmberSolvationBox.Octahedral
        elif args.solvent_box_shape == "orthogonal":
            fep.solvate.box_shape = flare.AmberSolvationBox.Orthogonal
        elif args.solvent_box_shape is None and args.membrane is not None:
            # Membrane requires an orthogonal box but the default is octahedral.
            fep.solvate.box_shape = flare.AmberSolvationBox.Orthogonal

        cmdutil.configure_dynamics_force_field_process(fep, args)

        if args.solvate_box_buffer is not None:
            with cmdutil.CheckValue("solvate-box-buffer"):
                fep.solvate.box_buffer = args.solvate_box_buffer

        if args.solvate_box_ionic_strength is not None:
            with cmdutil.CheckValue("solvate-box-ionic-strength"):
                fep.solvate.box_ionic_strength = args.solvate_box_ionic_strength

        cmdutil.configure_openmm_common_process(fep, args)
        cmdutil.configure_openmm_time_step_process(fep, args)
        cmdutil.configure_openmm_fep_ws_process(fep, args)
        cmdutil.configure_openmm_advanced_process(fep, args, process_is_fep=True)

        cmdutil.configure_gcncmc_process(fep, args)

    with cmdutil.CheckValue("localengines"):
        fep.local_engine_count = args.localengines

    cmdutil.configure_gpu_process(fep, args)
    save_project(project, args)

    return project


def _load_project(fep, project, args):
    fep_project = None
    if args.name:
        fep_projects = [proj for proj in project.fep_projects if proj.name == args.name]
        if not fep_projects:
            if _requires_input_project(fep):
                raise ValueError(f"no fep projects found called {args.name}")
            else:
                fep_project = project.fep_projects.new()
                fep_project.name = args.name
        elif len(fep_projects) == 1:
            fep_project = fep_projects[0]
        else:
            raise ValueError("multiple fep projects with the same name detected")
    else:
        if not project.fep_projects:
            if _requires_input_project(fep):
                raise ValueError("no fep projects found")
            else:
                fep_project = project.fep_projects.new()
        elif len(project.fep_projects) == 1:
            fep_project = project.fep_projects[0]
        else:
            names = (
                '"' + '", "'.join([fep_project.name for fep_project in project.fep_projects]) + '"'
            )
            raise ValueError(
                "multiple fep projects detected, "
                + "specify a fep project name with --name. "
                + f"The names of the fep projects are {names}."
            )

    if args.delete and args.continue_calculation:
        raise ValueError("--delete cannot be used with --continue")

    calc_type = calculation_type(fep)

    if args.delete:
        fep_project.clear_calculated_data(calc_type)

    if not args.continue_calculation and fep_project.is_calculation_started(calc_type):
        raise ValueError(
            "a calculation has already been run. "
            + "To continue with the previous calculation, "
            + 'please specify "--continue". '
            + 'To run the calculation from the beginning, specify "--delete".'
        )
    return fep_project


def save_project(project, args):
    if args.project:
        tmp_file = args.project + ".tmp"
        project.save(tmp_file)
        if os.path.exists(args.project):
            os.remove(args.project)
        os.rename(tmp_file, args.project)


def wait_for_finish(fep, project, fep_project, args):
    """Run the fep process while printing the progress."""
    logging_context = cmdutil.LoggingContext(
        fep_project.log, verbose=args.verbose, first=len(fep_project.log)
    )
    clear_progress_line = False
    try:
        last_save_time = time.time()
        while fep.is_running():
            time.sleep(1)
            if logging_context is not None and logging_context.verbose:
                cmdutil.print_project_logs(logging_context)
            else:
                cmdutil.print_progress(fep)
                clear_progress_line = True

            if time.time() - last_save_time > 60 * 5:
                last_save_time = time.time()
                # Save the project occasionally in case the job is killed
                save_project(project, args)
    except KeyboardInterrupt:
        # On the first Ctrl+C try to shutdown cleanly
        # On the second Ctrl+C `fep.wait()` will raise an exception
        # and it is allowed to kill this script
        try:
            if clear_progress_line:
                print("", file=sys.stderr)
                clear_progress_line = False
            print(
                "Calculation cancelled, shutting down FieldEngines, please wait",
                file=sys.stderr,
            )
            fep.cancel()
            fep.wait()
        finally:
            # Even if there a second Ctrl+C save the project
            save_project(project, args)

    finally:
        if clear_progress_line:
            print("", file=sys.stderr)
        # if Ctrl+C is pressed try to write out the final log files
        if logging_context is not None and logging_context.verbose:
            cmdutil.print_project_logs(logging_context)
